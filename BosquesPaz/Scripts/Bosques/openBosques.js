﻿
/// <summary>
/// Carga de modulos especificos
/// </summary>
/// <param name="modulo">Nombre del modulo principal</param>
/// <param name="idBosque">Identificador del bosque</param>
/// <param name="moduloBosque">Nombre del submodulo</param>
/// <param name="moduloBosqueId">Identificador del submodulo</param>
/// <remarks>
/// <h3>Calidad</h3>
/// </hr>
/// <b>Requerimiento:</b> Help Desk</br>
/// <b>Version:</b> 1</br>
/// <b>Autor:</b> Grupo Cubo Ltda.</br>
/// <b>Fecha:</b> 19/12/2017</br>
/// <b>Descripcion:</b>Realiza la carga de modulos especificos segun los valores que posean los parametros.
/// </remarks>
function LoadRoute(modulo, idBosque, moduloBosque, moduloBosqueId) {
    if (modulo) {
        openBosquesPaz(null, modulo);
        if (idBosque) {
            DetalleBosque(idBosque);
            if (moduloBosque) {
                if (moduloBosqueId) {
                    DetalleIndividuoEspecie(moduloBosqueId);
                }
            }
        }
    }
}

/// <summary>
/// Pestañas de navegacion principal
/// </summary>
/// <param name="evt">Nombre del evento</param>
/// <param name="BosquesPazName">Nombre del modulo</param>
/// <remarks>
/// <h3>Calidad</h3>
/// </hr>
/// <b>Requerimiento:</b> Help Desk</br>
/// <b>Version:</b> 1</br>
/// <b>Autor:</b> Grupo Cubo Ltda.</br>
/// <b>Fecha:</b> 19/12/2017</br>
/// <b>Descripcion:</b>Permite la navegacion entre las pestañas de navegacion, segun sus parametros.
/// </remarks>
function openBosquesPaz(evt, BosquesPazName) {
    var i, x, tablinks;
    x = document.getElementsByClassName("BosquesPaz");
    for (i = 0; i < x.length; i++) {
        x[i].style.display = "none";
    }
    tablinks = document.getElementsByClassName("tablink");
    for (i = 0; i < x.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" w3-border-red", "");
    }
    document.getElementById(BosquesPazName).style.display = "block";
    document.getElementById(BosquesPazName + 'Tab').className += " w3-border-red";
    if (BosquesPazName == 'Cobertura') {
        setTimeout(function () {
            cargarMapaBosques();
        }, 200);
    }
    if (BosquesPazName == 'Tablero') {
        setTimeout(function () {
            CargarGraficas();
        }, 200);
    }
}


/// <summary>
/// Pestañas de navegacion submodulos bosques
/// </summary>
/// <param name="evt">Nombre del evento</param>
/// <param name="BosquesPazName">Nombre del modulo</param>
/// <remarks>
/// <h3>Calidad</h3>
/// </hr>
/// <b>Requerimiento:</b> Help Desk</br>
/// <b>Version:</b> 1</br>
/// <b>Autor:</b> Grupo Cubo Ltda.</br>
/// <b>Fecha:</b> 19/12/2017</br>
/// <b>Descripcion:</b>Permite la navegacion entre las pestañas de navegacion, segun sus parametros.
/// </remarks>
function openBosques(evt, subBosqueName) {
    var i, x, tablinks;
    x = document.getElementsByClassName("bosqueSub");
    for (i = 0; i < x.length; i++) {
        x[i].style.display = "none";
    }
    tablinks = document.getElementsByClassName("bosquetablink");
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" w3-border-blue", "");
    }
    document.getElementById(subBosqueName).style.display = "block";
    document.getElementById(subBosqueName + 'Tab').className += " w3-border-blue";
}

function openOportunidades(evt, OportunidadName) {
    var i, x, tablinks;
    x = document.getElementsByClassName("oportunidad");
    for (i = 0; i < x.length; i++) {
        x[i].style.display = "none";
    }
    tablinks = document.getElementsByClassName("oportunidadtablink");
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" w3-border-blue", "");
    }
    document.getElementById(OportunidadName).style.display = "block";
    document.getElementById(OportunidadName + 'Tab').className += " w3-border-blue";
}

/// <summary>
/// Pestañas de navegacion submodulos estadisticas
/// </summary>
/// <param name="evt">Nombre del evento</param>
/// <param name="BosquesPazName">Nombre del modulo</param>
/// <remarks>
/// <h3>Calidad</h3>
/// </hr>
/// <b>Requerimiento:</b> Help Desk</br>
/// <b>Version:</b> 1</br>
/// <b>Autor:</b> Grupo Cubo Ltda.</br>
/// <b>Fecha:</b> 19/12/2017</br>
/// <b>Descripcion:</b>Permite la navegacion entre las pestañas de navegacion, segun sus parametros.
/// </remarks>
function openEstadistica(evt, SPName) {
    var i, tablinks;

    tablinks = document.getElementsByClassName("estadisticatablink");
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" w3-border-blue", "");
    }
    evt.currentTarget.firstElementChild.className += " w3-border-blue";

    if (SPName == 'SP1') {
        cargarSP1();
    }
    if (SPName == 'SP2') {
        cargarSP2();
    }
    if (SPName == 'SP3') {
        cargarSP3();
    }
    if (SPName == 'SP4') {
        cargarSP4();
    }
    if (SPName == 'SP5') {
        cargarSP5();
    }
    if (SPName == 'SP6') {
        cargarSP6();
    }
}
