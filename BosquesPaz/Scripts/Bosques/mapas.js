﻿function LoadMapa(id) {
    $('#MapasModal').modal('show');
    $.ajax({
        contentType: "application/json; charset=utf-8",
        type: "GET",
        dataType: 'json',
        url: '/Mapas/MapPupUp' + '?id=' + id,
        cache: false,
        success: function (result) {
            setTimeout(function () {
                var map = L.map('MapaCanvas').setView([parseFloat(result.lat), parseFloat(result.lng)], 10);
                L.tileLayer.provider('OpenStreetMap.Mapnik').addTo(map);
                L.marker([parseFloat(result.lat), parseFloat(result.lng)]).addTo(map);
            }, 500);

        },
        fail: function (xhr, status) {
            alert(status);
        }
    });
}

function getPolygon(coordinates) {
    var retorno = [];
    $.each(coordinates, function (i, item) {
        retorno.push([item[1], item[0]]);
    });
    return retorno;
}

function LoadMapaAll(idMapa) {
    $.ajax({
        contentType: "application/json; charset=utf-8",
        type: "GET",
        dataType: 'json',
        url: '/Mapas/BosquesDepartamentos',
        cache: false,
        success: function (result) {

            var departamentos = result.dtos;
            $.ajax({
                contentType: "application/json; charset=utf-8",
                type: "GET",
                dataType: 'json',
                url: '/Scripts/Poligonos/depto.json',
                cache: false,
                success: function (result) {
                    $.each(departamentos, function (index, value) {
                        var dt = value.toString();
                        if (dt.length == 1)
                            dt = '0' + dt;
                        $.each(result.features, function (i, d) {;
                            if (d.properties.DPTO == dt) {
                                var polygon = getPolygon(d.geometry.coordinates[0]);
                                var DPolygono = L.polygon(polygon).addTo(idMapa);
                                //console.log("departamento = " + d.properties.DPTO);
                            }
                        });
                    });

                },
                fail: function (xhr, status) {
                    alert(status);
                }
            });;
            var municipios = result.muni;
            $.ajax({
                contentType: "application/json; charset=utf-8",
                type: "GET",
                dataType: 'json',
                url: '/Scripts/Poligonos/mpio.json',
                cache: false,
                success: function (result) {
                    $.each(municipios, function (index, value) {
                        var val = value.toString();
                        var m = val.substring(val.length - 3, val.length);
                        var dt = val.substring(0, val.length - 3);
                        if (dt.length == 1)
                            dt = '0' + dt;
                        $.each(result.features, function (i, d) {
                            if (d.properties.DPTO == dt && d.properties.MPIO == m) {
                                var polygon = getPolygon(d.geometry.coordinates[0]);
                                var DPolygono = L.polygon(polygon).addTo(idMapa);
                                DPolygono.setStyle({ fillColor: '#00FF00' });
                                //console.log("Municipio = " + d.properties.MPIO + "  departamento = " + d.properties.DPTO);
                            }
                        });
                    });

                },
                fail: function (xhr, status) {
                    alert(status);
                }
            });
        },
        fail: function (xhr, status) {
            alert(status);
        }
    });
}
function CargarPoligono(mapa, coordenadas) {
    L.polygon(coordenadas).addTo(mapa);
}

function CargarDetalleBosqueMapa(idBosque) {
    var event = document.getElementById('idBosquesTab');
    openBosquesPaz(event, 'Bosques');
    DetalleBosque(idBosque);
}