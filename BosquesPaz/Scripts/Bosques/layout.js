﻿
/// <summary>
/// Funcion fecha plugin
/// </summary>
/// <remarks>
/// <h3>Calidad</h3>
/// </hr>
/// <b>Requerimiento:</b> Help Desk</br>
/// <b>Version:</b> 1</br>
/// <b>Autor:</b> Grupo Cubo Ltda.</br>
/// <b>Fecha:</b> 19/12/2017</br>
/// <b>Descripcion:</b>Define los parametros para el plugin de las cajas de textos de fecha.
/// </remarks>
function setDatePicker(conainer) {
    $('#' + conainer).val('')
    $('#' + conainer).datepicker({
        format: "dd/mm/yyyy",
        todayBtn: "linked",
        language: "es",
        clearBtn: true,
        autoclose: true,
        todayHighlight: true,
        multidate: false
    });
}

/// <summary>
/// Funcion carga de formularios
/// </summary>
/// <param name="_url">Direccion url</param>
/// <param name="seccion">Nombre del contenedor</param>
/// <remarks>
/// <h3>Calidad</h3>
/// </hr>
/// <b>Requerimiento:</b> Help Desk</br>
/// <b>Version:</b> 1</br>
/// <b>Autor:</b> Grupo Cubo Ltda.</br>
/// <b>Fecha:</b> 19/12/2017</br>
/// <b>Descripcion:</b>Realiza la carga de los formularios segun la url y el nombre del contenedor.
/// </remarks>
function CargarFormulario(_url, seccion, graph) {
    $('#' + seccion).html(" <div class='row'>\
                                <div class='col-md-4'>\
                                    <h3>Cargando...</h3>\
                                </div>\
                                <div class='col-md-4'>\
                                    <div class='col-md-10'>\
                                        <div style='width:200px; height:200px;'>\
                                            <div class='loader' id='loader'></div>\
                                        </div>\
                                    </div>\
                                </div>\
                                <div class='col-md-4'></div>\
                            </div>");
    $.ajax({
        contentType: 'application/html; charset=utf-8',
        type: "GET",
        dataType: 'html',
        url: _url,
        cache: false,
        success: function (result) {
            $('#' + seccion).html(result);
            if (graph)
                LoadGraficas();
        },
        error: function (xhr, status) {
            alert(status + _url);
        }
    });
};

