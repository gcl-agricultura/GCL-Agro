﻿/// <summary>
/// Carga o insercion de fotos
/// </summary>
/// <param name="id">Identificador del bosque</param>
/// <remarks>
/// <h3>Calidad</h3>
/// </hr>
/// <b>Requerimiento:</b> Help Desk</br>
/// <b>Version:</b> 1</br>
/// <b>Autor:</b> Grupo Cubo Ltda.</br>
/// <b>Fecha:</b> 19/12/2017</br>
/// <b>Descripcion:</b>Realiza la carga de las vistas para ver o guardar fotos de bosques.
/// </remarks>
function LoadPhotos(id) {
    $('#FotosModal').modal('show');

    $.ajax({
        contentType: 'application/html; charset=utf-8',
        type: "GET",
        dataType: 'html',
        url: 'Fotos/VisualizarFotos/' + id,
        cache: false,
        success: function (result) {
            $('#FotosCanvas').html(result);
        },
        fail: function (xhr, status) {
            alert(status);
        }
    });

    $.ajax({
        contentType: 'application/html; charset=utf-8',
        type: "GET",
        dataType: 'html',
        url: 'Fotos/LoadCargaFotos/' + id,
        cache: false,
        success: function (result) {
            $('#FotosCarga').html(result);
        },
        fail: function (xhr, status) {
            alert(status);
        }
    });
}

function LoadPhoto(id) {

    $.ajax({
        contentType: 'application/html; charset=utf-8',
        type: "GET",
        dataType: 'html',
        url: '/Fotos/VisualizarFotos/' + id,
        cache: false,
        success: function (result) {
            $('.'+id+'').html(result);
        },
        fail: function (xhr, status) {
            alert(status);
        }
    });
   
}

/// <summary>
/// Carga o insercion de fotos
/// </summary>
/// <param name="id">Identificador de individuos/especies</param>
/// <remarks>
/// <h3>Calidad</h3>
/// </hr>
/// <b>Requerimiento:</b> Help Desk</br>
/// <b>Version:</b> 1</br>
/// <b>Autor:</b> Grupo Cubo Ltda.</br>
/// <b>Fecha:</b> 19/12/2017</br>
/// <b>Descripcion:</b>Realiza la carga de las vistas para ver o guardar fotos de individuos/especies.
/// </remarks>
function LoadPhotosIndividuosEspecies(id) {
    $('#FotosModal').modal('show');

    $.ajax({
        contentType: 'application/html; charset=utf-8',
        type: "GET",
        dataType: 'html',
        url: 'Fotos/VisualizarFotosIndividuosEspecies/' + id,
        cache: false,
        success: function (result) {
            $('#FotosCanvas').html(result);
        },
        fail: function (xhr, status) {
            alert(status);
        }
    });

    $.ajax({
        contentType: 'application/html; charset=utf-8',
        type: "GET",
        dataType: 'html',
        url: 'Fotos/LoadCargaFotosIndividuosEspecies/' + id,
        cache: false,
        success: function (result) {
            $('.' + id + '').html(result);
        },
        fail: function (xhr, status) {
            alert(status);
        }
    });
}


function LoadPhotoIndividuosEspeciesCard(id) {
    var url = '';
    var urlDir = '/Fotos/VisualizarFotosIndividuosEspecies?id=';
    $.ajax({
        contentType: 'application/html; charset=utf-8',
        type: "GET",
        dataType: 'html',
        url: urlDir+ id,
        cache: false,
        success: function (result) {
            $('.ie_' + id + '').html(result);
        },
        fail: function (xhr, status) {
            alert(status);
        }
    });
    
}


function LoadAudio(id) {  
    var url = '';
    var urlDir = '/Audios/EscucharAudio?id=';

    $.ajax({
        contentType: 'application/html; charset=utf-8',
        type: "GET",
        dataType: 'html',
        url: urlDir + id,
        cache: false,
        success: function (result) {
            $('.h_' + id + '').html(result);
        },
        fail: function (xhr, status) {
            alert(status);
        }
    });
}

/// <summary>
/// Carga o insercion de fotos
/// </summary>
/// <param name="id">Identificador de productos/servicios</param>
/// <remarks>
/// <h3>Calidad</h3>
/// </hr>
/// <b>Requerimiento:</b> Help Desk</br>
/// <b>Version:</b> 1</br>
/// <b>Autor:</b> Grupo Cubo Ltda.</br>
/// <b>Fecha:</b> 19/12/2017</br>
/// <b>Descripcion:</b>Realiza la carga de las vistas para ver o guardar fotos de productos/servicios.
/// </remarks>
function LoadPhotosProductosServicios(id) {
    $('#FotosModal').modal('show');

    $.ajax({
        contentType: 'application/html; charset=utf-8',
        type: "GET",
        dataType: 'html',
        url: 'Fotos/VisualizarFotosProductosServicios/' + id,
        cache: false,
        success: function (result) {
            $('#FotosCanvas').html(result);
        },
        fail: function (xhr, status) {
            alert(status);
        }
    });

    $.ajax({
        contentType: 'application/html; charset=utf-8',
        type: "GET",
        dataType: 'html',
        url: 'Fotos/LoadCargaProductosServicios/' + id,
        cache: false,
        success: function (result) {
            $('#FotosCarga').html(result);
        },
        fail: function (xhr, status) {
            alert(status);
        }
    });
}


function LoadPhotoProductoServicio(id) {
    var url = '';
    var urlDir = '/Fotos/VisualizarFotosProductosServicios?id=';
    $.ajax({
        contentType: 'application/html; charset=utf-8',
        type: "GET",
        dataType: 'html',
        url: urlDir + id,
        cache: false,
        success: function (result) {
            $('.ps_' + id + '').html(result);
        },
        fail: function (xhr, status) {
            alert(status);
        }
    });

}

/// <summary>
/// Carga de fotos
/// </summary>
/// <remarks>
/// <h3>Calidad</h3>
/// </hr>
/// <b>Requerimiento:</b> Help Desk</br>
/// <b>Version:</b> 1</br>
/// <b>Autor:</b> Grupo Cubo Ltda.</br>
/// <b>Fecha:</b> 19/12/2017</br>
/// <b>Descripcion:</b>Realiza la carga de la vista para ver fotos de usuarios.
/// </remarks>
function LoadPhotosUsuarios() {
    $('#FotosModal').modal('show');

    $.ajax({
        contentType: 'application/html; charset=utf-8',
        type: "GET",
        dataType: 'html',
        url: 'Juego/VisualizarFotosUsuarios',
        cache: false,
        success: function (result) {
            $('#FotosCanvas').html(result);
        },
        fail: function (xhr, status) {
            alert(status);
        }
    });
}

/// <summary>
/// Pausar audio historias
/// </summary>
/// <remarks>
/// <h3>Calidad</h3>
/// </hr>
/// <b>Requerimiento:</b> Help Desk</br>
/// <b>Version:</b> 1</br>
/// <b>Autor:</b> Grupo Cubo Ltda.</br>
/// <b>Fecha:</b> 19/12/2017</br>
/// <b>Descripcion:</b>Pausa el sonido que se ejecuta en la vista de audios.
/// </remarks>
function CerrarAudio() {
    $(function () {
        document.getElementById('player').pause();
        playing = false;
        $(this).text("restart sound");
    });
}

/// <summary>
/// Carga o insercion de fotos
/// </summary>
/// <param name="id">Identificador de la historia</param>
/// <remarks>
/// <h3>Calidad</h3>
/// </hr>
/// <b>Requerimiento:</b> Help Desk</br>
/// <b>Version:</b> 1</br>
/// <b>Autor:</b> Grupo Cubo Ltda.</br>
/// <b>Fecha:</b> 19/12/2017</br>
/// <b>Descripcion:</b>Realiza la carga de las vistas para escuchar o guardar audios de historias.
/// </remarks>
function LoadCargaAudio(id) {
    $('#AudiosModal').modal('show');

    $.ajax({
        contentType: 'application/html; charset=utf-8',
        type: "GET",
        dataType: 'html',
        url: 'Audios/EscucharAudio/' + id,
        cache: false,
        success: function (result) {
            $('#AudiosCanvas').html(result);
        },
        fail: function (xhr, status) {
            alert(status);
        }
    });

    $.ajax({
        contentType: 'application/html; charset=utf-8',
        type: "GET",
        dataType: 'html',
        url: 'Audios/LoadCargaAudio/' + id,
        cache: false,
        success: function (result) {
            $('#AudiosCarga').html(result);
        },
        fail: function (xhr, status) {
            alert(status);
        }
    });
}



