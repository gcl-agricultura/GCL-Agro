﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(BosquesPaz.Startup))]
namespace BosquesPaz
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
