﻿using BosquesPaz.Services;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BosquesPaz.Controllers
{
    public class MapasController : Controller
    {
        private BosquesService serviceBosque { get { return new BosquesService(); } }
        // GET: Mapas
        public ActionResult Index()
        {
            var bosques = serviceBosque.ObtenerBosques();
            return PartialView(bosques);
        }

        public JsonResult MapPupUp( int id)
        {
            var model = serviceBosque.ObtenerBosque(id);
            return Json(new { lat = model.Latitud, lng = model.Longitud }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult Bosques()
        {
            var bosques = serviceBosque.ObtenerBosques();
            return Json(bosques, JsonRequestBehavior.AllowGet);
        }

        public JsonResult BosquesDepartamentos()
        {
            var departamentos = serviceBosque.ObtenerBosquesDepartamentos();
            var municipios = serviceBosque.ObtenerBosquesMunicipios();
            return Json(new { dtos = departamentos, muni = municipios}, JsonRequestBehavior.AllowGet);
        }

        public JsonResult Departamentos()
        {
            string jsonStr = Server.MapPath("~/Content/Poligonos/depto.json");
            string json = string.Empty;
            using (StreamReader reader = new StreamReader(jsonStr))
            {
                json = reader.ReadToEnd();
                var objeto = JsonConvert.DeserializeObject(json);
            }
            return Json(json, JsonRequestBehavior.AllowGet);
        }

        public JsonResult Municipios()
        {
            string jsonStr = Server.MapPath("~/Content/Poligonos/mpio.json");
            string json = string.Empty;
            using (StreamReader reader = new StreamReader(jsonStr))
            {
                json = reader.ReadToEnd();

            }
            return Json(json, JsonRequestBehavior.AllowGet);
        }

    }
}