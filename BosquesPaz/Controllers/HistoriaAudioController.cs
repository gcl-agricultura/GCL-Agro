﻿using BosquesPaz.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BosquesPaz.Models;
using BosquesPaz.Services;
using BosquesPaz.ServiceReferenceFuncionarios;

namespace BosquesPaz.Controllers
{
    /// <summary>
    /// Clase que responde a las acciones que solicitan las peticiones de entrada y salida. 
    /// </summary>
    /// <remarks>
    /// <h3>Calidad</h3>
    /// </hr>
    /// <b>Requerimiento: Help Desk</b></br>
    /// <b>Version: 1</b></br>
    /// <b>Autor: Grupo Cubo Ltda.</b></br>
    /// <b>Fecha: 19/12/2017<b></br>
    /// <b>Descripcion:</b> Funciona para visualizar las vistas de historias, realizar peticiones a la capa de servicios y ejecutar acciones.
    /// </remarks>
    public class HistoriaAudioController : Controller
    {
        /// <summary>
        /// Propiedad que instancia la clase HistoriasService
        /// </summary>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 19/12/2017</br>
        /// <b>Descripcion:</b> Propiedad que instancia el servicio HistoriasService, para enviar peticiones y recibir el resultado.
        /// </remarks>
        private HistoriasService serviceHistoria { get { return new HistoriasService(); } }

        /// <summary>
        /// Accion que realiza la carga de la vista index de historias
        /// </summary>
        /// <returns>Retorna vista parcial</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 19/12/2017</br>
        /// <b>Descripcion:</b> Accion que realiza el retorno de la vista parcial index de historias.
        /// </remarks>
        // GET: HistoriaAudio
        public ActionResult Index()
        {
            var infraestructura = new Infraestructura();
            EntidadInfoUsuario info = null;
            if (Session["infoUsuario"] != null)
            {
                info = Session["infoUsuario"] as EntidadInfoUsuario;
            }
            if (infraestructura.PermisoUsuario(info, "Editar", "Historias"))
            {
                return PartialView();
            }
            else
            {
                return RedirectToAction("IndexUsuarios");
            }
        }

        /// <summary>
        /// Accion que realiza la carga de la vista index de historias para los usuarios
        /// </summary>
        /// <returns>Retorna vista parcial</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 28/12/2017</br>
        /// <b>Descripcion:</b> Accion que realiza el retorno de la vista parcial index de historias.
        /// </remarks>
        public ActionResult IndexUsuarios()
        {
            return PartialView();
        }

        /// <summary>
        /// Método que serializa la informacion en formato JSON
        /// </summary>
        /// <param name="agrid">Parametors de la grilla</param>
        /// <returns>Retorna JsonResult</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 19/12/2017</br>
        /// <b>Descripcion:</b> Realiza la serializacion de los datos recibidos a formato JSON, para cargar la grilla de historias.
        /// </remarks>
        public JsonResult CargarGrilla(AnexGRID agrid)
        {
            return Json(DataSourceGrilla(agrid), JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Método que retorna AnexGRIDResponde, segun sus parametros.
        /// </summary>
        /// <param name="agrid">Parametors de la grilla</param>
        /// <returns>Retorna AnexGRIDResponde</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 19/12/2017</br>
        /// <b>Descripcion:</b> Método que permite obtener los datos de historias, y organizarlos con el proposito de retornarlos a una grilla.
        /// </remarks>
        [NonAction]
        public AnexGRIDResponde DataSourceGrilla(AnexGRID agrid)
        {
            try
            {
                IEnumerable<HistoriasBosques> registros = new List<HistoriasBosques>();
                IDictionary<string, string> filtros = new Dictionary<string, string>();
                if (agrid.filtros != null)
                    foreach (var filtro in agrid.filtros)
                    {
                        filtros.Add(filtro.columna, filtro.valor);
                    }

                registros = serviceHistoria.ObtenerHistoriasAudios(agrid.pagina, agrid.limite, agrid.columna, filtros, agrid.columna_orden != "DESC");

                if (registros != null)
                {
                    agrid.Inicializar();
                    int cantidadRegistros = serviceHistoria.CantidadRegistros;
                    agrid.SetData(registros, cantidadRegistros);
                }
                return agrid.responde();
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        /// <summary>
        /// Método que serializa la informacion en formato JSON
        /// </summary>
        /// <param name="agrid">Parametors de la grilla</param>
        /// <returns>Retorna JsonResult</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 19/12/2017</br>
        /// <b>Descripcion:</b> Realiza la serializacion de los datos recibidos a formato JSON, para cargar la grilla de historias para los usuarios.
        /// </remarks>
        public JsonResult CargarGrillaHistoriasUsuarios(AnexGRID agrid)
        {
            return Json(DataSourceGrillaHistoriasUsuarios(agrid), JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Método que retorna AnexGRIDResponde, segun sus parametros.
        /// </summary>
        /// <param name="agrid">Parametors de la grilla</param>
        /// <returns>Retorna AnexGRIDResponde</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 19/12/2017</br>
        /// <b>Descripcion:</b> Método que permite obtener los datos de historias para los usuarios, y organizarlos con el proposito de retornarlos a una grilla.
        /// </remarks>
        [NonAction]
        public AnexGRIDResponde DataSourceGrillaHistoriasUsuarios(AnexGRID agrid)
        {
            try
            {
                IEnumerable<HistoriasBosques> registros = new List<HistoriasBosques>();
                IDictionary<string, string> filtros = new Dictionary<string, string>();
                if (agrid.filtros != null)
                    foreach (var filtro in agrid.filtros)
                    {
                        filtros.Add(filtro.columna, filtro.valor);
                    }

                registros = serviceHistoria.ObtenerHistoriasAudiosUsuarios(agrid.pagina, agrid.limite, agrid.columna, filtros, agrid.columna_orden != "DESC");

                if (registros != null)
                {
                    agrid.Inicializar();
                    int cantidadRegistros = serviceHistoria.CantidadRegistros;
                    agrid.SetData(registros, cantidadRegistros);
                }
                return agrid.responde();
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }
    }
}