﻿using BosquesPaz.Models;
using BosquesPaz.Services;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BosquesPaz.Controllers
{
    /// <summary>
    /// Clase que responde a las acciones que solicitan las peticiones de entrada y salida. 
    /// </summary>
    /// <remarks>
    /// <h3>Calidad</h3>
    /// </hr>
    /// <b>Requerimiento: Help Desk</b></br>
    /// <b>Version: 1</b></br>
    /// <b>Autor: Grupo Cubo Ltda.</b></br>
    /// <b>Fecha: 19/12/2017<b></br>
    /// <b>Descripcion:</b> Funciona para visualizar las vistas de audios, realizar peticiones a la capa de servicios y ejecutar acciones.
    /// </remarks>
    public class AudiosController : Controller
    {
        /// <summary>
        /// Propiedad que instancia la clase AudiosService
        /// </summary>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 19/12/2017</br>
        /// <b>Descripcion:</b> Propiedad que instancia el servicio AudiosService, para enviar peticiones y recibir el resultado.
        /// </remarks>
        private AudiosService Servicio
        {
            get
            {
                return new AudiosService();
            }
        }

        /// <summary>
        /// Accion que realiza la carga de la vista index de audios
        /// </summary>
        /// <returns>Retorna vista</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 19/12/2017</br>
        /// <b>Descripcion:</b> Accion que realiza el retorno de la vista index de audios.
        /// </remarks>
        // GET: Audios
        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// Accion que guarda los datos de la vista cargar audios de audios
        /// </summary>
        /// <param name="viewModel">Instancia de AudiosHistorias</param>
        /// <returns>Retorna redirecion a raiz</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 19/12/2017</br>
        /// <b>Descripcion:</b> Recibe una instancia de AudiosHistorias y la envia a la clase de servicio.
        /// </remarks>
        [HttpPost]
        public ActionResult UploadAudios(AudiosHistorias viewModel)
        {
            try
            {
                string route = ConfigurationManager.AppSettings["AudiosFolder"];
                IList<AudiosHistorias> audios = new List<AudiosHistorias>();
                foreach (var file in viewModel.Audios)
                {
                    if (file != null && file.ContentLength > 0)
                    {
                        if (file.ContentLength > 150000000)
                        {
                            ModelState.AddModelError("audio", "El audio no puede exceder un tamaño de 150MB.");
                            PartialView(viewModel);
                        }
                        else
                        {
                            AudiosHistorias audio = new AudiosHistorias();
                            string Url = route + Guid.NewGuid() + Path.GetExtension(file.FileName);
                            file.SaveAs(Url);
                            audio.UrlAudio = Url;
                            audio.Id_HistoriaBosque = viewModel.Id_HistoriaBosque;
                            audios.Add(audio);
                        }
                    }
                }
                audios = Servicio.GuardarAudioBosque(audios).ToList();
                return Redirect("/");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Accion que realiza la carga de la vista escuchar audio de audios
        /// </summary>
        /// <param name="id">Identificador de la historia</param>
        /// <returns>Retorna vista parcial</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 19/12/2017</br>
        /// <b>Descripcion:</b>Accion que realiza el retorno de la vista parcial escuchar audio pasando como parametros una coleccion de AudiosHistorias.
        /// </remarks>
        [HttpGet]
        public ActionResult EscucharAudio(int? id)
        {
            IEnumerable<AudiosHistorias> Audio = Servicio.ObtenerAudioBosque((int)id);
            return PartialView("EscucharAudio", Audio);
        }

        /// <summary>
        /// Accion que realiza la carga de la vista cargar audios de audios
        /// </summary>
        /// <returns>Retorna vista</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 19/12/2017</br>
        /// <b>Descripcion:</b> Accion que realiza el retorno de la vista parcial cargar audios pasando como parametro una instancia de AudiosHistorias.
        /// </remarks>
        [HttpGet]
        public ActionResult LoadCargaAudio(int? id)
        {
            var model = new AudiosHistorias();
            model.Id_HistoriaBosque = (int)id;
            model.Audios = new List<HttpPostedFileBase>();
            return PartialView("CargarAudio", model);
        }
    }
}