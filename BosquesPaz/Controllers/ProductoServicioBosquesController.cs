﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BosquesPaz.Models;
using BosquesPaz.Services;
using BosquesPaz.Helpers;
using System.IO;

namespace BosquesPaz.Controllers
{
    /// <summary>
    /// Clase que responde a las acciones que solicitan las peticiones de entrada y salida. 
    /// </summary>
    /// <remarks>
    /// <h3>Calidad</h3>
    /// </hr>
    /// <b>Requerimiento: Help Desk</b></br>
    /// <b>Version: 1</b></br>
    /// <b>Autor: Grupo Cubo Ltda.</b></br>
    /// <b>Fecha: 19/12/2017<b></br>
    /// <b>Descripcion:</b> Funciona para visualizar las vistas de productos/servicios, realizar peticiones a la capa de servicios y ejecutar acciones.
    /// </remarks>
    public class ProductoServicioBosquesController : Controller
    {
        /// <summary>
        /// Propiedad que instancia la clase ProductosServiciosService
        /// </summary>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 19/12/2017</br>
        /// <b>Descripcion:</b> Propiedad que instancia el servicio ProductosServiciosService, para enviar peticiones y recibir el resultado.
        /// </remarks>
        private ProductosServiciosService servicio { get{ return new ProductosServiciosService(); } }
        private TiposProductosServiciosService servicioTiposProductosServicios { get { return new TiposProductosServiciosService(); } }


        /// <summary>
        /// Accion que realiza la carga de la vista index de producto/servicio
        /// </summary>
        /// <param name="id">Identificador del bosque</param>
        /// <returns>Retorna vista parcial</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 19/12/2017</br>
        /// <b>Descripcion:</b> Accion que realiza el retorno de la vista parcial index de producto/servicio segun el numero de identificador que ingrese.
        /// </remarks>
        // GET: ProductoServicioBosques
        public ActionResult Index(int id)
        {
            ProductosServicios ProductosServicios = servicio.ObtenerProductosServicios(id);

            ViewBag.Id =id ;
            ViewBag.Tipo = servicioTiposProductosServicios.ObtenerNombreTipoProductoServicioPorId(ProductosServicios.Id_TipoProductoServicio);
            ViewBag.Nombre = ProductosServicios.Nombre;
            ViewBag.Descripcion = ProductosServicios.Descripcion;
            ViewBag.Precio = ProductosServicios.Precio;
            ViewBag.NombreBosque = ProductosServicios.NombreBosque;
            ViewBag.Departamento = ProductosServicios.Departamento;
            ViewBag.Municipio = ProductosServicios.Municipio;

            return PartialView();
        }

        /// <summary>
        /// Método que serializa la informacion en formato JSON
        /// </summary>
        /// <param name="agrid">Parametors de la grilla</param>
        /// <returns>Retorna JsonResult</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 19/12/2017</br>
        /// <b>Descripcion:</b> Realiza la serializacion de los datos recibidos a formato JSON, para cargar la grilla de productos/servicios.
        /// </remarks>
        public JsonResult CargarGrilla(AnexGRID agrid)
        {
            return Json(DataSourceGrilla(agrid), JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Método que retorna AnexGRIDResponde, segun sus parametros.
        /// </summary>
        /// <param name="agrid">Parametors de la grilla</param>
        /// <returns>Retorna AnexGRIDResponde</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 19/12/2017</br>
        /// <b>Descripcion:</b> Método que permite obtener los datos de ProductosServicios, y organizarlos con el proposito de retornarlos a una grilla.
        /// </remarks>
        [NonAction]
        public AnexGRIDResponde DataSourceGrilla(AnexGRID agrid)
        {
            try
            {
                IEnumerable<ProductosServicios> registros = new List<ProductosServicios>();
                IDictionary<string, string> filtros = new Dictionary<string, string>();
                if (agrid.filtros != null)
                    foreach (var filtro in agrid.filtros)
                    {
                        filtros.Add(filtro.columna, filtro.valor);
                    }

                registros = servicio.ObtenerProductosServiciosBosques(agrid.pagina, agrid.limite, agrid.columna, filtros, agrid.columna_orden != "DESC");

                if (registros != null)
                {
                    agrid.Inicializar();
                    int cantidadRegistros = servicio.CantidadRegistros;
                    agrid.SetData(registros, cantidadRegistros);
                }
                return agrid.responde();
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }
    }
}