﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BosquesPaz.Models;
using BosquesPaz.Services;
using BosquesPaz.Helpers;
using BosquesPaz.ServiceReferenceFuncionarios;

namespace BosquesPaz.Controllers
{
    /// <summary>
    /// Clase que responde a las acciones que solicitan las peticiones de entrada y salida. 
    /// </summary>
    /// <remarks>
    /// <h3>Calidad</h3>
    /// </hr>
    /// <b>Requerimiento: Help Desk</b></br>
    /// <b>Version: 1</b></br>
    /// <b>Autor: Grupo Cubo Ltda.</b></br>
    /// <b>Fecha: 19/12/2017<b></br>
    /// <b>Descripcion:</b> Funciona para visualizar las vistas de patrocinador, realizar peticiones a la capa de servicios y ejecutar acciones.
    /// </remarks>
    public class PatrocinadorController : Controller
    {
        /// <summary>
        /// Propiedad que instancia la clase CombosService
        /// </summary>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 19/12/2017</br>
        /// <b>Descripcion:</b> Propiedad que instancia el servicio CombosService, para enviar peticiones y recibir el resultado.
        /// </remarks>
        private CombosService service { get { return new CombosService(); } }

        /// <summary>
        /// Propiedad que instancia la clase PatrocinadoresService
        /// </summary>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 19/12/2017</br>
        /// <b>Descripcion:</b> Propiedad que instancia el servicio PatrocinadoresService, para enviar peticiones y recibir el resultado.
        /// </remarks>
        private PatrocinadoresService servicePatrocinador { get { return new PatrocinadoresService(); } }

        /// <summary>
        /// Accion que realiza la carga de la vista index de patrocinadores
        /// </summary>
        /// <param name="id">Identificador del bosque</param>
        /// <returns>Retorna vista parcial</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 19/12/2017</br>
        /// <b>Descripcion:</b> Accion que realiza el retorno de la vista parcial index de patrocinadores segun el numero de identificador que ingrese.
        /// </remarks>

        /// <summary>
        /// Propiedad que instancia la clase PatrocinadoresService
        /// </summary>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 28/12/2017</br>
        /// <b>Descripcion:</b> Propiedad que instancia el servicio PatrocinadoresService, para enviar peticiones y recibir el resultado.
        /// </remarks>
        private PatrocinadoresService servicioPatrocinador { get { return new PatrocinadoresService(); } }
        // GET: Patrocinador
        public ActionResult Index()
        {
            return PartialView();
        }

        /// <summary>
        /// Accion que realiza la carga de la vista detalles de patrocinadores
        /// </summary>
        /// <param name="id">Identificador del patrocinador</param>
        /// <returns>Retorna vista parcial</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 19/12/2017</br>
        /// <b>Descripcion:</b> Accion que realiza el retorno de la vista parcial detalle de patrocinadores segun el numero de identificador que ingrese.
        /// </remarks>
        public ActionResult Details(int id)
        {
            var model = servicePatrocinador.ObtenerPatrocinadorBosque(id);
            return PartialView(model);
        }

        /// <summary>
        /// Accion que realiza la carga de la vista detalles de patrocinadores
        /// </summary>
        /// <param name="id">Identificador del patrocinador</param>
        /// <returns>Retorna vista parcial</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 19/12/2017</br>
        /// <b>Descripcion:</b> Accion que realiza el retorno de la vista parcial detalle de patrocinadores segun el numero de identificador que ingrese.
        /// </remarks>
        public ActionResult DetailsAdministrador(int id)
        {
            var model = servicePatrocinador.ObtenerPatrocinadorBosque(id);
            return PartialView(model);
        }

        /// <summary>
        /// Accion que realiza la carga de la vista crear de patrocinadores
        /// </summary>
        /// <param name="id">Identificador del bosque</param>
        /// <returns>Retorna vista parcial</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 19/12/2017</br>
        /// <b>Descripcion:</b> Accion que realiza el retorno de la vista parcial crear de patrocinaodres.
        /// </remarks>
        public ActionResult Create()
        {
            ViewBag.TiposPatrocinadores = service.ObtenerTiposPatrocinadores();
            ViewBag.EstadosPatrocinadores = service.ObtenerEstadosPatrocinadores();
            ViewBag.Departamentos = service.ObtenerDepartamentos();
            ViewBag.Municipios = new List<Municipios>();
            var model = new PatrocinadoresBosques();
            return PartialView(model);
        }

        /// <summary>
        /// Accion que guarda los datos de la vista crear de patrocinadores
        /// </summary>
        /// <param name="viewModel">Instancia de PatrocinadoresBosques</param>
        /// <returns>Retorna vista parcial</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 19/12/2017</br>
        /// <b>Descripcion:</b> Recibe una instancia de PatrocinadoresBosques y la envia a la clase de servicio, y retorna redireccion de accion con el identificador de la instancia como parametro.
        /// </remarks>
        [HttpPost]
        public ActionResult Create(PatrocinadoresBosques viewModel)
        {
            try
            {
                viewModel.Id_EstadoPatrocinador = 1;
                viewModel.FechaCreacion = DateTime.Now;
                viewModel.Observaciones = "No se ha agregado ninguna observación.";
                if (ModelState.IsValid)
                {
                    viewModel = servicePatrocinador.GuardarPatrocinadorBosque(viewModel);
                    return RedirectToAction("Details", new { id = viewModel.Id_PatrocinadorBosque });
                }
                ViewBag.TiposPatrocinadores = service.ObtenerTiposPatrocinadores();
                ViewBag.EstadosPatrocinadores = service.ObtenerEstadosPatrocinadores();
                ViewBag.Departamentos = service.ObtenerDepartamentos();
                ViewBag.Municipios = new List<Municipios>();
                return PartialView(viewModel);
            }
            catch
            {
                ViewBag.TiposPatrocinadores = service.ObtenerTiposPatrocinadores();
                ViewBag.EstadosPatrocinadores = service.ObtenerEstadosPatrocinadores();
                ViewBag.Departamentos = service.ObtenerDepartamentos();
                ViewBag.Municipios = new List<Municipios>();
                return PartialView(viewModel);
            }
        }

        /// <summary>
        /// Accion que realiza la carga de la vista editar de patrocinadores
        /// </summary>
        /// <param name="id">Identificador del patrocinador</param>
        /// <returns>Retorna vista parcial</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 19/12/2017</br>
        /// <b>Descripcion:</b> Accion que realiza el retorno de la vista parcial editar de patrocinadores segun el numero del identificador que ingrese.
        /// </remarks>
        public ActionResult Edit(int id)
        {
            ViewBag.TiposPatrocinadores = service.ObtenerTiposPatrocinadores();
            ViewBag.EstadosPatrocinadores = service.ObtenerEstadosPatrocinadores();
            ViewBag.Departamentos = service.ObtenerDepartamentos();
            ViewBag.Municipios = new List<Municipios>();
            var model = servicePatrocinador.ObtenerPatrocinadorBosque(id);
            return PartialView(model);
        }

        /// <summary>
        /// Accion que actualiza los datos de la vista editar de patrocinador
        /// </summary>
        /// <param name="viewModel">Instancia de PatrocinadoresBosques</param>
        /// <returns>Retorna vista parcial</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 19/12/2017</br>
        /// <b>Descripcion:</b> Recibe una instancia de PatrocinadoresBosques y la envia a la clase de servicio, y retorna redireccion de accion con el identificador de la instancia como parametro.
        /// </remarks>
        [HttpPost]
        public ActionResult Edit(PatrocinadoresBosques viewModel)
        {
            bool result = false;
            try
            {
                viewModel.Id_EstadoPatrocinador = 1;
                if (ModelState.IsValid)
                {
                    if (result = servicePatrocinador.ActualizarPatrocinadorBosque(viewModel))
                    {
                        return RedirectToAction("Details", new { id = viewModel.Id_PatrocinadorBosque });
                    }
                }
                ViewBag.TiposPatrocinadores = service.ObtenerTiposPatrocinadores();
                ViewBag.EstadosPatrocinadores = service.ObtenerEstadosPatrocinadores();
                ViewBag.Departamentos = service.ObtenerDepartamentos();
                ViewBag.Municipios = new List<Municipios>();
                return PartialView(viewModel);
            }
            catch
            {
                ViewBag.TiposPatrocinadores = service.ObtenerTiposPatrocinadores();
                ViewBag.EstadosPatrocinadores = service.ObtenerEstadosPatrocinadores();
                ViewBag.Departamentos = service.ObtenerDepartamentos();
                ViewBag.Municipios = new List<Municipios>();
                return PartialView(viewModel);
            }
        }

        /// <summary>
        /// Accion que realiza la carga de la vista editar de patrocinadores
        /// </summary>
        /// <param name="id">Identificador del patrocinador</param>
        /// <returns>Retorna vista parcial</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 19/12/2017</br>
        /// <b>Descripcion:</b> Accion que realiza el retorno de la vista parcial editar de patrocinadores segun el numero del identificador que ingrese.
        /// </remarks>
        public ActionResult EditAdministrador(int id)
        {
            ViewBag.EstadosPatrocinadores = service.ObtenerEstadosPatrocinadores();
            var model = servicePatrocinador.ObtenerPatrocinadorBosque(id);
            return PartialView(model);
        }

        /// <summary>
        /// Accion que actualiza los datos de la vista editar de patrocinador
        /// </summary>
        /// <param name="viewModel">Instancia de PatrocinadoresBosques</param>
        /// <returns>Retorna vista parcial</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 19/12/2017</br>
        /// <b>Descripcion:</b> Recibe una instancia de PatrocinadoresBosques y la envia a la clase de servicio, y retorna redireccion de accion con el identificador de la instancia como parametro.
        /// </remarks>
        [HttpPost]
        public ActionResult EditAdministrador(PatrocinadoresBosques viewModel)
        {
            bool result = false;
            EntidadInfoUsuario info = null;
            info = Session["infoUsuario"] as EntidadInfoUsuario;
            try
            {
                if (ModelState.IsValid)
                {
                    viewModel.FechaVerificacion = DateTime.Now;
                    viewModel.Usuario = info.Usuario;
                    if (result = servicePatrocinador.ActualizarPatrocinadorBosque(viewModel))
                    {
                        return RedirectToAction("DetailsAdministrador", new { id = viewModel.Id_PatrocinadorBosque });
                    }
                }
                ViewBag.EstadosPatrocinadores = service.ObtenerEstadosPatrocinadores();
                return PartialView(viewModel);
            }
            catch
            {
                ViewBag.EstadosPatrocinadores = service.ObtenerEstadosPatrocinadores();
                return PartialView(viewModel);
            }
        }

        /// <summary>
        /// Método que serializa la informacion en formato JSON
        /// </summary>
        /// <param name="agrid">Parametors de la grilla</param>
        /// <returns>Retorna JsonResult</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 19/12/2017</br>
        /// <b>Descripcion:</b> Realiza la serializacion de los datos recibidos a formato JSON, para cargar la grilla de patrocinadores segun el numero de identificador que ingrese.
        /// </remarks>
        public JsonResult CargarGrilla(AnexGRID agrid)
        {
            return Json(DataSourceGrilla(agrid), JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Método que retorna AnexGRIDResponde, segun sus parametros.
        /// </summary>
        /// <param name="id">Identificador del bosque</param>
        /// <param name="agrid">Parametors de la grilla</param>
        /// <returns>Retorna AnexGRIDResponde</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 19/12/2017</br>
        /// <b>Descripcion:</b> Método que permite obtener los datos de PatrocinadoresBosques, y organizarlos con el proposito de retornarlos a una grilla.
        /// </remarks>
        [NonAction]
        public AnexGRIDResponde DataSourceGrilla(AnexGRID agrid)
        {
            try
            {
                IEnumerable<PatrocinadoresBosques> registros = new List<PatrocinadoresBosques>();
                IDictionary<string, string> filtros = new Dictionary<string, string>();
                if (agrid.filtros != null)
                    foreach (var filtro in agrid.filtros)
                    {
                        filtros.Add(filtro.columna, filtro.valor);
                    }

                registros = servicePatrocinador.ObtenerPatrocinadoresBosques(agrid.pagina, agrid.limite, agrid.columna, filtros, agrid.columna_orden != "DESC");

                if (registros != null)
                {
                    agrid.Inicializar();
                    int cantidadRegistros = servicePatrocinador.CantidadRegistros;
                    agrid.SetData(registros, cantidadRegistros);
                }
                return agrid.responde();
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }


        public ActionResult VerificarCorreo()
        {
            var infraestructura = new Infraestructura();
            EntidadInfoUsuario info = null;
            if (Session["infoUsuario"] != null)
            {
                info = Session["infoUsuario"] as EntidadInfoUsuario;
            }
            if (infraestructura.PermisoUsuario(info, "VerDetalle", "Patrocinadores"))
            {
                return RedirectToAction("Index");
            }
            return PartialView();
        }


        [HttpPost]
        public ActionResult VerificarCorreo(PatrocinadoresBosques viewmodel)
        {
            int result;
            try
            {
                if (viewmodel.Correo != null)
                {
                    result = servicioPatrocinador.ObtenerPatrocinadorPorCorreo(viewmodel.Correo);
                    if (result > 0)
                    {
                        return RedirectToAction("Details", new { id = result});
                    }
                    else
                    {
                        return RedirectToAction("Create");
                    }
                }
                return PartialView();
            }
            catch (Exception)
            {
                return PartialView();
            }
        }
    }
}