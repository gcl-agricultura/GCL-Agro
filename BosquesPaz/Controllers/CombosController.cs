﻿using BosquesPaz.Services;
using System.Web.Mvc;

namespace BosquesPaz.Controllers
{
    /// <summary>
    /// Clase que responde a las acciones que solicitan las peticiones de entrada y salida. 
    /// </summary>
    /// <remarks>
    /// <h3>Calidad</h3>
    /// </hr>
    /// <b>Requerimiento: Help Desk</b></br>
    /// <b>Version: 1</b></br>
    /// <b>Autor: Grupo Cubo Ltda.</b></br>
    /// <b>Fecha: 19/12/2017<b></br>
    /// <b>Descripcion:</b> Funciona para realizar peticiones a al servicio CombosService.
    /// </remarks>
    public class CombosController : Controller
    {
        /// <summary>
        /// Propiedad que instancia la clase CombosService
        /// </summary>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 19/12/2017</br>
        /// <b>Descripcion:</b> Propiedad que instancia el servicio CombosService, para enviar peticiones y recibir el resultado.
        /// </remarks>
        private CombosService service { get { return new CombosService(); } }


        /// <summary>
        /// Accion que realiza la carga de la lista desplegable de municipios
        /// </summary>
        /// <param name="id">Identificador del departamento</param>
        /// <returns>Retorna JsonResult</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 19/12/2017</br>
        /// <b>Descripcion:</b> Accion que realiza el retorno de JsonResult para cargar la lista desplegable de municipios.
        /// </remarks>
        // GET: ObtenerMunicipios/id
        [HttpGet]
        public JsonResult ObtenerMunicipios(int id)
        {
            return Json(new SelectList(service.ObtenerMunicipios(id), "Id_Municipio", "Nombre"), JsonRequestBehavior.AllowGet);
        }
    }
}
