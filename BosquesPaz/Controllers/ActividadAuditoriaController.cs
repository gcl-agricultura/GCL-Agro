﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BosquesPaz.Models;
using BosquesPaz.Services;
using BosquesPaz.Helpers;

namespace BosquesPaz.Controllers
{
    /// <summary>
    /// Clase que responde a las acciones que solicitan las peticiones de entrada y salida. 
    /// </summary>
    /// <remarks>
    /// <h3>Calidad</h3>
    /// </hr>
    /// <b>Requerimiento: Help Desk</b></br>
    /// <b>Version: 1</b></br>
    /// <b>Autor: Grupo Cubo Ltda.</b></br>
    /// <b>Fecha: 19/12/2017<b></br>
    /// <b>Descripcion:</b> Funciona para visualizar la vista de auditorias actividades, realizar peticiones a la capa de servicios y ejecutar acciones.
    /// </remarks>
    public class ActividadAuditoriaController : Controller
    {
        /// <summary>
        /// Propiedad que instancia la clase ActividadesAuditoriasService
        /// </summary>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 19/12/2017</br>
        /// <b>Descripcion:</b> Propiedad que instancia el servicio ActividadesAuditoriasService, para enviar peticiones y recibir el resultado.
        /// </remarks>
        private ActividadesAuditoriasService serviceActividadAuditoria { get { return new ActividadesAuditoriasService(); } }
        // GET: ActividadAuditoria

        /// <summary>
        /// Accion que realiza la carga de la vista index de auditoria actividad
        /// </summary>
        /// <param name="id">Identificador de la actividad</param>
        /// <returns>Retorna vista parcial</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 19/12/2017</br>
        /// <b>Descripcion:</b> Accion que realiza el retorno de la vista parcial index de auditorias actividades segun el numero de identificador que ingrese.
        /// </remarks>
        public ActionResult Index(int id)
        {
            ViewBag.idActividadBosque = id; 
            return PartialView();
        }

        /// <summary>
        /// Método que serializa la informacion en formato JSON
        /// </summary>
        /// <param name="id">Identificador de la actividad</param>
        /// <param name="agrid">Parametors de la grilla</param>
        /// <returns>Retorna JsonResult</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 19/12/2017</br>
        /// <b>Descripcion:</b> Realiza la serializacion de los datos recibidos a formato JSON, para cargar la grilla de auditorias actividades segun el numero de identificador que ingrese.
        /// </remarks>
        public JsonResult CargarGrillaHistorial(AnexGRID agrid, int id)
        {
            return Json(DataSourceGrillaHistorial(agrid, id), JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Método que retorna AnexGRIDResponde, segun sus parametros.
        /// </summary>
        /// <param name="id">Identificador de la actividad</param>
        /// <param name="agrid">Parametors de la grilla</param>
        /// <returns>Retorna AnexGRIDResponde</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 19/12/2017</br>
        /// <b>Descripcion:</b> Método que permite obtener los datos de AudiActividadesBosques, y organizarlos con el proposito de retornarlos a una grilla.
        /// </remarks>
        [NonAction]
        public AnexGRIDResponde DataSourceGrillaHistorial(AnexGRID agrid, int id)
        {
            try
            {
                IEnumerable<AudiActividadesBosques> registros = new List<AudiActividadesBosques>();
                IDictionary<string, string> filtros = new Dictionary<string, string>();
                if (agrid.filtros != null)
                    foreach (var filtro in agrid.filtros)
                    {
                        filtros.Add(filtro.columna, filtro.valor);
                    }

                registros = serviceActividadAuditoria.ObtenerActividadBosquesAuditorias(agrid.pagina, agrid.limite, agrid.columna, filtros, agrid.columna_orden != "DESC", id);

                if (registros != null)
                {
                    agrid.Inicializar();
                    int cantidadRegistros = serviceActividadAuditoria.CantidadRegistros;
                    agrid.SetData(registros, cantidadRegistros);
                }
                return agrid.responde();
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }
    }
}