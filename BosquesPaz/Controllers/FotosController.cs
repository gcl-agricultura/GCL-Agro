﻿using BosquesPaz.Models;
using BosquesPaz.Services;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BosquesPaz.Controllers
{
    /// <summary>
    /// Clase que responde a las acciones que solicitan las peticiones de entrada y salida. 
    /// </summary>
    /// <remarks>
    /// <h3>Calidad</h3>
    /// </hr>
    /// <b>Requerimiento: Help Desk</b></br>
    /// <b>Version: 1</b></br>
    /// <b>Autor: Grupo Cubo Ltda.</b></br>
    /// <b>Fecha: 19/12/2017<b></br>
    /// <b>Descripcion:</b> Funciona para visualizar las vistas de fotos, realizar peticiones a la capa de servicios y ejecutar acciones.
    /// </remarks>
    public class FotosController : Controller
    {
        /// <summary>
        /// Propiedad que instancia la clase FotosService
        /// </summary>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 19/12/2017</br>
        /// <b>Descripcion:</b> Propiedad que instancia el servicio FotosService, para enviar peticiones y recibir el resultado.
        /// </remarks>
        private FotosService Servicio
        {
            get
            {
                return new FotosService();
            }
        }

        /// <summary>
        /// Accion que guarda los datos de la vista fotos carga de fotos
        /// </summary>
        /// <param name="viewModel">Instancia de FotosBosques</param>
        /// <returns>Retorna redirecion a raiz</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 19/12/2017</br>
        /// <b>Descripcion:</b> Recibe una instancia de FotosBosques y la envia a la clase de servicio.
        /// </remarks>
        [HttpPost]
        public ActionResult UploadFotos(FotosBosques viewModel)
        {
            try
            {
                string route = ConfigurationManager.AppSettings["FotosFolder"];
                IList<FotosBosques> Fotos = new List<FotosBosques>();
                foreach (var file in viewModel.Fotos)
                {
                    if (file != null && file.ContentLength > 0)
                    {
                        FotosBosques foto = new FotosBosques();
                        string Url = route + Guid.NewGuid() + Path.GetExtension(file.FileName);
                        file.SaveAs(Url);
                        foto.Url = Url;
                        foto.Id_Bosque = viewModel.Id_Bosque;
                        foto.Descripcion = "Test";
                        Fotos.Add(foto);
                    }
                }
                Fotos = Servicio.GuardarFotosBosque(Fotos).ToList();
                return Redirect("/");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Accion que carga la vista fotos visualizacion
        /// </summary>
        /// <param name="id">Identificador del bosque</param>
        /// <returns>Retorna vista parcial</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 19/12/2017</br>
        /// <b>Descripcion:</b> Retorna una vista parcial pasando como parametro una coleccion FotosBosques.
        /// </remarks>
        [HttpGet]
        public ActionResult VisualizarFotos(int? id)
        {
            IEnumerable<FotosBosques> Fotos = Servicio.ObtenerFotosBosque((int)id);
            return PartialView("FotosVisualizacion", Fotos);
        }

        [HttpGet]
        public ActionResult retonarFoto(int? id)
        {
            IEnumerable<FotosBosques> Fotos = Servicio.ObtenerFotosBosque((int)id);
            return PartialView("FotosVisualizacion", Fotos);
        }

        /// <summary>
        /// Accion que realiza la carga de la vista fotos carga de fotos
        /// </summary>
        /// <param name="id">Identificador del bosque</param>
        /// <returns>Retorna vista parcial</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 19/12/2017</br>
        /// <b>Descripcion:</b> Accion que realiza el retorno de la vista parcial fotos carga pasando como parametro una instancia de FotosBosques.
        /// </remarks>
        [HttpGet]
        public ActionResult LoadCargaFotos(int? id)
        {
            var model = new FotosBosques();
            model.Id_Bosque = (int)id;
            model.Fotos = new List<HttpPostedFileBase>();
            return PartialView("FotosCarga", model);
        }

        /// <summary>
        /// Accion que guarda los datos de la vista fotos carga individuos especies de fotos
        /// </summary>
        /// <param name="viewModel">Instancia de FotosIndividuosEspecies</param>
        /// <returns>Retorna redirecion al detalle de individuos/especies</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 19/12/2017</br>
        /// <b>Descripcion:</b> Recibe una instancia de FotosIndividuosEspecies y la envia a la clase de servicio.
        /// </remarks>
        [HttpPost]
        public ActionResult UploadFotosIndividuosEspecies(FotosIndividuosEspecies viewModel)
        {
            try
            {
                string route = ConfigurationManager.AppSettings["FotosFolder"];
                IList<FotosIndividuosEspecies> Fotos = new List<FotosIndividuosEspecies>();
                foreach (var file in viewModel.Fotos)
                {
                    if (file != null && file.ContentLength > 0)
                    {
                        FotosIndividuosEspecies foto = new FotosIndividuosEspecies();
                        string Url = route + Guid.NewGuid() + Path.GetExtension(file.FileName);
                        file.SaveAs(Url);
                        foto.Url = Url;
                        foto.Id_IndividuoEspecie = viewModel.Id_IndividuoEspecie;
                        foto.Descripcion = "Test";
                        Fotos.Add(foto);
                    }
                }
                Fotos = Servicio.GuardarIndividuosEspecies(Fotos).ToList();
                var id = Session["idBosque"];
                return Redirect("/?m=Bosques&id=" + Convert.ToInt16(id) + "&mb=Individuos&mbid=" + viewModel.Id_IndividuoEspecie);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Accion que carga la vista fotos visualizacion individuos especies
        /// </summary>
        /// <param name="id">Identificador de individuos/especies</param>
        /// <returns>Retorna vista parcial</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 19/12/2017</br>
        /// <b>Descripcion:</b> Retorna una vista parcial pasando como parametro una coleccion FotosIndividuosEspecies.
        /// </remarks>
        [HttpGet]
        public ActionResult VisualizarFotosIndividuosEspecies(int? id)
        {
            IEnumerable<FotosIndividuosEspecies> Fotos = Servicio.ObtenerFotosIndividuoEspecie((int)id);
            return PartialView("FotosVisualizacionindividuosEspecies", Fotos);
        }

        /// <summary>
        /// Accion que realiza la carga de la vista fotos indiviudos especies carga de fotos
        /// </summary>
        /// <param name="id">Identificador de individuos/especies</param>
        /// <returns>Retorna vista parcial</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 19/12/2017</br>
        /// <b>Descripcion:</b> Accion que realiza el retorno de la vista parcial fotos carga pasando como parametro una instancia de FotosIndividuosEspecies.
        /// </remarks>
        [HttpGet]
        public ActionResult LoadCargaFotosIndividuosEspecies(int? id)
        {
            var model = new FotosIndividuosEspecies();
            model.Id_IndividuoEspecie = (int)id;
            model.Fotos = new List<HttpPostedFileBase>();
            return PartialView("FotosCargaIndividuosEspecies", model);
        }

        /// <summary>
        /// Accion que guarda los datos de la vista fotos carga productos servicios de fotos
        /// </summary>
        /// <param name="viewModel">Instancia de FotosProductosServicios</param>
        /// <returns>Retorna redirecion al detalle de productos/servicios</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 19/12/2017</br>
        /// <b>Descripcion:</b> Recibe una instancia de FotosProductosServicios y la envia a la clase de servicio.
        /// </remarks>
        [HttpPost]
        public ActionResult UploadFotosProductosServicios(FotosProductosServicios viewModel)
        {
            try
            {
                string route = ConfigurationManager.AppSettings["FotosFolder"];
                IList<FotosProductosServicios> Fotos = new List<FotosProductosServicios>();
                foreach (var file in viewModel.Fotos)
                {
                    if (file != null && file.ContentLength > 0)
                    {
                        FotosProductosServicios foto = new FotosProductosServicios();
                        string Url = route + Guid.NewGuid() + Path.GetExtension(file.FileName);
                        file.SaveAs(Url);
                        foto.Url = Url;
                        foto.Id_ProductoServicio = viewModel.Id_ProductoServicio;
                        foto.Descripcion = "Test";
                        Fotos.Add(foto);
                    }
                }
                Fotos = Servicio.GuardarProductosServicios(Fotos).ToList();
                var id = Session["idBosque"];
                return Redirect("/?m=Bosques&id=" + Convert.ToInt16(id) +"&mb=Productos&mbid=" + viewModel.Id_ProductoServicio);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Accion que carga la vista fotos visualizacion producto servicios
        /// </summary>
        /// <param name="id">Identificador de productos/servicios</param>
        /// <returns>Retorna vista parcial</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 19/12/2017</br>
        /// <b>Descripcion:</b> Retorna una vista parcial pasando como parametro una coleccion FotosProductosServicios.
        /// </remarks>
        [HttpGet]
        public ActionResult VisualizarFotosProductosServicios(int? id)
        {
            IEnumerable<FotosProductosServicios> Fotos = Servicio.ObtenerFotosProductosServicios((int)id);
            return PartialView("FotosVisualizacionProductosServicios", Fotos);
        }

        /// <summary>
        /// Accion que realiza la carga de la vista fotos productos servicios carga de fotos
        /// </summary>
        /// <param name="id">Identificador de productos/servicios</param>
        /// <returns>Retorna vista parcial</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 19/12/2017</br>
        /// <b>Descripcion:</b> Accion que realiza el retorno de la vista parcial fotos carga pasando como parametro una instancia de FotosIndividuosEspecies.
        /// </remarks>
        [HttpGet]
        public ActionResult LoadCargaProductosServicios(int? id)
        {
            var model = new FotosProductosServicios();
            model.Id_ProductoServicio = (int)id;
            model.Fotos = new List<HttpPostedFileBase>();
            return PartialView("FotosCargaProductosServicios", model);
        }

        /// <summary>
        /// Metodo que verifica el formato de las fotos
        /// </summary>
        /// <param name="img">nombre de la imagen</param>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 19/12/2017</br>
        /// <b>Descripcion:</b> Metodo que verifica el formato de la imagen, para realizar la visualizacion de la foto.
        /// </remarks>
        [HttpGet]
        public void ProcessImageRequest(string img)
        {
            string route = ConfigurationManager.AppSettings["FotosFolder"];
            MemoryStream m = new MemoryStream();
            Image i = Image.FromFile(route + img);

            var ext = img.Split('.').LastOrDefault().ToLower();

            Response.Clear();
            Response.ClearContent();
            Response.ClearHeaders();
            Response.Buffer = true;
            Response.BufferOutput = true;
            Response.ContentType = "application/pdf";

            switch (ext)
            {
                case "jpeg":
                    i.Save(m, System.Drawing.Imaging.ImageFormat.Jpeg);
                    Response.ContentType = "image/jpeg";
                    break;
                case "jpg":
                    i.Save(m, System.Drawing.Imaging.ImageFormat.Jpeg);
                    Response.ContentType = "image/jpeg";
                    break;
                case "png":
                    i.Save(m, System.Drawing.Imaging.ImageFormat.Png);
                    Response.ContentType = "image/png";
                    break;
                case "gif":
                    i.Save(m, System.Drawing.Imaging.ImageFormat.Gif);
                    Response.ContentType = "image/gif";
                    break;
                case "tiff":
                    i.Save(m, System.Drawing.Imaging.ImageFormat.Tiff);
                    Response.ContentType = "image/tiff";
                    break;
            }

            Response.BinaryWrite(m.ToArray());
            Response.Flush();
        }
    }
}
