﻿using BosquesPaz.Models;
using BosquesPaz.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BosquesPaz.Controllers
{
    /// <summary>
    /// Clase que responde a las acciones que solicitan las peticiones de entrada y salida. 
    /// </summary>
    /// <remarks>
    /// <h3>Calidad</h3>
    /// </hr>
    /// <b>Requerimiento: Help Desk</b></br>
    /// <b>Version: 1</b></br>
    /// <b>Autor: Grupo Cubo Ltda.</b></br>
    /// <b>Fecha: 19/12/2017<b></br>
    /// <b>Descripcion:</b> Funciona para visualizar las vistas de home.
    /// </remarks>
    public class HomeController : Controller
    {

        private BosquesService serviceBosque { get { return new BosquesService(); } }
        private ProductosServiciosService serviceProductosServicios  { get { return new ProductosServiciosService(); } }
        private TematicasService serviceTematicasService  { get { return new TematicasService(); } }
        private IndividuosService serviceIndividuosEspecies { get { return new IndividuosService(); } }

        /// <summary>
        /// Accion que realiza la carga de la vista index de home
        /// </summary>
        /// <param name="m">Nombre del modulo principal</param>
        /// <param name="id">Identificador del modulo principal</param>
        /// <param name="mb">Nombre del submodulo</param>
        /// <param name="mbId">Identificador del submodulo</param>
        /// <returns>Retorna vista parcial</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 19/12/2017</br>
        /// <b>Descripcion:</b> Accion que realiza el retorno de la vista Index de home segun sus parametros.
        /// </remarks>
        public ActionResult Index(string m, int? id, string mb, int? mbId)
        {
            ViewBag.allBosques = serviceBosque.ObtenerBosques();
            ViewBag.allProductosServicios = serviceProductosServicios.ObtenerProductosServicios();
            ViewBag.allTematicas = serviceTematicasService.ObtenerTematicas();
            ViewBag.allIndividuosEspecies = serviceIndividuosEspecies.ObtenerIndividuosEspecies();

            IEnumerable<ProductosServicios> allPS = serviceProductosServicios.ObtenerProductosServicios();
            List<ProductosServicios> allServicios = new List<ProductosServicios>();
            List<ProductosServicios> alProductos = new List<ProductosServicios>();

            foreach (var item in allPS) {
                if (item.TipoProductoServicio == "Servicio                                          ") {
                    allServicios.Add(item);
                }
                else if(item.TipoProductoServicio == "Producto                                          ") {
                    alProductos.Add(item);
                }

            }


            ViewBag.allServicios = allServicios;
            ViewBag.allProductos = alProductos;


            ViewBag.modulo = m;
            if (id != null)
            {
                ViewBag.IdBosque = id;
                if (mbId != null)
                {
                    ViewBag.moduloSubId = mbId;
                }
            }
            else
            {
                ViewBag.IdBosque = 0;
                ViewBag.moduloSubId = 0;
            }
            ViewBag.moduloSub = mb;
            
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}