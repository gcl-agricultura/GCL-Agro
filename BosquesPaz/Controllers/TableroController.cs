﻿using BosquesPaz.Models;
using BosquesPaz.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BosquesPaz.Controllers
{
    public class TableroController : Controller
    {
        private TableroService servicio { get { return new TableroService(); } }
        // GET: Tablero
        public ActionResult Index()
        {
            return PartialView();
        }

        public JsonResult ActividadesBosques()
        {
            IEnumerable<ActividadesGRFBosques> result = servicio.ActividadesBosques();
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult BeneficiandoBosques()
        {
            IEnumerable<BeneficiandoBosques> result = servicio.BeneficiandoBosques();
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult CantidadBosques()
        {
            CantidadBosques result = servicio.CantidadBosques();
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult DistribucionBosques()
        {
            IEnumerable<DistribucionBosques> result = servicio.DistribucionBosques();
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult InversionesBosques()
        {
            IEnumerable<InversionesBosques> result = servicio.InversionesBosques();
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult InversionistasBosques()
        {
            IEnumerable<InversionistasBosques> result = servicio.InversionistasBosques();
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult LogrosBosques()
        {
            IEnumerable<LogrosBosques> result = servicio.LogrosBosques();
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult ProductosServiciosBosques()
        {
            IEnumerable<ProductosServiciosBosques> result = servicio.ProductosServiciosBosques();
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult HistorialIndicadores()
        {
            IEnumerable<HistorialIndicadores> result = servicio.HistorialIndicadores();
            return Json(result, JsonRequestBehavior.AllowGet);
        }
    }
}