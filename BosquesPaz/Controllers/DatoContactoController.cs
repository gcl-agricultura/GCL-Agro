﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BosquesPaz.Helpers;
using BosquesPaz.Models;
using BosquesPaz.Services;
using BosquesPaz.ServiceReferenceFuncionarios;

namespace BosquesPaz.Controllers
{
    /// <summary>
    /// Clase que responde a las acciones que solicitan las peticiones de entrada y salida. 
    /// </summary>
    /// <remarks>
    /// <h3>Calidad</h3>
    /// </hr>
    /// <b>Requerimiento: Help Desk</b></br>
    /// <b>Version: 1</b></br>
    /// <b>Autor: Grupo Cubo Ltda.</b></br>
    /// <b>Fecha: 19/12/2017<b></br>
    /// <b>Descripcion:</b> Funciona para visualizar las vistas de datos contactos, realizar peticiones a la capa de servicios y ejecutar acciones.
    /// </remarks>
    public class DatoContactoController : Controller
    {
        /// <summary>
        /// Propiedad que instancia la clase DatosContactosService
        /// </summary>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 19/12/2017</br>
        /// <b>Descripcion:</b> Propiedad que instancia el servicio DatosContactosService, para enviar peticiones y recibir el resultado.
        /// </remarks>
        private DatosContactosService serviceDatoContacto { get { return new DatosContactosService(); } }

        /// <summary>
        /// Accion que realiza la carga de la vista index de datos contactos
        /// </summary>
        /// <returns>Retorna vista parcial</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 19/12/2017</br>
        /// <b>Descripcion:</b> Accion que realiza el retorno de la vista index de datos contactos.
        /// </remarks>
        // GET: DatoContacto
        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// Accion que realiza la carga de la vista detalles de datos contactos
        /// </summary>
        /// <param name="id">Identificador de los datos de contacto</param>
        /// <returns>Retorna vista parcial</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 19/12/2017</br>
        /// <b>Descripcion:</b> Accion que realiza el retorno de la vista parcial detalle de los datos de contacto segun el numero de identificador que ingrese.
        /// </remarks>
        public ActionResult Details(int id)
        {
            var model = serviceDatoContacto.ObtenerDatoContacto(id);
            return PartialView(model);
        }

        /// <summary>
        /// Accion que realiza la carga de la vista SinPermiso de datos contactos
        /// </summary>
        /// <returns>Retorna vista parcial</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 19/12/2017</br>
        /// <b>Descripcion:</b> Accion que realiza el retorno de la vista SinPermiso de datos contactos.
        /// </remarks>
        public ActionResult SinPermiso()
        {
            return PartialView();
        }

        /// <summary>
        /// Accion que realiza la carga de la vista crear de datos contactos
        /// </summary>
        /// <param name="id">Identificador del bosque</param>
        /// <returns>Retorna vista parcial</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 19/12/2017</br>
        /// <b>Descripcion:</b> Accion que realiza el retorno de la vista parcial crear de datos contactos. si el contacto y ha sido creado con anterioridad, se retorna redireccion de accion con el identificador del parametro de entrada como parametro
        /// </remarks>
        public ActionResult Create(int id)
        {
            try
            {
                var infraestrucutura = new Infraestructura();
                EntidadInfoUsuario info = null;
                if (Session["infoUsuario"] != null)
                {
                    info = Session["infoUsuario"] as EntidadInfoUsuario;
                }
                var model = serviceDatoContacto.ObtenerDatoContacto(id);
                if (model != null)
                {
                    return RedirectToAction("Details", "DatoContacto", new { id });
                }
                else
                {
                    if (info != null && infraestrucutura.PermisoUsuario(info, "Crear", "DatosContacto"))
                    {
                        model = new DatosContactosBosques();
                        model.Id_DatoContactoBosque = id;
                        return PartialView(model);
                    }
                    return PartialView("SinPermiso");
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
            
        }

        /// <summary>
        /// Accion que guarda los datos de la vista crear de datos contactos
        /// </summary>
        /// <param name="viewModel">Instancia de DatosContactosBosques</param>
        /// <param name="id">Identificador de los datos de contacto</param>
        /// <returns>Retorna vista parcial</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 19/12/2017</br>
        /// <b>Descripcion:</b> Recibe una instancia de DatosContactosBosques y la envia a la clase de servicio, y retorna redireccion de accion con el identificador de la instancia como parametro,
        /// si el contacto y ha sido creado con anterioridad, se retorna redireccion de accion con el identificador del parametro de entrada como parametro
        /// </remarks>
        [HttpPost]
        public ActionResult Create(DatosContactosBosques viewModel, int id)
        {
            var info = Session["infoUsuario"] as EntidadInfoUsuario;

            string ipAddress = string.Empty;
            if (!string.IsNullOrEmpty(Request.ServerVariables[""]))
            {

                ipAddress = Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
            }
            else
            {
                ipAddress = Request.ServerVariables["REMOTE_ADDR"];
            }
            var model = serviceDatoContacto.ObtenerDatoContacto(id);
            try
            {
                viewModel.Aud_User = info.Usuario;
                viewModel.Aud_Origen = ipAddress;
                if (ModelState.IsValid)
                {
                    if (model != null)
                    {
                        return RedirectToAction("Details", "DatoContacto", new { viewModel, id });
                    }
                    else
                    {
                        viewModel = serviceDatoContacto.GuardarDatoContacto(viewModel);
                        return RedirectToAction("Details", "DatoContacto", new { id = viewModel.Id_DatoContactoBosque });
                    }
                }
                return PartialView(viewModel);
            }
            catch
            {
                return PartialView(viewModel);
            }
        }

        /// <summary>
        /// Accion que realiza la carga de la vista editar de datos contactos
        /// </summary>
        /// <param name="id">Identificador de los datos de contacto</param>
        /// <returns>Retorna vista parcial</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 19/12/2017</br>
        /// <b>Descripcion:</b> Accion que realiza el retorno de la vista parcial editar datos de contactos segun el numero del identificador que ingrese.
        /// </remarks>
        public ActionResult Edit(int id)
        {
            var model = serviceDatoContacto.ObtenerDatoContacto(id);
            return PartialView(model);
        }

        /// <summary>
        /// Accion que actualiza los datos de la vista editar de datos contactos
        /// </summary>
        /// <param name="viewModel">Instancia de DatosContactosBosques</param>
        /// <param name="id">Identificador de los datos de contacto</param>
        /// <returns>Retorna vista parcial</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 19/12/2017</br>
        /// <b>Descripcion:</b> Recibe una instancia de DatosContactosBosques y la envia a la clase de servicio, y retorna redireccion de accion con el identificador del parametro de entrada de entrada como parametro.
        /// </remarks>
        [HttpPost]
        public ActionResult Edit(DatosContactosBosques viewModel, int id)
        {
            bool result = false;
            try
            {
                if (ModelState.IsValid)
                {
                    if (result = serviceDatoContacto.ActualizarDatoContacto(viewModel))
                    {
                        return RedirectToAction("Details", "DatoContacto", new { id});
                    }
                }
                return PartialView(viewModel);
            }
            catch
            {
                return PartialView(viewModel);
            }
        }
    }
}