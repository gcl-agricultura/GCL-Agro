﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BosquesPaz.Models;
using BosquesPaz.Services;
using BosquesPaz.Helpers;
using BosquesPaz.ServiceReferenceFuncionarios;

namespace BosquesPaz.Controllers
{
    /// <summary>
    /// Clase que responde a las acciones que solicitan las peticiones de entrada y salida. 
    /// </summary>
    /// <remarks>
    /// <h3>Calidad</h3>
    /// </hr>
    /// <b>Requerimiento: Help Desk</b></br>
    /// <b>Version: 1</b></br>
    /// <b>Autor: Grupo Cubo Ltda.</b></br>
    /// <b>Fecha: 19/12/2017<b></br>
    /// <b>Descripcion:</b> Funciona para visualizar las vistas de juegos, realizar peticiones a la capa de servicios y ejecutar acciones.
    /// </remarks>
    public class JuegoController : Controller
    {
        /// <summary>
        /// Propiedad que instancia la clase IndividuosService
        /// </summary>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 19/12/2017</br>
        /// <b>Descripcion:</b> Propiedad que instancia el servicio IndividuosService, para enviar peticiones y recibir el resultado.
        /// </remarks>
        private IndividuosService serviceIndividuo { get { return new IndividuosService(); } }

        /// <summary>
        /// Propiedad que instancia la clase ProductosServiciosService
        /// </summary>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 19/12/2017</br>
        /// <b>Descripcion:</b> Propiedad que instancia el servicio ProductosServiciosService, para enviar peticiones y recibir el resultado.
        /// </remarks>
        private ProductosServiciosService serviceProducto { get { return new ProductosServiciosService(); } }

        /// <summary>
        /// Propiedad que instancia la clase HistoriasService
        /// </summary>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 19/12/2017</br>
        /// <b>Descripcion:</b> Propiedad que instancia el servicio HistoriasService, para enviar peticiones y recibir el resultado.
        /// </remarks>
        private HistoriasService serviceHistoria { get { return new HistoriasService(); } }

        /// <summary>
        /// Propiedad que instancia la clase FotosService
        /// </summary>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 19/12/2017</br>
        /// <b>Descripcion:</b> Propiedad que instancia el servicio FotosService, para enviar peticiones y recibir el resultado.
        /// </remarks>
        private FotosService serviceFoto { get { return new FotosService(); } }

        /// <summary>
        /// Propiedad que instancia la clase UsuariosJuegosService
        /// </summary>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 19/12/2017</br>
        /// <b>Descripcion:</b> Propiedad que instancia el servicio UsuariosJuegosService, para enviar peticiones y recibir el resultado.
        /// </remarks>
        private UsuariosJuegosService serviceJuego { get { return new UsuariosJuegosService(); } }

        /// <summary>
        /// Propiedad que instancia la clase DatosUsuariosServices
        /// </summary>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 19/12/2017</br>
        /// <b>Descripcion:</b> Propiedad que instancia el servicio DatosUsuariosServices, para enviar peticiones y recibir el resultado.
        /// </remarks>
        private DatosUsuariosServices serviceDatosUsuarios { get { return new DatosUsuariosServices(); } }

        /// <summary>
        /// Propiedad que instancia la clase DepartamentosService
        /// </summary>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 19/12/2017</br>
        /// <b>Descripcion:</b> Propiedad que instancia el servicio DepartamentosService, para enviar peticiones y recibir el resultado.
        /// </remarks>
        private DepartamentosService serviceDepartamento { get { return new DepartamentosService(); } }

        /// <summary>
        /// Propiedad que instancia la clase MunicipiosService
        /// </summary>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 19/12/2017</br>
        /// <b>Descripcion:</b> Propiedad que instancia el servicio MunicipiosService, para enviar peticiones y recibir el resultado.
        /// </remarks>
        private MunicipiosService serivceMunicipio { get { return new MunicipiosService(); } }

        /// <summary>
        /// Accion que realiza la carga de la vista index de juego
        /// </summary>
        /// <returns>Retorna vista parcial</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 19/12/2017</br>
        /// <b>Descripcion:</b> Accion que realiza el retorno de la vista parcial index de juego.
        /// </remarks>
        // GET: Juego
        public ActionResult Index()
        {
            return PartialView();
        }

        /// <summary>
        /// Accion que realiza la carga de la vista verificar usuario de juego
        /// </summary>
        /// <returns>Retorna vista parcial</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 19/12/2017</br>
        /// <b>Descripcion:</b> Accion que realiza el retorno de la vista parcial verificar usuario de juego.
        /// </remarks>
        public ActionResult VerificarUsuario()
        {
            if (Session["infoUsuario"] != null)
                return RedirectToAction("Index");

            return PartialView();
        }

        /// <summary>
        /// Accion que consulta los datos de la vista verificar usuario de juego
        /// </summary>
        /// <param name="viewModel">Instancia de DatosUsuarios</param>
        /// <returns>Retorna vista parcial</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 19/12/2017</br>
        /// <b>Descripcion:</b> Recibe una instancia de DatosUsuarios y la envia a la clase de servicio, y retorna redireccion de accion con el identificador de la instancia como parametro.
        /// </remarks>
        [HttpPost]
        public ActionResult VerificarUsuario(DatosUsuarios viewModel)
        {
            Session["usuarioJuegos"] = null;
            int result;
            try
            {
                if (viewModel.NickName != null)
                {
                    result = serviceDatosUsuarios.ObtenerDatosUsuariosPorNickName(viewModel.NickName);
                    if (result > 0)
                    {
                        Session["usuarioJuegos"] = viewModel.NickName;
                        return RedirectToAction("Index");
                    }
                    else
                    {
                        return RedirectToAction("Create");
                    }
                }

                return PartialView();
            }
            catch
            {
                return PartialView();
            }
        }

        /// <summary>
        /// Accion que realiza la carga de la vista crear de juego
        /// </summary>
        /// <returns>Retorna vista parcial</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 19/12/2017</br>
        /// <b>Descripcion:</b> Accion que realiza el retorno de la vista parcial crear de juego.
        /// </remarks>
        public ActionResult Create()
        {
            ViewBag.Departamentos = serviceDepartamento.ObtenerDepartamentos();
            ViewBag.Municipios = new List<Municipios>();
            var model = new DatosUsuarios();
            if (Session["FotoUsuario"] != null)
            {
                model.Id_FotoUsuario = Convert.ToInt16(Session["FotoUsuario"]);
            }
            return PartialView(model);
        }

        /// <summary>
        /// Accion que guarda los datos de la vista crear de juego
        /// </summary>
        /// <param name="viewModel">Instancia de DatosUsuarios</param>
        /// <returns>Retorna vista parcial</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 19/12/2017</br>
        /// <b>Descripcion:</b> Recibe una instancia de DatosUsuarios y la envia a la clase de servicio, y retorna redireccion de accion con el identificador de la instancia como parametro.
        /// </remarks>
        [HttpPost]
        public ActionResult Create(DatosUsuarios viewModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    viewModel = serviceDatosUsuarios.GuardarDatosUsuario(viewModel);
                    Session["usuarioJuegos"] = viewModel.NickName;
                    return RedirectToAction("Index");
                }
                ViewBag.Departamentos = serviceDepartamento.ObtenerDepartamentos();
                ViewBag.Municipios = new List<Municipios>();
                return PartialView(viewModel);
            }
            catch
            {
                ViewBag.Departamentos = serviceDepartamento.ObtenerDepartamentos();
                ViewBag.Municipios = new List<Municipios>();
                return PartialView(viewModel);
            }
        }

        /// <summary>
        /// Accion que realiza la carga de la vista sopa letras de juego
        /// </summary>
        /// <returns>Retorna vista parcial</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 19/12/2017</br>
        /// <b>Descripcion:</b> Accion que realiza el retorno de la vista parcial sopa letras de juego.
        /// </remarks>
        public ActionResult SopaLetras()
        {
            return PartialView();
        }

        /// <summary>
        /// Accion que guarda los datos de la vista sopa letras de juego
        /// </summary>
        /// <param name="juego">Nombre del juego</param>
        /// <param name="puntaje">Numero del puntaje</param>
        /// <param name="modulo">Nombre del modulo</param>
        /// <returns>Redireccion de accion</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 19/12/2017</br>
        /// <b>Descripcion:</b> Recibe datos de juego, modulo y puntaje, verfica el usuario e instancia DatosUsuarios, 
        /// envia a la clase de servicio, y retorna redireccion de accion.
        /// </remarks>
        public ActionResult GuardarJuego(string juego, int puntaje, string modulo)
        {
            UsuariosJuegos user = new UsuariosJuegos();
            string userJuego = null;
            if (Session["infoUsuario"] != null)
            {
                var info = Session["infoUsuario"] as EntidadInfoUsuario;
                userJuego = info.Usuario;
            }
            else
            {
                userJuego = Convert.ToString(Session["usuarioJuegos"]);
            }
            try
            {
                user.Usuario = userJuego;
                user.Juego = juego;
                user.Puntaje = puntaje;
                user.Imagen = "No aplica";
                user.Modulo = modulo;
                if (puntaje <= 240)
                {
                    user.Id_Puntaje = 1;
                }
                else if (puntaje > 241 && puntaje <= 420)
                {
                    user.Id_Puntaje = 2;
                }
                else
                {
                    user.Id_Puntaje = 3;
                }
                serviceJuego.GuardarUsuarioJuego(user);

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return RedirectToAction("Puntajes");
        }

        /// <summary>
        /// Accion que guarda los datos de la vistas rompecabezas de juego
        /// </summary>
        /// <param name="juego">Nombre del juego</param>
        /// <param name="puntaje">Numero del puntaje</param>
        /// <param name="modulo">Nombre del modulo</param>
        /// <returns>Redireccion de accion</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 19/12/2017</br>
        /// <b>Descripcion:</b> Recibe datos de juego, modulo y puntaje, verfica el usuario y la foto e instancia DatosUsuarios, 
        /// envia a la clase de servicio, y retorna redireccion de accion.
        /// </remarks>
        public ActionResult GuardarJuegoRompecabezas(string juego, int puntaje, string modulo)
        {
            UsuariosJuegos user = new UsuariosJuegos();
            string userJuego = null;
            if (Session["infoUsuario"] != null)
            {
                var info = Session["infoUsuario"] as EntidadInfoUsuario;
                userJuego = info.Usuario;
            }
            else
            {
                userJuego = Convert.ToString(Session["usuarioJuegos"]);
            }
            try
            {
                user.Usuario = userJuego;
                user.Juego = juego;
                user.Puntaje = puntaje;
                user.Imagen = Convert.ToString(Session["ImagenRompecabezas"]);
                user.Modulo = modulo;
                if (puntaje <= 60)
                {
                    user.Id_Puntaje = 1;
                }
                else if (puntaje > 60 && puntaje <= 120)
                {
                    user.Id_Puntaje = 2;
                }
                else
                {
                    user.Id_Puntaje = 3;
                }
                serviceJuego.GuardarUsuarioJuego(user);

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return RedirectToAction("Puntajes");
        }

        /// <summary>
        /// Accion que realiza la carga de la vista rompecabezas de juego
        /// </summary>
        /// <returns>Retorna vista parcial</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 19/12/2017</br>
        /// <b>Descripcion:</b> Accion que realiza el retorno de la vista parcial rompecaebezas de juego.
        /// </remarks>
        public ActionResult RompeCabezas()
        {
            return PartialView();
        }

        /// <summary>
        /// Accion que realiza la carga de la vista rompecabezas Producto de juego
        /// </summary>
        /// <returns>Retorna vista parcial</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 19/12/2017</br>
        /// <b>Descripcion:</b> Accion que realiza el retorno de la vista parcial rompecabezas producto de juego.
        /// </remarks>
        public ActionResult RompeCabezasProducto()
        {
            return PartialView();
        }

        /// <summary>
        /// Accion que realiza la carga de la vista rompecabezas bosque de juego
        /// </summary>
        /// <returns>Retorna vista parcial</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 19/12/2017</br>
        /// <b>Descripcion:</b> Accion que realiza el retorno de la vista parcial rompecabezas bosque de juego.
        /// </remarks>
        public ActionResult RompeCabezasBosque()
        {
            return PartialView();
        }

        /// <summary>
        /// Método que serializa la coleccion de individuos/especies a formato JSON
        /// </summary>
        /// <returns>Retorna JsonResult</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 19/12/2017</br>
        /// <b>Descripcion:</b> Realiza la serializacion de la coleccion obtenida y la retorna en un JsonResult.
        /// </remarks>
        public JsonResult ObtenerIndividuosEspecies()
        {
            try
            {
                var result = serviceIndividuo.ObtenerInversoIndividuosEspecies();
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Método que serializa la coleccion de productos/servicios a formato JSON
        /// </summary>
        /// <returns>Retorna JsonResult</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 19/12/2017</br>
        /// <b>Descripcion:</b> Realiza la serializacion de la coleccion obtenida y la retorna en un JsonResult.
        /// </remarks>
        public JsonResult ObtenerProductosServicios()
        {
            try
            {
                var result = serviceProducto.ObtenerInversoProductosServicios();
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Método que serializa la coleccion de historias a formato JSON
        /// </summary>
        /// <returns>Retorna JsonResult</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 19/12/2017</br>
        /// <b>Descripcion:</b> Realiza la serializacion de la coleccion obtenida y la retorna en un JsonResult.
        /// </remarks>
        public JsonResult ObtenerHistorias()
        {
            try
            {
                var result = serviceHistoria.ObtenerInversoHistorias();
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Accion que realiza la carga de la vista puntajes de juego
        /// </summary>
        /// <returns>Retorna vista parcial</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 19/12/2017</br>
        /// <b>Descripcion:</b> Accion que verifica el usuario y consulta los puntajes obtenidos en ateriores juegos, 
        /// realiza el retorno de la vista parcial puntajes de juego.
        /// </remarks>
        public ActionResult Puntajes()
        {
            string userJuego = null;
            UsuariosJuegos user = new UsuariosJuegos();
            if (Session["infoUsuario"] != null)
            {
                var info = Session["infoUsuario"] as EntidadInfoUsuario;
                userJuego = info.Usuario;
            }
            else
            {
                userJuego = Convert.ToString(Session["usuarioJuegos"]);
            }

            IEnumerable<FotosUsuarios> Fotos = serviceFoto.ObtenerFotosUsuariosPorId(serviceDatosUsuarios.ObtenerDatoUsuarioFotoPorNickName(userJuego));

            ViewBag.Usuario = userJuego;
            ViewBag.PuntajeSopaIndividuos = serviceJuego.PuntajeSopa(userJuego, "Individuos Especies", "Sopa de letras");
            ViewBag.PuntajeSopaHistorias = serviceJuego.PuntajeSopa(userJuego, "Historias", "Sopa de letras");
            ViewBag.PuntajeRompeIndividuos = serviceJuego.PuntajeRompe(userJuego, "Individuos Especies", "Rompecabezas");
            ViewBag.PuntajeRompeProductos = serviceJuego.PuntajeRompe(userJuego, "Productos Servicios", "Rompecabezas");
            ViewBag.PuntajeRompeBosques = serviceJuego.PuntajeRompe(userJuego, "Bosques", "Rompecabezas");

            return PartialView("puntajes", Fotos);
        }

        /// <summary>
        /// Accion que alamcena el identificador de fotos usuarios
        /// </summary>
        /// <param name="id">Identificador de la foto usuario</param>
        /// <returns>Redireccion de accion</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 19/12/2017</br>
        /// <b>Descripcion:</b> Accion que almacena el identificador de fotos usuarios y realiza redireccion de accion.
        /// </remarks>
        public ActionResult CargarImagenAvatar(int id)
        {
            IEnumerable<FotosUsuarios> Fotos = serviceFoto.ObtenerFotosUsuarios();
            Session["FotoUsuario"] = Fotos.ToList()[id-1].Id_FotoUsuario;
            return RedirectToAction("Create");
        }

        /// <summary>
        /// Accion que carga la vista fotos usuarios de juego
        /// </summary>
        /// <returns>Retorna vista parcial</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 19/12/2017</br>
        /// <b>Descripcion:</b> Retorna una vista parcial pasando como parametro una coleccion FotosUsuarios.
        /// </remarks>
        [HttpGet]
        public ActionResult VisualizarFotosUsuarios()
        {
            IEnumerable<FotosUsuarios> Fotos = serviceFoto.ObtenerFotosUsuarios();
            return PartialView("FotosUsuarios", Fotos);
        }

        
    }
}