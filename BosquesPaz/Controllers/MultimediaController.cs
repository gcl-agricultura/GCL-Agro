﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BosquesPaz.Controllers
{
    public class MultimediaController : Controller
    {
        /// <summary>
        /// Metodo que verifica el formato de las fotos
        /// </summary>
        /// <param name="img">nombre de la imagen</param>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 19/12/2017</br>
        /// <b>Descripcion:</b> Metodo que verifica el formato de la imagen, para realizar la visualizacion de la foto.
        /// </remarks>
        // GET: Multimedia/Imagen/<name>.<ext>
        public void Imagen(string img)
        {
            string route = ConfigurationManager.AppSettings["FotosFolder"];
            MemoryStream m = new MemoryStream();
            

            var ext = img.Split('.').LastOrDefault().ToLower();
            
            if(ext != "mp3") {
               
                if (System.IO.File.Exists(route + img))
                {
                    Image i = Image.FromFile(route + img);
                    Response.Clear();
                    Response.ClearContent();
                    Response.ClearHeaders();
                    Response.Buffer = true;
                    Response.BufferOutput = true;
                    Response.ContentType = "application/pdf";

                    switch (ext)
                    {
                        case "jpeg":
                            i.Save(m, System.Drawing.Imaging.ImageFormat.Jpeg);
                            Response.ContentType = "image/jpeg";
                            break;
                        case "jpg":
                            i.Save(m, System.Drawing.Imaging.ImageFormat.Jpeg);
                            Response.ContentType = "image/jpg";
                            break;
                        case "png":
                            i.Save(m, System.Drawing.Imaging.ImageFormat.Png);
                            Response.ContentType = "image/png";
                            break;
                        case "gif":
                            i.Save(m, System.Drawing.Imaging.ImageFormat.Gif);
                            Response.ContentType = "image/gif";
                            break;
                        case "tiff":
                            i.Save(m, System.Drawing.Imaging.ImageFormat.Tiff);
                            Response.ContentType = "image/tiff";
                            break;
                    }

                    Response.BinaryWrite(m.ToArray());
                    Response.Flush();

                }
            }




        }

        /// <summary>
        /// Metodo que verifica el formato de los audios
        /// </summary>
        /// <param name="aud">Nombre del audio</param>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 19/12/2017</br>
        /// <b>Descripcion:</b> Metodo que verifica el formato del audio, para poder escuchar el audio.
        /// </remarks>
        public FileResult Audio(string aud)
        {
            string route = ConfigurationManager.AppSettings["AudiosFolder"];
            string fileName = route + aud;
            string contentType = "application/pdf";

            var ext = aud.Split('.').LastOrDefault().ToLower();

            switch (ext)
            {
                case "wav":
                    contentType = "audio/x-wav";
                    break;
                case "mp3":
                    contentType = "audio/mp3";
                    break;
                case "aac":
                    contentType = "audio/aac";
                    break;
                case "wma":
                    contentType = "audio/x-ms-wma";
                    break;
            }
            return new FilePathResult(fileName, contentType);
        }

        /// <summary>
        /// Metodo que verifica el formato de las fotos
        /// </summary>
        /// <param name="img">nombre de la imagen</param>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 19/12/2017</br>
        /// <b>Descripcion:</b> Metodo que verifica el formato de la imagen, para realizar la visualizacion de la foto.
        /// </remarks>
        public void ImagenUsuarios(string img)
        {
            string route = ConfigurationManager.AppSettings["AvatarFolder"];
            MemoryStream m = new MemoryStream();
            Image i = Image.FromFile(route + img);

            var ext = img.Split('.').LastOrDefault().ToLower();

            Response.Clear();
            Response.ClearContent();
            Response.ClearHeaders();
            Response.Buffer = true;
            Response.BufferOutput = true;
            Response.ContentType = "application/pdf";

            switch (ext)
            {
                case "jpeg":
                    i.Save(m, System.Drawing.Imaging.ImageFormat.Jpeg);
                    Response.ContentType = "image/jpeg";
                    break;
                case "jpg":
                    i.Save(m, System.Drawing.Imaging.ImageFormat.Jpeg);
                    Response.ContentType = "image/jpeg";
                    break;
                case "png":
                    i.Save(m, System.Drawing.Imaging.ImageFormat.Png);
                    Response.ContentType = "image/png";
                    break;
                case "gif":
                    i.Save(m, System.Drawing.Imaging.ImageFormat.Gif);
                    Response.ContentType = "image/gif";
                    break;
                case "tiff":
                    i.Save(m, System.Drawing.Imaging.ImageFormat.Tiff);
                    Response.ContentType = "image/tiff";
                    break;
            }

            Response.BinaryWrite(m.ToArray());
            Response.Flush();
        }
    }
}
