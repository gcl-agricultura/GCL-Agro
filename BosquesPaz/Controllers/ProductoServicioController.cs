﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BosquesPaz.Models;
using BosquesPaz.Services;
using BosquesPaz.Helpers;
using BosquesPaz.ServiceReferenceFuncionarios;

namespace BosquesPaz.Controllers
{
    /// <summary>
    /// Clase que responde a las acciones que solicitan las peticiones de entrada y salida. 
    /// </summary>
    /// <remarks>
    /// <h3>Calidad</h3>
    /// </hr>
    /// <b>Requerimiento: Help Desk</b></br>
    /// <b>Version: 1</b></br>
    /// <b>Autor: Grupo Cubo Ltda.</b></br>
    /// <b>Fecha: 19/12/2017<b></br>
    /// <b>Descripcion:</b> Funciona para visualizar las vistas de productos/servicios, realizar peticiones a la capa de servicios y ejecutar acciones.
    /// </remarks>
    public class ProductoServicioController : Controller
    {
        /// <summary>
        /// Propiedad que instancia la clase CombosService
        /// </summary>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 19/12/2017</br>
        /// <b>Descripcion:</b> Propiedad que instancia el servicio CombosService, para enviar peticiones y recibir el resultado.
        /// </remarks>
        private CombosService service { get { return new CombosService(); } }

        /// <summary>
        /// Propiedad que instancia la clase ProductosServiciosService
        /// </summary>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 19/12/2017</br>
        /// <b>Descripcion:</b> Propiedad que instancia el servicio ProductosServiciosService, para enviar peticiones y recibir el resultado.
        /// </remarks>
        private ProductosServiciosService serviceProductoServico { get { return new ProductosServiciosService(); } }

        /// <summary>
        /// Accion que realiza la carga de la vista index de producto/servicio
        /// </summary>
        /// <param name="id">Identificador del bosque</param>
        /// <returns>Retorna vista parcial</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 19/12/2017</br>
        /// <b>Descripcion:</b> Accion que realiza el retorno de la vista parcial index de producto/servicio segun el numero de identificador que ingrese.
        /// </remarks>
        // GET: ProductoServicio
        public ActionResult Index(int id)
        {
            ViewBag.idBosque = id;
            return PartialView();
        }

        /// <summary>
        /// Accion que realiza la carga de la vista detalles de producto/servicio
        /// </summary>
        /// <param name="id">Identificador de la actividad</param>
        /// <returns>Retorna vista parcial</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 19/12/2017</br>
        /// <b>Descripcion:</b> Accion que realiza el retorno de la vista parcial detalle de producto/servicio segun el numero de identificador que ingrese.
        /// </remarks>
        // GET: Bosque/Details/5
        public ActionResult Details(int id)
        {
            var model = serviceProductoServico.ObtenerProductosServicios(id);
            return PartialView(model);
        }

        /// <summary>
        /// Accion que realiza la carga de la vista crear de producto/servicio
        /// </summary>
        /// <param name="id">Identificador del bosque</param>
        /// <returns>Retorna vista parcial</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 19/12/2017</br>
        /// <b>Descripcion:</b> Accion que realiza el retorno de la vista parcial crear de producto/servicio.
        /// </remarks>
        public ActionResult Create(int id)
        {
            ViewBag.TiposProductosServicios = service.ObtenerTiposProductosServicios();
            ViewBag.EstadosProductosServicios = service.ObtenerEstadosProductosServicios();
            var model = new ProductosServicios();
            model.Id_Bosque = id;
            return PartialView(model);
        }

        /// <summary>
        /// Accion que guarda los datos de la vista crear de producto/servicio
        /// </summary>
        /// <param name="viewModel">Instancia de ProductosServicios</param>
        /// <returns>Retorna vista parcial</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 19/12/2017</br>
        /// <b>Descripcion:</b> Recibe una instancia de ProductosServicios y la envia a la clase de servicio, y retorna redireccion de accion con el identificador de la instancia como parametro.
        /// </remarks>
        [HttpPost]
        public ActionResult Create(ProductosServicios viewModel, int id)
        {
            var info = Session["infoUsuario"] as EntidadInfoUsuario;

            string ipAddress = string.Empty;
            if (!string.IsNullOrEmpty(Request.ServerVariables[""]))
            {

                ipAddress = Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
            }
            else
            {
                ipAddress = Request.ServerVariables["REMOTE_ADDR"];
            }

            try
            {
                viewModel.Aud_User = info.Usuario;
                viewModel.Aud_Origen = ipAddress;
                if (ModelState.IsValid)
                {
                    viewModel = serviceProductoServico.GuardarProductoServicio(viewModel);
                    return RedirectToAction("Details", new { id = viewModel.Id_ProductoServicio });
                }
                ViewBag.TiposProductosServicios = service.ObtenerTiposProductosServicios();
                ViewBag.EstadosProductosServicios = service.ObtenerEstadosProductosServicios();
                return PartialView(viewModel);
            }
            catch
            {
                ViewBag.TiposIndicadores = service.ObtenerTiposIndicadores();
                ViewBag.EstadosIndicadores = service.ObtenerEstadosIndicadores();
                return PartialView(viewModel);
            }
        }

        /// <summary>
        /// Método que serializa la informacion en formato JSON
        /// </summary>
        /// <param name="id">Identificador del bosque</param>
        /// <param name="agrid">Parametors de la grilla</param>
        /// <returns>Retorna JsonResult</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 19/12/2017</br>
        /// <b>Descripcion:</b> Realiza la serializacion de los datos recibidos a formato JSON, para cargar la grilla de productos/servicios segun el numero de identificador que ingrese.
        /// </remarks>
        public JsonResult CargarGrilla(AnexGRID agrid, int id)
        {
            return Json(DataSourceGrilla(agrid, id), JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Método que retorna AnexGRIDResponde, segun sus parametros.
        /// </summary>
        /// <param name="id">Identificador del bosque</param>
        /// <param name="agrid">Parametors de la grilla</param>
        /// <returns>Retorna AnexGRIDResponde</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 19/12/2017</br>
        /// <b>Descripcion:</b> Método que permite obtener los datos de ProductosServicios, y organizarlos con el proposito de retornarlos a una grilla.
        /// </remarks>
        [NonAction]
        public AnexGRIDResponde DataSourceGrilla(AnexGRID agrid, int id)
        {
            try
            {
                IEnumerable<ProductosServicios> registros = new List<ProductosServicios>();
                IDictionary<string, string> filtros = new Dictionary<string, string>();
                if (agrid.filtros != null)
                    foreach (var filtro in agrid.filtros)
                    {
                        filtros.Add(filtro.columna, filtro.valor);
                    }

                registros = serviceProductoServico.ObtenerProductosServiciosBosques(agrid.pagina, agrid.limite, agrid.columna, filtros, agrid.columna_orden != "DESC", id);

                if (registros != null)
                {
                    agrid.Inicializar();
                    int cantidadRegistros = serviceProductoServico.CantidadRegistros;
                    agrid.SetData(registros, cantidadRegistros);
                }
                return agrid.responde();
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        /// <summary>
        /// Accion que realiza la carga de la vista editar de producto/servicio
        /// </summary>
        /// <param name="id">Identificador del producto/servicio</param>
        /// <returns>Retorna vista parcial</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 19/12/2017</br>
        /// <b>Descripcion:</b> Accion que realiza el retorno de la vista parcial editar de producto/servicio segun el numero del identificador que ingrese.
        /// </remarks>
        public ActionResult Edit(int id)
        {
            ViewBag.TiposProductosServicios = service.ObtenerTiposProductosServicios();
            ViewBag.EstadosProductosServicios = service.ObtenerEstadosProductosServicios();
            var model = serviceProductoServico.ObtenerProductosServicios(id);
            return PartialView(model);
        }

        /// <summary>
        /// Accion que actualiza los datos de la vista editar de producto/servicio
        /// </summary>
        /// <param name="viewModel">Instancia de ProductosServicios</param>
        /// <returns>Retorna vista parcial</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 19/12/2017</br>
        /// <b>Descripcion:</b> Recibe una instancia de ProductosServicios y la envia a la clase de servicio, y retorna redireccion de accion con el identificador de la instancia como parametro.
        /// </remarks>
        [HttpPost]
        public ActionResult Edit(ProductosServicios viewModel)
        {
            bool result = false;
            try
            {
                if (ModelState.IsValid)
                {
                    if (result = serviceProductoServico.ActualizarProductoServicio(viewModel))
                    {
                        return RedirectToAction("Details", new { id = viewModel.Id_ProductoServicio });
                    }
                }
                ViewBag.TiposProductosServicios = service.ObtenerTiposProductosServicios();
                ViewBag.EstadosProductosServicios = service.ObtenerEstadosProductosServicios();
                return PartialView(viewModel);
            }
            catch
            {
                ViewBag.TiposProductosServicios = service.ObtenerTiposProductosServicios();
                ViewBag.EstadosProductosServicios = service.ObtenerEstadosProductosServicios();
                return PartialView(viewModel);
            }
        }
    }
}