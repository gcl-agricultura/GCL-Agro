﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BosquesPaz.Helpers;
using BosquesPaz.Models;
using BosquesPaz.Services;
using BosquesPaz.ServiceReferenceFuncionarios;

namespace BosquesPaz.Controllers
{
    /// <summary>
    /// Clase que responde a las acciones que solicitan las peticiones de entrada y salida. 
    /// </summary>
    /// <remarks>
    /// <h3>Calidad</h3>
    /// </hr>
    /// <b>Requerimiento: Help Desk</b></br>
    /// <b>Version: 1</b></br>
    /// <b>Autor: Grupo Cubo Ltda.</b></br>
    /// <b>Fecha: 19/12/2017<b></br>
    /// <b>Descripcion:</b> Funciona para visualizar las vistas de fichas, realizar peticiones a la capa de servicios y ejecutar acciones.
    /// </remarks>
    public class FichaController : Controller
    {
        /// <summary>
        /// Propiedad que instancia la clase CombosService
        /// </summary>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 19/12/2017</br>
        /// <b>Descripcion:</b> Propiedad que instancia el servicio CombosService, para enviar peticiones y recibir el resultado.
        /// </remarks>
        private CombosService service { get { return new CombosService(); } }

        /// <summary>
        /// Propiedad que instancia la clase FichasService
        /// </summary>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 19/12/2017</br>
        /// <b>Descripcion:</b> Propiedad que instancia el servicio FichasService, para enviar peticiones y recibir el resultado.
        /// </remarks>
        private FichasService serviceFicha { get{ return new FichasService(); } }

        /// <summary>
        /// Accion que realiza la carga de la vista index de fichas
        /// </summary>
        /// <returns>Retorna vista</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 19/12/2017</br>
        /// <b>Descripcion:</b> Accion que realiza el retorno de la vista index de fichas.
        /// </remarks>
        // GET: Ficha
        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// Accion que realiza la carga de la vista detalles de fichas
        /// </summary>
        /// <param name="id">Identificador de la ficha</param>
        /// <returns>Retorna vista parcial</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 19/12/2017</br>
        /// <b>Descripcion:</b> Accion que realiza el retorno de la vista parcial detalle de fichas segun el numero de identificador que ingrese.
        /// </remarks>
        public ActionResult Details(int id)
        {
            var model = serviceFicha.ObtenerFichaBosque(id);
            return PartialView(model);
        }

        /// <summary>
        /// Accion que realiza la carga de la vista crear de fichas
        /// </summary>
        /// <param name="id">Identificador del bosque</param>
        /// <returns>Retorna vista parcial</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 19/12/2017</br>
        /// <b>Descripcion:</b> Accion que realiza el retorno de la vista parcial crear de fichas. si la ficha ha sido creada con anterioridad, se retorna redireccion de accion con el identificador del parametro de entrada como parametro
        /// </remarks>
        public ActionResult Create(int id)
        {
            try
            {
                var infraestrucutura = new Infraestructura();
                EntidadInfoUsuario info = null;
                if (Session["infoUsuario"] != null)
                {
                    info = Session["infoUsuario"] as EntidadInfoUsuario;
                }
                ViewBag.EstadosFichas = service.ObtenerEstadosFichas();
                var model = serviceFicha.ObtenerFichaBosque(id);
                if (model != null)
                {
                    return RedirectToAction("Details", "Ficha", new { id });
                }
                else
                {
                    if (info != null && infraestrucutura.PermisoUsuario(info, "Crear", "Ficha"))
                    {
                        model = new FichasBosques();
                        model.Id_FichaBosque = id;
                        return PartialView(model);
                    }
                    return PartialView("SinPermiso");
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            
        }

        /// <summary>
        /// Accion que guarda los datos de la vista crear de fichas
        /// </summary>
        /// <param name="viewModel">Instancia de FichasBosques</param>
        /// <param name="id">Identificador de la ficha</param>
        /// <returns>Retorna vista parcial</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 19/12/2017</br>
        /// <b>Descripcion:</b> Recibe una instancia de FichasBosques y la envia a la clase de servicio, y retorna redireccion de accion con el identificador de la instancia como parametro,
        /// si la ficha ha sido creada con anterioridad, se retorna redireccion de accion con el identificador del parametro de entrada como parametro
        /// </remarks>
        [HttpPost]
        public ActionResult Create(FichasBosques viewModel, int id)
        {
            var info = Session["infoUsuario"] as EntidadInfoUsuario;

            string ipAddress = string.Empty;
            if (!string.IsNullOrEmpty(Request.ServerVariables[""]))
            {

                ipAddress = Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
            }
            else
            {
                ipAddress = Request.ServerVariables["REMOTE_ADDR"];
            }
            var model = serviceFicha.ObtenerFichaBosque(id);
            try
            {
                viewModel.Aud_User = info.Usuario;
                viewModel.Aud_Origen = ipAddress;
                if (ModelState.IsValid)
                {
                    if(model != null)
                    {
                        return RedirectToAction("Details", "Ficha", new { viewModel, id });
                    }
                    else
                    {
                        viewModel = serviceFicha.GuardarFicha(viewModel);
                        return RedirectToAction("Details", "Bosque", new { id = viewModel.Id_FichaBosque });
                    }
                }
                ViewBag.EstadosFichas = service.ObtenerEstadosFichas();
                return PartialView(viewModel);
            }
            catch
            {
                ViewBag.EstadosFichas = service.ObtenerEstadosFichas();
                return PartialView(viewModel);
            }
        }

        /// <summary>
        /// Accion que realiza la carga de la vista editar de datos contactos
        /// </summary>
        /// <param name="id">Identificador de la ficha</param>
        /// <returns>Retorna vista parcial</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 19/12/2017</br>
        /// <b>Descripcion:</b> Accion que realiza el retorno de la vista parcial editar datos de contactos segun el numero del identificador que ingrese.
        /// </remarks>
        public ActionResult Edit(int id)
        {
            ViewBag.EstadosFichas = service.ObtenerEstadosFichas();
            var model = serviceFicha.ObtenerFichaBosque(id);
            return PartialView(model);
        }

        /// <summary>
        /// Accion que actualiza los datos de la vista editar de fichas
        /// </summary>
        /// <param name="viewModel">Instancia de FichasBosques</param>
        /// <returns>Retorna vista parcial</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 19/12/2017</br>
        /// <b>Descripcion:</b> Recibe una instancia de FichasBosques y la envia a la clase de servicio, y retorna redireccion de accion con el identificador de la instancia como parametro.
        /// </remarks>
        [HttpPost]
        public ActionResult Edit(FichasBosques viewModel, int id)
        {
            bool result = false;
            try
            {
                if (ModelState.IsValid)
                {
                    if (result = serviceFicha.ActualizarFicha(viewModel))
                    {
                        return RedirectToAction("Details", "Ficha", new { id });
                    }
                }
                ViewBag.EstadosFichas = service.ObtenerEstadosFichas();
                return PartialView(viewModel);
            }
            catch
            {
                ViewBag.EstadosFichas = service.ObtenerEstadosFichas();
                return PartialView(viewModel);
            }
        }
    }
}