﻿using BosquesPaz.Models;
using BosquesPaz.Services;
using BosquesPaz.Helpers;
using System.Collections.Generic;
using System.Web.Mvc;
using System;
using System.Web.Routing;
using BosquesPaz.ServiceReferenceFuncionarios;
using Newtonsoft.Json.Linq;
using System.Linq;

namespace BosquesPaz.Controllers
{
    /// <summary>
    /// Clase que responde a las acciones que solicitan las peticiones de entrada y salida. 
    /// </summary>
    /// <remarks>
    /// <h3>Calidad</h3>
    /// </hr>
    /// <b>Requerimiento: Help Desk</b></br>
    /// <b>Version: 1</b></br>
    /// <b>Autor: Grupo Cubo Ltda.</b></br>
    /// <b>Fecha: 19/12/2017<b></br>
    /// <b>Descripcion:</b> Funciona para visualizar las vistas de bosque, realizar peticiones a la capa de servicios y ejecutar acciones.
    /// </remarks>
    public class BosqueController : Controller
    {
        /// <summary>
        /// Propiedad que instancia la clase CombosService
        /// </summary>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 19/12/2017</br>
        /// <b>Descripcion:</b> Propiedad que instancia el servicio CombosService, para enviar peticiones y recibir el resultado.
        /// </remarks>
        private CombosService service { get { return new CombosService(); } }

        /// <summary>
        /// Propiedad que instancia la clase BosquesService
        /// </summary>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 19/12/2017</br>
        /// <b>Descripcion:</b> Propiedad que instancia el servicio BosquesService, para enviar peticiones y recibir el resultado.
        /// </remarks>
        private BosquesService serviceBosque { get { return new BosquesService(); } }
        private FichasService serviceFichasService { get { return new FichasService(); } }
        private ActividadService serviceActividadService { get { return new ActividadService(); } }
        private IndividuosService serviceIndividuosEspecies { get { return new IndividuosService(); } }
        private HistoriasService serviceHistoriasService { get { return new HistoriasService(); } }
        private ProductosServiciosService serviceProductosServiciosService { get { return new ProductosServiciosService(); } }

        /// <summary>
        /// Accion que realiza la carga de la vista index de bosques
        /// </summary>
        /// <returns>Retorna vista parcial</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 19/12/2017</br>
        /// <b>Descripcion:</b> Accion que realiza el retorno de la vista parcial index de bosques.
        /// </remarks>
        // GET: Bosque
        public ActionResult Index(int id)
        {
            Bosques Bosque = serviceBosque.ObtenerBosque(id);

            FichasBosques FichaBosque = serviceFichasService.ObtenerFichaBosque(id);
            List<ActividadesBosques> Actividad = serviceActividadService.ObtenerActividadesBosque(id);
            List<IndividuosEspecies> IndividuosEspeciesBosque = serviceIndividuosEspecies.ObtenerIndividuosEspeciesByBosque(id);
            List<HistoriasBosques> HistoriasBosque = serviceHistoriasService.ObtenerHistoriasBosque(id);
            List<ProductosServicios> ProductosServiciosBosque = serviceProductosServiciosService.ObtenerProductosServiciosBosque(id);

            ViewBag.Id_Bosque = Bosque.Id_Bosque;
            ViewBag.NombreBosque = Bosque.NombreBosque;
            ViewBag.Departamento = Bosque.Departamento;
            ViewBag.Municipio = Bosque.Municipio;
            ViewBag.Comunidad = Bosque.Comunidades;
            ViewBag.Latitud = Bosque.Latitud;
            ViewBag.Longitud = Bosque.Longitud;

            if (FichaBosque != null) { 
            ViewBag.NumeroArea = FichaBosque.NumeroArea;
            ViewBag.NumeroHabitantes = FichaBosque.NumeroHabitante;
            ViewBag.Territorio = FichaBosque.CaracteristicaTerritorio;
            ViewBag.Sociales = FichaBosque.CaracteristicaSocial;
            ViewBag.Demograficas = FichaBosque.CaracteristicaDemografica;
            ViewBag.MedioAmbientales = FichaBosque.CaracteristicaMedioAmbiental;
            ViewBag.DescripcionUbicacion = FichaBosque.DescripcionUbicacion;
            }

            if (Actividad != null)
            {
                ViewBag.ActividadesService = Actividad;
                ViewBag.ActividadesServiceSize = Actividad.Count();
            }

            if (IndividuosEspeciesBosque != null)
            {
                ViewBag.IndividuosEspeciesBosque = IndividuosEspeciesBosque;
                ViewBag.IndividuosEspeciesBosqueSize = IndividuosEspeciesBosque.Count();

            }

            if (HistoriasBosque != null)
            {
                ViewBag.HistoriasBosque = HistoriasBosque;
                ViewBag.HistoriasBosqueSize = HistoriasBosque.Count();

            }

            if (ProductosServiciosBosque != null)
            {
                ViewBag.ProductosServiciosBosque = ProductosServiciosBosque;
                ViewBag.ProductosServiciosBosqueSize = ProductosServiciosBosque.Count();

            }



            return PartialView();
        }

        /// <summary>
        /// Accion que realiza la carga de la vista detalles de bosques
        /// </summary>
        /// <param name="id">Identificador del bosque</param>
        /// <returns>Retorna vista parcial</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 19/12/2017</br>
        /// <b>Descripcion:</b> Accion que realiza el retorno de la vista parcial detalle de actividades segun el numero de identificador que ingrese.
        /// </remarks>
        // GET: Bosque/Details/5
        public ActionResult Details(int id)
        {
            ViewBag.BosqueModulo = Session["BosqueModulo"];
            ViewBag.BosqueModuloId = Convert.ToInt16(Session["BosqueModuloId"]);
            Session["BosqueModulo"] = null;
            Session["BosqueModuloId"] = null;
            var model = serviceBosque.ObtenerBosque(id);
            return PartialView(model);
        }

        /// <summary>
        /// Accion que realiza la carga de la vista crear de bosques
        /// </summary>
        /// <returns>Retorna vista parcial</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 19/12/2017</br>
        /// <b>Descripcion:</b> Accion que realiza el retorno de la vista parcial crear de bosques.
        /// </remarks>
        // GET: Bosque/Create
        public ActionResult Create()
        {
            ViewBag.Departamentos = service.ObtenerDepartamentos();
            ViewBag.Municipios = new List<Municipios>();
            var model = new Bosques();
            return PartialView(model);
        }

        /// <summary>
        /// Accion que guarda los datos de la vista crear de bosques
        /// </summary>
        /// <param name="viewModel">Instancia de Bosques</param>
        /// <returns>Retorna vista parcial</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 19/12/2017</br>
        /// <b>Descripcion:</b> Recibe una instancia de Bosques y la envia a la clase de servicio, y retorna redireccion de accion con el identificador de la instancia como parametro.
        /// </remarks>
        // POST: Bosque/Create
        [HttpPost]
        public ActionResult Create(Bosques viewModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    viewModel = serviceBosque.GuardarBosque(viewModel);
                    return RedirectToAction("Details", new { id = viewModel.Id_Bosque });
                }
                ViewBag.Departamentos = service.ObtenerDepartamentos();
                ViewBag.Municipios = new List<Municipios>();
                return PartialView(viewModel);
            }
            catch
            {
                ViewBag.Departamentos = service.ObtenerDepartamentos();
                ViewBag.Municipios = new List<Municipios>();
                return PartialView(viewModel);
            }
        }

        /// <summary>
        /// Accion que realiza la carga de la vista editar de actividad
        /// </summary>
        /// <param name="id">Identificador del bosque</param>
        /// <returns>Retorna vista parcial</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 19/12/2017</br>
        /// <b>Descripcion:</b> Accion que realiza el retorno de la vista parcial editar de bosques segun el numero del identificador que ingrese.
        /// </remarks>
        // GET: Bosque/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        /// <summary>
        /// Accion que actualiza los datos de la vista editar de bosques
        /// </summary>
        /// <param name="viewModel">Instancia de Bosques</param>
        /// <returns>Retorna vista parcial</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 19/12/2017</br>
        /// <b>Descripcion:</b> Recibe una instancia de Bosques y la envia a la clase de servicio, y retorna redireccion de accion con el identificador de la instancia como parametro.
        /// </remarks>
        // POST: Bosque/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        /// <summary>
        /// Método que serializa la informacion en formato JSON
        /// </summary>
        /// <param name="agrid">Parametors de la grilla</param>
        /// <returns>Retorna JsonResult</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 19/12/2017</br>
        /// <b>Descripcion:</b> Realiza la serializacion de los datos recibidos a formato JSON, para cargar la grilla de bosques.
        /// </remarks>
        public JsonResult CargarGrilla(AnexGRID agrid)
        {
                   
            return Json(DataSourceGrilla(agrid), JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult obtenerBosques()
        {
            var serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            IEnumerable<Bosques> listaBosques = serviceBosque.ObtenerBosques();
            string outputOfInts = serializer.Serialize(listaBosques);
            return Json(outputOfInts, JsonRequestBehavior.AllowGet);
            
        }


        /// <summary>
        /// Método que retorna AnexGRIDResponde, segun sus parametros.
        /// </summary>
        /// <param name="agrid">Parametors de la grilla</param>
        /// <returns>Retorna AnexGRIDResponde</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 19/12/2017</br>
        /// <b>Descripcion:</b> Método que permite obtener los datos de ActividadesBosques, y organizarlos con el proposito de retornarlos a una grilla.
        /// </remarks>
        [NonAction]
        public AnexGRIDResponde DataSourceGrilla(AnexGRID agrid)
        {
            try
            {
                IEnumerable<Bosques> registros = new List<Bosques>();
                IDictionary<string, string> filtros = new Dictionary<string, string>();
                if (agrid.filtros != null)
                    foreach (var filtro in agrid.filtros)
                    {
                        filtros.Add(filtro.columna, filtro.valor);
                    }

                registros = serviceBosque.ObtenerBosques(agrid.pagina, agrid.limite, agrid.columna, filtros, agrid.columna_orden != "DESC");

                if (registros != null)
                {
                    agrid.Inicializar();
                    int cantidadRegistros = serviceBosque.CantidadRegistros;
                    agrid.SetData(registros, cantidadRegistros);
                }
                return agrid.responde();
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }
    }
}
