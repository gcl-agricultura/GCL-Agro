﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BosquesPaz.Models;
using BosquesPaz.Services;
using BosquesPaz.Helpers;
using System.IO;
using BosquesPaz.ServiceReferenceFuncionarios;

namespace BosquesPaz.Controllers
{
    /// <summary>
    /// Clase que responde a las acciones que solicitan las peticiones de entrada y salida. 
    /// </summary>
    /// <remarks>
    /// <h3>Calidad</h3>
    /// </hr>
    /// <b>Requerimiento: Help Desk</b></br>
    /// <b>Version: 1</b></br>
    /// <b>Autor: Grupo Cubo Ltda.</b></br>
    /// <b>Fecha: 19/12/2017<b></br>
    /// <b>Descripcion:</b> Funciona para visualizar las vistas de historias, realizar peticiones a la capa de servicios y ejecutar acciones.
    /// </remarks>
    public class HistoriaController : Controller
    {
        /// <summary>
        /// Propiedad que instancia la clase CombosService
        /// </summary>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 19/12/2017</br>
        /// <b>Descripcion:</b> Propiedad que instancia el servicio CombosService, para enviar peticiones y recibir el resultado.
        /// </remarks>
        private CombosService service { get { return new CombosService(); } }

        /// <summary>
        /// Propiedad que instancia la clase HistoriasService
        /// </summary>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 19/12/2017</br>
        /// <b>Descripcion:</b> Propiedad que instancia el servicio HistoriasService, para enviar peticiones y recibir el resultado.
        /// </remarks>
        private HistoriasService serviceHistoria { get { return new HistoriasService(); } }

        /// <summary>
        /// Accion que realiza la carga de la vista index de hsitorias
        /// </summary>
        /// <param name="id">Identificador del bosque</param>
        /// <returns>Retorna vista parcial</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 19/12/2017</br>
        /// <b>Descripcion:</b> Accion que realiza el retorno de la vista parcial index de historias segun el numero de identificador que ingrese.
        /// </remarks>
        // GET: Historia
        public ActionResult Index() //(int id) se comenta para cargar vista de historia en EcoTech
        {
            /* var infraestructura = new Infraestructura();
             EntidadInfoUsuario info = null;
             if (Session["infoUsuario"] != null)
             {
                 info = Session["infoUsuario"] as EntidadInfoUsuario;
             }
             if (infraestructura.PermisoUsuario(info, "Editar", "Historias"))
             {
                 ViewBag.idBosque = id;
                 return PartialView();
             }
             else
             {
                 return RedirectToAction("IndexUsuarios", new { id = id });
             }   */

            ViewBag.allHistorias = serviceHistoria.ObtenerInversoHistorias();
          
            return PartialView();
        }

        public ActionResult HistoriasTematica(int IdTematica) 
        {
            IEnumerable<HistoriasBosques> allHistorias = serviceHistoria.ObtenerInversoHistorias();
            List<HistoriasBosques> historiasTematica = new List<HistoriasBosques>();


            foreach (var item in allHistorias)
            {
                if (item.Id_Tematica == IdTematica)
                {
                    historiasTematica.Add(item);
                }
            }

            string tematicaTitulo = null;
            if(IdTematica == 1)
            {
                tematicaTitulo = "Recuperación ecosistema";
            }else if (IdTematica == 2)
            {
                tematicaTitulo = "Historias de Vida";
            }
            else if (IdTematica == 3)
            {
                tematicaTitulo = "Turismo y Aventura";
            }


            ViewBag.tematicaTitulo = tematicaTitulo;
            ViewBag.allHistorias = historiasTematica;
            return PartialView("index");
        }

        /// <summary>
        /// Accion que realiza la carga de la vista index de hsitorias para los usuarios
        /// </summary>
        /// <param name="id">Identificador del bosque</param>
        /// <returns>Retorna vista parcial</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 19/12/2017</br>
        /// <b>Descripcion:</b> Accion que realiza el retorno de la vista parcial index para los usuarios de historias segun el numero de identificador que ingrese.
        /// </remarks>
        public ActionResult IndexUsuarios(int id)
        {
            ViewBag.idBosque = id;
            return PartialView();
        }

        /// <summary>
        /// Accion que realiza la carga de la vista detalles de Historias
        /// </summary>
        /// <param name="id">Identificador de la historia</param>
        /// <returns>Retorna vista parcial</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 19/12/2017</br>
        /// <b>Descripcion:</b> Accion que realiza el retorno de la vista parcial detalle de historia segun el numero de identificador que ingrese.
        /// </remarks>
        public ActionResult Details(int id)
        {
            ViewBag.model = serviceHistoria.ObtenerHistoriaBosque(id);
            return PartialView();
        }

        /// <summary>
        /// Accion que realiza la carga de la vista crear de historias
        /// </summary>
        /// <param name="id">Identificador del bosque</param>
        /// <returns>Retorna vista parcial</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 19/12/2017</br>
        /// <b>Descripcion:</b> Accion que realiza el retorno de la vista parcial crear de historias.
        /// </remarks>
        public ActionResult Create(int id)
        {
            ViewBag.Tematicas = service.ObtenerTematicas();
            ViewBag.EstadoHistorias = service.ObtenerEstadosHistorias();
            var model = new HistoriasBosques();
            model.Id_Bosque = id;
            return PartialView(model);
        }

        /// <summary>
        /// Accion que guarda los datos de la vista crear de historias
        /// </summary>
        /// <param name="viewModel">Instancia de HistoriasBosques</param>
        /// <returns>Retorna vista parcial</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 19/12/2017</br>
        /// <b>Descripcion:</b> Recibe una instancia de HistoriasBosques y la envia a la clase de servicio, y retorna redireccion de accion con el identificador de la instancia como parametro.
        /// </remarks>
        [HttpPost]
        public ActionResult Create(HistoriasBosques viewModel, HttpPostedFileBase UrlAudio)
        {
            var info = Session["infoUsuario"] as EntidadInfoUsuario;

            string ipAddress = string.Empty;
            if (!string.IsNullOrEmpty(Request.ServerVariables[""]))
            {

                ipAddress = Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
            }
            else
            {
                ipAddress = Request.ServerVariables["REMOTE_ADDR"];
            }

            try
            {
                viewModel.Aud_User = info.Usuario;
                viewModel.Aud_Origen = ipAddress;
                if (ModelState.IsValid)
                {
                    viewModel = serviceHistoria.GuardarHistoria(viewModel);
                    return RedirectToAction("Details", new { id = viewModel.Id_HistoriaBosque });
                }
                ViewBag.EstadoHistorias = service.ObtenerEstadosHistorias();
                ViewBag.Tematicas = service.ObtenerTematicas();
                return PartialView(viewModel);
            }
            catch 
            {
                ViewBag.EstadoHistorias = service.ObtenerEstadosHistorias();
                ViewBag.Tematicas = service.ObtenerTematicas();
                return PartialView(viewModel);
            }
        }

        /// <summary>
        /// Método que serializa la informacion en formato JSON
        /// </summary>
        /// <param name="id">Identificador del bosque</param>
        /// <param name="agrid">Parametors de la grilla</param>
        /// <returns>Retorna JsonResult</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 19/12/2017</br>
        /// <b>Descripcion:</b> Realiza la serializacion de los datos recibidos a formato JSON, para cargar la grilla de historias segun el numero de identificador que ingrese.
        /// </remarks>
        public JsonResult CargarGrilla(AnexGRID agrid, int id)
        {
            return Json(DataSourceGrilla(agrid, id), JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Método que retorna AnexGRIDResponde, segun sus parametros.
        /// </summary>
        /// <param name="id">Identificador del bosque</param>
        /// <param name="agrid">Parametors de la grilla</param>
        /// <returns>Retorna AnexGRIDResponde</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 19/12/2017</br>
        /// <b>Descripcion:</b> Método que permite obtener los datos de HistoriasBosques, y organizarlos con el proposito de retornarlos a una grilla.
        /// </remarks>
        [NonAction]
        public AnexGRIDResponde DataSourceGrilla(AnexGRID agrid, int id)
        {
            try
            {
                IEnumerable<HistoriasBosques> registros = new List<HistoriasBosques>();
                IDictionary<string, string> filtros = new Dictionary<string, string>();
                if (agrid.filtros != null)
                    foreach (var filtro in agrid.filtros)
                    {
                        filtros.Add(filtro.columna, filtro.valor);
                    }

                registros = serviceHistoria.ObtenerHistoriasBosques(agrid.pagina, agrid.limite, agrid.columna, filtros, agrid.columna_orden != "DESC", id);

                if (registros != null)
                {
                    agrid.Inicializar();
                    int cantidadRegistros = serviceHistoria.CantidadRegistros;
                    agrid.SetData(registros, cantidadRegistros);
                }
                return agrid.responde();
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        /// <summary>
        /// Accion que realiza la carga de la vista editar de historias
        /// </summary>
        /// <param name="id">Identificador de la historia</param>
        /// <returns>Retorna vista parcial</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 19/12/2017</br>
        /// <b>Descripcion:</b> Accion que realiza el retorno de la vista parcial editar de historias segun el numero del identificador que ingrese.
        /// </remarks>
        public ActionResult Edit(int id)
        {
            ViewBag.EstadoHistorias = service.ObtenerEstadosHistorias();
            ViewBag.Tematicas = service.ObtenerTematicas();
            var model = serviceHistoria.ObtenerHistoriaBosque(id);
            return PartialView(model);
        }

        /// <summary>
        /// Accion que actualiza los datos de la vista editar de historias
        /// </summary>
        /// <param name="viewModel">Instancia de HistoriasBosques</param>
        /// <returns>Retorna vista parcial</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 19/12/2017</br>
        /// <b>Descripcion:</b> Recibe una instancia de HistoriasBosques y la envia a la clase de servicio, y retorna redireccion de accion con el identificador de la instancia como parametro.
        /// </remarks>
        [HttpPost]
        public ActionResult Edit(HistoriasBosques viewModel)
        {
            bool result = false;
            try
            {
                if (ModelState.IsValid)
                {
                    if (result = serviceHistoria.ActualizarHistoria(viewModel))
                    {
                        return RedirectToAction("Details", new { id = viewModel.Id_HistoriaBosque });
                    }
                }
                ViewBag.EstadoHistorias = service.ObtenerEstadosHistorias();
                ViewBag.Tematicas = service.ObtenerTematicas();
                return PartialView(viewModel);
            }
            catch
            {
                ViewBag.EstadoHistorias = service.ObtenerEstadosHistorias();
                ViewBag.Tematicas = service.ObtenerTematicas();
                return PartialView(viewModel);
            }
        }

        /// <summary>
        /// Método que serializa la informacion en formato JSON
        /// </summary>
        /// <param name="id">Identificador del bosque</param>
        /// <param name="agrid">Parametors de la grilla</param>
        /// <returns>Retorna JsonResult</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 19/12/2017</br>
        /// <b>Descripcion:</b> Realiza la serializacion de los datos recibidos a formato JSON, para cargar la grilla de historias para los usuarios segun el numero de identificador que ingrese.
        /// </remarks>
        public JsonResult CargarGrillaUsuarios(AnexGRID agrid, int id)
        {
            return Json(DataSourceGrillaUsuarios(agrid, id), JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Método que retorna AnexGRIDResponde, segun sus parametros.
        /// </summary>
        /// <param name="id">Identificador del bosque</param>
        /// <param name="agrid">Parametors de la grilla</param>
        /// <returns>Retorna AnexGRIDResponde</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 19/12/2017</br>
        /// <b>Descripcion:</b> Método que permite obtener los datos de HistoriasBosques para los usuarios, y organizarlos con el proposito de retornarlos a una grilla.
        /// </remarks>
        [NonAction]
        public AnexGRIDResponde DataSourceGrillaUsuarios(AnexGRID agrid, int id)
        {
            try
            {
                IEnumerable<HistoriasBosques> registros = new List<HistoriasBosques>();
                IDictionary<string, string> filtros = new Dictionary<string, string>();
                if (agrid.filtros != null)
                    foreach (var filtro in agrid.filtros)
                    {
                        filtros.Add(filtro.columna, filtro.valor);
                    }

                registros = serviceHistoria.ObtenerHistoriasBosquesUsuarios(agrid.pagina, agrid.limite, agrid.columna, filtros, agrid.columna_orden != "DESC", id);

                if (registros != null)
                {
                    agrid.Inicializar();
                    int cantidadRegistros = serviceHistoria.CantidadRegistros;
                    agrid.SetData(registros, cantidadRegistros);
                }
                return agrid.responde();
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }
    }
}