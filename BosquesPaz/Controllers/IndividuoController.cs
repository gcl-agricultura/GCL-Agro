﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BosquesPaz.Models;
using BosquesPaz.Services;
using BosquesPaz.Helpers;
using PagedList.Mvc;
using System.Web.Routing;
using BosquesPaz.ServiceReferenceFuncionarios;

namespace BosquesPaz.Controllers
{
    /// <summary>
    /// Clase que responde a las acciones que solicitan las peticiones de entrada y salida. 
    /// </summary>
    /// <remarks>
    /// <h3>Calidad</h3>
    /// </hr>
    /// <b>Requerimiento: Help Desk</b></br>
    /// <b>Version: 1</b></br>
    /// <b>Autor: Grupo Cubo Ltda.</b></br>
    /// <b>Fecha: 19/12/2017<b></br>
    /// <b>Descripcion:</b> Funciona para visualizar las vistas de individuos/especies, realizar peticiones a la capa de servicios y ejecutar acciones.
    /// </remarks>
    public class IndividuoController : Controller
    {
        /// <summary>
        /// Propiedad que instancia la clase CombosService
        /// </summary>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 19/12/2017</br>
        /// <b>Descripcion:</b> Propiedad que instancia el servicio CombosService, para enviar peticiones y recibir el resultado.
        /// </remarks>
        private CombosService service { get { return new CombosService(); } }

        /// <summary>
        /// Propiedad que instancia la clase IndividuosService
        /// </summary>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 19/12/2017</br>
        /// <b>Descripcion:</b> Propiedad que instancia el servicio IndividuosService, para enviar peticiones y recibir el resultado.
        /// </remarks>
        private IndividuosService serviceIndividuo { get { return new IndividuosService(); } }

        /// <summary>
        /// Accion que realiza la carga de la vista index de individuos/especies
        /// </summary>
        /// <param name="id">Identificador del bosque</param>
        /// <returns>Retorna vista parcial</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 19/12/2017</br>
        /// <b>Descripcion:</b> Accion que realiza el retorno de la vista parcial index de individuos/especies segun el numero de identificador que ingrese.
        /// </remarks>
        // GET: Individuo
        public ActionResult Index(int id)
        {
            ViewBag.idBosque = id;
            return PartialView();
        }

        /// <summary>
        /// Accion que realiza la carga de la vista detalles de individuo/especie
        /// </summary>
        /// <param name="id">Identificador del individuo/especie</param>
        /// <returns>Retorna vista parcial</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 19/12/2017</br>
        /// <b>Descripcion:</b> Accion que realiza el retorno de la vista parcial detalle de individuos/especies segun el numero de identificador que ingrese.
        /// </remarks>
        // GET: Bosque/Details/5
        public ActionResult Details(int id)
        {
            var model = serviceIndividuo.ObtenerIndividuosEspecies(id);
            return PartialView(model);
        }

        /// <summary>
        /// Accion que realiza la carga de la vista crear de individuo/especie
        /// </summary>
        /// <param name="id">Identificador del bosque</param>
        /// <returns>Retorna vista parcial</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 19/12/2017</br>
        /// <b>Descripcion:</b> Accion que realiza el retorno de la vista parcial crear de individuos/especies.
        /// </remarks>
        public ActionResult Create(int id)
        {
            ViewBag.TiposIndividuosEspecies = service.ObtenerTiposIndividuosEspecies();
            ViewBag.UnidadesIndividuosEspecies = service.ObtenerUnidadesIndividuosEspecies();
            var model = new IndividuosEspecies();
            model.Id_Bosque = id;
            return PartialView(model);
        }

        /// <summary>
        /// Accion que guarda los datos de la vista crear de individuo/especie
        /// </summary>
        /// <param name="viewModel">Instancia de IndividuosEspecies</param>
        /// <returns>Retorna vista parcial</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 19/12/2017</br>
        /// <b>Descripcion:</b> Recibe una instancia de IndividuosEspecies y la envia a la clase de servicio, y retorna redireccion de accion con el identificador de la instancia como parametro.
        /// </remarks>
        [HttpPost]
        public ActionResult Create(IndividuosEspecies viewModel)
        {
            var info = Session["infoUsuario"] as EntidadInfoUsuario;

            string ipAddress = string.Empty;
            if (!string.IsNullOrEmpty(Request.ServerVariables[""]))
            {

                ipAddress = Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
            }
            else
            {
                ipAddress = Request.ServerVariables["REMOTE_ADDR"];
            }

            try
            {
                viewModel.Aud_User = info.Usuario;
                viewModel.Aud_Origen = ipAddress;
                if (ModelState.IsValid)
                {
                    viewModel = serviceIndividuo.GuardarIndividuo(viewModel);
                    return RedirectToAction("Details", new { id = viewModel.Id_IndividuoEspecie });
                }
                ViewBag.TiposIndividuosEspecies = service.ObtenerTiposIndividuosEspecies();
                ViewBag.UnidadesIndividuosEspecies = service.ObtenerUnidadesIndividuosEspecies();
                return PartialView(viewModel);
            }
            catch
            {
                ViewBag.TiposIndividuosEspecies = service.ObtenerTiposIndividuosEspecies();
                ViewBag.UnidadesIndividuosEspecies = service.ObtenerUnidadesIndividuosEspecies();
                return PartialView(viewModel);
            }
        }

        /// <summary>
        /// Método que serializa la informacion en formato JSON
        /// </summary>
        /// <param name="id">Identificador del bosque</param>
        /// <param name="agrid">Parametors de la grilla</param>
        /// <returns>Retorna JsonResult</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 19/12/2017</br>
        /// <b>Descripcion:</b> Realiza la serializacion de los datos recibidos a formato JSON, para cargar la grilla de individuos/especies segun el numero de identificador que ingrese.
        /// </remarks>
        public JsonResult CargarGrilla(AnexGRID agrid, int id)
        {
            return Json(DataSourceGrilla(agrid, id), JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Método que retorna AnexGRIDResponde, segun sus parametros.
        /// </summary>
        /// <param name="id">Identificador del bosque</param>
        /// <param name="agrid">Parametors de la grilla</param>
        /// <returns>Retorna AnexGRIDResponde</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 19/12/2017</br>
        /// <b>Descripcion:</b> Método que permite obtener los datos de IndividuosEspecies, y organizarlos con el proposito de retornarlos a una grilla.
        /// </remarks>
        [NonAction]
        public AnexGRIDResponde DataSourceGrilla(AnexGRID agrid, int id)
        {
            try
            {
                IEnumerable<IndividuosEspecies> registros = new List<IndividuosEspecies>();
                IDictionary<string, string> filtros = new Dictionary<string, string>();
                if (agrid.filtros != null)
                    foreach (var filtro in agrid.filtros)
                    {
                        filtros.Add(filtro.columna, filtro.valor);
                    }

                registros = serviceIndividuo.ObtenerIndividuosEspeciesBosques(agrid.pagina, agrid.limite, agrid.columna, filtros, agrid.columna_orden != "DESC", id);

                if (registros != null)
                {
                    agrid.Inicializar();
                    int cantidadRegistros = serviceIndividuo.CantidadRegistros;
                    agrid.SetData(registros, cantidadRegistros);
                }
                return agrid.responde();
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        /// <summary>
        /// Accion que realiza la carga de la vista editar de individuo/especie
        /// </summary>
        /// <param name="id">Identificador del individuo/especie</param>
        /// <returns>Retorna vista parcial</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 19/12/2017</br>
        /// <b>Descripcion:</b> Accion que realiza el retorno de la vista parcial editar de individuos/especies segun el numero del identificador que ingrese.
        /// </remarks>
        public ActionResult Edit(int id)
        {
            ViewBag.TiposIndividuosEspecies = service.ObtenerTiposIndividuosEspecies();
            ViewBag.UnidadesIndividuosEspecies = service.ObtenerUnidadesIndividuosEspecies();
            var model = serviceIndividuo.ObtenerIndividuosEspecies(id);
            return PartialView(model);
        }

        /// <summary>
        /// Accion que actualiza los datos de la vista editar de individuo/especie
        /// </summary>
        /// <param name="viewModel">Instancia de IndividuosEspecies</param>
        /// <returns>Retorna vista parcial</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 19/12/2017</br>
        /// <b>Descripcion:</b> Recibe una instancia de IndividuosEspecies y la envia a la clase de servicio, y retorna redireccion de accion con el identificador de la instancia como parametro.
        /// </remarks>
        [HttpPost]
        public ActionResult Edit(IndividuosEspecies viewModel)
        {
            bool result = false;
            try
            {
                if (ModelState.IsValid)
                {
                    if (result = serviceIndividuo.ActualizarIndividuo(viewModel))
                    {
                        return RedirectToAction("Details", new { id = viewModel.Id_IndividuoEspecie });
                    }
                }
                ViewBag.TiposIndividuosEspecies = service.ObtenerTiposIndividuosEspecies();
                ViewBag.UnidadesIndividuosEspecies = service.ObtenerUnidadesIndividuosEspecies();
                return PartialView(viewModel);
            }
            catch
            {
                ViewBag.TiposIndividuosEspecies = service.ObtenerTiposIndividuosEspecies();
                ViewBag.UnidadesIndividuosEspecies = service.ObtenerUnidadesIndividuosEspecies();
                return PartialView(viewModel);
            }
        }


    }
}