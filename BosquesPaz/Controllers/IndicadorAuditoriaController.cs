﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BosquesPaz.Models;
using BosquesPaz.Services;
using BosquesPaz.Helpers;

namespace BosquesPaz.Controllers
{
    /// <summary>
    /// Clase que responde a las acciones que solicitan las peticiones de entrada y salida. 
    /// </summary>
    /// <remarks>
    /// <h3>Calidad</h3>
    /// </hr>
    /// <b>Requerimiento: Help Desk</b></br>
    /// <b>Version: 1</b></br>
    /// <b>Autor: Grupo Cubo Ltda.</b></br>
    /// <b>Fecha: 19/12/2017<b></br>
    /// <b>Descripcion:</b> Funciona para visualizar la vista de auditorias indicadores, realizar peticiones a la capa de servicios y ejecutar acciones.
    /// </remarks>
    public class IndicadorAuditoriaController : Controller
    {
        /// <summary>
        /// Propiedad que instancia la clase IndicadoresAuditoriasService
        /// </summary>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 19/12/2017</br>
        /// <b>Descripcion:</b> Propiedad que instancia el servicio IndicadoresAuditoriasService, para enviar peticiones y recibir el resultado.
        /// </remarks>
        private IndicadoresAuditoriasService serviceIndicadorAuditoria { get { return new IndicadoresAuditoriasService(); } }

        /// <summary>
        /// Accion que realiza la carga de la vista index de auditoria actividad
        /// </summary>
        /// <param name="id">Identificador del indicador</param>
        /// <returns>Retorna vista parcial</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 19/12/2017</br>
        /// <b>Descripcion:</b> Accion que realiza el retorno de la vista parcial index de auditorias indicadores segun el numero de identificador que ingrese.
        /// </remarks>
        // GET: IndicadorAuditoria
        public ActionResult Index(int id)
        {
            ViewBag.idIndicadorBosque = id;
            return PartialView();
        }
        // GET: IndicadorAuditoria
        public ActionResult IndexPrincipal()
        {
            return PartialView();
        }

        /// <summary>
        /// Método que serializa la informacion en formato JSON
        /// </summary>
        /// <param name="id">Identificador del indicador</param>
        /// <param name="agrid">Parametors de la grilla</param>
        /// <returns>Retorna JsonResult</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 19/12/2017</br>
        /// <b>Descripcion:</b> Realiza la serializacion de los datos recibidos a formato JSON, para cargar la grilla de auditorias indicadores segun el numero de identificador que ingrese.
        /// </remarks>
        public JsonResult CargarGrillaHistorial(AnexGRID agrid, int id)
        {
            return Json(DataSourceGrillaHistorial(agrid, id, true), JsonRequestBehavior.AllowGet);
        }
        public JsonResult CargarGrillaHistorialPrincipal(AnexGRID agrid)
        { 
            return Json(DataSourceGrillaHistorial(agrid, 0, false), JsonRequestBehavior.AllowGet);
        }

        [NonAction]
        public AnexGRIDResponde DataSourceGrillaHistorial(AnexGRID agrid, int id, bool isId)
        {
            try
            {
                IEnumerable<AudiIndicadoresBosques> registros = new List<AudiIndicadoresBosques>();
                IDictionary<string, string> filtros = new Dictionary<string, string>();
                if (agrid.filtros != null)
                    foreach (var filtro in agrid.filtros)
                    {
                        filtros.Add(filtro.columna, filtro.valor);
                    }

                registros = serviceIndicadorAuditoria.ObtenerIndicadoresAuditoriasBosques(agrid.pagina, agrid.limite, agrid.columna, filtros, agrid.columna_orden != "DESC", id, isId);

                if (registros != null)
                {
                    agrid.Inicializar();
                    int cantidadRegistros = serviceIndicadorAuditoria.CantidadRegistros;
                    agrid.SetData(registros, cantidadRegistros);
                }
                return agrid.responde();
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }
    }
}