﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BosquesPaz.Models;
using BosquesPaz.Services;
using System.Web.Mvc;
using BosquesPaz.Helpers;
using BosquesPaz.ServiceReferenceFuncionarios;

namespace BosquesPaz.Controllers
{
    /// <summary>
    /// Clase que responde a las acciones que solicitan las peticiones de entrada y salida. 
    /// </summary>
    /// <remarks>
    /// <h3>Calidad</h3>
    /// </hr>
    /// <b>Requerimiento: Help Desk</b></br>
    /// <b>Version: 1</b></br>
    /// <b>Autor: Grupo Cubo Ltda.</b></br>
    /// <b>Fecha: 19/12/2017<b></br>
    /// <b>Descripcion:</b> Funciona para visualizar las vistas de actividades, realizar peticiones a la capa de servicios y ejecutar acciones.
    /// </remarks>
    public class ActividadController : Controller
    {
        /// <summary>
        /// Propiedad que instancia la clase CombosService
        /// </summary>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 19/12/2017</br>
        /// <b>Descripcion:</b> Propiedad que instancia el servicio CombosService, para enviar peticiones y recibir el resultado.
        /// </remarks>
        private CombosService service { get { return new CombosService(); } }

        /// <summary>
        /// Propiedad que instancia la clase ActividadService
        /// </summary>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 19/12/2017</br>
        /// <b>Descripcion:</b> Propiedad que instancia el servicio ActividadService, para enviar peticiones y recibir el resultado.
        /// </remarks>
        private ActividadService serviceActividad { get { return new ActividadService(); } }

        /// <summary>
        /// Accion que realiza la carga de la vista index de actividad
        /// </summary>
        /// <param name="id">Identificador del bosque</param>
        /// <returns>Retorna vista parcial</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 19/12/2017</br>
        /// <b>Descripcion:</b> Accion que realiza el retorno de la vista parcial index de activides segun el numero de identificador que ingrese.
        /// </remarks>
        // GET: Actividad
        public ActionResult Index(int id)
        {
            ViewBag.idBosque = id;
            return PartialView();
        }

        /// <summary>
        /// Accion que realiza la carga de la vista detalles de actividad
        /// </summary>
        /// <param name="id">Identificador de la actividad</param>
        /// <returns>Retorna vista parcial</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 19/12/2017</br>
        /// <b>Descripcion:</b> Accion que realiza el retorno de la vista parcial detalle de actividades segun el numero de identificador que ingrese.
        /// </remarks>
        // GET: Actividad/Details/5
        public ActionResult Details(int id)
        {
            var model = serviceActividad.ObtenerActividadBosque(id);
            return PartialView(model);
        }

        /// <summary>
        /// Accion que realiza la carga de la vista crear de actividad
        /// </summary>
        /// <param name="id">Identificador del bosque</param>
        /// <returns>Retorna vista parcial</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 19/12/2017</br>
        /// <b>Descripcion:</b> Accion que realiza el retorno de la vista parcial crear de actividades.
        /// </remarks>
        public ActionResult Create(int id)
        {
            ViewBag.TiposActividades = service.ObtenerTiposActividades();
            ViewBag.EstadosActividades = service.ObtenerEstadosActividades();
            var model = new ActividadesBosques();
            model.Id_Bosque = id;
            return PartialView(model);
        }

        /// <summary>
        /// Accion que guarda los datos de la vista crear de actividad
        /// </summary>
        /// <param name="viewModel">Instancia de ActividadesBosques</param>
        /// <returns>Retorna vista parcial</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 19/12/2017</br>
        /// <b>Descripcion:</b> Recibe una instancia de ActividadesBosques y la envia a la clase de servicio, y retorna redireccion de accion con el identificador de la instancia como parametro.
        /// </remarks>
        [HttpPost]
        public ActionResult Create(ActividadesBosques viewModel)
        {
            var info = Session["infoUsuario"] as EntidadInfoUsuario;

            string ipAddress = string.Empty;
            if (!string.IsNullOrEmpty(Request.ServerVariables[""]))
            {

                ipAddress = Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
            }
            else
            {
                ipAddress = Request.ServerVariables["REMOTE_ADDR"];
            }
            try
            {
                viewModel.Aud_User = info.Usuario;
                viewModel.Aud_Origen = ipAddress;
                if (ModelState.IsValid)
                {
                    if (viewModel.FechaInicio <= Convert.ToDateTime("01/01/1900")
                        || viewModel.FechaFin <= Convert.ToDateTime("01/01/1900"))
                    {
                        viewModel.FechaInicio = Convert.ToDateTime("01/01/1900");
                        viewModel.FechaFin = Convert.ToDateTime("01/01/1900");
                    }
                    viewModel = serviceActividad.GuardarActividad(viewModel); 
                    return RedirectToAction("Details", new { id = viewModel.Id_ActividadBosque });
                }
                ViewBag.TiposActividades = service.ObtenerTiposActividades();
                ViewBag.EstadosActividades = service.ObtenerEstadosActividades();
                return PartialView(viewModel);
            }
            catch
            {
                ViewBag.TiposActividades = service.ObtenerEstadosActividades();
                ViewBag.Estados = service.ObtenerEstadosActividades();
                return PartialView(viewModel);
            }
        }

        /// <summary>
        /// Método que serializa la informacion en formato JSON
        /// </summary>
        /// <param name="id">Identificador del bosque</param>
        /// <param name="agrid">Parametors de la grilla</param>
        /// <returns>Retorna JsonResult</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 19/12/2017</br>
        /// <b>Descripcion:</b> Realiza la serializacion de los datos recibidos a formato JSON, para cargar la grilla de actividades segun el numero de identificador que ingrese.
        /// </remarks>
        public JsonResult CargarGrilla(AnexGRID agrid, int id)
        {
            return Json(DataSourceGrilla(agrid, id), JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Método que retorna AnexGRIDResponde, segun sus parametros.
        /// </summary>
        /// <param name="id">Identificador del bosque</param>
        /// <param name="agrid">Parametors de la grilla</param>
        /// <returns>Retorna AnexGRIDResponde</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 19/12/2017</br>
        /// <b>Descripcion:</b> Método que permite obtener los datos de ActividadesBosques, y organizarlos con el proposito de retornarlos a una grilla.
        /// </remarks>
        [NonAction]
        public AnexGRIDResponde DataSourceGrilla(AnexGRID agrid, int id)
        {
            try
            {
                IEnumerable<ActividadesBosques> registros = new List<ActividadesBosques>();
                IDictionary<string, string> filtros = new Dictionary<string, string>();
                if (agrid.filtros != null)
                    foreach (var filtro in agrid.filtros)
                    {
                        filtros.Add(filtro.columna, filtro.valor);
                    }

                registros = serviceActividad.ObtenerActividadBosques(agrid.pagina, agrid.limite, agrid.columna, filtros, agrid.columna_orden != "DESC", id);

                if (registros != null)
                {
                    agrid.Inicializar();
                    int cantidadRegistros = serviceActividad.CantidadRegistros;
                    agrid.SetData(registros, cantidadRegistros);
                }
                return agrid.responde();
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        /// <summary>
        /// Accion que realiza la carga de la vista editar de actividad
        /// </summary>
        /// <param name="id">Identificador de la actividad</param>
        /// <returns>Retorna vista parcial</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 19/12/2017</br>
        /// <b>Descripcion:</b> Accion que realiza el retorno de la vista parcial editar de actividades segun el numero del identificador que ingrese.
        /// </remarks>
        public ActionResult Edit(int id)
        {
            ViewBag.TiposActividades = service.ObtenerTiposActividades();
            ViewBag.EstadosActividades = service.ObtenerEstadosActividades();
            var model = serviceActividad.ObtenerActividadBosque(id);
            return PartialView(model);
        }

        /// <summary>
        /// Accion que actualiza los datos de la vista editar de actividad
        /// </summary>
        /// <param name="viewModel">Instancia de ActividadesBosques</param>
        /// <returns>Retorna vista parcial</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 19/12/2017</br>
        /// <b>Descripcion:</b> Recibe una instancia de ActividadesBosques y la envia a la clase de servicio, y retorna redireccion de accion con el identificador de la instancia como parametro.
        /// </remarks>
        [HttpPost]
        public ActionResult Edit(ActividadesBosques viewModel)
        {
            bool result = false;
            try
            {
                if (ModelState.IsValid)
                {
                    if (viewModel.FechaInicio <= Convert.ToDateTime("01/01/1900")
                        || viewModel.FechaFin <= Convert.ToDateTime("01/01/1900"))
                    {
                        viewModel.FechaInicio = Convert.ToDateTime("01/01/1900");
                        viewModel.FechaFin = Convert.ToDateTime("01/01/1900");
                    }
                    if (result = serviceActividad.ActualizarActividad(viewModel))
                    {
                        return RedirectToAction("Details", new { id = viewModel.Id_ActividadBosque });
                    }
                }
                ViewBag.TiposActividades = service.ObtenerTiposActividades();
                ViewBag.EstadosActividades = service.ObtenerEstadosActividades();
                return PartialView(viewModel);
            }
            catch
            {
                ViewBag.TiposActividades = service.ObtenerEstadosActividades();
                ViewBag.Estados = service.ObtenerEstadosActividades();
                return PartialView(viewModel);
            }
        }
    }
}