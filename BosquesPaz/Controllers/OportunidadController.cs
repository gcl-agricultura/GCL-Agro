﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BosquesPaz.Models;
using BosquesPaz.Services;
using BosquesPaz.Helpers;
using BosquesPaz.ServiceReferenceFuncionarios;

namespace BosquesPaz.Controllers
{
    /// <summary>
    /// Clase que responde a las acciones que solicitan las peticiones de entrada y salida. 
    /// </summary>
    /// <remarks>
    /// <h3>Calidad</h3>
    /// </hr>
    /// <b>Requerimiento: Help Desk</b></br>
    /// <b>Version: 1</b></br>
    /// <b>Autor: Grupo Cubo Ltda.</b></br>
    /// <b>Fecha: 19/12/2017<b></br>
    /// <b>Descripcion:</b> Funciona para visualizar las vistas de oportunidades, realizar peticiones a la capa de servicios y ejecutar acciones.
    /// </remarks>
    public class OportunidadController : Controller
    {
        /// <summary>
        /// Propiedad que instancia la clase PatrocinadoresService
        /// </summary>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 19/12/2017</br>
        /// <b>Descripcion:</b> Propiedad que instancia el servicio PatrocinadoresService, para enviar peticiones y recibir el resultado.
        /// </remarks>
        private PatrocinadoresService servicioPatrocinador { get { return new PatrocinadoresService(); } }

        /// <summary>
        /// Propiedad que instancia la clase CombosService
        /// </summary>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 19/12/2017</br>
        /// <b>Descripcion:</b> Propiedad que instancia el servicio CombosService, para enviar peticiones y recibir el resultado.
        /// </remarks>
        private CombosService service { get { return new CombosService(); } }

        /// <summary>
        /// Propiedad que instancia la clase OportunidadesServices
        /// </summary>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 19/12/2017</br>
        /// <b>Descripcion:</b> Propiedad que instancia el servicio OportunidadesServices, para enviar peticiones y recibir el resultado.
        /// </remarks>
        private OportunidadesServices servicioOportunidad { get { return new OportunidadesServices(); } }

        /// <summary>
        /// Accion que realiza la carga de la vista index de oportunidad
        /// </summary>
        /// <param name="id">Identificador del patrocinador</param>
        /// <returns>Retorna vista parcial</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 19/12/2017</br>
        /// <b>Descripcion:</b> Accion que realiza el retorno de la vista parcial index de oportunidad segun el numero de identificador que ingrese.
        /// </remarks>
        public ActionResult Index(int? id)
        {
            if (id != null)
            {
                ViewBag.idPatrocinador = id;
            }
            ViewBag.UsuarioAdmin = null;
            var infraestructura = new Infraestructura();
            EntidadInfoUsuario info = null;
            if (Session["infoUsuario"] != null)
            {
                info = Session["infoUsuario"] as EntidadInfoUsuario;
            }
            if (infraestructura.PermisoUsuario(info, "VerDetalle", "Oportunidades"))
            {
                ViewBag.UsuarioAdmin = "UsuarioAdmin";
            }
            return PartialView();
        }

        /// <summary>
        /// Accion que realiza la carga de la vista index de oportunidades Inversiones donaciones
        /// </summary>
        /// <param name="id">Identificador del patrocinador</param>
        /// <returns>Retorna vista parcial</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 29/12/2017</br>
        /// <b>Descripcion:</b> Accion que realiza el retorno de la vista parcial index de oportunidades Inversiones Oportunidades segun el numero de identificador que ingrese.
        /// </remarks>
        public ActionResult IndexInversionesDonaciones(int id)
        {
            ViewBag.idPatrocinador = id;
            return PartialView();
        }

        /// <summary>
        /// Accion que realiza la carga de la vista index de oportunidades Compras alianzas
        /// </summary>
        /// <param name="id">Identificador del patrocinador</param>
        /// <returns>Retorna vista parcial</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 29/12/2017</br>
        /// <b>Descripcion:</b> Accion que realiza el retorno de la vista parcial index de oportunidades compras alianzas segun el numero de identificador que ingrese.
        /// </remarks>
        public ActionResult IndexComprasAlianzas(int id)
        {
            ViewBag.idPatrocinador = id;
            return PartialView();
        }

        /// <summary>
        /// Accion que realiza la carga de la vista detalles de patrocinador
        /// </summary>
        /// <param name="id">Identificador del patrocinador</param>
        /// <returns>Retorna vista parcial</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 19/12/2017</br>
        /// <b>Descripcion:</b> Accion que realiza el retorno de la vista parcial detalle de patrocinador segun el numero de identificador que ingrese.
        /// </remarks>
        public ActionResult Details(int id)
        {
            var model = servicioPatrocinador.ObtenerPatrocinadorBosque(id);
            return PartialView(model);
        }

        /// <summary>
        /// Accion que realiza la carga de la vista detalles de oportunidad
        /// </summary>
        /// <param name="id">Identificador de la oportunidad</param>
        /// <returns>Retorna vista parcial</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 19/12/2017</br>
        /// <b>Descripcion:</b> Accion que realiza el retorno de la vista parcial detalle de oportunidad segun el numero de identificador que ingrese.
        /// </remarks>
        public ActionResult DetailsOportunidad(int id)
        {
            var model = servicioOportunidad.ObtenerOportunidad(id);
            return PartialView(model);
        }

        /// <summary>
        /// Accion que realiza la carga de la vista crear de oportunidad
        /// </summary>
        /// <param name="id">Identificador del patrocinador</param>
        /// <returns>Retorna vista parcial</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 19/12/2017</br>
        /// <b>Descripcion:</b> Accion que realiza el retorno de la vista parcial crear de patrocinador.
        /// </remarks>
        public ActionResult CreateOportunidad(int id)
        {
            var model = new Oportunidades();
            model.Id_PatrocinadorBosque = id;
            return PartialView(model);
        }

        /// <summary>
        /// Accion que guarda los datos de la vista crear de oportunidad
        /// </summary>
        /// <param name="viewModel">Instancia de Oportunidades</param>
        /// <returns>Retorna vista parcial</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 19/12/2017</br>
        /// <b>Descripcion:</b> Recibe una instancia de Oportunidades y la envia a la clase de servicio, y retorna redireccion de accion con el identificador de la instancia como parametro.
        /// </remarks>
        [HttpPost]
        public ActionResult CreateOportunidad(Oportunidades viewModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    viewModel = servicioOportunidad.GuardarOportunidad(viewModel);
                    return RedirectToAction("DetailsOportunidad", new { id = viewModel.Id_Oportunidad });
                }
                return PartialView(viewModel);
            }
            catch (Exception)
            {
                return PartialView(viewModel);
            }
        }

        /// <summary>
        /// Accion que realiza la carga de la vista verificar correo de oportunidad
        /// </summary>
        /// <returns>Retorna vista parcial</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 19/12/2017</br>
        /// <b>Descripcion:</b> Accion que realiza el retorno de la vista parcial verificar correo de oportunidad segun el numero de identificador que ingrese.
        /// </remarks>
        public ActionResult VerificarCorreo()
        {
            ViewBag.EstadoRegistro = null;
            var infraestructura = new Infraestructura();
            EntidadInfoUsuario info = null;
            if (Session["infoUsuario"] != null)
            {
                info = Session["infoUsuario"] as EntidadInfoUsuario;
            }
            if (infraestructura.PermisoUsuario(info, "VerDetalle", "Oportunidades"))
            {
                return RedirectToAction("Index");
            }
            return PartialView();
        }

        /// <summary>
        /// Accion que consulta el correo de un patrocinador
        /// </summary>
        /// <param name="viewModel">Instancia de PatrocinadoresBosques</param>
        /// <returns>Retorna vista parcial</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 19/12/2017</br>
        /// <b>Descripcion:</b> Recibe una instancia de PatrocinadoresBosques y la envia a la clase de servicio, si el patrocinador se registro con anterioridad,
        /// se retorna redireccion de accion index con identificador de la instancia como parametro, si no, se retorna redireccion de accion crear.
        /// </remarks>
        [HttpPost]
        public ActionResult VerificarCorreo(PatrocinadoresBosques viewmodel)
        {
            int result;
            ViewBag.EstadoRegistro = null;
            try
            {
                if (viewmodel.Correo != null)
                {
                    result = servicioPatrocinador.ObtenerPatrocinadorPorCorreo(viewmodel.Correo);
                    if (result > 0)
                    {
                        return RedirectToAction("Index", new { id = result });
                    }
                    else
                    {
                        ViewBag.EstadoRegistro = "Sin registro";
                        return PartialView();
                    }
                }
                return PartialView();
            }
            catch (Exception)
            {
                return PartialView();
            }
        }

        /// <summary>
        /// Método que serializa la informacion en formato JSON
        /// </summary>
        /// <param name="id">Identificador del patrocinador</param>
        /// <param name="agrid">Parametors de la grilla</param>
        /// <returns>Retorna JsonResult</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 19/12/2017</br>
        /// <b>Descripcion:</b> Realiza la serializacion de los datos recibidos a formato JSON, para cargar la grilla de oportunidades segun el numero de identificador que ingrese.
        /// </remarks>
        public JsonResult CargarGrilla(AnexGRID agrid, int id)
        {
            return Json(DataSourceGrilla(agrid, id), JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Método que retorna AnexGRIDResponde, segun sus parametros.
        /// </summary>
        /// <param name="id">Identificador del patrocinador</param>
        /// <param name="agrid">Parametors de la grilla</param>
        /// <returns>Retorna AnexGRIDResponde</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 19/12/2017</br>
        /// <b>Descripcion:</b> Método que permite obtener los datos de Oportunidades, y organizarlos con el proposito de retornarlos a una grilla.
        /// </remarks>
        [NonAction]
        public AnexGRIDResponde DataSourceGrilla(AnexGRID agrid, int id)
        {
            try
            {
                IEnumerable<Oportunidades> registros = new List<Oportunidades>();
                IDictionary<string, string> filtros = new Dictionary<string, string>();
                if (agrid.filtros != null)
                    foreach (var filtro in agrid.filtros)
                    {
                        filtros.Add(filtro.columna, filtro.valor);
                    }

                registros = servicioOportunidad.ObtenerOportunidades(agrid.pagina, agrid.limite, agrid.columna, filtros, agrid.columna_orden != "DESC", id);

                if (registros != null)
                {
                    agrid.Inicializar();
                    int cantidadRegistros = servicioOportunidad.CantidadRegistros;
                    agrid.SetData(registros, cantidadRegistros);
                }
                return agrid.responde();
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        /// <summary>
        /// Accion que realiza la carga de la vista index de oportunidades Inverciones donaciones para el administrador
        /// </summary>
        /// <returns>Retorna vista parcial</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 29/12/2017</br>
        /// <b>Descripcion:</b> Accion que realiza el retorno de la vista parcial index de oportunidades Inversiones donaciones para el administrador.
        /// </remarks>
        public ActionResult IndexAdminInversionesDonaciones()
        {
            return PartialView();
        }

        /// <summary>
        /// Accion que realiza la carga de la vista index de oportunidades Compras alianzas para el administrador
        /// </summary>
        /// <returns>Retorna vista parcial</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 29/12/2017</br>
        /// <b>Descripcion:</b> Accion que realiza el retorno de la vista parcial index de oportunidades Compras alianzas para el administrador.
        /// </remarks>
        public ActionResult IndexAdminComprasAlianzas()
        {
            return PartialView();
        }

        /// <summary>
        /// Método que serializa la informacion en formato JSON
        /// </summary>
        /// <param name="agrid">Parametors de la grilla</param>
        /// <returns>Retorna JsonResult</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 29/12/2017</br>
        /// <b>Descripcion:</b> Realiza la serializacion de los datos recibidos a formato JSON, para cargar la grilla de oportunidades.
        /// </remarks>
        public JsonResult CargarGrillaAdmin(AnexGRID agrid)
        {
            return Json(DataSourceGrillaAdmin(agrid), JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Método que retorna AnexGRIDResponde, segun sus parametros.
        /// </summary>
        /// <param name="agrid">Parametors de la grilla</param>
        /// <returns>Retorna AnexGRIDResponde</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 29/12/2017</br>
        /// <b>Descripcion:</b> Método que permite obtener los datos de Oportunidades, y organizarlos con el proposito de retornarlos a una grilla.
        /// </remarks>
        [NonAction]
        public AnexGRIDResponde DataSourceGrillaAdmin(AnexGRID agrid)
        {
            try
            {
                IEnumerable<Oportunidades> registros = new List<Oportunidades>();
                IDictionary<string, string> filtros = new Dictionary<string, string>();
                if (agrid.filtros != null)
                    foreach (var filtro in agrid.filtros)
                    {
                        filtros.Add(filtro.columna, filtro.valor);
                    }

                registros = servicioOportunidad.ObtenerOportunidadesAdmin(agrid.pagina, agrid.limite, agrid.columna, filtros, agrid.columna_orden != "DESC");

                if (registros != null)
                {
                    agrid.Inicializar();
                    int cantidadRegistros = servicioOportunidad.CantidadRegistros;
                    agrid.SetData(registros, cantidadRegistros);
                }
                return agrid.responde();
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        public ActionResult CreateOportunidadComprasAlianzas(int id)
        {
            ViewBag.TiposPatrocinadores = service.ObtenerTiposPatrocinadoresComprasAlianzas();
            var model = new PatrocinadoresBosques();
            model.Id_PatrocinadorBosque = id;
            return PartialView(model);
        }
    }
}