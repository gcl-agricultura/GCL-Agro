﻿using BosquesPaz.Helpers;
using BosquesPaz.Models;
using BosquesPaz.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BosquesPaz.Controllers
{
    /// <summary>
    /// Clase que responde a las acciones que solicitan las peticiones de entrada y salida. 
    /// </summary>
    /// <remarks>
    /// <h3>Calidad</h3>
    /// </hr>
    /// <b>Requerimiento: Help Desk</b></br>
    /// <b>Version: 1</b></br>
    /// <b>Autor: Grupo Cubo Ltda.</b></br>
    /// <b>Fecha: 19/12/2017<b></br>
    /// <b>Descripcion:</b> Funciona para visualizar las vistas de estadisticas, realizar peticiones a la capa de servicios y cargar grillas.
    /// </remarks>
    public class EstadisticasController : Controller
    {
        /// <summary>
        /// Propiedad que instancia la clase EstadisticasService
        /// </summary>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 19/12/2017</br>
        /// <b>Descripcion:</b> Propiedad que instancia el servicio EstadisticasService, para enviar peticiones y recibir el resultado.
        /// </remarks>
        private EstadisticasService service { get { return new EstadisticasService(); } }

        /// <summary>
        /// Método que serializa la informacion en formato JSON
        /// </summary>
        /// <param name="agrid">Parametors de la grilla</param>
        /// <returns>Retorna JsonResult</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 19/12/2017</br>
        /// <b>Descripcion:</b> Realiza la serializacion de los datos recibidos a formato JSON, para cargar la grilla del procedimiento almacenado SP_DepartamentoCantidadBosque.
        /// </remarks>
        // GET: Estadisticas
        public JsonResult DepartamentoCantidadBosque(AnexGRID agrid)
        {
            try
            {
                IEnumerable<SP_DepartamentoCantidadBosque> registros = new List<SP_DepartamentoCantidadBosque>();
                IDictionary<string, string> filtros = new Dictionary<string, string>();
                if (agrid.filtros != null)
                    foreach (var filtro in agrid.filtros)
                    {
                        filtros.Add(filtro.columna, filtro.valor);
                    }

                registros = service.DepartamentoCantidadBosque(agrid.pagina, agrid.limite, agrid.columna, filtros, agrid.columna_orden != "DESC");

                if (registros != null)
                {
                    agrid.Inicializar();
                    int cantidadRegistros = 0;
                    agrid.SetData(registros, cantidadRegistros);
                }
                return Json(agrid.responde(), JsonRequestBehavior.AllowGet); ;
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        /// <summary>
        /// Método que serializa la informacion en formato JSON
        /// </summary>
        /// <param name="agrid">Parametors de la grilla</param>
        /// <returns>Retorna JsonResult</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 19/12/2017</br>
        /// <b>Descripcion:</b> Realiza la serializacion de los datos recibidos a formato JSON, para cargar la grilla del procedimiento almacenado SP_DepartamentoTipoEstadoCantidadBosque.
        /// </remarks>
        public JsonResult DepartamentoTipoEstadoCantidadBosque(AnexGRID agrid)
        {

            try
            {
                IEnumerable<SP_DepartamentoTipoEstadoCantidadBosque> registros = new List<SP_DepartamentoTipoEstadoCantidadBosque>();
                IDictionary<string, string> filtros = new Dictionary<string, string>();
                if (agrid.filtros != null)
                    foreach (var filtro in agrid.filtros)
                    {
                        filtros.Add(filtro.columna, filtro.valor);
                    }

                registros = service.DepartamentoTipoEstadoCantidadBosque(agrid.pagina, agrid.limite, agrid.columna, filtros, agrid.columna_orden != "DESC");

                if (registros != null)
                {
                    agrid.Inicializar();
                    int cantidadRegistros = 0;
                    agrid.SetData(registros, cantidadRegistros);
                }
                return Json(agrid.responde(), JsonRequestBehavior.AllowGet); ;
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        /// <summary>
        /// Método que serializa la informacion en formato JSON
        /// </summary>
        /// <param name="agrid">Parametors de la grilla</param>
        /// <returns>Retorna JsonResult</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 19/12/2017</br>
        /// <b>Descripcion:</b> Realiza la serializacion de los datos recibidos a formato JSON, para cargar la grilla del procedimiento almacenado SP_DepartamentoTipoEstadoPresupuestoCantidadBosque.
        /// </remarks>
        public JsonResult DepartamentoTipoEstadoPresupuestoCantidadBosque(AnexGRID agrid)
        {

            try
            {
                IEnumerable<SP_DepartamentoTipoEstadoPresupuestoCantidadBosque> registros = new List<SP_DepartamentoTipoEstadoPresupuestoCantidadBosque>();
                IDictionary<string, string> filtros = new Dictionary<string, string>();
                if (agrid.filtros != null)
                    foreach (var filtro in agrid.filtros)
                    {
                        filtros.Add(filtro.columna, filtro.valor);
                    }

                registros = service.DepartamentoTipoEstadoPresupuestoCantidadBosque(agrid.pagina, agrid.limite, agrid.columna, filtros, agrid.columna_orden != "DESC");

                if (registros != null)
                {
                    agrid.Inicializar();
                    int cantidadRegistros = 0;
                    agrid.SetData(registros, cantidadRegistros);
                }
                return Json(agrid.responde(), JsonRequestBehavior.AllowGet); ;
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        /// <summary>
        /// Método que serializa la informacion en formato JSON
        /// </summary>
        /// <param name="agrid">Parametors de la grilla</param>
        /// <returns>Retorna JsonResult</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 19/12/2017</br>
        /// <b>Descripcion:</b> Realiza la serializacion de los datos recibidos a formato JSON, para cargar la grilla del procedimiento almacenado SP_DepartamentoTipoNombreCantidadBosque.
        /// </remarks>
        public JsonResult DepartamentoTipoNombreCantidadBosque(AnexGRID agrid)
        {

            try
            {
                IEnumerable<SP_DepartamentoTipoNombreCantidadBosque> registros = new List<SP_DepartamentoTipoNombreCantidadBosque>();
                IDictionary<string, string> filtros = new Dictionary<string, string>();
                if (agrid.filtros != null)
                    foreach (var filtro in agrid.filtros)
                    {
                        filtros.Add(filtro.columna, filtro.valor);
                    }

                registros = service.DepartamentoTipoNombreCantidadBosque(agrid.pagina, agrid.limite, agrid.columna, filtros, agrid.columna_orden != "DESC");

                if (registros != null)
                {
                    agrid.Inicializar();
                    int cantidadRegistros = 0;
                    agrid.SetData(registros, cantidadRegistros);
                }
                return Json(agrid.responde(), JsonRequestBehavior.AllowGet); ;
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        /// <summary>
        /// Método que serializa la informacion en formato JSON
        /// </summary>
        /// <param name="agrid">Parametors de la grilla</param>
        /// <returns>Retorna JsonResult</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 19/12/2017</br>
        /// <b>Descripcion:</b> Realiza la serializacion de los datos recibidos a formato JSON, para cargar la grilla del procedimiento almacenado SP_DepartamentoTipoNombreEstadoBosque.
        /// </remarks>
        public JsonResult DepartamentoTipoNombreEstadoBosque(AnexGRID agrid)
        {

            try
            {
                IEnumerable<SP_DepartamentoTipoNombreEstadoBosque> registros = new List<SP_DepartamentoTipoNombreEstadoBosque>();
                IDictionary<string, string> filtros = new Dictionary<string, string>();
                if (agrid.filtros != null)
                    foreach (var filtro in agrid.filtros)
                    {
                        filtros.Add(filtro.columna, filtro.valor);
                    }

                registros = service.DepartamentoTipoNombreEstadoBosque(agrid.pagina, agrid.limite, agrid.columna, filtros, agrid.columna_orden != "DESC");

                if (registros != null)
                {
                    agrid.Inicializar();
                    int cantidadRegistros = 0;
                    agrid.SetData(registros, cantidadRegistros);
                }
                return Json(agrid.responde(), JsonRequestBehavior.AllowGet); ;
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        /// <summary>
        /// Método que serializa la informacion en formato JSON
        /// </summary>
        /// <param name="agrid">Parametors de la grilla</param>
        /// <returns>Retorna JsonResult</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 19/12/2017</br>
        /// <b>Descripcion:</b> Realiza la serializacion de los datos recibidos a formato JSON, para cargar la grilla del procedimiento almacenado SP_DepartamentoTipoUnidadCantidadBosque.
        /// </remarks>
        public JsonResult DepartamentoTipoUnidadCantidadBosque(AnexGRID agrid)
        {

            try
            {
                IEnumerable<SP_DepartamentoTipoUnidadCantidadBosque> registros = new List<SP_DepartamentoTipoUnidadCantidadBosque>();
                IDictionary<string, string> filtros = new Dictionary<string, string>();
                if (agrid.filtros != null)
                    foreach (var filtro in agrid.filtros)
                    {
                        filtros.Add(filtro.columna, filtro.valor);
                    }

                registros = service.DepartamentoTipoUnidadCantidadBosque(agrid.pagina, agrid.limite, agrid.columna, filtros, agrid.columna_orden != "DESC");

                if (registros != null)
                {
                    agrid.Inicializar();
                    int cantidadRegistros = 0;
                    agrid.SetData(registros, cantidadRegistros);
                }
                return Json(agrid.responde(), JsonRequestBehavior.AllowGet); ;
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }
    }
}