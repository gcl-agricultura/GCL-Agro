﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BosquesPaz.Models;
using BosquesPaz.Services;
using BosquesPaz.Helpers;

namespace BosquesPaz.Controllers
{
    /// <summary>
    /// Clase que responde a las acciones que solicitan las peticiones de entrada y salida. 
    /// </summary>
    /// <remarks>
    /// <h3>Calidad</h3>
    /// </hr>
    /// <b>Requerimiento: Help Desk</b></br>
    /// <b>Version: 1</b></br>
    /// <b>Autor: Grupo Cubo Ltda.</b></br>
    /// <b>Fecha: 19/12/2017<b></br>
    /// <b>Descripcion:</b> Funciona para visualizar la vista de auditorias individuos/especies, realizar peticiones a la capa de servicios y ejecutar acciones.
    /// </remarks>
    public class IndividuoAuditoriaController : Controller
    {
        /// <summary>
        /// Propiedad que instancia la clase IndividuosEspeciesAuditoriasService
        /// </summary>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 19/12/2017</br>
        /// <b>Descripcion:</b> Propiedad que instancia el servicio IndividuosEspeciesAuditoriasService, para enviar peticiones y recibir el resultado.
        /// </remarks>
        private IndividuosEspeciesAuditoriasService serviceIndividuoAuditoria { get { return new IndividuosEspeciesAuditoriasService(); } }
        // GET: IndividuoAuditoria
        public ActionResult Index(int id)
        {
            ViewBag.idIndividuoEspecie = id;
            return PartialView();
        }

        public JsonResult CargarGrillaHistorial(AnexGRID agrid, int id)
        {
            return Json(DataSourceGrillaHistorial(agrid, id), JsonRequestBehavior.AllowGet);
        }

        [NonAction]
        public AnexGRIDResponde DataSourceGrillaHistorial(AnexGRID agrid, int id)
        {
            try
            {
                IEnumerable<AudiIndividuosEspecies> registros = new List<AudiIndividuosEspecies>();
                IDictionary<string, string> filtros = new Dictionary<string, string>();
                if (agrid.filtros != null)
                    foreach (var filtro in agrid.filtros)
                    {
                        filtros.Add(filtro.columna, filtro.valor);
                    }

                registros = serviceIndividuoAuditoria.ObtenerIndividuosEspeciesAuditorias(agrid.pagina, agrid.limite, agrid.columna, filtros, agrid.columna_orden != "DESC", id);

                if (registros != null)
                {
                    agrid.Inicializar();
                    int cantidadRegistros = serviceIndividuoAuditoria.CantidadRegistros;
                    agrid.SetData(registros, cantidadRegistros);
                }
                return agrid.responde();
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }
    }
}