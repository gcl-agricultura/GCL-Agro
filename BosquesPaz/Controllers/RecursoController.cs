﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BosquesPaz.Models;
using BosquesPaz.Services;
using System.Web.Mvc;
using BosquesPaz.Helpers;
using System.Net;
using BosquesPaz.ServiceReferenceFuncionarios;

namespace BosquesPaz.Controllers
{
    /// <summary>
    /// Clase que responde a las acciones que solicitan las peticiones de entrada y salida. 
    /// </summary>
    /// <remarks>
    /// <h3>Calidad</h3>
    /// </hr>
    /// <b>Requerimiento: Help Desk</b></br>
    /// <b>Version: 1</b></br>
    /// <b>Autor: Grupo Cubo Ltda.</b></br>
    /// <b>Fecha: 19/12/2017<b></br>
    /// <b>Descripcion:</b> Funciona para visualizar las vistas de recursos, realizar peticiones a la capa de servicios y ejecutar acciones.
    /// </remarks>
    public class RecursoController : Controller
    {
        /// <summary>
        /// Propiedad que instancia la clase CombosService
        /// </summary>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 19/12/2017</br>
        /// <b>Descripcion:</b> Propiedad que instancia el servicio CombosService, para enviar peticiones y recibir el resultado.
        /// </remarks>
        private CombosService service { get { return new CombosService(); } }

        /// <summary>
        /// Propiedad que instancia la clase RecursosService
        /// </summary>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 19/12/2017</br>
        /// <b>Descripcion:</b> Propiedad que instancia el servicio RecursosService, para enviar peticiones y recibir el resultado.
        /// </remarks>
        private RecursosService serviceRecurso { get { return new RecursosService(); } }

        /// <summary>
        /// Accion que realiza la carga de la vista index de recurso
        /// </summary>
        /// <param name="id">Identificador del bosque</param>
        /// <returns>Retorna vista parcial</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 19/12/2017</br>
        /// <b>Descripcion:</b> Accion que realiza el retorno de la vista parcial index de recursos segun el numero de identificador que ingrese.
        /// </remarks>
        //id GET: Recurso
        public ActionResult Index(int id)
        {
            ViewBag.idBosque = id;
            return PartialView();
        }

        /// <summary>
        /// Accion que realiza la carga de la vista detalles de recurso
        /// </summary>
        /// <param name="id">Identificador del recurso</param>
        /// <returns>Retorna vista parcial</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 19/12/2017</br>
        /// <b>Descripcion:</b> Accion que realiza el retorno de la vista parcial detalle de recursos segun el numero de identificador que ingrese.
        /// </remarks>
        // GET: Bosque/Details/5
        public ActionResult Details(int id)
        {
            var model = serviceRecurso.ObtenerRecursoBosque(id);
            return PartialView(model);
        }

        /// <summary>
        /// Accion que realiza la carga de la vista crear de recurso
        /// </summary>
        /// <param name="id">Identificador del bosque</param>
        /// <returns>Retorna vista parcial</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 19/12/2017</br>
        /// <b>Descripcion:</b> Accion que realiza el retorno de la vista parcial crear de recursos.
        /// </remarks>
        // GET: Recurso/Create/id
        public ActionResult Create(int id)
        {
            ViewBag.TiposRecursos = service.ObtenerTiposRecursos();
            ViewBag.Estados = service.ObtnerEstados();
            var model = new RecursosBosques();
            model.Id_Bosque = id;
            return PartialView(model);
        }

        /// <summary>
        /// Accion que guarda los datos de la vista crear de recurso
        /// </summary>
        /// <param name="viewModel">Instancia de RecursosBosques</param>
        /// <returns>Retorna vista parcial</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 19/12/2017</br>
        /// <b>Descripcion:</b> Recibe una instancia de RecursosBosques y la envia a la clase de servicio, y retorna redireccion de accion con el identificador de la instancia como parametro.
        /// </remarks>
        [HttpPost]
        public ActionResult Create(RecursosBosques viewModel)
        {
            var info = Session["infoUsuario"] as EntidadInfoUsuario;

            string ipAddress = string.Empty;
            if (!string.IsNullOrEmpty(Request.ServerVariables[""]))
            {

                ipAddress = Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
            }
            else
            {
                ipAddress = Request.ServerVariables["REMOTE_ADDR"];
            }

            try
            {
                viewModel.Aud_User = info.Usuario;
                viewModel.Aud_Origen = ipAddress;
                if (ModelState.IsValid)
                {
                    if (viewModel.FechaActualizacion <= Convert.ToDateTime("01/01/1900"))
                        viewModel.FechaActualizacion = Convert.ToDateTime("01/01/1900");
                    viewModel = serviceRecurso.GuardarRecurso(viewModel);
                    return RedirectToAction("Details", new { id = viewModel.Id_RecursoBosque });
                }
                ViewBag.TiposRecursos = service.ObtenerTiposRecursos();
                ViewBag.Estados = service.ObtnerEstados();
                return PartialView(viewModel);
            }
            catch
            {
                ViewBag.TiposRecursos = service.ObtenerTiposRecursos();
                ViewBag.Estados = service.ObtnerEstados();
                return PartialView(viewModel);
            }
        }

        /// <summary>
        /// Método que serializa la informacion en formato JSON
        /// </summary>
        /// <param name="id">Identificador del bosque</param>
        /// <param name="agrid">Parametors de la grilla</param>
        /// <returns>Retorna JsonResult</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 19/12/2017</br>
        /// <b>Descripcion:</b> Realiza la serializacion de los datos recibidos a formato JSON, para cargar la grilla de recursos segun el numero de identificador que ingrese.
        /// </remarks>
        public JsonResult CargarGrilla(AnexGRID agrid,int id)
        {
            return Json(DataSourceGrilla(agrid, id), JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Método que retorna AnexGRIDResponde, segun sus parametros.
        /// </summary>
        /// <param name="id">Identificador del bosque</param>
        /// <param name="agrid">Parametors de la grilla</param>
        /// <returns>Retorna AnexGRIDResponde</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 19/12/2017</br>
        /// <b>Descripcion:</b> Método que permite obtener los datos de RecursosBosques, y organizarlos con el proposito de retornarlos a una grilla.
        /// </remarks>
        [NonAction]
        public AnexGRIDResponde DataSourceGrilla(AnexGRID agrid, int id)
        {
            try
            {
                IEnumerable<RecursosBosques> registros = new List<RecursosBosques>();
                IDictionary<string, string> filtros = new Dictionary<string, string>();
                if (agrid.filtros != null)
                    foreach (var filtro in agrid.filtros)
                    {
                        filtros.Add(filtro.columna, filtro.valor);
                    }

                registros = serviceRecurso.ObtenerRecursosBosques(agrid.pagina, agrid.limite, agrid.columna, filtros, agrid.columna_orden != "DESC", id);

                if (registros != null)
                {
                    agrid.Inicializar();
                    int cantidadRegistros = serviceRecurso.CantidadRegistros;
                    agrid.SetData(registros, cantidadRegistros);
                }
                return agrid.responde();
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        /// <summary>
        /// Accion que realiza la carga de la vista editar de recurso
        /// </summary>
        /// <param name="id">Identificador del recurso</param>
        /// <returns>Retorna vista parcial</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 19/12/2017</br>
        /// <b>Descripcion:</b> Accion que realiza el retorno de la vista parcial editar de recursos segun el numero del identificador que ingrese.
        /// </remarks>
        public ActionResult Edit(int id)
        {
            ViewBag.TiposRecursos = service.ObtenerTiposRecursos();
            ViewBag.Estados = service.ObtnerEstados();
            var model = serviceRecurso.ObtenerRecursoBosque(id);
            return PartialView(model);
        }

        /// <summary>
        /// Accion que actualiza los datos de la vista editar de recursos
        /// </summary>
        /// <param name="viewModel">Instancia de RecursosBosques</param>
        /// <returns>Retorna vista parcial</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 19/12/2017</br>
        /// <b>Descripcion:</b> Recibe una instancia de RecursosBosques y la envia a la clase de servicio, y retorna redireccion de accion con el identificador de la instancia como parametro.
        /// </remarks>
        [HttpPost]
        public ActionResult Edit(RecursosBosques viewModel)
        {
            bool result = false;
            try
            {
                if (ModelState.IsValid)
                {
                    if(result = serviceRecurso.ActualizarRecurso(viewModel))
                    {
                        return RedirectToAction("Details", new { id = viewModel.Id_RecursoBosque });
                    }   
                }
                ViewBag.TiposRecursos = service.ObtenerTiposRecursos();
                ViewBag.Estados = service.ObtnerEstados();
                return PartialView(viewModel);
            }
            catch
            {
                ViewBag.TiposRecursos = service.ObtenerTiposRecursos();
                ViewBag.Estados = service.ObtnerEstados();
                return PartialView(viewModel);
            }
        }
    }
}