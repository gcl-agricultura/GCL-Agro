﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BosquesPaz.Models;
using BosquesPaz.Repositories;

namespace BosquesPaz.Services
{
    /// <summary>
    /// Clase que verifica que las peticiones de los datos sean correctas. 
    /// </summary>
    /// <remarks>
    /// <h3>Calidad</h3>
    /// </hr>
    /// <b>Requerimiento:</b> Help Desk</br>
    /// <b>Version:</b> 1</br>
    /// <b>Autor:</b> Grupo Cubo Ltda.</br>
    /// <b>Fecha:</b> 18/12/2017</br>
    /// <b>Descripcion:</b> Funciona para solicitar los datos al repositorio de audios y retornar los datos obtenidos.
    /// </remarks>
    public class AudiosService
    {
        /// <summary>
        /// Propiedad que instancia la clase AudiosRepository
        /// </summary>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 18/12/2017</br>
        /// <b>Descripcion:</b> Propiedad que instancia el repositorio AudiosRepository, para enviar peticiones y recibir el resultado.
        /// </remarks>
        private AudiosRepository repositorio
        {
            get
            {
                return new AudiosRepository();
            }
        }

        /// <summary>
        /// Obtener datos de AudiosHistorias de la base de datos.
        /// </summary>
        /// <param name="idHistoriaBosque">Identificador de la historia de un bosque</param>
        /// <returns>Retorna coleccion de AudiosHistorias</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 18/12/2017</br>
        /// <b>Descripcion:</b> Obtiene una instancia de la AudiosHistorias segun el numero de identificador que ingrese.
        /// </remarks>
        public IEnumerable<AudiosHistorias> ObtenerAudioBosque(int idHistoriaBosque)
        {
            IEnumerable<AudiosHistorias> result = new List<AudiosHistorias>();
            try
            {
                var query = repositorio.ObtenerAudioBosque(idHistoriaBosque);
                if (query != null && query.Count() > 0)
                {
                    result = query;
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }

        /// <summary>
        /// Guardar una instancia de AudiosHistorias en la base de datos.
        /// </summary>
        /// <param name="audio">Coleccion de AudiosHistorias</param>
        /// <returns>Retorna coleccion de AudiosHistorias</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 18/12/2017</br>
        /// <b>Descripcion:</b> Recibi una coleccion de AudiosHistorias y la envia a la clase de repositorio, y retorna la coleccion.
        /// </remarks>
        public IEnumerable<AudiosHistorias> GuardarAudioBosque(IEnumerable<AudiosHistorias> audio)
        {
            IEnumerable<AudiosHistorias> result = new List<AudiosHistorias>();
            var IdHistoriaBosque = audio.FirstOrDefault().Id_HistoriaBosque;
            try
            {
                repositorio.GuardarAudiosHistorias(audio);
                result = ObtenerAudioBosque(IdHistoriaBosque);
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return result;
        }
    }
}