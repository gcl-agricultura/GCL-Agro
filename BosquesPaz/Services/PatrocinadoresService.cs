﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BosquesPaz.Models;
using BosquesPaz.Repositories;
using System.Linq.Expressions;
using BosquesPaz.Repositories.Especificaciones;


namespace BosquesPaz.Services
{
    /// <summary>
    /// Clase que verifica que las peticiones de los datos sean correctas. 
    /// </summary>
    /// <remarks>
    /// <h3>Calidad</h3>
    /// </hr>
    /// <b>Requerimiento:</b> Help Desk</br>
    /// <b>Version:</b> 1</br>
    /// <b>Autor:</b> Grupo Cubo Ltda.</br>
    /// <b>Fecha:</b> 18/12/2017</br>
    /// <b>Descripcion:</b> Funciona para solicitar los datos al repositorio de patrocinadores y retornar los datos obtenidos.
    /// </remarks>
    public class PatrocinadoresService
    {
        /// <summary>
        /// Propiedad que instancia la clase PatrocinadoresRepository
        /// </summary>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 18/12/2017</br>
        /// <b>Descripcion:</b> Propiedad que instancia el repositorio PatrocinadoresRepository, para enviar peticiones y recibir el resultado.
        /// </remarks>
        private PatrocinadoresRepository repositorio { get { return new PatrocinadoresRepository(); } }

        /// <summary>
        /// Propiedad que instancia la clase EstadosPatrocinadoresService
        /// </summary>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 18/12/2017</br>
        /// <b>Descripcion:</b> Propiedad que instancia el servicio EstadosPatrocinadoresService, para enviar peticiones y recibir el resultado.
        /// </remarks>
        private EstadosPatrocinadoresService ServicioEstado { get { return new EstadosPatrocinadoresService(); } }

        /// <summary>
        /// Propiedad que instancia la clase TiposPatrocinadoresService
        /// </summary>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 18/12/2017</br>
        /// <b>Descripcion:</b> Propiedad que instancia el servicio TiposPatrocinadoresService, para enviar peticiones y recibir el resultado.
        /// </remarks>
        private TiposPatrocinadoresService ServicioTipo { get { return new TiposPatrocinadoresService(); } }

        /// <summary>
        /// Propiedad de número entero
        /// </summary>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 13/12/2017</br>
        /// <b>Descripcion:</b> Propiedad que define un número entero, para verificar el numero de registros que existen en una entidad.
        /// </remarks>
        public int CantidadRegistros { get; private set; }

        /// <summary>
        /// Guardar una instancia de PatrocinadoresBosques en la base de datos.
        /// </summary>
        /// <param name="patrocinador">Instancia de PatrocinadoresBosques</param>
        /// <returns>Retorna instancia de PatrocinadoresBosques</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 18/12/2017</br>
        /// <b>Descripcion:</b> Recibe una instancia de PatrocinadoresBosques y la envia a la clase de repositorio, y retorna la instancia.
        /// </remarks>
        public PatrocinadoresBosques GuardarPatrocinadorBosque(PatrocinadoresBosques patrocinador)
        {
            try
            {
                patrocinador = repositorio.Create(patrocinador);
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return patrocinador;
        }

        /// <summary>
        /// Actualizar datos de PatrocinadoresBosques en la base de datos.
        /// </summary>
        /// <param name="patrocinador">Instancia de PatrocinadoresBosques</param>
        /// <returns>Retorna valor booleano</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 18/12/2017</br>
        /// <b>Descripcion:</b> Recibe una instancia de PatrocinadoresBosques y la envia a la clase de repositorio, y retorna un valor booleano.
        /// </remarks>
        public bool ActualizarPatrocinadorBosque(PatrocinadoresBosques patrocinador)
        {
            bool result = false;
            try
            {
                if (repositorio.Update(patrocinador))
                {
                    result = true;
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return result;
        }

        /// <summary>
        /// Obtener datos de PatrocinadoresBosques de la base de datos.
        /// </summary>
        /// <param name="idPatrocinadorBosque">Identificador del patrocinador</param>
        /// <returns>Retorna instancia de PatrocinadoresBosques</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 18/12/2017</br>
        /// <b>Descripcion:</b> Obtiene una instancia del patrocinador segun el numero de identificador que ingrese.
        /// </remarks>
        public PatrocinadoresBosques ObtenerPatrocinadorBosque(int idPatrocinadorBosque)
        {
            PatrocinadoresBosques result = null;
            try
            {
                result = repositorio.GetByKey(idPatrocinadorBosque);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }

        /// <summary>
        /// Obtener identificador de PatrocinadoresBosques de la base de datos.
        /// </summary>
        /// <param name="correo">Correo del patrocinador</param>
        /// <returns>Retorna numero entero </returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 18/12/2017</br>
        /// <b>Descripcion:</b> Obtiene el identificador del patrocinador segun el correo que ingrese.
        /// </remarks>
        public int ObtenerPatrocinadorPorCorreo(string correo)
        {
            int result = 0;
            try
            {
                Expression<Func<PatrocinadoresBosques, bool>> predicate = x => x.Correo == correo;
                result = repositorio.GetBy(predicate).FirstOrDefault().Id_PatrocinadorBosque;
            }
            catch (Exception)
            {
                return result;
            }
            return result;
        }

        /// <summary>
        /// Método que retorna una coleccion de datos, segun sus parametros.
        /// </summary>
        /// <param name="pageIndex">Número de paginas.</param>
        /// <param name="pageCount">Número de paginaciones.</param>
        /// <param name="columnaOrden">Cadena de caracteres de una columna.</param>
        /// <param name="filtros">Cadena de caracteres</param>
        /// <param name="ascending">Tipo de dato verdadero o falso</param>
        /// <returns>Retorna coleccion de PatrocinadoresBosques</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 18/12/2017</br>
        /// <b>Descripcion:</b> Método que permite obtener los datos de PatrocinadoresBosques y organizarlos con el proposito de retornarlos a una grilla.
        /// </remarks>
        public IEnumerable<PatrocinadoresBosques> ObtenerPatrocinadoresBosques(int pageIndex, int pageCount, string columnaOrden, IDictionary<string, string> filtros, bool ascending)
        {
            Expression<Func<PatrocinadoresBosques, bool>> predicate = x => x.Id_PatrocinadorBosque > 0;
            Expression<Func<PatrocinadoresBosques, int>> order = x => x.Id_PatrocinadorBosque;
            Expression<Func<PatrocinadoresBosques, DateTime>> orderDate = null;
            bool first = true;
            bool isDate = false;

            foreach (var filtro in filtros)
            {
                switch (filtro.Key)
                {
                    default:
                        predicate = x => false;
                        break;
                }
                first = false;
            }

            CantidadRegistros = repositorio.Count(predicate);

            switch (columnaOrden)
            {
                case nameof(PatrocinadoresBosques.Id_PatrocinadorBosque):
                    order = x => x.Id_PatrocinadorBosque;
                    isDate = false;
                    break;
            }

            IEnumerable<PatrocinadoresBosques> result;
            if (isDate)
                result = repositorio.GetPagedElements<DateTime>(pageIndex, pageCount, orderDate, predicate, ascending);
            else
                result = repositorio.GetPagedElements<int>(pageIndex, pageCount, order, predicate, ascending);

            return result.ToList();
        }
    }
}