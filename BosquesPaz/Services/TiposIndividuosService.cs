﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BosquesPaz.Models;
using BosquesPaz.Repositories;
using System.Linq.Expressions;

namespace BosquesPaz.Services
{
    /// <summary>
    /// Clase que verifica que las peticiones de los datos sean correctas. 
    /// </summary>
    /// <remarks>
    /// <h3>Calidad</h3>
    /// </hr>
    /// <b>Requerimiento:</b> Help Desk</br>
    /// <b>Version:</b> 1</br>
    /// <b>Autor:</b> Grupo Cubo Ltda.</br>
    /// <b>Fecha:</b> 18/12/2017</br>
    /// <b>Descripcion:</b> Funciona para solicitar los datos al repositorio de tipos de individuos/especies y retornar los datos obtenidos.
    /// </remarks>
    public class TiposIndividuosService
    {
        /// <summary>
        /// Propiedad que instancia la clase TiposIndividuosRepository
        /// </summary>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 18/12/2017</br>
        /// <b>Descripcion:</b> Propiedad que instancia el repositorio TiposIndividuosRepository, para enviar peticiones y recibir el resultado.
        /// </remarks>
        private TiposIndividuosRepository repositorio
        {
            get
            {
                return new TiposIndividuosRepository();
            }
        }

        /// <summary>
        /// Obtener coleccion de TiposIndividuosEspecies de la base de datos.
        /// </summary>
        /// <returns>Retorna coleccion de TiposIndividuosEspecies</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 18/12/2017</br>
        /// <b>Descripcion:</b> Obtiene una coleccion de los tipos de individuos/especies.
        /// </remarks>
        public IEnumerable<TiposIndividuosEspecies> ObtenerIndividuosEspecies()
        {
            IEnumerable<TiposIndividuosEspecies> tiposIndividuosEspecies = new List<TiposIndividuosEspecies>();
            try
            {
                tiposIndividuosEspecies = repositorio.GetAll();
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return tiposIndividuosEspecies;        
        }

        /// <summary>
        /// Obtener identificador de TiposIndividuosEspecies de la base de datos.
        /// </summary>
        /// <param name="nombre">Nombre del tipo de individuos/especies</param>
        /// <returns>Retorna numero entero</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 18/12/2017</br>
        /// <b>Descripcion:</b> Obtiene el identificador del tipo de individuos/especies segun el nombre que se ingrese.
        /// </remarks>
        public int ObtenerIdTipoIndividuoPorNombre(string nombre)
        {
            int result;
            try
            {
                Expression<Func<TiposIndividuosEspecies, bool>> predicate = x => x.Nombre == nombre;
                result = repositorio.GetBy(predicate).FirstOrDefault().Id_TipoIndividuoEspecie;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }

        /// <summary>
        /// Obtener nombre de TiposIndividuosEspecies de la base de datos.
        /// </summary>
        /// <param name="idTipoIndividuoEspecie">Identificador de tipo de individuo/especie</param>
        /// <returns>Retorna cadena de caracteres</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 18/12/2017</br>
        /// <b>Descripcion:</b> Obtiene el nombre del tipo de individuo/especie segun el numero de identificador que se ingrese.
        /// </remarks>
        public string ObtenerNombreTipoIndividuoPorId(int idTipoIndividuoEspecie)
        {
            string result;
            try
            {
                Expression<Func<TiposIndividuosEspecies, bool>> predicate = x => x.Id_TipoIndividuoEspecie == idTipoIndividuoEspecie;
                result = repositorio.GetBy(predicate).FirstOrDefault().Nombre;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }
    }
}