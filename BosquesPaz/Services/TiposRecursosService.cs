﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BosquesPaz.Models;
using System.Linq.Expressions;
using BosquesPaz.Repositories;

namespace BosquesPaz.Services
{
    /// <summary>
    /// Clase que verifica que las peticiones de los datos sean correctas. 
    /// </summary>
    /// <remarks>
    /// <h3>Calidad</h3>
    /// </hr>
    /// <b>Requerimiento:</b> Help Desk</br>
    /// <b>Version:</b> 1</br>
    /// <b>Autor:</b> Grupo Cubo Ltda.</br>
    /// <b>Fecha:</b> 18/12/2017</br>
    /// <b>Descripcion:</b> Funciona para solicitar los datos al repositorio de tipos de recursos y retornar los datos obtenidos.
    /// </remarks>
    public class TiposRecursosService
    {
        /// <summary>
        /// Propiedad que instancia la clase TiposRecursosRepository
        /// </summary>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 18/12/2017</br>
        /// <b>Descripcion:</b> Propiedad que instancia el repositorio TiposRecursosRepository, para enviar peticiones y recibir el resultado.
        /// </remarks>
        private TiposRecursosRepository repositorio
        {
            get
            {
                return new TiposRecursosRepository();
            }
        }

        /// <summary>
        /// Obtener coleccion de TiposRecursos de la base de datos.
        /// </summary>
        /// <returns>Retorna coleccion de TiposRecursos</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 18/12/2017</br>
        /// <b>Descripcion:</b> Obtiene una coleccion de los tipos de recursos.
        /// </remarks>
        public IEnumerable<TiposRecursos> ObtenerTiposRecursos()
        {
            IEnumerable<TiposRecursos> tiposRecursos = new List<TiposRecursos>();
            try
            {
                tiposRecursos = repositorio.GetAll();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return tiposRecursos;
        }

        /// <summary>
        /// Obtener identificador de TiposRecursos de la base de datos.
        /// </summary>
        /// <param name="nombre">Nombre del tipo de recursos</param>
        /// <returns>Retorna numero entero</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 18/12/2017</br>
        /// <b>Descripcion:</b> Obtiene el identificador del tipo de recursos segun el nombre que se ingrese.
        /// </remarks>
        public int ObtenerIdTiposRecursosPorNombre(string nombre)
        {
            int result;
            try
            {
                Expression<Func<TiposRecursos, bool>> predicate = x => x.Nombre == nombre;
                result = repositorio.GetBy(predicate).FirstOrDefault().Id_TipoRecurso;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return result;
        }

        /// <summary>
        /// Obtener nombre de TiposRecursos de la base de datos.
        /// </summary>
        /// <param name="idTipoRecurso">Identificador de tipo de recurso</param>
        /// <returns>Retorna cadena de caracteres</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 18/12/2017</br>
        /// <b>Descripcion:</b> Obtiene el nombre del tipo de recurso segun el numero de identificador que se ingrese.
        /// </remarks>
        public string ObtenerNombreTipoRecursoPorId(int idTipoRecurso)
        {
            string result = string.Empty;
            try
            {
                Expression<Func<TiposRecursos, bool>> predicate = x => x.Id_TipoRecurso == idTipoRecurso;
                result = repositorio.GetBy(predicate).FirstOrDefault().Nombre;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return result;
        }
    }
}