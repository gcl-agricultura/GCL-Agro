﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BosquesPaz.Models;
using BosquesPaz.Repositories;
using System.Linq.Expressions;
using BosquesPaz.Repositories.Especificaciones;

namespace BosquesPaz.Services
{
    /// <summary>
    /// Clase que verifica que las peticiones de los datos sean correctas. 
    /// </summary>
    /// <remarks>
    /// <h3>Calidad</h3>
    /// </hr>
    /// <b>Requerimiento:</b> Help Desk</br>
    /// <b>Version:</b> 1</br>
    /// <b>Autor:</b> Grupo Cubo Ltda.</br>
    /// <b>Fecha:</b> 18/12/2017</br>
    /// <b>Descripcion: Funciona para solicitar los datos al repositorio de datos de contacto y retornar los datos obtenidos.
    /// </remarks>
    public class DatosContactosService
    {
        /// <summary>
        /// Propiedad que instancia la clase DatosContactosRepository
        /// </summary>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 18/12/2017</br>
        /// <b>Descripcion:</b> Propiedad que instancia el repositorio DatosContactosRepository, para enviar peticiones y recibir el resultado.
        /// </remarks>
        private DatosContactosRepository repositorio { get { return new DatosContactosRepository(); } }

        /// <summary>
        /// Guardar una instancia de DatosContactosBosques en la base de datos.
        /// </summary>
        /// <param name="datoContacto">Instancia de DatosContactosBosques</param>
        /// <returns>Retorna instancia de DatosContactosBosques</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 18/12/2017</br>
        /// <b>Descripcion:</b> Recibi una instancia de DatosContactosBosques y la envia a la clase de repositorio, y retorna la instancia.
        /// </remarks>
        public DatosContactosBosques GuardarDatoContacto(DatosContactosBosques datoContacto)
        {
            try
            {
                datoContacto = repositorio.Create(datoContacto);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return datoContacto;
        }

        /// <summary>
        /// Actualizar datos de DatosContactosBosques en la base de datos.
        /// </summary>
        /// <param name="datoContacto">Instancia de DatosContactosBosques</param>
        /// <returns>Retorna valor booleano</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 18/12/2017</br>
        /// <b>Descripcion:</b> Recibe una instancia de DatosContactosBosques y la envia a la clase de repositorio, y retorna un valor booleano.
        /// </remarks>
        public bool ActualizarDatoContacto(DatosContactosBosques datoContacto)
        {
            bool result = false;
            try
            {
                result = repositorio.Update(datoContacto);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }

        /// <summary>
        /// Obtener datos de DatosContactosBosques de la base de datos.
        /// </summary>
        /// <param name="idDatoContactoBosque">Identificador de los datos de contacto</param>
        /// <returns>Retorna instancia de DatosContactosBosques</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 18/12/2017</br>
        /// <b>Descripcion:</b> Obtiene una instancia de los datos de contacto segun el numero de identificador que ingrese.
        /// </remarks>
        public DatosContactosBosques ObtenerDatoContacto(int idDatoContactoBosque)
        {
            DatosContactosBosques result = null;
            try
            {
                result = repositorio.GetByKey(idDatoContactoBosque);
            }
            catch (Exception)
            {
                throw;
            }
            return result;
        }
    }
}