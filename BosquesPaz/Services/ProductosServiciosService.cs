﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BosquesPaz.Models;
using BosquesPaz.Repositories;
using System.Linq.Expressions;

namespace BosquesPaz.Services
{
    /// <summary>
    /// Clase que verifica que las peticiones de los datos sean correctas. 
    /// </summary>
    /// <remarks>
    /// <h3>Calidad</h3>
    /// </hr>
    /// <b>Requerimiento:</b> Help Desk</br>
    /// <b>Version:</b> 1</br>
    /// <b>Autor:</b> Grupo Cubo Ltda.</br>
    /// <b>Fecha:</b> 18/12/2017</br>
    /// <b>Descripcion:</b> Funciona para solicitar los datos al repositorio de productos/servicios y retornar los datos obtenidos.
    /// </remarks>
    public class ProductosServiciosService
    {
        /// <summary>
        /// Propiedad que instancia la clase ProductosServiciosRepository
        /// </summary>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 18/12/2017</br>
        /// <b>Descripcion:</b> Propiedad que instancia el repositorio ProductosServiciosRepository, para enviar peticiones y recibir el resultado.
        /// </remarks>
        private ProductosServiciosRepository repositorio { get { return new ProductosServiciosRepository(); } }

        /// <summary>
        /// Propiedad de número entero
        /// </summary>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 13/12/2017</br>
        /// <b>Descripcion:</b> Propiedad que define un número entero, para verificar el numero de registros que existen en una entidad.
        /// </remarks>
        public int CantidadRegistros { get; private set; }

        /// <summary>
        /// Propiedad que instancia la clase ProductosServiciosRepository
        /// </summary>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 18/12/2017</br>
        /// <b>Descripcion:</b> Propiedad que instancia el repositorio ProductosServiciosAuditoriasRepository, para enviar peticiones y recibir el resultado.
        /// </remarks>
        private ProductosServiciosAuditoriasRepository repositorioAuditoria { get{ return new ProductosServiciosAuditoriasRepository(); } }

        /// <summary>
        /// Guardar una instancia de ProductosServicios en la base de datos.
        /// </summary>
        /// <param name="productoServicio">Instancia de ProductosServicios</param>
        /// <returns>Retorna instancia de ProductosServicios</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 18/12/2017</br>
        /// <b>Descripcion:</b> Recibe una instancia de ProductosServicios y la envia a la clase de repositorio, y retorna la instancia.
        /// </remarks>
        public ProductosServicios GuardarProductoServicio(ProductosServicios productoServicio)
        {
            try
            {
                productoServicio = repositorio.Create(productoServicio);
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return productoServicio;
        }

        /// <summary>
        /// Actualizar datos de ProductosServicios en la base de datos.
        /// </summary>
        /// <param name="productoServicio">Instancia de ProductosServicios</param>
        /// <returns>Retorna valor booleano</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 18/12/2017</br>
        /// <b>Descripcion:</b> Recibe una instancia de ProductosServicios y la envia a la clase de repositorio, y retorna un valor booleano.
        /// </remarks>
        public bool ActualizarProductoServicio(ProductosServicios productoServicio)
        {
            bool result = false;
            try
            {
                if (repositorio.Update(productoServicio))
                {
                    result = true;
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return result;
        }

        /// <summary>
        /// Obtener datos de ProductosServicios de la base de datos.
        /// </summary>
        /// <param name="idProductoServicio">Identificador del producto/servicio</param>
        /// <returns>Retorna instancia de ProductosServicios</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 18/12/2017</br>
        /// <b>Descripcion:</b> Obtiene una instancia de los productos/servicios segun el numero de identificador que ingrese.
        /// </remarks>
        public ProductosServicios ObtenerProductosServicios(int idProductoServicio)
        {
            ProductosServicios result = null;
            try
            {
                result = repositorio.GetByKey(idProductoServicio);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }

        /// <summary>
        /// Obtener coleccion de ProductosServicios de la base de datos.
        /// </summary>
        /// <returns>Retorna coleccion de ProductosServicios</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 18/12/2017</br>
        /// <b>Descripcion:</b> Obtiene una coleccion de los productos/servicios con los datos de forma inversa para el modulo de juegos.
        /// </remarks>
        public IEnumerable<ProductosServicios> ObtenerInversoProductosServicios()
        {
            IEnumerable<ProductosServicios> result = null;
            try
            {
                result = repositorio.GetAll().Reverse();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }


        public IEnumerable<ProductosServicios> ObtenerProductosServicios()
        {
            IEnumerable<ProductosServicios> result = null;
            try
            {
                result = repositorio.GetAll();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }


        public List<ProductosServicios> ObtenerProductosServiciosBosque(int id)
        {
            IEnumerable<ProductosServicios> all = null;
            List<ProductosServicios> result = new List<ProductosServicios>();
            try
            {
                all = repositorio.GetAll();
                foreach(var item in all)
                {
                    if(item.Id_Bosque == id)
                    {
                        result.Add(item);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }
        /// <summary>
        /// Obtener fecha auditoria de ProductosServicios de la base de datos.
        /// </summary>
        /// <param name="idProductoServicio">Identificador del producto/servicio</param>
        /// <returns>Retorna cadena de caracteres</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 18/12/2017</br>
        /// <b>Descripcion:</b> Obtiene la fecha de auidtoria de los productos/servicios segun el numero de identificador que ingrese.
        /// </remarks>
        public string ObtenerFechaAuditoriaPorId(int idProductoServicio)
        {
            string result = string.Empty;
            IEnumerable<AudiProductosServicios> lista = new List<AudiProductosServicios>();
            try
            {
                Expression<Func<AudiProductosServicios, bool>> predicate = x => x.Id_ProductoServicio == idProductoServicio;
                lista = repositorioAuditoria.GetBy(predicate).ToList();
                if (lista.Count() > 0)
                {
                    result = Convert.ToString(lista.Last().FechaAuditoria);
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return result;
        }

        /// <summary>
        /// Método que retorna una coleccion de datos, segun sus parametros.
        /// </summary>
        /// <param name="pageIndex">Número de paginas.</param>
        /// <param name="pageCount">Número de paginaciones.</param>
        /// <param name="columnaOrden">Cadena de caracteres de una columna.</param>
        /// <param name="filtros">Cadena de caracteres</param>
        /// <param name="ascending">Tipo de dato verdadero o falso</param>
        /// <param name="id">Identificador del bosque</param>
        /// <returns>Retorna coleccion de ProductosServicios</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 18/12/2017</br>
        /// <b>Descripcion:</b> Método que permite obtener los datos de ProductosServicios y organizarlos con el proposito de retornarlos a una grilla.
        /// </remarks>
        public IEnumerable<ProductosServicios> ObtenerProductosServiciosBosques(int pageIndex, int pageCount, string columnaOrden, IDictionary<string, string> filtros, bool ascending, int id)
        {
            Expression<Func<ProductosServicios, bool>> predicate = x => x.Id_Bosque == id;
            Expression<Func<ProductosServicios, int>> order = x => x.Id_ProductoServicio;
            Expression<Func<ProductosServicios, DateTime>> orderDate = null;
            bool first = true;
            bool isDate = false;

            foreach (var filtro in filtros)
            {
                switch (filtro.Key)
                {
                    default:
                        predicate = x => false;
                        break;
                        //case nameof(ProductosServicios.TipoActividad):
                        //    int TipoActividad = ServicioTipo.ObtenerIdTiposActividadesPorNombre(filtro.Value);
                        //    predicate = (first ? x => x.Id_TipoActividad == TipoActividad : predicate.And(x => x.Id_TipoActividad == TipoActividad));
                        //    break;
                        //case nameof(ProductosServicios.EstadoActividad):
                        //    int EstadoActividad = ServicioEstado.ObtenerIdEstadosActividadesPorNombre(filtro.Value);
                        //    predicate = (first ? x => x.Id_EstadosActividades == EstadoActividad : predicate.And(x => x.Id_EstadosActividades == EstadoActividad));
                        //    break;
                        //case nameof(ProductosServicios.NombreActividad):
                        //    string NombreActividad = filtro.Value;
                        //    predicate = (first ? x => x.NombreActividad == NombreActividad : predicate.And(x => x.NombreActividad == NombreActividad));
                        //    break;

                        //case nameof(ProductosServicios.FechaInicioStr):
                        //    DateTime FechaInicio = Convert.ToDateTime(filtro.Value);
                        //    predicate = (first ? x => x.FechaInicio == FechaInicio : predicate.And(x => x.FechaInicio == FechaInicio));
                        //    break;

                        //case nameof(ProductosServicios.FechaFinStr):
                        //    DateTime FechaFin = Convert.ToDateTime(filtro.Value);
                        //    predicate = (first ? x => x.FechaFin == FechaFin : predicate.And(x => x.FechaFin == FechaFin));
                        //    break;

                        //case nameof(ProductosServicios.PorcentajeAvance):
                        //    int PorcentajeAvance = Convert.ToInt32(filtro.Value);
                        //    predicate = (first ? x => x.PorcentajeAvance == PorcentajeAvance : predicate.And(x => x.PorcentajeAvance == PorcentajeAvance));
                        //    break;

                        //case nameof(ProductosServicios.Avance):
                        //    string Avance = filtro.Value;
                        //    predicate = (first ? x => x.Avance == Avance : predicate.And(x => x.Avance == Avance));
                        //    break;
                }
                first = false;
            }

            CantidadRegistros = repositorio.Count(predicate);

            switch (columnaOrden)
            {
                case nameof(ProductosServicios.Id_ProductoServicio):
                    order = x => x.Id_ProductoServicio;
                    isDate = false;
                    break;
            }


            IEnumerable<ProductosServicios> result;
            if (isDate)
                result = repositorio.GetPagedElements<DateTime>(pageIndex, pageCount, orderDate, predicate, ascending);
            else
                result = repositorio.GetPagedElements<int>(pageIndex, pageCount, order, predicate, ascending);

            return result.ToList();
        }

        /// <summary>
        /// Método que retorna una coleccion de datos, segun sus parametros.
        /// </summary>
        /// <param name="pageIndex">Número de paginas.</param>
        /// <param name="pageCount">Número de paginaciones.</param>
        /// <param name="columnaOrden">Cadena de caracteres de una columna.</param>
        /// <param name="filtros">Cadena de caracteres</param>
        /// <param name="ascending">Tipo de dato verdadero o falso</param>
        /// <returns>Retorna coleccion de ProductosServicios</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 18/12/2017</br>
        /// <b>Descripcion:</b> Método que permite obtener los datos de ProductosServicios y organizarlos con el proposito de retornarlos a una grilla.
        /// </remarks>
        public IEnumerable<ProductosServicios> ObtenerProductosServiciosBosques(int pageIndex, int pageCount, string columnaOrden, IDictionary<string, string> filtros, bool ascending)
        {
            Expression<Func<ProductosServicios, bool>> predicate = x => x.Id_Bosque > 0;
            Expression<Func<ProductosServicios, int>> order = x => x.Id_Bosque;
            Expression<Func<ProductosServicios, DateTime>> orderDate = null;
            bool first = true;
            bool isDate = false;

            foreach (var filtro in filtros)
            {
                switch (filtro.Key)
                {
                    default:
                        predicate = x => false;
                        break;

                }
                first = false;
            }

            CantidadRegistros = repositorio.Count(predicate);

            switch (columnaOrden)
            {
                case nameof(ProductosServicios.Id_Bosque):
                    order = x => x.Id_Bosque;
                    isDate = false;
                    break;

            }
            
            IEnumerable<ProductosServicios> result;
            if (isDate)
                result = repositorio.GetPagedElements<DateTime>(pageIndex, pageCount, orderDate, predicate, ascending);
            else
                result = repositorio.GetPagedElements<int>(pageIndex, pageCount, order, predicate, ascending);

            return result.ToList();
        }
    }
}