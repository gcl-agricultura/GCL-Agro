﻿using BosquesPaz.Models;
using BosquesPaz.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;

namespace BosquesPaz.Services
{
    /// <summary>
    /// Clase que verifica que las peticiones de los datos sean correctas. 
    /// </summary>
    /// <remarks>
    /// <h3>Calidad</h3>
    /// </hr>
    /// <b>Requerimiento:</b> Help Desk</br>
    /// <b>Version:</b> 1</br>
    /// <b>Autor:</b> Grupo Cubo Ltda.</br>
    /// <b>Fecha:</b> 18/12/2017</br>
    /// <b>Descripcion:</b> Funciona para solicitar los datos al repositorio de estados historias y retornar los datos obtenidos.
    /// </remarks>
    public class EstadosHistoriasService
    {
        /// <summary>
        /// Propiedad que instancia la clase EstadosHistoriasRepository
        /// </summary>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 18/12/2017</br>
        /// <b>Descripcion:</b> Propiedad que instancia el repositorio EstadosHistoriasRepository, para enviar peticiones y recibir el resultado.
        /// </remarks>
        private EstadosHistoriasRepository repositorio
        {
            get
            {
                return new EstadosHistoriasRepository();
            }
        }

        /// <summary>
        /// Obtener coleccion de EstadosHistorias de la base de datos.
        /// </summary>
        /// <returns>Retorna coleccion de EstadosHistorias</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 18/12/2017</br>
        /// <b>Descripcion:</b> Obtiene una coleccion de los estados de historias.
        /// </remarks>
        public IEnumerable<EstadosHistorias> ObtenerEstadosHistorias()
        {
            IEnumerable<EstadosHistorias> estadosHistorias = new List<EstadosHistorias>();
            try
            {
                estadosHistorias = repositorio.GetAll();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return estadosHistorias;
        }

        /// <summary>
        /// Obtener identificador de EstadosHistorias de la base de datos.
        /// </summary>
        /// <param name="nombre">nombre del estado de la historia</param>
        /// <returns>Retorna numero entero</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 18/12/2017</br>
        /// <b>Descripcion:</b> Obtiene el identificador del estado de la historia segun el nombre que ingrese.
        /// </remarks>
        public int ObtenerIdEstadosHistoriasPorNombre(string nombre)
        {
            int result;
            try
            {
                Expression<Func<EstadosHistorias, bool>> predicate = x => x.Nombre == nombre;
                result = repositorio.GetBy(predicate).FirstOrDefault().Id_EstadoHistoria;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return result;
        }

        /// <summary>
        /// Obtener el nombre de EstadosHistorias de la base de datos.
        /// </summary>
        /// <param name="idEstadosHistorias">Identificador del estado de la historia</param>
        /// <returns>Retorna cadena de caracteres</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 18/12/2017</br>
        /// <b>Descripcion:</b> Obtiene el nombre del estado de la actividad segun el numero de identificador que ingrese.
        /// </remarks>
        public string ObtenerNombreEstadosHistoriasPorId(int idEstadosHistorias)
        {
            string result = string.Empty;
            try
            {
                Expression<Func<EstadosHistorias, bool>> predicate = x => x.Id_EstadoHistoria == idEstadosHistorias;
                result = repositorio.GetBy(predicate).FirstOrDefault().Nombre;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return result;
        }
    }
}