﻿using BosquesPaz.Models;
using BosquesPaz.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;

namespace BosquesPaz.Services
{
    /// <summary>
    /// Clase que verifica que las peticiones de los datos sean correctas. 
    /// </summary>
    /// <remarks>
    /// <h3>Calidad</h3>
    /// </hr>
    /// <b>Requerimiento:</b> Help Desk</br>
    /// <b>Version:</b> 1</br>
    /// <b>Autor:</b> Grupo Cubo Ltda.</br>
    /// <b>Fecha:</b> 18/12/2017</br>>
    /// <b>Descripcion:</b> Funciona para solicitar los datos al repositorio de municipios y retornar los datos obtenidos.
    /// </remarks>
    public class MunicipiosService
    {
        /// <summary>
        /// Propiedad que instancia la clase MunicipiosRepository
        /// </summary>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 18/12/2017</br>
        /// <b>Descripcion:</b> Propiedad que instancia el repositorio MunicipiosRepository, para enviar peticiones y recibir el resultado.
        /// </remarks>
        private MunicipiosRepository repositorio
        {
            get
            {
                return new MunicipiosRepository();
            }
        }

        /// <summary>
        /// Obtener coleccion del codigo dane de Municipios de la base de datos.
        /// </summary>
        /// <param name="municipios">Lista de municipios</param>
        /// <returns>Retorna coleccion de numeros enteros</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 18/12/2017</br>
        /// <b>Descripcion:</b> Obtiene una coleccion de los municipios, segun la lista que ingrese.
        /// </remarks>
        public IEnumerable<int> ObtenerCodigoDaneMunicipios(IList<int> municipios)
        {
            IEnumerable<int> result = null;
            try
            {
                Expression<Func<Municipios, bool>> predicate = x => municipios.Contains(x.Id_Municipio);
                var muni = repositorio.GetBy(predicate).ToList();
                if (muni.Count > 0)
                {
                    result = muni.Select(d => d.CodigoDane);
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }


            return result;
        }

        /// <summary>
        /// Obtener coleccion de municipios de la base de datos.
        /// </summary>
        /// <param name="idDepartamento">Identificador del departamento</param>
        /// <returns>Retorna coleccion de Municipios</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 18/12/2017</br>
        /// <b>Descripcion:</b> Obtiene una coleccion de los municipios, segun el numero de indentificador que ingrese.
        /// </remarks>
        public IEnumerable<Municipios> ObtenerMunicipiosPorIdDepartamento(int idDepartamento)
        {
            IEnumerable<Municipios> Municipios = new List<Municipios>();
            try
            {
                Expression<Func<Municipios, bool>> predicate = x => x.Id_Departamento == idDepartamento;
                Municipios = repositorio.GetBy(predicate);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return Municipios;
        }

        /// <summary>
        /// Obtener identificador del municipio de la base de datos.
        /// </summary>
        /// <param name="nombre">nombre del municipio</param>
        /// <returns>Retorna numero entero</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 18/12/2017</br>
        /// <b>Descripcion:</b> Obtiene el numero de indentificador del municipio, segun el nombre que ingrese.
        /// </remarks>
        public int ObtenerIdMunicipioPorNombre(string nombre)
        {
            int result;
            try
            {
                Expression<Func<Municipios, bool>> predicate = x => x.Nombre == nombre;
                result = repositorio.GetBy(predicate).FirstOrDefault().Id_Departamento;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return result;
        }

        /// <summary>
        /// Obtener nombre del municipio de la base de datos.
        /// </summary>
        /// <param name="idMunicipio">Identificador del municipio</param>
        /// <returns>Retorna cadena de caracteres</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 18/12/2017</br>
        /// <b>Descripcion:</b> Obtiene el nombre municipio, segun el numero de identificador que ingrese.
        /// </remarks>
        public string ObtenerNombreMunicipioPorId(int idMunicipio)
        {
            string result = string.Empty;
            try
            {
                Expression<Func<Municipios, bool>> predicate = x => x.Id_Municipio == idMunicipio;
                result = repositorio.GetBy(predicate).FirstOrDefault().Nombre;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return result;
        }
    }
}