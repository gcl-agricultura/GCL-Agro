﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BosquesPaz.Models;
using BosquesPaz.Repositories;
using System.Collections;

namespace BosquesPaz.Services
{
    /// <summary>
    /// Clase que verifica que las peticiones de los datos sean correctas. 
    /// </summary>
    /// <remarks>
    /// <h3>Calidad</h3>
    /// </hr>
    /// <b>Requerimiento:</b> Help Desk</br>
    /// <b>Version:</b> 1</br>
    /// <b>Autor:</b> Grupo Cubo Ltda.</br>
    /// <b>Fecha:</b> 18/12/2017</br>
    /// <b>Descripcion:</b> Funciona para solicitar los datos al repositorio de usuarios juegos y retornar los datos obtenidos.
    /// </remarks>
    public class UsuariosJuegosService
    {
        /// <summary>
        /// Propiedad que instancia la clase UsuariosJuegosRepository
        /// </summary>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 18/12/2017</br>
        /// <b>Descripcion:</b> Propiedad que instancia el repositorio UsuariosJuegosRepository, para enviar peticiones y recibir el resultado.
        /// </remarks>
        private UsuariosJuegosRepository repositorio { get { return new UsuariosJuegosRepository(); } }

        /// <summary>
        /// Propiedad que instancia la clase NivelesPuntajesRepository
        /// </summary>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 18/12/2017</br>
        /// <b>Descripcion:</b> Propiedad que instancia el repositorio NivelesPuntajesRepository, para enviar peticiones y recibir el resultado.
        /// </remarks>
        private NivelesPuntajesRepository repositorioNivel { get{ return new NivelesPuntajesRepository(); } }

        /// <summary>
        /// Guardar una instancia de UsuariosJuegos en la base de datos.
        /// </summary>
        /// <param name="usuarioJuego">Instancia de UsuariosJuegos</param>
        /// <returns>Retorna instancia de UsuariosJuegos</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 18/12/2017</br>
        /// <b>Descripcion:</b> Recibe una instancia de UsuariosJuegos y la envia a la clase de repositorio, y retorna la instancia.
        /// </remarks>
        public UsuariosJuegos GuardarUsuarioJuego(UsuariosJuegos usuarioJuego)
        {
            try
            {
                usuarioJuego = repositorio.Create(usuarioJuego);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return usuarioJuego;
        }

        /// <summary>
        /// Obtener total de puntos de UsuariosJuegos.
        /// </summary>
        /// <param name="usuario">Nombre de un usuario</param>
        /// <param name="modulo">Modulo del juego</param>
        /// <param name="juego">Nombre del juego</param>
        /// <returns>Retorna numero entero</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 18/12/2017</br>
        /// <b>Descripcion:</b> Realiza la consulta del usuario de juegos segun el usario, modulo y nombre del juego y los retorna el total de puntos obtenidos en el juego sopa de letras.
        /// </remarks>
        public int PuntajeSopa(string usuario, string modulo, string juego)
        {
            IEnumerable<UsuariosJuegos> user = null;
            IEnumerable<NivelesPuntajes> puntaje = null;
            int total = 0;
            try
            {
                user = repositorio.ObtenerUsuarioJuegoPorNickName(usuario, modulo, juego);
                foreach (var i in user)
                {
                    puntaje = repositorioNivel.ObtenerNivelPuntaje(i.Id_Puntaje);
                    foreach (var j in puntaje)
                    {
                        total += j.NivelPuntaje;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return total;
        }

        /// <summary>
        /// Obtener total de puntos de UsuariosJuegos.
        /// </summary>
        /// <param name="usuario">Nombre de un usuario</param>
        /// <param name="modulo">Modulo del juego</param>
        /// <param name="juego">Nombre del juego</param>
        /// <returns>Retorna numero entero</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 18/12/2017</br>
        /// <b>Descripcion:</b> Realiza la consulta del usuario de juegos segun el usario, modulo y nombre del juego y los retorna el total de puntos obtenidos en el juego rompecabezas.
        /// </remarks>
        public int PuntajeRompe(string usuario, string modulo, string juego)
        {
            IEnumerable<UsuariosJuegos> user = null;
            IEnumerable<NivelesPuntajes> puntaje = null;
            int total = 0;
            try
            {
                user = repositorio.ObtenerUsuarioJuegoPorNickName(usuario, modulo, juego);
                foreach (var i in user)
                {
                    puntaje = repositorioNivel.ObtenerNivelPuntaje(i.Id_Puntaje);
                    foreach (var j in puntaje)
                    {
                        total += j.NivelPuntaje;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return total;
        }
    }
}