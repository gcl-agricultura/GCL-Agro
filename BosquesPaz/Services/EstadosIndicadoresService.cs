﻿using BosquesPaz.Models;
using BosquesPaz.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;

namespace BosquesPaz.Services
{
    /// <summary>
    /// Clase que verifica que las peticiones de los datos sean correctas. 
    /// </summary>
    /// <remarks>
    /// <h3>Calidad</h3>
    /// </hr>
    /// <b>Requerimiento:</b> Help Desk</br>
    /// <b>Version:</b> 1</br>
    /// <b>Autor:</b> Grupo Cubo Ltda.</br>
    /// <b>Fecha:</b> 18/12/2017</br>
    /// <b>Descripcion:</b> Funciona para solicitar los datos al repositorio de estados indicadores y retornar los datos obtenidos.
    /// </remarks>
    public class EstadosIndicadoresService
    {
        /// <summary>
        /// Propiedad que instancia la clase EstadosIndicadoresRepository
        /// </summary>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 18/12/2017</br>
        /// <b>Descripcion:</b> Propiedad que instancia el repositorio EstadosIndicadoresRepository, para enviar peticiones y recibir el resultado.
        /// </remarks>
        private EstadosIndicadoresRepository repositorio
        {
            get
            {
                return new EstadosIndicadoresRepository();
            }
        }

        /// <summary>
        /// Obtener coleccion de EstadosIndicadores de la base de datos.
        /// </summary>
        /// <returns>Retorna coleccion de EstadosIndicadores</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 18/12/2017</br>
        /// <b>Descripcion:</b> Obtiene una coleccion de los estados de indicadores.
        /// </remarks>
        public IEnumerable<EstadosIndicadores> ObtenerEstadosIndicadores()
        {
            IEnumerable<EstadosIndicadores> estadosIndicadores = new List<EstadosIndicadores>();
            try
            {
                estadosIndicadores = repositorio.GetAll();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return estadosIndicadores;
        }

        /// <summary>
        /// Obtener identificador de EstadosIndicadores de la base de datos.
        /// </summary>
        /// <param name="nombre">nombre del estado del indicador</param>
        /// <returns>Retorna numero entero</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 18/12/2017</br>
        /// <b>Descripcion:</b> Obtiene el identificador del estado del indicador segun el nombre que ingrese.
        /// </remarks>
        public int ObtenerIdEstadoIndicadorPorNombre(string nombre)
        {
            int result;
            try
            {
                Expression<Func<EstadosIndicadores, bool>> predicate = x => x.Nombre == nombre;
                result = repositorio.GetBy(predicate).FirstOrDefault().Id_EstadoIndicador;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return result;
        }

        /// <summary>
        /// Obtener el nombre de EstadosIndicadores de la base de datos.
        /// </summary>
        /// <param name="idEstadoIndicador">Indentificador del estado del indicador</param>
        /// <returns>Retorna cadena de caracteres</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 18/12/2017</br>
        /// <b>Descripcion:</b> Obtiene el nombre del estado del indicador segun el numero de identificador que ingrese.
        /// </remarks>
        public string ObtenerNombreEstadoIndicadorPorId(int idEstadoIndicador)
        {
            string result = string.Empty;
            try
            {
                Expression<Func<EstadosIndicadores, bool>> predicate = x => x.Id_EstadoIndicador == idEstadoIndicador;
                result = repositorio.GetBy(predicate).FirstOrDefault().Nombre;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return result;
        }
    }
}