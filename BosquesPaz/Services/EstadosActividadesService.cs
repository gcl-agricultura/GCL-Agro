﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BosquesPaz.Models;
using System.Linq.Expressions;
using BosquesPaz.Repositories;

namespace BosquesPaz.Services
{
    /// <summary>
    /// Clase que verifica que las peticiones de los datos sean correctas. 
    /// </summary>
    /// <remarks>
    /// <h3>Calidad</h3>
    /// </hr>
    /// <b>Requerimiento:</b> Help Desk</br>
    /// <b>Version:</b> 1</br>
    /// <b>Autor:</b> Grupo Cubo Ltda.</br>
    /// <b>Fecha:</b> 18/12/2017</br>
    /// <b>Descripcion:</b> Funciona para solicitar los datos al repositorio de estados actividades y retornar los datos obtenidos.
    /// </remarks>
    public class EstadosActividadesService
    {
        /// <summary>
        /// Propiedad que instancia la clase EstadosActividadesRepository
        /// </summary>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 18/12/2017</br>
        /// <b>Descripcion:</b> Propiedad que instancia el repositorio EstadosActividadesRepository, para enviar peticiones y recibir el resultado.
        /// </remarks>
        private EstadosActividadesRepository repositorio
        {
            get
            {
                return new EstadosActividadesRepository();
            }
        }

        /// <summary>
        /// Obtener coleccion de EstadosActividades de la base de datos.
        /// </summary>
        /// <returns>Retorna coleccion de EstadosActividades</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 18/12/2017</br>
        /// <b>Descripcion:</b> Obtiene una coleccion de los estados de actividades.
        /// </remarks>
        public IEnumerable<EstadosActividades> ObtenerEstadosActividades()
        {
            IEnumerable<EstadosActividades> estadosActividades = new List<EstadosActividades>();
            try
            {
                estadosActividades = repositorio.GetAll();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return estadosActividades;
        }

        /// <summary>
        /// Obtener identificador de EstadosActividades de la base de datos.
        /// </summary>
        /// <param name="nombre">nombre del estado de la actividad</param>
        /// <returns>Retorna numero entero</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 18/12/2017</br>
        /// <b>Descripcion:</b> Obtiene el identificador del estado de la actividad segun el nombre que ingrese.
        /// </remarks>
        public int ObtenerIdEstadosActividadesPorNombre(string nombre)
        {
            int result;
            try
            {
                Expression<Func<EstadosActividades, bool>> predicate = x => x.Nombre == nombre;
                result = repositorio.GetBy(predicate).FirstOrDefault().Id_EstadosActividades; 
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }

        /// <summary>
        /// Obtener el nombre de EstadosActividades de la base de datos.
        /// </summary>
        /// <param name="idEstadoActividad">Identificador del estado de la actividad</param>
        /// <returns>Retorna cadena de caracteres</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 18/12/2017</br>
        /// <b>Descripcion:</b> Obtiene el nombre del estado de la actividad segun el numero de identificador que ingrese.
        /// </remarks>
        public string ObtenerNombreEstadoActividadPorId(int idEstadoActividad)
        {
            string result = string.Empty;
            try
            {
                Expression<Func<EstadosActividades, bool>> predicate = x => x.Id_EstadosActividades == idEstadoActividad;
                result = repositorio.GetBy(predicate).FirstOrDefault().Nombre;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }
    }
}