﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BosquesPaz.Models;
using System.Linq.Expressions;
using BosquesPaz.Repositories;

namespace BosquesPaz.Services
{
    /// <summary>
    /// Clase que verifica que las peticiones de los datos sean correctas. 
    /// </summary>
    /// <remarks>
    /// <h3>Calidad</h3>
    /// </hr>
    /// <b>Requerimiento:</b> Help Desk</br>
    /// <b>Version:</b> 1</br>
    /// <b>Autor:</b> Grupo Cubo Ltda.</br>
    /// <b>Fecha:</b> 18/12/2017</br>
    /// <b>Descripcion:</b> Funciona para solicitar los datos al repositorio de tipos de productos/servicios y retornar los datos obtenidos.
    /// </remarks>
    public class TiposProductosServiciosService
    {
        /// <summary>
        /// Propiedad que instancia la clase TiposProductosServiciosRepository
        /// </summary>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 18/12/2017</br>
        /// <b>Descripcion:</b> Propiedad que instancia el repositorio TiposProductosServiciosRepository, para enviar peticiones y recibir el resultado.
        /// </remarks>
        private TiposProductosServiciosRepository repositorio
        {
            get
            {
                return new TiposProductosServiciosRepository();
            }
        }

        /// <summary>
        /// Obtener coleccion de TiposProductosServicios de la base de datos.
        /// </summary>
        /// <returns>Retorna coleccion de TiposProductosServicios</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 18/12/2017</br>
        /// <b>Descripcion:</b> Obtiene una coleccion de los tipos de productos/servicios.
        /// </remarks>
        public IEnumerable<TiposProductosServicios> ObtenerTiposProductosServicios()
        {
            IEnumerable<TiposProductosServicios> tipoProductosServicios = new List<TiposProductosServicios>();
            try
            {
                tipoProductosServicios = repositorio.GetAll();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return tipoProductosServicios;
        }

        /// <summary>
        /// Obtener identificador de TiposProductosServicios de la base de datos.
        /// </summary>
        /// <param name="nombre">Nombre del tipo de productos/servicios</param>
        /// <returns>Retorna numero entero</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 18/12/2017</br>
        /// <b>Descripcion:</b> Obtiene el identificador del tipo de productos/servicios segun el nombre que se ingrese.
        /// </remarks>
        public int ObtenerIdTipoProductoServicioPorNombre(string nombre)
        {
            int result;
            try
            {
                Expression<Func<TiposProductosServicios, bool>> predicate = x => x.Nombre == nombre;
                result = repositorio.GetBy(predicate).FirstOrDefault().Id_TipoProductoServicio;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }

        /// <summary>
        /// Obtener nombre de TiposProductosServicios de la base de datos.
        /// </summary>
        /// <param name="idProductoServicio">Identificador de tipo de producto/servicio</param>
        /// <returns>Retorna cadena de caracteres</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 18/12/2017</br>
        /// <b>Descripcion:</b> Obtiene el nombre del tipo de producto/servicio segun el numero de identificador que se ingrese.
        /// </remarks>
        public string ObtenerNombreTipoProductoServicioPorId(int idProductoServicio)
        {
            string result;
            try
            {
                Expression<Func<TiposProductosServicios, bool>> predicate = x => x.Id_TipoProductoServicio == idProductoServicio;
                result = repositorio.GetBy(predicate).FirstOrDefault().Nombre;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return result;
        }
    }
}