﻿using BosquesPaz.Models;
using BosquesPaz.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BosquesPaz.Services
{
    /// <summary>
    /// Clase que verifica que las peticiones de los datos sean correctas. 
    /// </summary>
    /// <remarks>
    /// <h3>Calidad</h3>
    /// </hr>
    /// <b>Requerimiento:</b> Help Desk</br>
    /// <b>Version:</b> 1</br>
    /// <b>Autor:</b> Grupo Cubo Ltda.</br>
    /// <b>Fecha:</b> 18/12/2017</br>
    /// <b>Descripcion:</b> Funciona para solicitar los datos al repositorio de combos y retornar los datos obtenidos.
    /// </remarks>
    public class CombosService
    {
        /// <summary>
        /// Propiedad que instancia la clase CombosRepository
        /// </summary>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 18/12/2017</br>
        /// <b>Descripcion:</b> Propiedad que instancia el repositorio CombosRepository, para enviar peticiones y recibir el resultado.
        /// </remarks>
        private CombosRepository repositorio
        {
            get
            {
                return new CombosRepository();
            }
        }

        /// <summary>
        /// Obtener coleccion de Departamentos de la base de datos.
        /// </summary>
        /// <returns>Retorna coleccion de Departamentos</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 18/12/2017</br>
        /// <b>Descripcion:</b> Obtiene una coleccion de los departamentos.
        /// </remarks>
        public IEnumerable<Departamentos> ObtenerDepartamentos()
        {
            IEnumerable<Departamentos> departamentos = new List<Departamentos>();
            try
            {
                departamentos = repositorio.ObtenerDepartamentos();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return departamentos;
        }

        /// <summary>
        /// Obtener coleccion de Municipios de la base de datos.
        /// </summary>
        /// <param name="idDepartamento">Identificador del departamento</param>
        /// <returns>Retorna coleccion de Municipios</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 18/12/2017</br>
        /// <b>Descripcion:</b> Obtiene una coleccion de los municipios.
        /// </remarks>
        public IEnumerable<Municipios> ObtenerMunicipios(int idDepartamento)
        {
            IEnumerable<Municipios> Municipios = new List<Municipios>();
            try
            {
                Municipios = repositorio.ObtenerMunicipios(idDepartamento);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return Municipios;
        }

        /// <summary>
        /// Obtener coleccion de TiposRecursos de la base de datos.
        /// </summary>
        /// <returns>Retorna coleccion de TiposRecursos</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 18/12/2017</br>
        /// <b>Descripcion:</b> Obtiene una coleccion de los tipos de recursos.
        /// </remarks>
        public IEnumerable<TiposRecursos> ObtenerTiposRecursos()
        {
            IEnumerable<TiposRecursos> TiposRecursos = new List<TiposRecursos>();
            try
            {
                TiposRecursos = repositorio.ObtenerTiposRecursos();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return TiposRecursos;
        }

        /// <summary>
        /// Obtener coleccion de Estados de la base de datos.
        /// </summary>
        /// <returns>Retorna coleccion de Estados</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 18/12/2017</br>
        /// <b>Descripcion:</b> Obtiene una coleccion de los estados de recursos.
        /// </remarks>
        public IEnumerable<Estados> ObtnerEstados()
        {
            IEnumerable<Estados> Estados = new List<Estados>();
            try
            {
                Estados = repositorio.ObtenerEstados();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return Estados;
        }

        /// <summary>
        /// Obtener coleccion de TiposActividades de la base de datos.
        /// </summary>
        /// <returns>Retorna coleccion de TiposActividades</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 18/12/2017</br>
        /// <b>Descripcion:</b> Obtiene una coleccion de los tipos de actividades.
        /// </remarks>
        public IEnumerable<TiposActividades> ObtenerTiposActividades()
        {
            IEnumerable<TiposActividades> TiposActividades = new List<TiposActividades>();
            try
            {
                TiposActividades = repositorio.ObtenerTiposActividades();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return TiposActividades;
        }

        /// <summary>
        /// Obtener coleccion de EstadosActividades de la base de datos.
        /// </summary>
        /// <returns>Retorna coleccion de EstadosActividades</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 18/12/2017</br>
        /// <b>Descripcion:</b> Obtiene una coleccion de los estados de actividades.
        /// </remarks>
        public IEnumerable<EstadosActividades> ObtenerEstadosActividades()
        {
            IEnumerable<EstadosActividades> EstadosActividades = new List<EstadosActividades>();
            try
            {
                EstadosActividades = repositorio.ObtenerEstadosActividades();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return EstadosActividades;
        }

        /// <summary>
        /// Obtener coleccion de TiposIndividuosEspecies de la base de datos.
        /// </summary>
        /// <returns>Retorna coleccion de TiposIndividuosEspecies</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 18/12/2017</br>
        /// <b>Descripcion:</b> Obtiene una coleccion de los tipos de individuos/especies.
        /// </remarks>
        public IEnumerable<TiposIndividuosEspecies> ObtenerTiposIndividuosEspecies()
        {
            IEnumerable<TiposIndividuosEspecies> TiposIndividuosEspecies = new List<TiposIndividuosEspecies>();
            try
            {
                TiposIndividuosEspecies = repositorio.ObtenerTiposIndividuosEspecies();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return TiposIndividuosEspecies;
        }

        /// <summary>
        /// Obtener coleccion de UnidadesIndividuosEspecies de la base de datos.
        /// </summary>
        /// <returns>Retorna coleccion de UnidadesIndividuosEspecies</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 18/12/2017</br>
        /// <b>Descripcion:</b> Obtiene una coleccion de las unidades de individuos/especies.
        /// </remarks>
        public IEnumerable<UnidadesIndividuosEspecies> ObtenerUnidadesIndividuosEspecies()
        {
            IEnumerable<UnidadesIndividuosEspecies> UnidadesIndividuosEspecies = new List<UnidadesIndividuosEspecies>();
            try
            {
                UnidadesIndividuosEspecies = repositorio.ObtenerUnidadesIndividuosEspecies();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return UnidadesIndividuosEspecies;
        }

        /// <summary>
        /// Obtener coleccion de TiposIndicadores de la base de datos.
        /// </summary>
        /// <returns>Retorna coleccion de TiposIndicadores</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 18/12/2017</br>
        /// <b>Descripcion:</b> Obtiene una coleccion de los tipos de indicadores.
        /// </remarks>
        public IEnumerable<TiposIndicadores> ObtenerTiposIndicadores()
        {
            IEnumerable<TiposIndicadores> TiposIndicadores = new List<TiposIndicadores>();
            try
            {
                TiposIndicadores = repositorio.ObtenerTiposIndicadores();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return TiposIndicadores;
        }

        /// <summary>
        /// Obtener coleccion de EstadosIndicadores de la base de datos.
        /// </summary>
        /// <returns>Retorna coleccion de EstadosIndicadores</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 18/12/2017</br>
        /// <b>Descripcion:</b> Obtiene una coleccion de los estados de indicadores.
        /// </remarks>
        public IEnumerable<EstadosIndicadores> ObtenerEstadosIndicadores()
        {
            IEnumerable<EstadosIndicadores> EstadosIndicadores = new List<EstadosIndicadores>();
            try
            {
                EstadosIndicadores = repositorio.ObtenerEstadosIndicadores();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return EstadosIndicadores;
        }

        /// <summary>
        /// Obtener coleccion de TiposProductosServicios de la base de datos.
        /// </summary>
        /// <returns>Retorna coleccion de TiposProductosServicios</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 18/12/2017</br>
        /// <b>Descripcion:</b> Obtiene una coleccion de los tipos de productos/servicios.
        /// </remarks>
        public IEnumerable<TiposProductosServicios> ObtenerTiposProductosServicios()
        {
            IEnumerable<TiposProductosServicios> TiposProductosServicios = new List<TiposProductosServicios>();
            try
            {
                TiposProductosServicios = repositorio.ObtenerTiposProductosServicios();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return TiposProductosServicios;
        }

        /// <summary>
        /// Obtener coleccion de EstadosProductosServicios de la base de datos.
        /// </summary>
        /// <returns>Retorna coleccion de EstadosProductosServicios</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 18/12/2017</br>
        /// <b>Descripcion:</b> Obtiene una coleccion de los estados de productos/servicios.
        /// </remarks>
        public IEnumerable<EstadosProductosServicios> ObtenerEstadosProductosServicios()
        {
            IEnumerable<EstadosProductosServicios> EstadosProductosServicios = new List<EstadosProductosServicios>();
            try
            {
                EstadosProductosServicios = repositorio.ObtenerEstadosProductosServicios();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return EstadosProductosServicios;
        }

        /// <summary>
        /// Obtener coleccion de EstadosFichas de la base de datos.
        /// </summary>
        /// <returns>Retorna coleccion de EstadosFichas</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 18/12/2017</br>
        /// <b>Descripcion:</b> Obtiene una coleccion de los estados de fichas.
        /// </remarks>
        public IEnumerable<EstadosFichas> ObtenerEstadosFichas()
        {
            IEnumerable<EstadosFichas> EstadosFichas = new List<EstadosFichas>();
            try
            {
                EstadosFichas = repositorio.ObtenerEstadosFichas();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return EstadosFichas;
        }

        /// <summary>
        /// Obtener coleccion de Tematicas de la base de datos.
        /// </summary>
        /// <returns>Retorna coleccion de Tematicas</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 18/12/2017</br>
        /// <b>Descripcion:</b> Obtiene una coleccion de las tematicas de Historias.
        /// </remarks>
        public IEnumerable<Tematicas> ObtenerTematicas()
        {
            IEnumerable<Tematicas> Tematicas = new List<Tematicas>();
            try
            {
                Tematicas = repositorio.ObtenerTematicas();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return Tematicas;
        }

        /// <summary>
        /// Obtener coleccion de EstadosHistorias de la base de datos.
        /// </summary>
        /// <returns>Retorna coleccion de EstadosHistorias</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 18/12/2017</br>
        /// <b>Descripcion:</b> Obtiene una coleccion de los estados de Historias.
        /// </remarks>
        public IEnumerable<EstadosHistorias> ObtenerEstadosHistorias()
        {
            IEnumerable<EstadosHistorias> EstadosHistorias = new List<EstadosHistorias>();
            try
            {
                EstadosHistorias = repositorio.ObtenerEstadosHistorias();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return EstadosHistorias;
        }

        /// <summary>
        /// Obtener coleccion de TiposPatrocinadores de la base de datos.
        /// </summary>
        /// <returns>Retorna coleccion de TiposPatrocinadores</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 18/12/2017</br>
        /// <b>Descripcion:</b> Obtiene una coleccion de los tipos de patrocinadores.
        /// </remarks>
        public IEnumerable<TiposPatrocinadores> ObtenerTiposPatrocinadores()
        {
            IEnumerable<TiposPatrocinadores> TiposPatrocinadores = new List<TiposPatrocinadores>();
            try
            {
                TiposPatrocinadores = repositorio.ObtenerTiposPatrocinadores();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return TiposPatrocinadores;
        }

        /// <summary>
        /// Obtener coleccion de EstadosPatrocinadores de la base de datos.
        /// </summary>
        /// <returns>Retorna coleccion de EstadosPatrocinadores</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 18/12/2017</br>
        /// <b>Descripcion:</b> Obtiene una coleccion de los estados de patrocinadores.
        /// </remarks>
        public IEnumerable<EstadosPatrocinadores> ObtenerEstadosPatrocinadores()
        {
            IEnumerable<EstadosPatrocinadores> EstadosPatrocinadores = new List<EstadosPatrocinadores>();
            try
            {
                EstadosPatrocinadores = repositorio.ObtenerEstadosPatrocinadores();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return EstadosPatrocinadores;
        }

        /// <summary>
        /// Obtener coleccion de TiposPatrocinadores para compras alianzas de la base de datos.
        /// </summary>
        /// <returns>Retorna coleccion de TiposPatrocinadores</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 29/12/2017</br>
        /// <b>Descripcion:</b> Obtiene una coleccion de los tipos de patrocinadores para compras alianzas.
        /// </remarks>
        public IEnumerable<TiposPatrocinadores> ObtenerTiposPatrocinadoresComprasAlianzas()
        {
            IEnumerable<TiposPatrocinadores> TiposPatrocinadoresComprasAlianzas = new List<TiposPatrocinadores>();
            try
            {
                TiposPatrocinadoresComprasAlianzas = repositorio.ObtnerPatrocinadoresComprasAlianzas();
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return TiposPatrocinadoresComprasAlianzas;
        }

        /// <summary>
        /// Obtener coleccion de Bosques de la base de datos.
        /// </summary>
        /// <returns>Retorna coleccion de Bosques</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 18/12/2017</br>
        /// <b>Descripcion:</b> Obtiene una coleccion de los Bosques.
        /// </remarks>
        public IEnumerable<Bosques> ObtenerBosques()
        {
            IEnumerable<Bosques> Bosques = new List<Bosques>();
            try
            {
                Bosques = repositorio.ObtenerBosques();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return Bosques;
        }
    }
}