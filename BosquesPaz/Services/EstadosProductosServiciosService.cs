﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BosquesPaz.Models;
using System.Linq.Expressions;
using BosquesPaz.Repositories;

namespace BosquesPaz.Services
{
    /// <summary>
    /// Clase que verifica que las peticiones de los datos sean correctas. 
    /// </summary>
    /// <remarks>
    /// <h3>Calidad</h3>
    /// </hr>
    /// <b>Requerimiento:</b> Help Desk</br>
    /// <b>Version:</b> 1</br>
    /// <b>Autor:</b> Grupo Cubo Ltda.</br>
    /// <b>Fecha:</b> 18/12/2017</br>
    /// <b>Descripcion:</b> Funciona para solicitar los datos al repositorio de estados productos/servicios y retornar los datos obtenidos.
    /// </remarks>
    public class EstadosProductosServiciosService
    {
        /// <summary>
        /// Propiedad que instancia la clase EstadosProductosServiciosRepository
        /// </summary>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 18/12/2017</br>
        /// <b>Descripcion:</b> Propiedad que instancia el repositorio EstadosProductosServiciosRepository, para enviar peticiones y recibir el resultado.
        /// </remarks>
        private EstadosProductosServiciosRepository repositorio
        {
            get
            {
                return new EstadosProductosServiciosRepository();
            }
        }

        /// <summary>
        /// Obtener coleccion de EstadosProductosServicios de la base de datos.
        /// </summary>
        /// <returns>Retorna coleccion de EstadosProductosServicios</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 18/12/2017</br>
        /// <b>Descripcion:</b> Obtiene una coleccion de los estados de productos/servicios.
        /// </remarks>
        public IEnumerable<EstadosProductosServicios> ObtenerEstadosProductosServicios()
        {
            IEnumerable<EstadosProductosServicios> estadosProductosServicios = new List<EstadosProductosServicios>();
            try
            {
                estadosProductosServicios = repositorio.GetAll();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return estadosProductosServicios;
        }

        /// <summary>
        /// Obtener identificador de EstadosProductosServicios de la base de datos.
        /// </summary>
        /// <param name="nombre">nombre del estado del producto/servicio</param>
        /// <returns>Retorna numero entero</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 18/12/2017</br>
        /// <b>Descripcion:</b> Obtiene el identificador del estado del estado producto/servicio segun el nombre que ingrese.
        /// </remarks>
        public int ObtenerIdEstadosProductoServicioPorNombre(string nombre)
        {
            int result;
            try
            {
                Expression<Func<EstadosProductosServicios, bool>> predicate = x => x.Nombre == nombre;
                result = repositorio.GetBy(predicate).FirstOrDefault().Id_EstadoProductoServicio;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }

        /// <summary>
        /// Obtener el nombre de EstadosProductosServicios de la base de datos.
        /// </summary>
        /// <param name="idEstadoProductoServicio">Identificador del estado del producto/servicio</param>
        /// <returns>Retorna cadena de caracteres</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 18/12/2017</br>
        /// <b>Descripcion:</b> Obtiene el nombre del estado del producto/servicio segun el numero de identificador que ingrese.
        /// </remarks>
        public string ObtenerNombreProductoServicioPorId(int idEstadoProductoServicio)
        {
            string result = string.Empty;
            try
            {
                Expression<Func<EstadosProductosServicios, bool>> predicate = x => x.Id_EstadoProductoServicio == idEstadoProductoServicio;
                result = repositorio.GetBy(predicate).FirstOrDefault().Nombre;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }
    }
}