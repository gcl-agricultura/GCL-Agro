﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BosquesPaz.Models;
using BosquesPaz.Repositories;
using System.Linq.Expressions;

namespace BosquesPaz.Services
{
    /// <summary>
    /// Clase que verifica que las peticiones de los datos sean correctas. 
    /// </summary>
    /// <remarks>
    /// <h3>Calidad</h3>
    /// </hr>
    /// <b>Requerimiento:</b> Help Desk</br>
    /// <b>Version:</b> 1</br>
    /// <b>Autor:</b> Grupo Cubo Ltda.</br>
    /// <b>Fecha:</b> 18/12/2017</br>
    /// <b>Descripcion:</b> Funciona para solicitar los datos al repositorio de indicadores y retornar los datos obtenidos.
    /// </remarks>
    public class IndicadoresService
    {
        /// <summary>
        /// Propiedad que instancia la clase IndicadoresRepository
        /// </summary>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 18/12/2017</br>
        /// <b>Descripcion:</b> Propiedad que instancia el repositorio IndicadoresRepository, para enviar peticiones y recibir el resultado.
        /// </remarks>
        private IndicadoresRepository repositorio { get { return new IndicadoresRepository(); } }

        /// <summary>
        /// Propiedad de número entero
        /// </summary>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 18/12/2017</br>
        /// <b>Descripcion:</b> Propiedad que define un número entero, para verificar el numero de registros que existen en una entidad.
        /// </remarks>
        public int CantidadRegistros { get; private set; }

        /// <summary>
        /// Guardar una instancia de IndicadoresBosques en la base de datos.
        /// </summary>
        /// <param name="indicador">Instancia de IndicadoresBosques</param>
        /// <returns>Retorna instancia de IndicadoresBosques</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 18/12/2017</br>
        /// <b>Descripcion:</b> Recibe una instancia de IndicadoresBosques y la envia a la clase de repositorio, y retorna la instancia.
        /// </remarks>
        public IndicadoresBosques GuardarIndicador(IndicadoresBosques indicador)
        {
            try
            {
                indicador = repositorio.Create(indicador);
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return indicador;
        }

        /// <summary>
        /// Actualizar datos de IndicadoresBosques en la base de datos.
        /// </summary>
        /// <param name="indicador">Instancia de IndicadoresBosques</param>
        /// <returns>Retorna valor booleano</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 18/12/2017</br>
        /// <b>Descripcion:</b> Recibe una instancia de IndicadoresBosques y la envia a la clase de repositorio, y retorna un valor booleano.
        /// </remarks>
        public bool ActualizarIndicador(IndicadoresBosques indicador)
        {
            bool result = false;
            try
            {
                if (repositorio.Update(indicador))
                {
                    result = true;
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return result;
        }

        /// <summary>
        /// Obtener datos de IndicadoresBosques de la base de datos.
        /// </summary>
        /// <param name="idIndicadorBosque">Identificador del indicador</param>
        /// <returns>Retorna instancia de IndicadoresBosques</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 18/12/2017</br>
        /// <b>Descripcion:</b> Obtiene una instancia del indicador segun el numero de identificador que ingrese.
        /// </remarks>
        public IndicadoresBosques ObtenerIndicadoresBosques(int idIndicadorBosque)
        {
            IndicadoresBosques result = null;
            try
            {
                result = repositorio.GetByKey(idIndicadorBosque);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }

        /// <summary>
        /// Método que retorna una coleccion de datos, segun sus parametros.
        /// </summary>
        /// <param name="pageIndex">Número de paginas.</param>
        /// <param name="pageCount">Número de paginaciones.</param>
        /// <param name="columnaOrden">Cadena de caracteres de una columna.</param>
        /// <param name="filtros">Cadena de caracteres</param>
        /// <param name="ascending">Tipo de dato verdadero o falso</param>
        /// <param name="id">Identificador del bosque</param>
        /// <returns>Retorna coleccion de IndicadoresBosques</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 18/12/2017</br>
        /// <b>Descripcion:</b> Método que permite obtener los datos de IndicadoresBosques y organizarlos con el proposito de retornarlos a una grilla.
        /// </remarks>
        public IEnumerable<IndicadoresBosques> ObtenerIndicadoresBosquesBosques(int pageIndex, int pageCount, string columnaOrden, IDictionary<string, string> filtros, bool ascending, int id)
        {
            Expression<Func<IndicadoresBosques, bool>> predicate = x => x.Id_Bosque == id;
            Expression<Func<IndicadoresBosques, int>> order = x => x.Id_IndicadorBosque;
            Expression<Func<IndicadoresBosques, DateTime>> orderDate = null;
            bool first = true;
            bool isDate = false;

            foreach (var filtro in filtros)
            {
                switch (filtro.Key)
                {
                    default:
                        predicate = x => false;
                        break;
                    //case nameof(IndicadoresBosques.TipoActividad):
                    //    int TipoActividad = ServicioTipo.ObtenerIdTiposActividadesPorNombre(filtro.Value);
                    //    predicate = (first ? x => x.Id_TipoActividad == TipoActividad : predicate.And(x => x.Id_TipoActividad == TipoActividad));
                    //    break;
                    //case nameof(IndicadoresBosques.EstadoActividad):
                    //    int EstadoActividad = ServicioEstado.ObtenerIdEstadosActividadesPorNombre(filtro.Value);
                    //    predicate = (first ? x => x.Id_EstadosActividades == EstadoActividad : predicate.And(x => x.Id_EstadosActividades == EstadoActividad));
                    //    break;
                    //case nameof(IndicadoresBosques.NombreActividad):
                    //    string NombreActividad = filtro.Value;
                    //    predicate = (first ? x => x.NombreActividad == NombreActividad : predicate.And(x => x.NombreActividad == NombreActividad));
                    //    break;

                    //case nameof(IndicadoresBosques.FechaInicioStr):
                    //    DateTime FechaInicio = Convert.ToDateTime(filtro.Value);
                    //    predicate = (first ? x => x.FechaInicio == FechaInicio : predicate.And(x => x.FechaInicio == FechaInicio));
                    //    break;

                    //case nameof(IndicadoresBosques.FechaFinStr):
                    //    DateTime FechaFin = Convert.ToDateTime(filtro.Value);
                    //    predicate = (first ? x => x.FechaFin == FechaFin : predicate.And(x => x.FechaFin == FechaFin));
                    //    break;

                    //case nameof(IndicadoresBosques.PorcentajeAvance):
                    //    int PorcentajeAvance = Convert.ToInt32(filtro.Value);
                    //    predicate = (first ? x => x.PorcentajeAvance == PorcentajeAvance : predicate.And(x => x.PorcentajeAvance == PorcentajeAvance));
                    //    break;

                    //case nameof(IndicadoresBosques.Avance):
                    //    string Avance = filtro.Value;
                    //    predicate = (first ? x => x.Avance == Avance : predicate.And(x => x.Avance == Avance));
                    //    break;
                }
                first = false;
            }

            CantidadRegistros = repositorio.Count(predicate);

            switch (columnaOrden)
            {
                case nameof(IndicadoresBosques.Id_IndicadorBosque):
                    order = x => x.Id_IndicadorBosque;
                    isDate = false;
                    break;
            }


            IEnumerable<IndicadoresBosques> result;
            if (isDate)
                result = repositorio.GetPagedElements<DateTime>(pageIndex, pageCount, orderDate, predicate, ascending);
            else
                result = repositorio.GetPagedElements<int>(pageIndex, pageCount, order, predicate, ascending);

            return result.ToList();
        }





    }
}