﻿using BosquesPaz.Models;
using BosquesPaz.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;

namespace BosquesPaz.Services
{
    /// <summary>
    /// Clase que verifica que las peticiones de los datos sean correctas. 
    /// </summary>
    /// <remarks>
    /// <h3>Calidad</h3>
    /// </hr>
    /// <b>Requerimiento:</b> Help Desk</br>
    /// <b>Version:</b> 1</br>
    /// <b>Autor:</b> Grupo Cubo Ltda.</br>
    /// <b>Fecha:</b> 18/12/2017</br>
    /// <b>Descripcion:</b> Funciona para solicitar los datos al repositorio de estadisticas y retornar los datos obtenidos.
    /// </remarks>
    public class EstadisticasService
    {
        /// <summary>
        /// Propiedad que instancia la clase EstadisticasRepository
        /// </summary>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 18/12/2017</br>
        /// <b>Descripcion:</b> Propiedad que instancia el repositorio EstadisticasRepository, para enviar peticiones y recibir el resultado.
        /// </remarks>
        EstadisticasRepository repositorio
        {
            get
            {
                return new EstadisticasRepository();
            }
        }

        /// <summary>
        /// Propiedad de número entero
        /// </summary>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 18/12/2017</br>
        /// <b>Descripcion:</b> Propiedad que define un número entero, para verificar el numero de registros que existen en una entidad.
        /// </remarks>
        public int CantidadRegistros { get; private set; }

        /// <summary>
        /// Método que retorna una coleccion de datos, segun sus parametros.
        /// </summary>
        /// <param name="pageIndex">Número de paginas.</param>
        /// <param name="pageCount">Número de paginaciones.</param>
        /// <param name="columnaOrden">Cadena de caracteres de una columna.</param>
        /// <param name="filtros">Cadena de caracteres</param>
        /// <param name="ascending">Tipo de dato verdadero o falso</param>
        /// <returns>Retorna coleccion de SP_DepartamentoCantidadBosque</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 18/12/2017</br>
        /// <b>Descripcion:</b> Método que permite obtener los datos del procedimiento almacenado SP_DepartamentoCantidadBosque, y organizarlos con el proposito de retornarlos a una grilla.
        /// </remarks>
        public IEnumerable<SP_DepartamentoCantidadBosque> DepartamentoCantidadBosque(int pageIndex, int pageCount, string columnaOrden, IDictionary<string, string> filtros, bool ascending)
        {
            IEnumerable<SP_DepartamentoCantidadBosque> result = null;
            try
            {
                result = repositorio.DepartamentoCantidadBosque(pageIndex, pageCount);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }

        /// <summary>
        /// Método que retorna una coleccion de datos, segun sus parametros.
        /// </summary>
        /// <param name="pageIndex">Número de paginas.</param>
        /// <param name="pageCount">Número de paginaciones.</param>
        /// <param name="columnaOrden">Cadena de caracteres de una columna.</param>
        /// <param name="filtros">Cadena de caracteres</param>
        /// <param name="ascending">Tipo de dato verdadero o falso</param>
        /// <returns>Retorna coleccion de SP_DepartamentoTipoEstadoCantidadBosque</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 18/12/2017</br>
        /// <b>Descripcion:</b> Método que permite obtener los datos del procedimiento almacenado SP_DepartamentoTipoEstadoCantidadBosque, y organizarlos con el proposito de retornarlos a una grilla.
        /// </remarks>
        public IEnumerable<SP_DepartamentoTipoEstadoCantidadBosque> DepartamentoTipoEstadoCantidadBosque(int pageIndex, int pageCount, string columnaOrden, IDictionary<string, string> filtros, bool ascending)
        {
            IEnumerable<SP_DepartamentoTipoEstadoCantidadBosque> result = null;
            try
            {
                result = repositorio.DepartamentoTipoEstadoCantidadBosque(pageIndex, pageCount);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }

        /// <summary>
        /// Método que retorna una coleccion de datos, segun sus parametros.
        /// </summary>
        /// <param name="pageIndex">Número de paginas.</param>
        /// <param name="pageCount">Número de paginaciones.</param>
        /// <param name="columnaOrden">Cadena de caracteres de una columna.</param>
        /// <param name="filtros">Cadena de caracteres</param>
        /// <param name="ascending">Tipo de dato verdadero o falso</param>
        /// <returns>Retorna coleccion de SP_DepartamentoTipoEstadoPresupuestoCantidadBosque</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 18/12/2017</br>
        /// <b>Descripcion:</b> Método que permite obtener los datos del procedimiento almacenado SP_DepartamentoTipoEstadoPresupuestoCantidadBosque, y organizarlos con el proposito de retornarlos a una grilla.
        /// </remarks>
        public IEnumerable<SP_DepartamentoTipoEstadoPresupuestoCantidadBosque> DepartamentoTipoEstadoPresupuestoCantidadBosque(int pageIndex, int pageCount, string columnaOrden, IDictionary<string, string> filtros, bool ascending)
        {
            IEnumerable<SP_DepartamentoTipoEstadoPresupuestoCantidadBosque> result = null;
            try
            {
                result = repositorio.DepartamentoTipoEstadoPresupuestoCantidadBosque(pageIndex, pageCount);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }

        /// <summary>
        /// Método que retorna una coleccion de datos, segun sus parametros.
        /// </summary>
        /// <param name="pageIndex">Número de paginas.</param>
        /// <param name="pageCount">Número de paginaciones.</param>
        /// <param name="columnaOrden">Cadena de caracteres de una columna.</param>
        /// <param name="filtros">Cadena de caracteres</param>
        /// <param name="ascending">Tipo de dato verdadero o falso</param>
        /// <returns>Retorna coleccion de SP_DepartamentoTipoNombreCantidadBosque</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 18/12/2017</br>
        /// <b>Descripcion:</b> Método que permite obtener los datos del procedimiento almacenado SP_DepartamentoTipoNombreCantidadBosque, y organizarlos con el proposito de retornarlos a una grilla.
        /// </remarks>
        public IEnumerable<SP_DepartamentoTipoNombreCantidadBosque> DepartamentoTipoNombreCantidadBosque(int pageIndex, int pageCount, string columnaOrden, IDictionary<string, string> filtros, bool ascending)
        {
            IEnumerable<SP_DepartamentoTipoNombreCantidadBosque> result = null;
            try
            {
                result = repositorio.DepartamentoTipoNombreCantidadBosque(pageIndex, pageCount);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }

        /// <summary>
        /// Método que retorna una coleccion de datos, segun sus parametros.
        /// </summary>
        /// <param name="pageIndex">Número de paginas.</param>
        /// <param name="pageCount">Número de paginaciones.</param>
        /// <param name="columnaOrden">Cadena de caracteres de una columna.</param>
        /// <param name="filtros">Cadena de caracteres</param>
        /// <param name="ascending">Tipo de dato verdadero o falso</param>
        /// <returns>Retorna coleccion de SP_DepartamentoTipoNombreEstadoBosque</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 18/12/2017</br>
        /// <b>Descripcion:</b> Método que permite obtener los datos del procedimiento almacenado SP_DepartamentoTipoNombreEstadoBosque, y organizarlos con el proposito de retornarlos a una grilla.
        /// </remarks>
        public IEnumerable<SP_DepartamentoTipoNombreEstadoBosque> DepartamentoTipoNombreEstadoBosque(int pageIndex, int pageCount, string columnaOrden, IDictionary<string, string> filtros, bool ascending)
        {
            IEnumerable<SP_DepartamentoTipoNombreEstadoBosque> result = null;
            try
            {
                result = repositorio.DepartamentoTipoNombreEstadoBosque(pageIndex, pageCount);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }

        /// <summary>
        /// Método que retorna una coleccion de datos, segun sus parametros.
        /// </summary>
        /// <param name="pageIndex">Número de paginas.</param>
        /// <param name="pageCount">Número de paginaciones.</param>
        /// <param name="columnaOrden">Cadena de caracteres de una columna.</param>
        /// <param name="filtros">Cadena de caracteres</param>
        /// <param name="ascending">Tipo de dato verdadero o falso</param>
        /// <returns>Retorna coleccion de SP_DepartamentoTipoUnidadCantidadBosque</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 18/12/2017</br>
        /// <b>Descripcion:</b> Método que permite obtener los datos del procedimiento almacenado SP_DepartamentoTipoUnidadCantidadBosque, y organizarlos con el proposito de retornarlos a una grilla.
        /// </remarks>
        public IEnumerable<SP_DepartamentoTipoUnidadCantidadBosque> DepartamentoTipoUnidadCantidadBosque(int pageIndex, int pageCount, string columnaOrden, IDictionary<string, string> filtros, bool ascending)
        {
            IEnumerable<SP_DepartamentoTipoUnidadCantidadBosque> result = null;
            try
            {
                result = repositorio.DepartamentoTipoUnidadCantidadBosque(pageIndex, pageCount);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }
    }
}