﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BosquesPaz.Helpers;
using BosquesPaz.ServiceReferenceFuncionarios;
using System.ServiceModel;

namespace BosquesPaz.Services
{
    /// <summary>
    /// Clase que verifica que las peticiones de los datos sean correctas. 
    /// </summary>
    /// <remarks>
    /// <h3>Calidad</h3>
    /// </hr>
    /// <b>Requerimiento:</b> Help Desk</br>
    /// <b>Version:</b> 1</br>
    /// <b>Autor:</b> Grupo Cubo Ltda.</br>
    /// <b>Fecha:</b> 18/12/2017</br>
    /// <b>Descripcion:</b> Funciona para solicitar los datos al helper infraestructura retornar los datos obtenidos.
    /// </remarks>
    public class LoginService
    {
        /// <summary>
        /// Propiedad que instancia la clase Infraestructura
        /// </summary>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 18/12/2017</br>
        /// <b>Descripcion:</b> Propiedad que instancia el helper Infraestructura, para enviar peticiones y recibir el resultado.
        /// </remarks>
        private Infraestructura infraestructura { get{ return new Infraestructura(); } }

        /// <summary>
        /// Conceder permisos a usuarios dentro de la aplicacion.
        /// </summary>
        /// <param name="user">nombre o alias del usuario</param>
        /// <param name="password">contraseña del usuario</param>
        /// <returns>Retorna valor booleano </returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 18/12/2017</br>
        /// <b>Descripcion:</b> Recibe el nombre de usuario y la contraseña, para verificar si el usuario posee permisos dentro de la aplicación.
        /// </remarks>
        public bool IsAutorizado(string user, string password)
        {
            bool isVlaid = false;

            EntidadInfoUsuario usuario = null;
            try
            {
                usuario = infraestructura.AutorizaUsuario(user, password);
                if (usuario != null)
                {
                    isVlaid = true;
                }
            }
            catch(Exception ex)
            {
                isVlaid = false;
            }
            return isVlaid;
        }

        /// <summary>
        /// Verificar nombre o alias del usuario.
        /// </summary>
        /// <param name="user">nombre o alias del usuario</param>
        /// <param name="password">contraseña del usuario</param>
        /// <returns>Retorna instancia de EntidadInfoUsuario</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 18/12/2017</br>
        /// <b>Descripcion:</b> Recibe el nombre de usuario y la contraseña, para retornar una instancia del usuario.
        /// </remarks>
        public EntidadInfoUsuario ObtenerEntidadInfoUsuario(string user, string password)
        {
            EntidadInfoUsuario usuario = null;
            try
            {
                usuario = infraestructura.AutorizaUsuario(user, password);
                if(usuario != null)
                {
                    return usuario;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return usuario;
        }
    }
}