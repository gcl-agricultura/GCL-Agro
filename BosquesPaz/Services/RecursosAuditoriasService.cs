﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BosquesPaz.Models;
using BosquesPaz.Repositories;
using System.Linq.Expressions;
using BosquesPaz.Repositories.Especificaciones;

namespace BosquesPaz.Services
{
    /// <summary>
    /// Clase que verifica que las peticiones de los datos sean correctas. 
    /// </summary>
    /// <remarks>
    /// <h3>Calidad</h3>
    /// </hr>
    /// <b>Requerimiento:</b> Help Desk</br>
    /// <b>Version:</b> 1</br>
    /// <b>Autor:</b> Grupo Cubo Ltda.</br>
    /// <b>Fecha:</b> 18/12/2017</br>
    /// <b>Descripcion:</b> Funciona para solicitar los datos al repositorio de auditorias recursos y retornar los datos obtenidos.
    /// </remarks>
    public class RecursosAuditoriasService
    {
        /// <summary>
        /// Propiedad que instancia la clase RecursosAuditoriasRepository
        /// </summary>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 18/12/2017</br>
        /// <b>Descripcion:</b> Propiedad que instancia el repositorio RecursosAuditoriasRepository, para enviar peticiones y recibir el resultado.
        /// </remarks>
        private RecursosAuditoriasRepository repositorioAuditorias { get { return new RecursosAuditoriasRepository(); } }

        /// <summary>
        /// Propiedad que instancia la clase EstadosService
        /// </summary>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 18/12/2017</br>
        /// <b>Descripcion:</b> Propiedad que instancia el servicio EstadosService, para enviar peticiones y recibir el resultado.
        /// </remarks>
        private EstadosService ServicioEstado { get { return new EstadosService(); } }

        /// <summary>
        /// Propiedad que instancia la clase TiposRecursosService
        /// </summary>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 18/12/2017</br>
        /// <b>Descripcion:</b> Propiedad que instancia el servicio TiposRecursosService, para enviar peticiones y recibir el resultado.
        /// </remarks>
        private TiposRecursosService ServicioTipo { get { return new TiposRecursosService(); } }

        /// <summary>
        /// Propiedad de número entero
        /// </summary>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 13/12/2017</br>
        /// <b>Descripcion:</b> Propiedad que define un número entero, para verificar el numero de registros que existen en una entidad.
        /// </remarks>
        public int CantidadRegistros { get; private set; }

        /// <summary>
        /// Método que retorna una coleccion de datos, segun sus parametros.
        /// </summary>
        /// <param name="pageIndex">Número de paginas.</param>
        /// <param name="pageCount">Número de paginaciones.</param>
        /// <param name="columnaOrden">Cadena de caracteres de una columna.</param>
        /// <param name="filtros">Cadena de caracteres</param>
        /// <param name="ascending">Tipo de dato verdadero o falso</param>
        /// <returns>Retorna coleccion de AudiRecursosBosques</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 18/12/2017</br>
        /// <b>Descripcion:</b> Método que permite obtener los datos de AudiRecursosBosques y organizarlos con el proposito de retornarlos a una grilla.
        /// </remarks>
        public IEnumerable<AudiRecursosBosques> ObtenerRecursosBosquesAuditorias(int pageIndex, int pageCount, string columnaOrden, IDictionary<string, string> filtros, bool ascending, int id)
        {
            Expression<Func<AudiRecursosBosques, bool>> predicate = x => x.Id_RecursoBosque == id;
            Expression<Func<AudiRecursosBosques, int>> order = x => x.Id_RecursoBosqueAuditoria;
            Expression<Func<AudiRecursosBosques, DateTime>> orderDate = null;
            bool first = true;
            bool isDate = false;

            foreach (var filtro in filtros)
            {
                switch (filtro.Key)
                {
                    default:
                        predicate = x => false;
                        break;
                    case nameof(AudiRecursosBosques):
                        string NombreRecursoAuditoria = filtro.Value;
                        predicate = (first ? x => x.NombreRecursoAuditoria == NombreRecursoAuditoria : predicate.And(x => x.NombreRecursoAuditoria == NombreRecursoAuditoria));
                        break;
                    case nameof(AudiRecursosBosques.TipoRecurso):
                        int Id_TipoRecurso = ServicioTipo.ObtenerIdTiposRecursosPorNombre(filtro.Value);
                        predicate = (first ? x => x.Id_TipoRecurso == Id_TipoRecurso : predicate.And(x => x.Id_TipoRecurso == Id_TipoRecurso));
                        break;
                    case nameof(AudiRecursosBosques.Estado):
                        int Id_Estado = ServicioEstado.ObtenerIdEstadoPorNombre(filtro.Value);
                        predicate = (first ? x => x.Id_Estado == Id_Estado : predicate.And(x => x.Id_Estado == Id_Estado));
                        break;
                }
                first = false;
            }

            CantidadRegistros = repositorioAuditorias.Count(predicate);

            switch (columnaOrden)
            {
                case nameof(AudiRecursosBosques.Id_RecursoBosqueAuditoria):
                    order = x => x.Id_RecursoBosqueAuditoria;
                    isDate = false;
                    break;
            }


            IEnumerable<AudiRecursosBosques> result;
            if (isDate)
                result = repositorioAuditorias.GetPagedElements<DateTime>(pageIndex, pageCount, orderDate, predicate, ascending);
            else
                result = repositorioAuditorias.GetPagedElements<int>(pageIndex, pageCount, order, predicate, ascending);

            return result.ToList();
        }
    }
}