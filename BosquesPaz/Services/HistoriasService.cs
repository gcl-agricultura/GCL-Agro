﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BosquesPaz.Models;
using BosquesPaz.Repositories;
using System.Linq.Expressions;
using BosquesPaz.Repositories.Especificaciones;

namespace BosquesPaz.Services
{
    /// <summary>
    /// Clase que verifica que las peticiones de los datos sean correctas. 
    /// </summary>
    /// <remarks>
    /// <h3>Calidad</h3>
    /// </hr>
    /// <b>Requerimiento:</b> Help Desk</br>
    /// <b>Version:</b> 1</br>
    /// <b>Autor:</b> Grupo Cubo Ltda.</br>
    /// <b>Fecha:</b> 18/12/2017</br>
    /// <b>Descripcion:</b> Funciona para solicitar los datos al repositorio de historias y retornar los datos obtenidos.
    /// </remarks>
    public class HistoriasService
    {
        /// <summary>
        /// Propiedad que instancia la clase HistoriasRepository
        /// </summary>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 18/12/2017</br>
        /// <b>Descripcion:</b> Propiedad que instancia el repositorio HistoriasRepository, para enviar peticiones y recibir el resultado.
        /// </remarks>
        private HistoriasRepository repositorio { get { return new HistoriasRepository(); } }

        /// <summary>
        /// Propiedad que instancia la clase HistoriasAuditoriasRepository
        /// </summary>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 18/12/2017</br>
        /// <b>Descripcion:</b> Propiedad que instancia el repositorio HistoriasAuditoriasRepository, para enviar peticiones y recibir el resultado.
        /// </remarks>
        private HistoriasAuditoriasRepository repositorioAuditoria { get{ return new HistoriasAuditoriasRepository(); } }

        /// <summary>
        /// Propiedad que instancia la clase TematicasService
        /// </summary>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 18/12/2017</br>
        /// <b>Descripcion:</b> Propiedad que instancia el servicio TematicasService, para enviar peticiones y recibir el resultado.
        /// </remarks>
        private TematicasService ServicioTematica { get { return new TematicasService(); } }

        /// <summary>
        /// Propiedad de número entero
        /// </summary>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 18/12/2017</br>
        /// <b>Descripcion:</b> Propiedad que define un número entero, para verificar el numero de registros que existen en una entidad.
        /// </remarks>
        public int CantidadRegistros { get; private set; }

        /// <summary>
        /// Guardar una instancia de HistoriasBosques en la base de datos.
        /// </summary>
        /// <param name="historia">Instancia de HistoriasBosques de </param>
        /// <returns>Retorna instancia de HistoriasBosques</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 18/12/2017</br>
        /// <b>Descripcion:</b> Recibe una instancia de HistoriasBosques y la envia a la clase de repositorio, y retorna la instancia.
        /// </remarks>
        public HistoriasBosques GuardarHistoria(HistoriasBosques historia)
        {
            try
            {
                historia = repositorio.Create(historia);
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return historia;
        }

        /// <summary>
        /// Actualizar datos de HistoriasBosques en la base de datos.
        /// </summary>
        /// <param name="historia">Instancia de HistoriasBosques</param>
        /// <returns>Retorna valor booleano</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 18/12/2017</br>
        /// <b>Descripcion:</b> Recibe una instancia de HistoriasBosques y la envia a la clase de repositorio, y retorna un valor booleano.
        /// </remarks>
        public bool ActualizarHistoria(HistoriasBosques historia)
        {
            bool result = false;
            try
            {
                if (repositorio.Update(historia))
                {
                    result = true;
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return result;
        }

        /// <summary>
        /// Obtener datos de HistoriasBosques de la base de datos.
        /// </summary>
        /// <param name="idHistoriaBosque">Identificador de la historia</param>
        /// <returns>Retorna instancia de HistoriasBosques</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 18/12/2017</br>
        /// <b>Descripcion:</b> Obtiene una instancia de la historia segun el numero de identificador que ingrese.
        /// </remarks>
        public HistoriasBosques ObtenerHistoriaBosque(int idHistoriaBosque)
        {
            HistoriasBosques result = null;
            try
            {
                result = repositorio.GetByKey(idHistoriaBosque);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }

        /// <summary>
        /// Obtener coleccion de HistoriasBosques de la base de datos.
        /// </summary>
        /// <returns>Retorna coleccion de HistoriasBosques</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 18/12/2017</br>>
        /// <b>Descripcion:</b> Obtiene una coleccion de la historias de los bosques.
        /// </remarks>
        public IEnumerable<HistoriasBosques> ObtenerHistoriasBosques()
        {
            IEnumerable<HistoriasBosques> result = null;
            try
            {
                result = repositorio.GetAll();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }

        public List<HistoriasBosques> ObtenerHistoriasBosque(int id)
        {
            IEnumerable<HistoriasBosques> all = null;
            List<HistoriasBosques> result = new List<HistoriasBosques>();
            try
            {
                all = repositorio.GetAll();
                foreach (HistoriasBosques item in all) {
                    if(item.Id_Bosque == id)
                    {
                        result.Add(item);
                    }
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }

        /// <summary>
        /// Obtener fecha de AudiHistoriasBosques de la base de datos.
        /// </summary>
        /// <param name="idHistoriaBosque">Identificador de la historia</param>
        /// <returns>Retorna cadena de caracteres</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 18/12/2017</br>
        /// <b>Descripcion:</b> Obtiene una fecha de la auditoria de la historia.
        /// </remarks>
        public string ObtenerFechaAuditoriaPorId(int idHistoriaBosque)
        {
            string result = string.Empty;
            IEnumerable<AudiHistoriasBosques> lista = new List<AudiHistoriasBosques>();
            try
            {
                Expression<Func<AudiHistoriasBosques, bool>> predicate = x => x.Id_HistoriaBosque == idHistoriaBosque;
                lista = repositorioAuditoria.GetBy(predicate).ToList();
                if (lista.Count() > 0)
                {
                    result = Convert.ToString(lista.Last().FechaAuditoria);
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return result;
        }

        /// <summary>
        /// Obtener coleccion de HistoriasBosques de la base de datos.
        /// </summary>
        /// <returns>Retorna coleccion de HistoriasBosques</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 18/12/2017</br>
        /// <b>Descripcion:</b> Obtiene una coleccion de HistoriasBosques con los datos de forma inversa para el modulo de juegos.
        /// </remarks>
        public IEnumerable<HistoriasBosques> ObtenerInversoHistorias()
        {
            IEnumerable<HistoriasBosques> result = null;
            try
            {
                result = repositorio.GetAll().Reverse();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }




        /// <summary>
        /// Método que retorna una coleccion de datos, segun sus parametros.
        /// </summary>
        /// <param name="pageIndex">Número de paginas.</param>
        /// <param name="pageCount">Número de paginaciones.</param>
        /// <param name="columnaOrden">Cadena de caracteres de una columna.</param>
        /// <param name="filtros">Cadena de caracteres</param>
        /// <param name="ascending">Tipo de dato verdadero o falso</param>
        /// <param name="id">Identificador del bosque</param>
        /// <returns>Retorna coleccion de HistoriasBosques</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 18/12/2017</br>
        /// <b>Descripcion:</b> Método que permite obtener los datos de HistoriasBosques, y organizarlos con el proposito de retornarlos a una grilla.
        /// </remarks>
        public IEnumerable<HistoriasBosques> ObtenerHistoriasBosques(int pageIndex, int pageCount, string columnaOrden, IDictionary<string, string> filtros, bool ascending, int id)
        {
            Expression<Func<HistoriasBosques, bool>> predicate = x => x.Id_Bosque == id;
            Expression<Func<HistoriasBosques, int>> order = x => x.Id_HistoriaBosque;
            Expression<Func<HistoriasBosques, DateTime>> orderDate = null;
            bool first = true;
            bool isDate = false;

            foreach (var filtro in filtros)
            {
                switch (filtro.Key)
                {
                    default:
                        predicate = x => false;
                        break;
                    case nameof(HistoriasBosques):
                        string Nombre = filtro.Value;
                        predicate = (first ? x => x.Nombre == Nombre : predicate.And(x => x.Nombre == Nombre));
                        break;
                    case nameof(HistoriasBosques.Tematicas):
                        int Id_Tematica = ServicioTematica.ObtenerIdTematicasPorNombre(filtro.Value);
                        predicate = (first ? x => x.Id_Tematica == Id_Tematica : predicate.And(x => x.Id_Tematica == Id_Tematica));
                        break;
                }
                first = false;
            }

            CantidadRegistros = repositorio.Count(predicate);

            switch (columnaOrden)
            {
                case nameof(HistoriasBosques.Id_HistoriaBosque):
                    order = x => x.Id_HistoriaBosque;
                    isDate = false;
                    break;
            }


            IEnumerable<HistoriasBosques> result;
            if (isDate)
                result = repositorio.GetPagedElements<DateTime>(pageIndex, pageCount, orderDate, predicate, ascending);
            else
                result = repositorio.GetPagedElements<int>(pageIndex, pageCount, order, predicate, ascending);

            return result.ToList();
        }

        /// <summary>
        /// Método que retorna una coleccion de datos, segun sus parametros.
        /// </summary>
        /// <param name="pageIndex">Número de paginas.</param>
        /// <param name="pageCount">Número de paginaciones.</param>
        /// <param name="columnaOrden">Cadena de caracteres de una columna.</param>
        /// <param name="filtros">Cadena de caracteres</param>
        /// <param name="ascending">Tipo de dato verdadero o falso</param>
        /// <returns>Retorna coleccion de HistoriasBosques</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 18/12/2017</br>
        /// <b>Descripcion:</b> Método que permite obtener los datos de HistoriasBosques, y organizarlos con el proposito de retornarlos a una grilla.
        /// </remarks>
        public IEnumerable<HistoriasBosques> ObtenerHistoriasAudios(int pageIndex, int pageCount, string columnaOrden, IDictionary<string, string> filtros, bool ascending)
        {
            Expression<Func<HistoriasBosques, bool>> predicate = x => x.Id_Bosque > 0;
            Expression<Func<HistoriasBosques, int>> order = x => x.Id_Bosque;
            Expression<Func<HistoriasBosques, DateTime>> orderDate = null;
            bool first = true;
            bool isDate = false;

            foreach (var filtro in filtros)
            {
                switch (filtro.Key)
                {
                    default:
                        predicate = x => false;
                        break;
                    case nameof(HistoriasBosques):
                        string Nombre = filtro.Value;
                        predicate = (first ? x => x.Nombre == Nombre : predicate.And(x => x.Nombre == Nombre));
                        break;
                    case nameof(HistoriasBosques.Tematicas):
                        int Id_Tematica = ServicioTematica.ObtenerIdTematicasPorNombre(filtro.Value);
                        predicate = (first ? x => x.Id_Tematica == Id_Tematica : predicate.And(x => x.Id_Tematica == Id_Tematica));
                        break;

                }
                first = false;
            }

            CantidadRegistros = repositorio.Count(predicate);

            switch (columnaOrden)
            {
                case nameof(HistoriasBosques.Id_Bosque):
                    order = x => x.Id_Bosque;
                    isDate = false;
                    break;

            }


            IEnumerable<HistoriasBosques> result;
            if (isDate)
                result = repositorio.GetPagedElements<DateTime>(pageIndex, pageCount, orderDate, predicate, ascending);
            else
                result = repositorio.GetPagedElements<int>(pageIndex, pageCount, order, predicate, ascending);

            return result.ToList();
        }

        /// <summary>
        /// Método que retorna una coleccion de datos, segun sus parametros.
        /// </summary>
        /// <param name="pageIndex">Número de paginas.</param>
        /// <param name="pageCount">Número de paginaciones.</param>
        /// <param name="columnaOrden">Cadena de caracteres de una columna.</param>
        /// <param name="filtros">Cadena de caracteres</param>
        /// <param name="ascending">Tipo de dato verdadero o falso</param>
        /// <returns>Retorna coleccion de HistoriasBosques</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 18/12/2017</br>
        /// <b>Descripcion:</b> Método que permite obtener los datos de HistoriasBosques, y organizarlos con el proposito de retornarlos a una grilla.
        /// </remarks>
        public IEnumerable<HistoriasBosques> ObtenerHistoriasAudiosUsuarios(int pageIndex, int pageCount, string columnaOrden, IDictionary<string, string> filtros, bool ascending)
        {
            Expression<Func<HistoriasBosques, bool>> predicate = x => x.Id_EstadoHistoria == 2;
            Expression<Func<HistoriasBosques, int>> order = x => x.Id_Bosque;
            Expression<Func<HistoriasBosques, DateTime>> orderDate = null;
            bool first = true;
            bool isDate = false;

            foreach (var filtro in filtros)
            {
                switch (filtro.Key)
                {
                    default:
                        predicate = x => false;
                        break;
                    case nameof(HistoriasBosques):
                        string Nombre = filtro.Value;
                        predicate = (first ? x => x.Nombre == Nombre : predicate.And(x => x.Nombre == Nombre));
                        break;
                    case nameof(HistoriasBosques.Tematicas):
                        int Id_Tematica = ServicioTematica.ObtenerIdTematicasPorNombre(filtro.Value);
                        predicate = (first ? x => x.Id_Tematica == Id_Tematica : predicate.And(x => x.Id_Tematica == Id_Tematica));
                        break;

                }
                first = false;
            }

            CantidadRegistros = repositorio.Count(predicate);

            switch (columnaOrden)
            {
                case nameof(HistoriasBosques.Id_Bosque):
                    order = x => x.Id_Bosque;
                    isDate = false;
                    break;

            }


            IEnumerable<HistoriasBosques> result;
            if (isDate)
                result = repositorio.GetPagedElements<DateTime>(pageIndex, pageCount, orderDate, predicate, ascending);
            else
                result = repositorio.GetPagedElements<int>(pageIndex, pageCount, order, predicate, ascending);

            return result.ToList();
        }

        /// <summary>
        /// Método que retorna una coleccion de datos, segun sus parametros.
        /// </summary>
        /// <param name="pageIndex">Número de paginas.</param>
        /// <param name="pageCount">Número de paginaciones.</param>
        /// <param name="columnaOrden">Cadena de caracteres de una columna.</param>
        /// <param name="filtros">Cadena de caracteres</param>
        /// <param name="ascending">Tipo de dato verdadero o falso</param>
        /// <param name="id">Identificador del bosque</param>
        /// <returns>Retorna coleccion de HistoriasBosques</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 18/12/2017</br>
        /// <b>Descripcion:</b> Método que permite obtener los datos de HistoriasBosques para los usuarios, y organizarlos con el proposito de retornarlos a una grilla.
        /// </remarks>
        public IEnumerable<HistoriasBosques> ObtenerHistoriasBosquesUsuarios(int pageIndex, int pageCount, string columnaOrden, IDictionary<string, string> filtros, bool ascending, int id)
        {
            Expression<Func<HistoriasBosques, bool>> predicate = x => x.Id_Bosque == id && x.Id_EstadoHistoria == 2;
            Expression<Func<HistoriasBosques, int>> order = x => x.Id_HistoriaBosque;
            Expression<Func<HistoriasBosques, DateTime>> orderDate = null;
            bool first = true;
            bool isDate = false;

            foreach (var filtro in filtros)
            {
                switch (filtro.Key)
                {
                    default:
                        predicate = x => false;
                        break;
                    case nameof(HistoriasBosques):
                        string Nombre = filtro.Value;
                        predicate = (first ? x => x.Nombre == Nombre : predicate.And(x => x.Nombre == Nombre));
                        break;
                    case nameof(HistoriasBosques.Tematicas):
                        int Id_Tematica = ServicioTematica.ObtenerIdTematicasPorNombre(filtro.Value);
                        predicate = (first ? x => x.Id_Tematica == Id_Tematica : predicate.And(x => x.Id_Tematica == Id_Tematica));
                        break;
                }
                first = false;
            }

            CantidadRegistros = repositorio.Count(predicate);

            switch (columnaOrden)
            {
                case nameof(HistoriasBosques.Id_HistoriaBosque):
                    order = x => x.Id_HistoriaBosque;
                    isDate = false;
                    break;
            }


            IEnumerable<HistoriasBosques> result;
            if (isDate)
                result = repositorio.GetPagedElements<DateTime>(pageIndex, pageCount, orderDate, predicate, ascending);
            else
                result = repositorio.GetPagedElements<int>(pageIndex, pageCount, order, predicate, ascending);

            return result.ToList();
        }
    }
}