﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BosquesPaz.Models;
using BosquesPaz.Repositories;
using System.Linq.Expressions;
using Newtonsoft.Json;
using System.Web.Http.Results;

namespace BosquesPaz.Services
{
    /// <summary>
    /// Clase que verifica que las peticiones de los datos sean correctas. 
    /// </summary>
    /// <remarks>
    /// <h3>Calidad</h3>
    /// </hr>
    /// <b>Requerimiento:</b> Help Desk</br>
    /// <b>Version:</b> 1</br>
    /// <b>Autor:</b> Grupo Cubo Ltda.</br>
    /// <b>Fecha:</b> 18/12/2017</br>
    /// <b>Descripcion:</b> Funciona para solicitar los datos al repositorio de individuos/especies y retornar los datos obtenidos.
    /// </remarks>
    public class IndividuosService
    {
        /// <summary>
        /// Propiedad que instancia la clase IndividuosRepository
        /// </summary>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 18/12/2017</br>
        /// <b>Descripcion:</b> Propiedad que instancia el repositorio IndividuosRepository, para enviar peticiones y recibir el resultado.
        /// </remarks>
        private IndividuosRepository repositorio { get { return new IndividuosRepository(); } }

        /// <summary>
        /// Propiedad de número entero
        /// </summary>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 18/12/2017</br>
        /// <b>Descripcion:</b> Propiedad que define un número entero, para verificar el numero de registros que existen en una entidad.
        /// </remarks>
        public int CantidadRegistros { get; private set; }

        /// <summary>
        /// Guardar una instancia de HistoriasBosques en la base de datos.
        /// </summary>
        /// <param name="individuo">Instancia de IndividuosEspecies</param>
        /// <returns>Retorna instancia de IndividuosEspecies</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 18/12/2017</br>
        /// <b>Descripcion:</b> Recibe una instancia de IndividuosEspecies y la envia a la clase de repositorio, y retorna la instancia.
        /// </remarks>
        public IndividuosEspecies GuardarIndividuo(IndividuosEspecies individuo)
        {
            try
            {
                individuo = repositorio.Create(individuo);
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return individuo;
        }

        /// <summary>
        /// Actualizar datos de IndividuosEspecies en la base de datos.
        /// </summary>
        /// <param name="individuo">Instancia de IndividuosEspecies</param>
        /// <returns>Retorna valor booleano</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 18/12/2017</br>
        /// <b>Descripcion:</b> Recibe una instancia de IndividuosEspecies y la envia a la clase de repositorio, y retorna un valor booleano.
        /// </remarks>
        public bool ActualizarIndividuo(IndividuosEspecies individuo)
        {
            bool result = false;
            try
            {
                if (repositorio.Update(individuo))
                {
                    result = true;
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return result;
        }

        /// <summary>
        /// Obtener datos de IndividuosEspecies de la base de datos.
        /// </summary>
        /// <param name="idIndividuoEspecie">Identificador del individuo/especie</param>
        /// <returns>Retorna instancia de IndividuosEspecies</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 18/12/2017</br>
        /// <b>Descripcion:</b> Obtiene una instancia del individuo/especie segun el numero de identificador que ingrese.
        /// </remarks>
        public IndividuosEspecies ObtenerIndividuosEspecies(int idIndividuoEspecie)
        {
            IndividuosEspecies result = null;
            try
            {
                result = repositorio.GetByKey(idIndividuoEspecie);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }

        /// <summary>
        /// Obtener coleccion de IndividuosEspecies de la base de datos.
        /// </summary>
        /// <returns>Retorna coleccion de IndividuosEspecies</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 18/12/2017</br>
        /// <b>Descripcion:</b> Obtiene una coleccion del individuo/especie.
        /// </remarks>
        public IEnumerable<IndividuosEspecies> ObtenerIndividuosEspecies()
        {
            IEnumerable<IndividuosEspecies> result = null;
            try
            {
                result = repositorio.GetAll();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }


        public List<IndividuosEspecies> ObtenerIndividuosEspeciesByBosque(int id)
        {
            IEnumerable<IndividuosEspecies> result = null;
            List<IndividuosEspecies> individuosBosque = new List<IndividuosEspecies>();

            try
            {
                result = repositorio.GetAll();
            }
            catch (Exception ex)
            {
                throw ex;
            }

            foreach (var individuo in result) {
                if(individuo.Id_Bosque == id)
                {
                    individuosBosque.Add(individuo);
                }
            }

            return individuosBosque;
        }





        /// <summary>
        /// Obtener coleccion de IndividuosEspecies de la base de datos.
        /// </summary>
        /// <returns>Retorna coleccion de IndividuosEspecies</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 18/12/2017</br>
        /// <b>Descripcion:</b> Obtiene una coleccion del individuo/especie con los datos de forma inversa para el modulo de juegos.
        /// </remarks>
        public IEnumerable<IndividuosEspecies> ObtenerInversoIndividuosEspecies()
        {
            IEnumerable<IndividuosEspecies> result = null;
            try
            {
                result = repositorio.GetAll().Reverse();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }

        /// <summary>
        /// Método que retorna una coleccion de datos, segun sus parametros.
        /// </summary>
        /// <param name="pageIndex">Número de paginas.</param>
        /// <param name="pageCount">Número de paginaciones.</param>
        /// <param name="columnaOrden">Cadena de caracteres de una columna.</param>
        /// <param name="filtros">Cadena de caracteres</param>
        /// <param name="ascending">Tipo de dato verdadero o falso</param>
        /// <param name="id">Identificador del bosque</param>
        /// <returns>Retorna coleccion de IndividuosEspecies</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 18/12/2017</br>
        /// <b>Descripcion:</b> Método que permite obtener los datos de IndividuosEspecies, y organizarlos con el proposito de retornarlos a una grilla.
        /// </remarks>
        public IEnumerable<IndividuosEspecies> ObtenerIndividuosEspeciesBosques(int pageIndex, int pageCount, string columnaOrden, IDictionary<string, string> filtros, bool ascending, int id)
        {
            Expression<Func<IndividuosEspecies, bool>> predicate = x => x.Id_Bosque == id;
            Expression<Func<IndividuosEspecies, int>> order = x => x.Id_IndividuoEspecie;
            Expression<Func<IndividuosEspecies, DateTime>> orderDate = null;
            bool first = true;
            bool isDate = false;

            foreach (var filtro in filtros)
            {
                switch (filtro.Key)
                {
                    default:
                        predicate = x => false;
                        break;
                        //case nameof(IndividuosEspecies.TipoActividad):
                        //    int TipoActividad = ServicioTipo.ObtenerIdTiposActividadesPorNombre(filtro.Value);
                        //    predicate = (first ? x => x.Id_TipoActividad == TipoActividad : predicate.And(x => x.Id_TipoActividad == TipoActividad));
                        //    break;
                        //case nameof(IndividuosEspecies.EstadoActividad):
                        //    int EstadoActividad = ServicioEstado.ObtenerIdEstadosActividadesPorNombre(filtro.Value);
                        //    predicate = (first ? x => x.Id_EstadosActividades == EstadoActividad : predicate.And(x => x.Id_EstadosActividades == EstadoActividad));
                        //    break;
                        //case nameof(IndividuosEspecies.NombreActividad):
                        //    string NombreActividad = filtro.Value;
                        //    predicate = (first ? x => x.NombreActividad == NombreActividad : predicate.And(x => x.NombreActividad == NombreActividad));
                        //    break;

                        //case nameof(IndividuosEspecies.FechaInicioStr):
                        //    DateTime FechaInicio = Convert.ToDateTime(filtro.Value);
                        //    predicate = (first ? x => x.FechaInicio == FechaInicio : predicate.And(x => x.FechaInicio == FechaInicio));
                        //    break;

                        //case nameof(IndividuosEspecies.FechaFinStr):
                        //    DateTime FechaFin = Convert.ToDateTime(filtro.Value);
                        //    predicate = (first ? x => x.FechaFin == FechaFin : predicate.And(x => x.FechaFin == FechaFin));
                        //    break;

                        //case nameof(IndividuosEspecies.PorcentajeAvance):
                        //    int PorcentajeAvance = Convert.ToInt32(filtro.Value);
                        //    predicate = (first ? x => x.PorcentajeAvance == PorcentajeAvance : predicate.And(x => x.PorcentajeAvance == PorcentajeAvance));
                        //    break;

                        //case nameof(IndividuosEspecies.Avance):
                        //    string Avance = filtro.Value;
                        //    predicate = (first ? x => x.Avance == Avance : predicate.And(x => x.Avance == Avance));
                        //    break;
                }
                first = false;
            }

            CantidadRegistros = repositorio.Count(predicate);

            switch (columnaOrden)
            {
                case nameof(IndividuosEspecies.Id_IndividuoEspecie):
                    order = x => x.Id_IndividuoEspecie;
                    isDate = false;
                    break;
            }


            IEnumerable<IndividuosEspecies> result;
            if (isDate)
                result = repositorio.GetPagedElements<DateTime>(pageIndex, pageCount, orderDate, predicate, ascending);
            else
                result = repositorio.GetPagedElements<int>(pageIndex, pageCount, order, predicate, ascending);

            return result.ToList();
        }

    }
}