﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BosquesPaz.Models;
using BosquesPaz.Repositories;
using System.Linq.Expressions;
using BosquesPaz.Repositories.Especificaciones;

namespace BosquesPaz.Services
{
    /// <summary>
    /// Clase que verifica que las peticiones de los datos sean correctas. 
    /// </summary>
    /// <remarks>
    /// <h3>Calidad</h3>
    /// </hr>
    /// <b>Requerimiento:</b> Help Desk</br>
    /// <b>Version:</b> 1</br>
    /// <b>Autor:</b> Grupo Cubo Ltda.</br>
    /// <b>Fecha:</b> 18/12/2017</br>
    /// <b>Descripcion:</b> Funciona para solicitar los datos al repositorio de fichas y retornar los datos obtenidos.
    /// </remarks>
    public class FichasService
    {
        /// <summary>
        /// Propiedad que instancia la clase FichasRepository
        /// </summary>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 18/12/2017</br>
        /// <b>Descripcion:</b> Propiedad que instancia el repositorio FichasRepository, para enviar peticiones y recibir el resultado.
        /// </remarks>
        private FichasRepository repositorio { get { return new FichasRepository(); } }


        /// <summary>
        /// Guardar una instancia de FichasBosques en la base de datos.
        /// </summary>
        /// <param name="ficha">Instancia de FichasBosques</param>
        /// <returns>Retorna instancia de FichasBosques</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 18/12/2017</br>
        /// <b>Descripcion:</b> Recibi una instancia de FichasBosques y la envia a la clase de repositorio, y retorna la instancia.
        /// </remarks>
        public FichasBosques GuardarFicha(FichasBosques ficha)
        {
            try
            {
                ficha = repositorio.Create(ficha);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return ficha;
        }

        /// <summary>
        /// Actualizar datos de ActividadesBosques en la base de datos.
        /// </summary>
        /// <param name="ficha">Instancia de FichasBosques</param>
        /// <returns>Retorna valor booleano</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 18/12/2017</br>
        /// <b>Descripcion:</b> Recibe una instancia de FichasBosques y la envia a la clase de repositorio, y retorna un valor booleano.
        /// </remarks>
        public bool ActualizarFicha(FichasBosques ficha)
        {
            bool result = false;
            try
            {
                if (repositorio.Update(ficha))
                {
                    result = true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }

        /// <summary>
        /// Obtener datos de FichasBosques de la base de datos.
        /// </summary>
        /// <param name="idFichaBosque">Identificador de la ficha</param>
        /// <returns>Retorna instancia de FichasBosques</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 18/12/2017</br>
        /// <b>Descripcion:</b> Obtiene una instancia de la ficha segun el numero de identificador que ingrese.
        /// </remarks>
        public FichasBosques ObtenerFichaBosque(int idFichaBosque)
        {
            FichasBosques result = null;
            try
            {
                result = repositorio.GetByKey(idFichaBosque);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }
    }
}