﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BosquesPaz.Models;
using BosquesPaz.Repositories;
using System.Linq.Expressions;
using BosquesPaz.Repositories.Especificaciones;

namespace BosquesPaz.Services
{
    /// <summary>
    /// Clase que verifica que las peticiones de los datos sean correctas. 
    /// </summary>
    /// <remarks>
    /// <h3>Calidad</h3>
    /// </hr>
    /// <b>Requerimiento:</b> Help Desk</br>
    /// <b>Version:</b> 1</br>
    /// <b>Autor:</b> Grupo Cubo Ltda.</br>
    /// <b>Fecha:</b> 18/12/2017</br>
    /// <b>Descripcion</b>: Funciona para solicitar los datos al repositorio de oportunidades y retornar los datos obtenidos.
    /// </remarks>
    public class OportunidadesServices
    {
        /// <summary>
        /// Propiedad que instancia la clase OportunidadesRepository
        /// </summary>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 18/12/2017</br>
        /// <b>Descripcion:</b> Propiedad que instancia el repositorio OportunidadesRepository, para enviar peticiones y recibir el resultado.
        /// </remarks>
        private OportunidadesRepository repositorio { get{ return new OportunidadesRepository(); } }

        /// <summary>
        /// Propiedad de número entero
        /// </summary>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 18/12/2017</br>
        /// <b>Descripcion:</b> Propiedad que define un número entero, para verificar el numero de registros que existen en una entidad.
        /// </remarks>
        public int CantidadRegistros { get; private set; }

        /// <summary>
        /// Guardar una instancia de Oportunidades en la base de datos.
        /// </summary>
        /// <param name="historia">Instancia de HistoriasBosques de </param>
        /// <param name="oportunidad">Instancia de Oportunidades</param>
        /// <returns>Retorna instancia de Oportunidades</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 18/12/2017</br>
        /// <b>Descripcion:</b> Recibe una instancia de Oportunidades y la envia a la clase de repositorio, y retorna la instancia.
        /// </remarks>
        public Oportunidades GuardarOportunidad(Oportunidades oportunidad)
        {
            try
            {
                oportunidad = repositorio.Create(oportunidad);
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return oportunidad;
        }

        /// <summary>
        /// Obtener datos de Oportunidades de la base de datos.
        /// </summary>
        /// <param name="idOportunidad">Identificador de la oportunidad</param>
        /// <returns>Retorna instancia de Oportunidades</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 18/12/2017</br>
        /// <b>Descripcion:</b> Obtiene una instancia de la oportunidad segun el numero de identificador que ingrese.
        /// </remarks>
        public Oportunidades ObtenerOportunidad(int idOportunidad)
        {
            Oportunidades result = null;
            try
            {
                result = repositorio.GetByKey(idOportunidad);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }

        /// <summary>
        /// Método que retorna una coleccion de datos, segun sus parametros.
        /// </summary>
        /// <param name="pageIndex">Número de paginas.</param>
        /// <param name="pageCount">Número de paginaciones.</param>
        /// <param name="columnaOrden">Cadena de caracteres de una columna.</param>
        /// <param name="filtros">Cadena de caracteres</param>
        /// <param name="ascending">Tipo de dato verdadero o falso</param>
        /// <param name="id">Identificador del patrocinador</param>
        /// <returns>Retorna coleccion de Oportunidades</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 18/12/2017</br>
        /// <b>Descripcion:</b> Método que permite obtener los datos de Oportunidades, y organizarlos con el proposito de retornarlos a una grilla.
        /// </remarks>
        public IEnumerable<Oportunidades> ObtenerOportunidades(int pageIndex, int pageCount, string columnaOrden, IDictionary<string, string> filtros, bool ascending, int id)
        {
            Expression<Func<Oportunidades, bool>> predicate = x => x.Id_PatrocinadorBosque == id;
            Expression<Func<Oportunidades, int>> order = x => x.Id_Oportunidad;
            Expression<Func<Oportunidades, DateTime>> orderDate = null;
            bool first = true;
            bool isDate = false;

            foreach (var filtro in filtros)
            {
                switch (filtro.Key)
                {
                    default:
                        predicate = x => false;
                        break;
                }
                first = false;
            }

            CantidadRegistros = repositorio.Count(predicate);

            switch (columnaOrden)
            {
                case nameof(Oportunidades.Id_Oportunidad):
                    order = x => x.Id_Oportunidad;
                    isDate = false;
                    break;
            }


            IEnumerable<Oportunidades> result;
            if (isDate)
                result = repositorio.GetPagedElements<DateTime>(pageIndex, pageCount, orderDate, predicate, ascending);
            else
                result = repositorio.GetPagedElements<int>(pageIndex, pageCount, order, predicate, ascending);

            return result.ToList();
        }

        /// <summary>
        /// Método que retorna una coleccion de datos, segun sus parametros.
        /// </summary>
        /// <param name="pageIndex">Número de paginas.</param>
        /// <param name="pageCount">Número de paginaciones.</param>
        /// <param name="columnaOrden">Cadena de caracteres de una columna.</param>
        /// <param name="filtros">Cadena de caracteres</param>
        /// <param name="ascending">Tipo de dato verdadero o falso</param>
        /// <returns>Retorna coleccion de Oportunidades</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 18/12/2017</br>
        /// <b>Descripcion:</b> Método que permite obtener los datos de Oportunidades para el administrador, y organizarlos con el proposito de retornarlos a una grilla.
        /// </remarks>
        public IEnumerable<Oportunidades> ObtenerOportunidadesAdmin(int pageIndex, int pageCount, string columnaOrden, IDictionary<string, string> filtros, bool ascending)
        {
            Expression<Func<Oportunidades, bool>> predicate = x => x.Id_PatrocinadorBosque > 0;
            Expression<Func<Oportunidades, int>> order = x => x.Id_Oportunidad;
            Expression<Func<Oportunidades, DateTime>> orderDate = null;
            bool first = true;
            bool isDate = false;

            foreach (var filtro in filtros)
            {
                switch (filtro.Key)
                {
                    default:
                        predicate = x => false;
                        break;
                }
                first = false;
            }

            CantidadRegistros = repositorio.Count(predicate);

            switch (columnaOrden)
            {
                case nameof(Oportunidades.Id_Oportunidad):
                    order = x => x.Id_Oportunidad;
                    isDate = false;
                    break;
            }


            IEnumerable<Oportunidades> result;
            if (isDate)
                result = repositorio.GetPagedElements<DateTime>(pageIndex, pageCount, orderDate, predicate, ascending);
            else
                result = repositorio.GetPagedElements<int>(pageIndex, pageCount, order, predicate, ascending);

            return result.ToList();
        }
    }
}