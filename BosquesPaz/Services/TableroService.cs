﻿using BosquesPaz.Models;
using BosquesPaz.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BosquesPaz.Helpers;
using BosquesPaz.ServiceReferenceFuncionarios;

namespace BosquesPaz.Services
{
    public class TableroService
    {
        private TableroRepository repositorio
        {
            get
            {
                return new TableroRepository();
            }
        }
        public IEnumerable<ActividadesGRFBosques> ActividadesBosques()
        {
            IEnumerable<ActividadesGRFBosques> result = new List<ActividadesGRFBosques>();
            try
            {
                result = repositorio.ActividadesBosques();
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return result;
        }

        public IEnumerable<BeneficiandoBosques> BeneficiandoBosques()
        {
            IEnumerable<BeneficiandoBosques> result = new List<BeneficiandoBosques>();
            try
            {
                result = repositorio.BeneficiandoBosques();
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return result;
        }

        public CantidadBosques CantidadBosques()
        {
            CantidadBosques result = new CantidadBosques();
            try
            {
                result = repositorio.CantidadBosques();
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return result;
        }

        public IEnumerable<DistribucionBosques> DistribucionBosques()
        {
            IEnumerable<DistribucionBosques> result = new List<DistribucionBosques>();
            try
            {
                result = repositorio.DistribucionBosques();
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return result;
        }

        public IEnumerable<InversionesBosques> InversionesBosques()
        {
            IEnumerable<InversionesBosques> result = new List<InversionesBosques>();
            try
            {

                result = repositorio.InversionesBosques();
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return result;
        }

        public IEnumerable<InversionistasBosques> InversionistasBosques()
        {
            IEnumerable<InversionistasBosques> result = new List<InversionistasBosques>();
            try
            {
                result = repositorio.InversionistasBosques();
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return result;
        }

        public IEnumerable<LogrosBosques> LogrosBosques()
        {
            IEnumerable<LogrosBosques> result = new List<LogrosBosques>();
            try
            {
                result = repositorio.LogrosBosques();
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return result;
        }

        public IEnumerable<ProductosServiciosBosques> ProductosServiciosBosques()
        {
            IEnumerable<ProductosServiciosBosques> result = new List<ProductosServiciosBosques>();
            try
            {

                result = repositorio.ProductosServiciosBosques();
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return result;
        }

        public IEnumerable<HistorialIndicadores> HistorialIndicadores()
        {
            IEnumerable<HistorialIndicadores> result = new List<HistorialIndicadores>();
            try
            {

                result = repositorio.GetAll();
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return result;
        }

    }
}