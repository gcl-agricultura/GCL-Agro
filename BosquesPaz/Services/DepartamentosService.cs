﻿using BosquesPaz.Models;
using BosquesPaz.Repositories;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Web;

namespace BosquesPaz.Services
{
    /// <summary>
    /// Clase que verifica que las peticiones de los datos sean correctas. 
    /// </summary>
    /// <remarks>
    /// <h3>Calidad</h3>
    /// </hr>
    /// <b>Requerimiento:</b> Help Desk</br>
    /// <b>Version:</b> 1</br>
    /// <b>Autor:</b> Grupo Cubo Ltda.</br>
    /// <b>Fecha:</b> 18/12/2017</br>
    /// <b>Descripcion:</b> Funciona para solicitar los datos al repositorio de departamentos y retornar los datos obtenidos.
    /// </remarks>
    public class DepartamentosService
    {
        /// <summary>
        /// Propiedad que instancia la clase DepartamentosRepository
        /// </summary>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 18/12/2017</br>
        /// <b>Descripcion:</b> Propiedad que instancia el repositorio DepartamentosRepository, para enviar peticiones y recibir el resultado.
        /// </remarks>
        private DepartamentosRepository repositorio
        {
            get
            {
                return new DepartamentosRepository();
            }
        }

        /// <summary>
        /// Obtener datos de los Departamentos de la base de datos.
        /// </summary>
        /// <returns>Retorna coleccion de Departamentos</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 18/12/2017</br>>
        /// <b>Descripcion:</b> Obtiene una coleccion de los departamentos.
        /// </remarks>
        public IEnumerable<Departamentos> ObtenerDepartamentos()
        {
            IEnumerable<Departamentos> departamentos = new List<Departamentos>();
            try
            {
                departamentos = repositorio.GetAll();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return departamentos;
        }

        /// <summary>
        /// Obtener el identificador de Departamentos de la base de datos.
        /// </summary>
        /// <param name="nombre">nombre del departemento</param>
        /// <returns>Retorna numero entero</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 18/12/2017</br>
        /// <b>Descripcion:</b> Obtiene el identificador del departamento, segun el nombre que ingrese.
        /// </remarks>
        public int ObtenerIdDepartamentoPorNombre(string nombre)
        {
            int result;
            try
            {
                Expression<Func<Departamentos, bool>> predicate = x => x.Nombre == nombre;
                result = repositorio.GetBy(predicate).FirstOrDefault().Id_Departamento;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return result;
        }

        /// <summary>
        /// Obtener el nombre de Departamentos de la base de datos.
        /// </summary>
        /// <param name="idDepartamento">Identificador del departamento</param>
        /// <returns>Retorna cadena de caracteres</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 18/12/2017</br>>
        /// <b>Descripcion:</b> Obtiene el nombre del departamento, segun el numero de indentificador que ingrese.
        /// </remarks>
        public string ObtenerNombreDepartamentoPorId(int idDepartamento)
        {
            string result = string.Empty;
            try
            {
                Expression<Func<Departamentos, bool>> predicate = x => x.Id_Departamento == idDepartamento;
                result = repositorio.GetBy(predicate).FirstOrDefault().Nombre;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return result;
        }

        /// <summary>
        /// Obtener coleccion del codigo dane de Departamentos de la base de datos.
        /// </summary>
        /// <param name="departamentos">Lista de departamentos</param>
        /// <returns>Retorna coleccion de numeros enteros</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 18/12/2017</br>
        /// <b>Descripcion:</b> Obtiene una coleccion del departamento, segun la lista que ingrese.
        /// </remarks>
        public IEnumerable<int> ObtenerCodigoDaneDepartamentos(IList<int> departamentos)
        {
            IEnumerable<int> result = null;
            try
            {
                Expression<Func<Departamentos, bool>> predicate = x => departamentos.Contains(x.Id_Departamento);
                var deptos = repositorio.GetBy(predicate).ToList();
                if (deptos.Count > 0)
                {
                    result = deptos.Select(d => d.CodigoDane);
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }


            return result;
        }

    }
}