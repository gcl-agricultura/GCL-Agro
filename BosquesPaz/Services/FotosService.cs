﻿using BosquesPaz.Models;
using BosquesPaz.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BosquesPaz.Helpers;
using BosquesPaz.ServiceReferenceFuncionarios;

namespace BosquesPaz.Services
{
    /// <summary>
    /// Clase que verifica que las peticiones de los datos sean correctas. 
    /// </summary>
    /// <remarks>
    /// <h3>Calidad</h3>
    /// </hr>
    /// <b>Requerimiento:</b> Help Desk</br>
    /// <b>Version:</b> 1</br>
    /// <b>Autor:</b> Grupo Cubo Ltda.</br>
    /// <b>Fecha:</b> 18/12/2017</br>
    /// <b>Descripcion:</b> Funciona para solicitar los datos al repositorio de fotos y retornar los datos obtenidos.
    /// </remarks>
    public class FotosService
    {
        /// <summary>
        /// Propiedad que instancia la clase FotosRepository
        /// </summary>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 18/12/2017</br>
        /// <b>Descripcion:</b> Propiedad que instancia el repositorio FotosRepository, para enviar peticiones y recibir el resultado.
        /// </remarks>
        private FotosRepository repositorio
        {
            get
            {
                return new FotosRepository();
            }
        }

        /// <summary>
        /// Propiedad que instancia la clase UsuariosJuegosRepository
        /// </summary>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 18/12/2017</br>
        /// <b>Descripcion:</b> Propiedad que instancia el repositorio UsuariosJuegosRepository, para enviar peticiones y recibir el resultado.
        /// </remarks>
        private UsuariosJuegosRepository usuarioJuegoRepository { get { return new UsuariosJuegosRepository(); } }

        /// <summary>
        /// Obtener datos de FotosBosques de la base de datos.
        /// </summary>
        /// <param name="info">Instancia de la entidad de un usuario</param>
        /// <returns>Retorna coleccion de FotosBosques</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 18/12/2017</br>
        /// <b>Descripcion:</b> Obtiene una coleccion de fotos bosques segun el nombre del usurio que ingrese.
        /// </remarks>
        public IEnumerable<FotosBosques> ObtenerFotosBosques(EntidadInfoUsuario info)
        {
            IEnumerable<FotosBosques> foto = null;
            bool secuencia = true;
            var infraestructura = new Infraestructura();
            var coleccionFotoId = new List<int>();
            var result = repositorio.ObtenerFotosBosques();
            var userGame = usuarioJuegoRepository.ObtenerUsuarioJuego();
            Random numero = new Random();
            try
            {
                for (int i = 0; i < result.Count(); i++)
                {
                    secuencia = true;
                    for (int j = 0; j < userGame.Count(); j++)
                    {
                        if (result.ToList()[i].Url == userGame.ToList()[j].Imagen && info.Usuario == userGame.ToList()[j].Usuario)
                        {
                            secuencia = false;
                            j = userGame.Count();
                        }
                    }
                    if (secuencia == true)
                    {
                        coleccionFotoId.Add(result.ToList()[i].Id_FotoBosque);
                    }
                }
                if (coleccionFotoId.Count() > 0)
                {
                    var id = numero.Next(coleccionFotoId.First(), coleccionFotoId.Last() + 1);
                    foto = repositorio.ObtenerFotosBosquesPorId(id);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return foto;
        }

        /// <summary>
        /// Obtener coleccion de FotosBosques de la base de datos.
        /// </summary>
        /// <param name="idBosque">Identificador del bosque</param>
        /// <returns>Retorna coleccion de FotosBosques</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 18/12/2017</br>
        /// <b>Descripcion:</b> Obtiene una coleccion de fotos bosques segun el numero de identificador que ingrese.
        /// </remarks>
        public IEnumerable<FotosBosques> ObtenerFotosBosque(int idBosque)
        {
            IEnumerable<FotosBosques> result = new List<FotosBosques>();
            try
            {
                var query = repositorio.ObtenerFotosBosque(idBosque);
                if (query != null && query.Count() > 0)
                {
                    result = query;
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }

        /// <summary>
        /// Guardar una coleccion de FotosBosques en la base de datos.
        /// </summary>
        /// <param name="fotos">Coleccion de fotos bosques</param>
        /// <returns>Retorna coleccion de FotosBosques</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 18/12/2017</br>
        /// <b>Descripcion:</b> Recibe una coleccion de FotosBosques y la envia a la clase de repositorio, y retorna la coleccion.
        /// </remarks>
        public IEnumerable<FotosBosques> GuardarFotosBosque(IEnumerable<FotosBosques> fotos)
        {
            IEnumerable<FotosBosques> result = new List<FotosBosques>();
            var IdBosque = fotos.FirstOrDefault().Id_Bosque;
            try
            {
                repositorio.GuardarFotosBosque(fotos);
                result = ObtenerFotosBosque(IdBosque);
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return result;
        }

        /// <summary>
        /// Obtener datos de FotosIndividuosEspecies de la base de datos.
        /// </summary>
        /// <param name="info">Instancia de la entidad de un usuario</param>
        /// <returns>Retorna coleccion de FotosIndividuosEspecies</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 18/12/2017</br>
        /// <b>Descripcion:</b> Obtiene una coleccion de fotos individuos/especies segun el nombre del usurio que ingrese.
        /// </remarks>
        public IEnumerable<FotosIndividuosEspecies> ObtenerFotoIndividuoEspecie(EntidadInfoUsuario info)
        {
            IEnumerable<FotosIndividuosEspecies> foto = null;
            bool secuencia = true;
            var infraestructura = new Infraestructura();
            var coleccionFotoId = new List<int>();
            var result = repositorio.ObtenerFotosIndividuosEspecies();
            var userGame = usuarioJuegoRepository.ObtenerUsuarioJuego();
            Random numero = new Random();
            try
            {
                for (int i = 0; i < result.Count(); i++)
                {
                    secuencia = true;
                    for (int j = 0; j < userGame.Count(); j++)
                    {
                        if (result.ToList()[i].Url == userGame.ToList()[j].Imagen && info.Usuario == userGame.ToList()[j].Usuario)
                        {
                            secuencia = false;
                            j = userGame.Count();
                        }
                    }
                    if (secuencia == true)
                    {
                        coleccionFotoId.Add(result.ToList()[i].Id_FotoIndividuoEspecie);
                    }
                }
                if (coleccionFotoId.Count() > 0)
                {
                    var id = numero.Next(coleccionFotoId.First(), coleccionFotoId.Last() + 1);
                    foto = repositorio.ObtenerFotoIndividuoEspeciePorId(id);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return foto;
        }

        /// <summary>
        /// Obtener coleccion de FotosIndividuosEspecies de la base de datos.
        /// </summary>
        /// <param name="idIndividuoEspecie">Identificador de los individuos/especies</param>
        /// <returns>Retorna coleccion de FotosIndividuosEspecies</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 18/12/2017</br>
        /// <b>Descripcion:</b> Obtiene una coleccion de fotos individuos/especies segun el numero de identificador que ingrese.
        /// </remarks>
        public IEnumerable<FotosIndividuosEspecies> ObtenerFotosIndividuoEspecie(int idIndividuoEspecie)
        {
            IEnumerable<FotosIndividuosEspecies> result = new List<FotosIndividuosEspecies>();
            try
            {
                var query = repositorio.ObtenerFotosIndividuosEspecies(idIndividuoEspecie);
                if (query != null && query.Count() > 0)
                {
                    result = query;
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }

        /// <summary>
        /// Guardar una coleccion de FotosIndividuosEspecies en la base de datos.
        /// </summary>
        /// <param name="fotos">Coleccion de fotos individuos especies</param>
        /// <returns>Retorna coleccion de FotosIndividuosEspecies</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 18/12/2017</br>
        /// <b>Descripcion:</b> Recibe una coleccion de FotosIndividuosEspecies y la envia a la clase de repositorio, y retorna la coleccion.
        /// </remarks>
        public IEnumerable<FotosIndividuosEspecies> GuardarIndividuosEspecies(IEnumerable<FotosIndividuosEspecies> fotos)
        {
            IEnumerable<FotosIndividuosEspecies> result = new List<FotosIndividuosEspecies>();
            var IdIndividuoEspecie = fotos.FirstOrDefault().Id_IndividuoEspecie;
            try
            {
                repositorio.GuardarFotosIndividuosEspecies(fotos);
                result = ObtenerFotosIndividuoEspecie(IdIndividuoEspecie);
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return result;
        }

        /// <summary>
        /// Obtener datos de FotosProductosServicios de la base de datos.
        /// </summary>
        /// <param name="info">Instancia de la entidad de un usuario</param>
        /// <returns>Retorna coleccion de FotosProductosServicios</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 18/12/2017</br>
        /// <b>Descripcion:</b> Obtiene una coleccion de fotos productos/servicios segun el nombre del usurio que ingrese.
        /// </remarks>
        public IEnumerable<FotosProductosServicios> ObtenerFotoProductos(EntidadInfoUsuario info)
        {
            IEnumerable<FotosProductosServicios> foto = null;
            bool secuencia = true;
            var infraestructura = new Infraestructura();
            var coleccionFotoId = new List<int>();
            var result = repositorio.ObtenerFotosProductosServicios();
            var userGame = usuarioJuegoRepository.ObtenerUsuarioJuego();
            Random numero = new Random();
            try
            {
                for (int i = 0; i < result.Count(); i++)
                {
                    secuencia = true;
                    for (int j = 0; j < userGame.Count(); j++)
                    {
                        if (result.ToList()[i].Url == userGame.ToList()[j].Imagen && info.Usuario == userGame.ToList()[j].Usuario)
                        {
                            secuencia = false;
                            j = userGame.Count();
                        }
                    }
                    if (secuencia == true)
                    {
                        coleccionFotoId.Add(result.ToList()[i].Id_FotoProductoServicio);
                    }
                }
                if (coleccionFotoId.Count() > 0)
                {
                    var id = numero.Next(coleccionFotoId.First(), coleccionFotoId.Last() + 1);
                    foto = repositorio.ObtenerFotoProductosServiciosPorId(id);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return foto;
        }

        /// <summary>
        /// Obtener coleccion de FotosProductosServicios de la base de datos.
        /// </summary>
        /// <param name="idProductosServicios">Identificador de los productos/servicios</param>
        /// <returns>Retorna coleccion de FotosProductosServicios</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 18/12/2017</br>
        /// <b>Descripcion:</b> Obtiene una coleccion de fotos productos/servicios segun el numero de identificador que ingrese.
        /// </remarks>
        public IEnumerable<FotosProductosServicios> ObtenerFotosProductosServicios(int idProductosServicios)
        {
            IEnumerable<FotosProductosServicios> result = new List<FotosProductosServicios>();
            try
            {
                var query = repositorio.ObtenerFotosProductosServicios(idProductosServicios);
                if (query != null && query.Count() > 0)
                {
                    result = query;
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }

        /// <summary>
        /// Guardar una coleccion de FotosProductosServicios en la base de datos.
        /// </summary>
        /// <param name="fotos">Coleccion de fotos productos/servicios</param>
        /// <returns>Retorna coleccion de FotosProductosServicios</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 18/12/2017</br>
        /// <b>Descripcion:</b> Recibe una coleccion de FotosProductosServicios y la envia a la clase de repositorio, y retorna la coleccion.
        /// </remarks>
        public IEnumerable<FotosProductosServicios> GuardarProductosServicios(IEnumerable<FotosProductosServicios> fotos)
        {
            IEnumerable<FotosProductosServicios> result = new List<FotosProductosServicios>();
            var IdProductosServicios = fotos.FirstOrDefault().Id_ProductoServicio;
            try
            {
                repositorio.GuardarFotosProductosServicios(fotos);
                result = ObtenerFotosProductosServicios(IdProductosServicios);
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return result;
        }

        /// <summary>
        /// Obtener coleccion de FotosIndividuosEspecies de la base de datos.
        /// </summary>
        /// <returns>Retorna coleccion de FotosIndividuosEspecies</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 18/12/2017</br>
        /// <b>Descripcion:</b> Obtiene una coleccion de fotos individuos/especies.
        /// </remarks>
        public IEnumerable<FotosIndividuosEspecies> ObtenerFotoIndividuoEspecieSinUsuario()
        {
            IEnumerable<FotosIndividuosEspecies> foto = null;
            var coleccionFotoId = new List<int>();
            var result = repositorio.ObtenerFotosIndividuosEspecies();
            Random numero = new Random();
            try
            {
                foreach (var item in result)
                {
                    coleccionFotoId.Add(item.Id_FotoIndividuoEspecie);
                }
                var id = numero.Next(coleccionFotoId.First(), coleccionFotoId.Last() + 1);
                foto = repositorio.ObtenerFotoIndividuoEspeciePorId(id);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return foto;
        }

        /// <summary>
        /// Obtener coleccion de FotosProductosServicios de la base de datos.
        /// </summary>
        /// <returns>Retorna coleccion de FotosProductosServicios</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 18/12/2017</br>
        /// <b>Descripcion:</b> Obtiene una coleccion de fotos productos/servicios.
        /// </remarks>
        public IEnumerable<FotosProductosServicios> ObtenerFotoProductosServiciosSinUsuario()
        {
            IEnumerable<FotosProductosServicios> foto = null;
            var coleccionFotoId = new List<int>();
            var result = repositorio.ObtenerFotosProductosServicios();
            Random numero = new Random();
            try
            {
                foreach (var item in result)
                {
                    coleccionFotoId.Add(item.Id_FotoProductoServicio);
                }
                var id = numero.Next(coleccionFotoId.First(), coleccionFotoId.Last() + 1);
                foto = repositorio.ObtenerFotoProductosServiciosPorId(id);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return foto;
        }

        /// <summary>
        /// Obtener coleccion de FotosBosques de la base de datos.
        /// </summary>
        /// <returns>Retorna coleccion de FotosBosques</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 18/12/2017</br>
        /// <b>Descripcion:</b> Obtiene una coleccion de fotos de bosques.
        /// </remarks>
        public IEnumerable<FotosBosques> ObtenerFotoFotosBosquesSinUsuario()
        {
            IEnumerable<FotosBosques> foto = null;
            var coleccionFotoId = new List<int>();
            var result = repositorio.ObtenerFotosBosques();
            Random numero = new Random();
            try
            {
                foreach (var item in result)
                {
                    coleccionFotoId.Add(item.Id_FotoBosque);
                }
                var id = numero.Next(coleccionFotoId.First(), coleccionFotoId.Last() + 1);
                foto = repositorio.ObtenerFotosBosquesPorId(id);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return foto;
        }

        /// <summary>
        /// Obtener coleccion de FotosUsuarios de la base de datos.
        /// </summary>
        /// <returns>Retorna coleccion de FotosUsuarios</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 18/12/2017</br>
        /// <b>Descripcion:</b> Obtiene una coleccion de fotos de usuarios.
        /// </remarks>
        public IEnumerable<FotosUsuarios> ObtenerFotosUsuarios()
        {
            IEnumerable<FotosUsuarios> result = new List<FotosUsuarios>();
            try
            {
                var query = repositorio.ObtenerFotosUsuarios();
                if (query != null && query.Count() > 0)
                {
                    result = query;
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }

        /// <summary>
        /// Obtener coleccion de FotosUsuarios de la base de datos.
        /// </summary>
        /// <param name="idFotoUsuario">Identificador de la foto de usuario</param>
        /// <returns>Retorna coleccion de FotosUsuarios</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 18/12/2017</br>
        /// <b>Descripcion:</b> Obtiene una coleccion de fotos de usuarios segun el numero de identificador que ingrese.
        /// </remarks>
        public IEnumerable<FotosUsuarios> ObtenerFotosUsuariosPorId(int idFotoUsuario)
        {
            IEnumerable<FotosUsuarios> result = new List<FotosUsuarios>();
            try
            {
                var query = repositorio.ObtenerFotosUsuariosPorId(idFotoUsuario);
                if (query != null && query.Count() > 0)
                {
                    result = query;
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }
    }
}