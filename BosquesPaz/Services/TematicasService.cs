﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BosquesPaz.Models;
using System.Linq.Expressions;
using BosquesPaz.Repositories;

namespace BosquesPaz.Services
{
    /// <summary>
    /// Clase que verifica que las peticiones de los datos sean correctas. 
    /// </summary>
    /// <remarks>
    /// <h3>Calidad</h3>
    /// </hr>
    /// <b>Requerimiento:</b> Help Desk</br>
    /// <b>Version:</b> 1</br>
    /// <b>Autor:</b> Grupo Cubo Ltda.</br>
    /// <b>Fecha:</b> 18/12/2017</br>
    /// <b>Descripcion:</b> Funciona para solicitar los datos al repositorio de tematicas y retornar los datos obtenidos.
    /// </remarks>
    public class TematicasService
    {
        /// <summary>
        /// Propiedad que instancia la clase TematicasRepository
        /// </summary>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 18/12/2017</br>
        /// <b>Descripcion:</b> Propiedad que instancia el repositorio TematicasRepository, para enviar peticiones y recibir el resultado.
        /// </remarks>
        private TematicasRepository repositorio
        {
            get
            {
                return new TematicasRepository();
            }
        }

        /// <summary>
        /// Obtener coleccion de Tematicas de la base de datos.
        /// </summary>
        /// <returns>Retorna coleccion de Tematicas</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 18/12/2017</br>
        /// <b>Descripcion:</b> Obtiene una coleccion de tematicas.
        /// </remarks>
        public IEnumerable<Tematicas> ObtenerTematicas()
        {
            IEnumerable<Tematicas> tematicas = new List<Tematicas>();
            try
            {
                tematicas = repositorio.GetAll();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return tematicas;
        }

        /// <summary>
        /// Obtener identificador de Tematicas de la base de datos.
        /// </summary>
        /// <param name="nombre">Nombre de la tematica</param>
        /// <returns>Retorna numero entero</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 18/12/2017</br>
        /// <b>Descripcion:</b> Obtiene el identificador de tematicas segun el nombre que se ingrese.
        /// </remarks>
        public int ObtenerIdTematicasPorNombre(string nombre)
        {
            int result;
            try
            {
                Expression<Func<Tematicas, bool>> predicate = x => x.Nombre == nombre;
                result = repositorio.GetBy(predicate).FirstOrDefault().Id_Tematica;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return result;
        }

        /// <summary>
        /// Obtener nombre de Tematicas de la base de datos.
        /// </summary>
        /// <param name="idTematica">Identificador de la tematica</param>
        /// <returns>Retorna cadena de caracteres</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 18/12/2017</br>
        /// <b>Descripcion:</b> Obtiene el nombre de tematicas segun el numero de identificador que se ingrese.
        /// </remarks>
        public string ObtenerNombreTematicasPorId(int idTematica)
        {
            string result = string.Empty;
            try
            {
                Expression<Func<Tematicas, bool>> predicate = x => x.Id_Tematica == idTematica;
                result = repositorio.GetBy(predicate).FirstOrDefault().Nombre;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return result;
        }
    }
}