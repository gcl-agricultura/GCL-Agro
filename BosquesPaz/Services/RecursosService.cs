﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BosquesPaz.Models;
using BosquesPaz.Repositories;
using System.Linq.Expressions;
using BosquesPaz.Repositories.Especificaciones;

namespace BosquesPaz.Services
{
    /// <summary>
    /// Clase que verifica que las peticiones de los datos sean correctas. 
    /// </summary>
    /// <remarks>
    /// <h3>Calidad</h3>
    /// </hr>
    /// <b>Requerimiento:</b> Help Desk</br>
    /// <b>Version:</b> 1</br>
    /// <b>Autor:</b> Grupo Cubo Ltda.</br>
    /// <b>Fecha:</b> 18/12/2017</br>
    /// <b>Descripcion:</b> Funciona para solicitar los datos al repositorio de recursos y retornar los datos obtenidos.
    /// </remarks>
    public class RecursosService
    {
        /// <summary>
        /// Propiedad que instancia la clase RecursosRepository
        /// </summary>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 18/12/2017</br>
        /// <b>Descripcion:</b> Propiedad que instancia el repositorio RecursosRepository, para enviar peticiones y recibir el resultado.
        /// </remarks>
        private RecursosRepository repositorio { get { return new RecursosRepository(); } }

        /// <summary>
        /// Propiedad que instancia la clase EstadosService
        /// </summary>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 18/12/2017</br>
        /// <b>Descripcion:</b> Propiedad que instancia el servicio EstadosService, para enviar peticiones y recibir el resultado.
        /// </remarks>
        private EstadosService ServicioEstado { get { return new EstadosService(); } }

        /// <summary>
        /// Propiedad que instancia la clase TiposRecursosService
        /// </summary>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 18/12/2017</br>
        /// <b>Descripcion:</b> Propiedad que instancia el servicio TiposRecursosService, para enviar peticiones y recibir el resultado.
        /// </remarks>
        private TiposRecursosService ServicioTipo { get { return new TiposRecursosService(); } }

        /// <summary>
        /// Propiedad de número entero
        /// </summary>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 13/12/2017</br>
        /// <b>Descripcion:</b> Propiedad que define un número entero, para verificar el numero de registros que existen en una entidad.
        /// </remarks>
        public int CantidadRegistros { get; private set; }

        /// <summary>
        /// Guardar una instancia de RecursosBosques en la base de datos.
        /// </summary>
        /// <param name="recurso">Instancia de RecursosBosques</param>
        /// <returns>Retorna instancia de RecursosBosques</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 18/12/2017</br>
        /// <b>Descripcion:</b> Recibe una instancia de RecursosBosques y la envia a la clase de repositorio, y retorna la instancia.
        /// </remarks>
        public RecursosBosques GuardarRecurso(RecursosBosques recurso)
        {
            try
            {
                recurso = repositorio.Create(recurso);
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return recurso;
        }

        /// <summary>
        /// Actualizar datos de RecursosBosques en la base de datos.
        /// </summary>
        /// <param name="recurso">Instancia de RecursosBosques</param>
        /// <returns>Retorna instancia de RecursosBosques</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 18/12/2017</br>
        /// <b>Descripcion:</b> Recibe una instancia de RecursosBosques y la envia a la clase de repositorio, y retorna un valor booleano.
        /// </remarks>
        public bool ActualizarRecurso(RecursosBosques recurso)
        {
            bool result = false;
            try
            {
                if (repositorio.Update(recurso))
                {
                    result = true;
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return result;
        }

        /// <summary>
        /// Obtener datos de RecursosBosques de la base de datos.
        /// </summary>
        /// <param name="idRecursoBosque">Identificador del recurso</param>
        /// <returns>Retorna instancia de RecursosBosques</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 18/12/2017</br>
        /// <b>Descripcion:</b> Obtiene una instancia del recurso segun el numero de identificador que ingrese.
        /// </remarks>
        public RecursosBosques ObtenerRecursoBosque(int idRecursoBosque)
        {
            RecursosBosques result = null;
            try
            {
                result = repositorio.GetByKey(idRecursoBosque);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }

        /// <summary>
        /// Método que retorna una coleccion de datos, segun sus parametros.
        /// </summary>
        /// <param name="pageIndex">Número de paginas.</param>
        /// <param name="pageCount">Número de paginaciones.</param>
        /// <param name="columnaOrden">Cadena de caracteres de una columna.</param>
        /// <param name="filtros">Cadena de caracteres</param>
        /// <param name="ascending">Tipo de dato verdadero o falso</param>
        /// <param name="id">Identificador del bosque</param>
        /// <returns>Retorna coleccion de RecursosBosques</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 18/12/2017</br>
        /// <b>Descripcion:</b> Método que permite obtener los datos de RecursosBosques y organizarlos con el proposito de retornarlos a una grilla.
        /// </remarks>
        public IEnumerable<RecursosBosques> ObtenerRecursosBosques(int pageIndex, int pageCount, string columnaOrden, IDictionary<string, string> filtros, bool ascending, int id)
        {
            Expression<Func<RecursosBosques, bool>> predicate = x => x.Id_Bosque == id;
            Expression<Func<RecursosBosques, int>> order = x => x.Id_RecursoBosque;
            Expression<Func<RecursosBosques, DateTime>> orderDate = null;
            bool first = true;
            bool isDate = false;

            foreach (var filtro in filtros)
            {
                switch (filtro.Key)
                {
                    default:
                        predicate = x => false;
                        break;
                    case nameof(RecursosBosques):
                        string NombreRecurso = filtro.Value;
                        predicate = (first ? x => x.NombreRecurso == NombreRecurso : predicate.And(x => x.NombreRecurso == NombreRecurso));
                        break;
                    case nameof(RecursosBosques.TipoRecurso):
                        int Id_TipoRecurso = ServicioTipo.ObtenerIdTiposRecursosPorNombre(filtro.Value);
                        predicate = (first ? x => x.Id_TipoRecurso == Id_TipoRecurso : predicate.And(x => x.Id_TipoRecurso == Id_TipoRecurso));
                        break;
                    case nameof(RecursosBosques.Estado):
                        int Id_Estado = ServicioEstado.ObtenerIdEstadoPorNombre(filtro.Value);
                        predicate = (first ? x => x.Id_Estado == Id_Estado : predicate.And(x => x.Id_Estado == Id_Estado));
                        break;
                }
                first = false;
            }

            CantidadRegistros = repositorio.Count(predicate);

            switch (columnaOrden)
            {
                case nameof(RecursosBosques.Id_RecursoBosque):
                    order = x => x.Id_RecursoBosque;
                    isDate = false;
                    break;
            }


            IEnumerable<RecursosBosques> result;
            if (isDate)
                result = repositorio.GetPagedElements<DateTime>(pageIndex, pageCount, orderDate, predicate, ascending);
            else
                result = repositorio.GetPagedElements<int>(pageIndex, pageCount, order, predicate, ascending);

            return result.ToList();
        }

        
    }
}