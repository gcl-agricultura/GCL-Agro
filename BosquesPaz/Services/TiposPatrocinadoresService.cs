﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BosquesPaz.Models;
using System.Linq.Expressions;
using BosquesPaz.Repositories;

namespace BosquesPaz.Services
{
    /// <summary>
    /// Clase que verifica que las peticiones de los datos sean correctas. 
    /// </summary>
    /// <remarks>
    /// <h3>Calidad</h3>
    /// </hr>
    /// <b>Requerimiento:</b> Help Desk</br>
    /// <b>Version:</b> 1</br>
    /// <b>Autor:</b> Grupo Cubo Ltda.</br>
    /// <b>Fecha:</b> 18/12/2017</br>
    /// <b>Descripcion:</b> Funciona para solicitar los datos al repositorio de tipos de patrocinadores y retornar los datos obtenidos.
    /// </remarks>
    public class TiposPatrocinadoresService
    {
        /// <summary>
        /// Propiedad que instancia la clase TiposPatrocinadoresRepository
        /// </summary>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 18/12/2017</br>
        /// <b>Descripcion:</b> Propiedad que instancia el repositorio TiposPatrocinadoresRepository, para enviar peticiones y recibir el resultado.
        /// </remarks>
        private TiposPatrocinadoresRepository repositorio
        {
            get
            {
                return new TiposPatrocinadoresRepository();
            }
        }

        /// <summary>
        /// Obtener coleccion de TiposPatrocinadores de la base de datos.
        /// </summary>
        /// <returns>Retorna coleccion de TiposPatrocinadores</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 18/12/2017</br>
        /// <b>Descripcion:</b> Obtiene una coleccion de los tipos de patrocinadores.
        /// </remarks>
        public IEnumerable<TiposPatrocinadores> ObtenerTiposPatrocinador()
        {
            IEnumerable<TiposPatrocinadores> tiposPatrocinadores = new List<TiposPatrocinadores>();
            try
            {
                tiposPatrocinadores = repositorio.GetAll();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return tiposPatrocinadores;
        }

        /// <summary>
        /// Obtener identificador de TiposPatrocinadores de la base de datos.
        /// </summary>
        /// <param name="nombre">Nombre del tipo de patrocinador</param>
        /// <returns>Retorna numero entero</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 18/12/2017</br>
        /// <b>Descripcion:</b> Obtiene el identificador del tipo de patrocinador segun el nombre que se ingrese.
        /// </remarks>
        public int ObtenerIdTiposPatrocinadoresPorNombre(string nombre)
        {
            int result;
            try
            {
                Expression<Func<TiposPatrocinadores, bool>> predicate = x => x.Nombre == nombre;
                result = repositorio.GetBy(predicate).FirstOrDefault().Id_TipoPatrocinador;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return result;
        }

        /// <summary>
        /// Obtener nombre de TiposPatrocinadores de la base de datos.
        /// </summary>
        /// <param name="idTiposPatrocinador">Identificador de tipo de patrocinador</param>
        /// <returns>Retorna cadena de caracteres</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 18/12/2017</br>
        /// <b>Descripcion:</b> Obtiene el nombre del tipo de patrocinador segun el numero de identificador que se ingrese.
        /// </remarks>
        public string ObtenerNombreTiposPatrocinadorPorId(int idTiposPatrocinador)
        {
            string result = string.Empty;
            try
            {
                Expression<Func<TiposPatrocinadores, bool>> predicate = x => x.Id_TipoPatrocinador == idTiposPatrocinador;
                result = repositorio.GetBy(predicate).FirstOrDefault().Nombre;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return result;
        }
    }
}