﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BosquesPaz.Models;
using System.Linq.Expressions;
using BosquesPaz.Repositories;

namespace BosquesPaz.Services
{
    /// <summary>
    /// Clase que verifica que las peticiones de los datos sean correctas. 
    /// </summary>
    /// <remarks>
    /// <h3>Calidad</h3>
    /// </hr>
    /// <b>Requerimiento:</b> Help Desk</br>
    /// <b>Version:</b> 1</br>
    /// <b>Autor:</b> Grupo Cubo Ltda.</br>
    /// <b>Fecha:</b> 18/12/2017</br>
    /// <b>Descripcion:</b> Funciona para solicitar los datos al repositorio de tipos de indicadores y retornar los datos obtenidos.
    /// </remarks>
    public class TiposIndicadoresService
    {
        /// <summary>
        /// Propiedad que instancia la clase TiposIndicadoresRepository
        /// </summary>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 18/12/2017</br>
        /// <b>Descripcion:</b> Propiedad que instancia el repositorio TiposIndicadoresRepository, para enviar peticiones y recibir el resultado.
        /// </remarks>
        private TiposIndicadoresRepository repositorio
        {
            get
            {
                return new TiposIndicadoresRepository();
            }
        }

        /// <summary>
        /// Obtener coleccion de TiposIndicadores de la base de datos.
        /// </summary>
        /// <returns>Retorna coleccion de TiposIndicadores</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 18/12/2017</br>
        /// <b>Descripcion:</b> Obtiene una coleccion de los tipos de indicadores.
        /// </remarks>
        public IEnumerable<TiposIndicadores> ObtenerTiposIndicadores()
        {
            IEnumerable<TiposIndicadores> tiposIndicadores = new List<TiposIndicadores>();
            try
            {
                tiposIndicadores = repositorio.GetAll();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return tiposIndicadores;
        }

        /// <summary>
        /// Obtener identificador de TiposIndicadores de la base de datos.
        /// </summary>
        /// <param name="nombre">Nombre del tipo de indicador</param>
        /// <returns>Retorna numero entero</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 18/12/2017</br>
        /// <b>Descripcion:</b> Obtiene el identificador del tipo de indicador segun el nombre que se ingrese.
        /// </remarks>
        public int ObtenerIdTiposIndicadoresPorNombre(string nombre)
        {
            int result;
            try
            {
                Expression<Func<TiposIndicadores, bool>> predicate = x => x.Nombre == nombre;
                result = repositorio.GetBy(predicate).FirstOrDefault().Id_TipoIndicador;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return result;
        }

        /// <summary>
        /// Obtener nombre de TiposIndicadores de la base de datos.
        /// </summary>
        /// <param name="idTipoIndicador">Identificador de tipo de indicador</param>
        /// <returns>Retorna cadena de caracteres</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 18/12/2017</br>
        /// <b>Descripcion:</b> Obtiene el nombre del tipo de indicador segun el numero de identificador que se ingrese.
        /// </remarks>
        public string ObtenerNombreTipoIndicadorPorId(int idTipoIndicador)
        {
            string result = string.Empty;
            try
            {
                Expression<Func<TiposIndicadores, bool>> predicate = x => x.Id_TipoIndicador == idTipoIndicador;
                result = repositorio.GetBy(predicate).FirstOrDefault().Nombre;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return result;
        }
    }
}