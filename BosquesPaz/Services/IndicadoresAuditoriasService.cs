﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BosquesPaz.Models;
using BosquesPaz.Repositories;
using System.Linq.Expressions;

namespace BosquesPaz.Services
{
    /// <summary>
    /// Clase que verifica que las peticiones de los datos sean correctas. 
    /// </summary>
    /// <remarks>
    /// <h3>Calidad</h3>
    /// </hr>
    /// <b>Requerimiento:</b> Help Desk</br>
    /// <b>Version:</b> 1</br>
    /// <b>Autor:</b> Grupo Cubo Ltda.</br>
    /// <b>Fecha:</b> 18/12/2017</br>
    /// <b>Descripcion:</b> Funciona para solicitar los datos al repositorio de auditorias indicadores y retornar los datos obtenidos.
    /// </remarks>
    public class IndicadoresAuditoriasService
    {
        /// <summary>
        /// Propiedad que instancia la clase TiposIndicadoresService
        /// </summary>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 18/12/2017</br>
        /// <b>Descripcion:</b> Propiedad que instancia el servicio TiposIndicadoresService, para enviar peticiones y recibir el resultado.
        /// </remarks>
        private TiposIndicadoresService ServicioTipo { get{ return new TiposIndicadoresService(); } }

        /// <summary>
        /// Propiedad que instancia la clase EstadosIndicadoresService
        /// </summary>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 18/12/2017</br>
        /// <b>Descripcion:</b> Propiedad que instancia el servicio EstadosIndicadoresService, para enviar peticiones y recibir el resultado.
        /// </remarks>
        private EstadosIndicadoresService ServicioEstado { get{ return new EstadosIndicadoresService(); } }

        /// <summary>
        /// Propiedad que instancia la clase IndicadoresAuditoriasRepository
        /// </summary>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 18/12/2017</br>
        /// <b>Descripcion:</b> Propiedad que instancia el repositorio IndicadoresAuditoriasRepository, para enviar peticiones y recibir el resultado.
        /// </remarks>
        private IndicadoresAuditoriasRepository repositorioAuditorias { get { return new IndicadoresAuditoriasRepository(); } }

        /// <summary>
        /// Propiedad de número entero
        /// </summary>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 18/12/2017</br>
        /// <b>Descripcion:</b> Propiedad que define un número entero, para verificar el numero de registros que existen en una entidad.
        /// </remarks>
        public int CantidadRegistros { get; private set; }

        /// <summary>
        /// Método que retorna una coleccion de datos, segun sus parametros.
        /// </summary>
        /// <param name="pageIndex">Número de paginas.</param>
        /// <param name="pageCount">Número de paginaciones.</param>
        /// <param name="columnaOrden">Cadena de caracteres de una columna.</param>
        /// <param name="filtros">Cadena de caracteres</param>
        /// <param name="ascending">Tipo de dato verdadero o falso</param>
        /// <param name="id">Identificador del indicador</param>
        /// <returns>Retorna coleccion de AudiIndicadoresBosques</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 18/12/2017</br>
        /// <b>Descripcion:</b> Método que permite obtener los datos de AudiIndicadoresBosques y organizarlos con el proposito de retornarlos a una grilla.
        /// </remarks>
        public IEnumerable<AudiIndicadoresBosques> ObtenerIndicadoresAuditoriasBosques(int pageIndex, int pageCount, string columnaOrden, IDictionary<string, string> filtros, bool ascending, int id, bool isId)
        {
            Expression<Func<AudiIndicadoresBosques, bool>> predicate = x => x.Id_IndicadorBosque == id;
            if(!isId)
                predicate = x => true;
            Expression<Func<AudiIndicadoresBosques, int>> order = x => x.Id_IndicadorBosqueAuditoria;
            Expression<Func<AudiIndicadoresBosques, DateTime>> orderDate = null;
            bool first = true;
            bool isDate = false;

            foreach (var filtro in filtros)
            {
                switch (filtro.Key)
                {
                    default:
                        predicate = x => false;
                        break;
                    //case nameof(AudiIndicadoresBosques.TipoIndicador):
                    //    int TipoActividad = ServicioTipo.ObtenerIdTiposIndicadoresPorNombre(filtro.Value);
                    //    predicate = (first ? x => x.Id_TipoActividad == TipoActividad : predicate.And(x => x.Id_TipoActividad == TipoActividad));
                    //    break;
                    //case nameof(IndicadoresBosques.EstadoIndicador):
                    //    int EstadoActividad = ServicioEstado.ObtenerIdEstadoIndicadorPorNombre(filtro.Value);
                    //    predicate = (first ? x => x.Id_EstadoIndicador == EstadoActividad : predicate.And(x => x.Id_EstadoIndicador == EstadoActividad));
                    //    break;
                        //case nameof(IndicadoresBosques.NombreActividad):
                        //    string NombreActividad = filtro.Value;
                        //    predicate = (first ? x => x.NombreActividad == NombreActividad : predicate.And(x => x.NombreActividad == NombreActividad));
                        //    break;

                        //case nameof(IndicadoresBosques.FechaInicioStr):
                        //    DateTime FechaInicio = Convert.ToDateTime(filtro.Value);
                        //    predicate = (first ? x => x.FechaInicio == FechaInicio : predicate.And(x => x.FechaInicio == FechaInicio));
                        //    break;

                        //case nameof(IndicadoresBosques.FechaFinStr):
                        //    DateTime FechaFin = Convert.ToDateTime(filtro.Value);
                        //    predicate = (first ? x => x.FechaFin == FechaFin : predicate.And(x => x.FechaFin == FechaFin));
                        //    break;

                        //case nameof(IndicadoresBosques.PorcentajeAvance):
                        //    int PorcentajeAvance = Convert.ToInt32(filtro.Value);
                        //    predicate = (first ? x => x.PorcentajeAvance == PorcentajeAvance : predicate.And(x => x.PorcentajeAvance == PorcentajeAvance));
                        //    break;

                        //case nameof(IndicadoresBosques.Avance):
                        //    string Avance = filtro.Value;
                        //    predicate = (first ? x => x.Avance == Avance : predicate.And(x => x.Avance == Avance));
                        //    break;
                }
                first = false;
            }

            CantidadRegistros = repositorioAuditorias.Count(predicate);

            switch (columnaOrden)
            {
                case nameof(AudiIndicadoresBosques.Id_IndicadorBosqueAuditoria):
                    order = x => x.Id_IndicadorBosqueAuditoria;
                    isDate = false;
                    break;
            }


            IEnumerable<AudiIndicadoresBosques> result;
            if (isDate)
                result = repositorioAuditorias.GetPagedElements<DateTime>(pageIndex, pageCount, orderDate, predicate, ascending);
            else
                result = repositorioAuditorias.GetPagedElements<int>(pageIndex, pageCount, order, predicate, ascending);

            return result.ToList();
        }
    }
}