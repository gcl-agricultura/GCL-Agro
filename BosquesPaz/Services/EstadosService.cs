﻿using BosquesPaz.Models;
using BosquesPaz.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;

namespace BosquesPaz.Services
{
    /// <summary>
    /// Clase que verifica que las peticiones de los datos sean correctas. 
    /// </summary>
    /// <remarks>
    /// <h3>Calidad</h3>
    /// </hr>
    /// <b>Requerimiento:</b> Help Desk</br>
    /// <b>Version:</b> 1</br>
    /// <b>Autor:</b> Grupo Cubo Ltda.</br>
    /// <b>Fecha:</b> 18/12/2017</br>
    /// <b>Descripcion:</b> Funciona para solicitar los datos al repositorio de estados actividades y retornar los datos obtenidos.
    /// </remarks>
    public class EstadosService
    {
        /// <summary>
        /// Propiedad que instancia la clase EstadosRepository
        /// </summary>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 18/12/2017</br>
        /// <b>Descripcion:</b> Propiedad que instancia el repositorio EstadosRepository, para enviar peticiones y recibir el resultado.
        /// </remarks>
        private EstadosRepository repositorio
        {
            get
            {
                return new EstadosRepository();
            }
        }

        /// <summary>
        /// Obtener coleccion de Estados de la base de datos.
        /// </summary>
        /// <returns>Retorna coleccion de Estados</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 18/12/2017</br>
        /// <b>Descripcion:</b> Obtiene una coleccion de los estados de recursos.
        /// </remarks>
        public IEnumerable<Estados> ObtenerEstados()
        {
            IEnumerable<Estados> estados = new List<Estados>();
            try
            {
                estados = repositorio.GetAll();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return estados;
        }

        /// <summary>
        /// Obtener identificador de Estados de la base de datos.
        /// </summary>
        /// <param name="nombre">nombre del estado de recursos</param>
        /// <returns>Retorna numero entero</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 18/12/2017</br>
        /// <b>Descripcion:</b> Obtiene el identificador del estado de recursos segun el nombre que ingrese.
        /// </remarks>
        public int ObtenerIdEstadoPorNombre(string nombre)
        {
            int result;
            try
            {
                Expression<Func<Estados, bool>> predicate = x => x.Nombre == nombre;
                result = repositorio.GetBy(predicate).FirstOrDefault().Id_Estado;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return result;
        }

        /// <summary>
        /// Obtener el nombre de EstadosActividades de la base de datos.
        /// </summary>
        /// <param name="idEstado">Identificador del estado de recursos</param>
        /// <returns>Retorna cadena de caracteres</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 18/12/2017</br>
        /// <b>Descripcion:</b> Obtiene el nombre del estado de recursos segun el numero de identificador que ingrese.
        /// </remarks>
        public string ObtenerNombreEstadoPorId(int idEstado)
        {
            string result = string.Empty;
            try
            {
                Expression<Func<Estados, bool>> predicate = x => x.Id_Estado == idEstado;
                result = repositorio.GetBy(predicate).FirstOrDefault().Nombre;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return result;
        }
    }
}