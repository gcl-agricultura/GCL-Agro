﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BosquesPaz.Models;
using BosquesPaz.Repositories;
using System.Linq.Expressions;
using BosquesPaz.Repositories.Especificaciones;

namespace BosquesPaz.Services
{
    /// <summary>
    /// Clase que verifica que las peticiones de los datos sean correctas. 
    /// </summary>
    /// <remarks>
    /// <h3>Calidad</h3>
    /// </hr>
    /// <b>Requerimiento:</b> Help Desk</br>
    /// <b>Version:</b> 1</br>
    /// <b>Autor:</b> Grupo Cubo Ltda.</br>
    /// <b>Fecha:</b> 18/12/2017</br>
    /// <b>Descripcion:</b> Funciona para solicitar los datos al repositorio de datos usuarios y retornar los datos obtenidos.
    /// </remarks>
    public class DatosUsuariosServices
    {
        /// <summary>
        /// Propiedad que instancia la clase DatosUsuariosRepository
        /// </summary>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 18/12/2017</br>
        /// <b>Descripcion:</b> Propiedad que instancia el repositorio DatosUsuariosRepository, para enviar peticiones y recibir el resultado.
        /// </remarks>
        private DatosUsuariosRepository repositorio { get{ return new DatosUsuariosRepository(); } }

        /// <summary>
        /// Guardar una instancia de DatosUsuarios en la base de datos.
        /// </summary>
        /// <param name="datosUsuarios">Instancia de datos de DatosUsuarios</param>
        /// <returns>Retorna instancia de DatosUsuarios</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 18/12/2017</br>
        /// <b>Descripcion:</b> Recibe una instancia de DatosUsuarios y la envia a la clase de repositorio, y retorna la instancia.
        /// </remarks>
        public DatosUsuarios GuardarDatosUsuario(DatosUsuarios datosUsuarios)
        {
            try
            {
                datosUsuarios = repositorio.Create(datosUsuarios);
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return datosUsuarios;
        }

        /// <summary>
        /// Obtener datos de DatosUsuarios de la base de datos.
        /// </summary>
        /// <param name="idDatoUsuario">Identificador de los datos de usaurio</param>
        /// <returns>Retorna instancia de DatosUsuarios</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 18/12/2017</br>
        /// <b>Descripcion:</b> Obtiene una instancia de los datos de usuarios segun el numero de identificador que ingrese.
        /// </remarks>
        public DatosUsuarios ObtenerDatosUsuarios(int idDatoUsuario)
        {
            DatosUsuarios result = null;
            try
            {
                result = repositorio.GetByKey(idDatoUsuario);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }

        /// <summary>
        /// Obtener identificador de DatosUsuarios de la base de datos.
        /// </summary>
        /// <param name="nickName">nick name o alias de los datos de usaurio</param>
        /// <returns>Retorna numero entero</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 18/12/2017</br>
        /// <b>Descripcion:</b> Obtiene el identificador de los datos de usuarios segun el nick name o alias que ingrese.
        /// </remarks>
        public int ObtenerDatosUsuariosPorNickName(string nickName)
        {
            int result = 0;
            try
            {
                Expression<Func<DatosUsuarios, bool>> predicate = x => x.NickName == nickName;
                result = repositorio.GetBy(predicate).FirstOrDefault().Id_DatoUsuario;
            }
            catch (Exception)
            {
                return result;
            }
            return result;
        }

        /// <summary>
        /// Obtener identificador de la foto de DatosUsuarios de la base de datos.
        /// </summary>
        /// <param name="nickName">nick name o alias de los datos de usaurio</param>
        /// <returns>Retorna numero entero</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 18/12/2017</br>
        /// <b>Descripcion:</b> Obtiene el identificador de la foto de los datos de usuarios segun el nick name o alias que ingrese.
        /// </remarks>
        public int ObtenerDatoUsuarioFotoPorNickName(string nickName)
        {
            int result = 0;
            try
            {
                Expression<Func<DatosUsuarios, bool>> predicate = x => x.NickName == nickName;
                result = repositorio.GetBy(predicate).FirstOrDefault().Id_FotoUsuario;
            }
            catch (Exception)
            {
                return result;
            }
            return result;
        }
    }
}