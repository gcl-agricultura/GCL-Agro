﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BosquesPaz.Models;
using BosquesPaz.Repositories;
using System.Linq.Expressions;
using BosquesPaz.Repositories.Especificaciones;

namespace BosquesPaz.Services
{
    /// <summary>
    /// Clase que verifica que las peticiones de los datos sean correctas. 
    /// </summary>
    /// <remarks>
    /// <h3>Calidad</h3>
    /// </hr>
    /// <b>Requerimiento:</b> Help Desk</br>
    /// <b>Version:</b> 1</br>
    /// <b>Autor:</b> Grupo Cubo Ltda.</br>
    /// <b>Fecha:</b> 13/12/2017</br>
    /// <b>Descripcion:</b> Funciona para solicitar los datos al repositorio de auditorias de actividades y retornar los datos obtenidos.
    /// </remarks>
    public class ActividadesAuditoriasService
    {
        /// <summary>
        /// Propiedad que instancia la clase ActividadesAuditoriasRepository
        /// </summary>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 13/12/2017</br>
        /// <b>Descripcion: </b>Propiedad que instancia el repositorio ActividadesAuditoriasRepository, para enviar peticiones y recibir el resultado.
        /// </remarks>
        private ActividadesAuditoriasRepository repositorioAuditorias { get { return new ActividadesAuditoriasRepository(); } }

        /// <summary>
        /// Propiedad que instancia la clase EstadosActividadesService
        /// </summary>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 13/12/2017</br>
        /// <b>Descripcion:</b> Propiedad que instancia el repositorio EstadosActividadesService, para enviar peticiones y recibir el resultado.
        /// </remarks>
        private EstadosActividadesService ServicioEstado { get { return new EstadosActividadesService(); } }

        /// <summary>
        /// Propiedad que instancia la clase EstadosActividadesService
        /// </summary>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 13/12/2017</br>
        /// <b>Descripcion: </b>Propiedad que instancia el repositorio TiposActividadesService, para enviar peticiones y recibir el resultado.
        /// </remarks>
        private TiposActividadesService ServicioTipo { get { return new TiposActividadesService(); } }

        /// <summary>
        /// Propiedad de numero entero
        /// </summary>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 13/12/2017</br>
        /// <b>Descripcion:</b> Propiedad que define un numero entero, para verificar el numero de registros que existen en una entidad.
        /// </remarks>
        public int CantidadRegistros { get; private set; }

        /// <summary>
        /// Método que retorna una coleccion de datos, segun sus parametros.
        /// </summary>
        /// <param name="pageIndex">Número de paginas.</param>
        /// <param name="pageCount">Número de paginaciones.</param>
        /// <param name="columnaOrden">Cadena de caracteres de una columna.</param>
        /// <param name="filtros">Cadena de caracteres</param>
        /// <param name="ascending">Tipo de dato verdadero o falso</param>
        /// <param name="id">Identificador de la actividad</param>
        /// <returns>Retorna coleccion de AudiActividadesBosques</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 13/12/2017</br>
        /// <b>Descripcion:</b> Método que permite obtener los datos de AudiActividadesBosques, y organizarlos con el proposito de retornarlos a una grilla.
        /// </remarks>
        public IEnumerable<AudiActividadesBosques> ObtenerActividadBosquesAuditorias(int pageIndex, int pageCount, string columnaOrden, IDictionary<string, string> filtros, bool ascending, int id)
        {
            Expression<Func<AudiActividadesBosques, bool>> predicate = x => x.Id_ActividadBosque == id;
            Expression<Func<AudiActividadesBosques, int>> order = x => x.Id_ActividadBosqueAuditoria;
            Expression<Func<AudiActividadesBosques, DateTime>> orderDate = null;
            bool first = true;
            bool isDate = false;

            foreach (var filtro in filtros)
            {
                switch (filtro.Key)
                {
                    default:
                        predicate = x => false;
                        break;
                    case nameof(AudiActividadesBosques.TipoActividad):
                        int TipoActividad = ServicioTipo.ObtenerIdTiposActividadesPorNombre(filtro.Value);
                        predicate = (first ? x => x.Id_TipoActividad == TipoActividad : predicate.And(x => x.Id_TipoActividad == TipoActividad));
                        break;
                    case nameof(AudiActividadesBosques.EstadoActividad):
                        int EstadoActividad = ServicioEstado.ObtenerIdEstadosActividadesPorNombre(filtro.Value);
                        predicate = (first ? x => x.Id_EstadosActividades == EstadoActividad : predicate.And(x => x.Id_EstadosActividades == EstadoActividad));
                        break;
                    case nameof(AudiActividadesBosques.NombreActividadAuditoria):
                        string NombreActividad = filtro.Value;
                        predicate = (first ? x => x.NombreActividadAuditoria == NombreActividad : predicate.And(x => x.NombreActividadAuditoria == NombreActividad));
                        break;

                    case nameof(AudiActividadesBosques.FechaInicioStr):
                        DateTime FechaInicio = Convert.ToDateTime(filtro.Value);
                        predicate = (first ? x => x.FechaInicioAuditoria == FechaInicio : predicate.And(x => x.FechaInicioAuditoria == FechaInicio));
                        break;

                    case nameof(AudiActividadesBosques.FechaFinStr):
                        DateTime FechaFin = Convert.ToDateTime(filtro.Value);
                        predicate = (first ? x => x.FechaFinAuditoria == FechaFin : predicate.And(x => x.FechaFinAuditoria == FechaFin));
                        break;

                    case nameof(AudiActividadesBosques.PorcentajeAvanceAuditoria):
                        int PorcentajeAvance = Convert.ToInt32(filtro.Value);
                        predicate = (first ? x => x.PorcentajeAvanceAuditoria == PorcentajeAvance : predicate.And(x => x.PorcentajeAvanceAuditoria == PorcentajeAvance));
                        break;

                    case nameof(AudiActividadesBosques.Avance):
                        string Avance = filtro.Value;
                        predicate = (first ? x => x.Avance == Avance : predicate.And(x => x.Avance == Avance));
                        break;
                }
                first = false;
            }

            CantidadRegistros = repositorioAuditorias.Count(predicate);

            switch (columnaOrden)
            {
                case nameof(AudiActividadesBosques.Id_ActividadBosqueAuditoria):
                    order = x => x.Id_ActividadBosqueAuditoria;
                    isDate = false;
                    break;
            }


            IEnumerable<AudiActividadesBosques> result;
            if (isDate)
                result = repositorioAuditorias.GetPagedElements<DateTime>(pageIndex, pageCount, orderDate, predicate, ascending);
            else
                result = repositorioAuditorias.GetPagedElements<int>(pageIndex, pageCount, order, predicate, ascending);

            return result.ToList();
        }
    }
}