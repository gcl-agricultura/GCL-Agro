﻿using BosquesPaz.Models;
using BosquesPaz.Repositories;
using BosquesPaz.Repositories.Especificaciones;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;

namespace BosquesPaz.Services
{
    /// <summary>
    /// Clase que verifica que las peticiones de los datos sean correctas. 
    /// </summary>
    /// <remarks>
    /// <h3>Calidad</h3>
    /// </hr>
    /// <b>Requerimiento:</b> Help Desk</br>
    /// <b>Version:</b> 1</br>
    /// <b>Autor:</b> Grupo Cubo Ltda.</br>
    /// <b>Fecha:</b> 18/12/2017</br>
    /// <b>Descripcion:</b> Funciona para solicitar los datos al repositorio de bosques y retornar los datos obtenidos.
    /// </remarks>
    public class BosquesService
    {
        /// <summary>
        /// Propiedad que instancia la clase BosquesRepository
        /// </summary>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 18/12/2017</br>
        /// <b>Descripcion:</b> Propiedad que instancia el repositorio BosquesRepository, para enviar peticiones y recibir el resultado.
        /// </remarks>
        private BosquesRepository repositorio { get { return new BosquesRepository(); } }

        /// <summary>
        /// Propiedad que instancia la clase DepartamentosService
        /// </summary>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 18/12/2017</br>
        /// <b>Descripcion:</b> Propiedad que instancia el servicio DepartamentosService, para enviar peticiones y recibir el resultado.
        /// </remarks>
        private DepartamentosService ServicioDepartamento { get { return new DepartamentosService(); } }

        /// <summary>
        /// Propiedad que instancia la clase MunicipiosService
        /// </summary>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 18/12/2017</br>
        /// <b>Descripcion:</b> Propiedad que instancia el servicio MunicipiosService, para enviar peticiones y recibir el resultado.
        /// </remarks>
        private MunicipiosService ServicioMunicipio { get { return new MunicipiosService(); } }

        /// <summary>
        /// Propiedad de número entero
        /// </summary>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 18/12/2017</br>
        /// <b>Descripcion:</b> Propiedad que define un número entero, para verificar el numero de registros que existen en una entidad.
        /// </remarks>
        public int CantidadRegistros { get; private set; }

        /// <summary>
        /// Guardar una instancia de Bosques en la base de datos.
        /// </summary>
        /// <param name="bosque">Instancia de Bosques</param>
        /// <returns>Retorna instancia de Bosques</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 18/12/2017</br>
        /// <b>Descripcion:</b>Recibe una instancia de Bosques y la envia a la clase de repositorio, y retorna la instancia.
        /// </remarks>
        public Bosques GuardarBosque(Bosques bosque)
        {
            try
            {
                bosque = repositorio.Create(bosque);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return bosque;
        }

        /// <summary>
        /// Obtener datos de Bosques de la base de datos.
        /// </summary>
        /// <param name="idBosque">Identificador del bosque</param>
        /// <returns>Retorna instancia de Bosques</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 18/12/2017</br>
        /// <b>Descripcion:</b> Obtiene una instancia del bosque segun el numero de identificador que ingrese.
        /// </remarks>
        public Bosques ObtenerBosque(int idBosque)
        {
            Bosques result = null;
            try
            {
                result = repositorio.GetByKey(idBosque);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }

        /// <summary>
        /// Obtener coleccion de Bosques de la base de datos.
        /// </summary>
        /// <returns>Retorna coleccion de Bosques</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 18/12/2017</br>
        /// <b>Descripcion:</b> Obtiene una coleccion de todos los bosques.
        /// </remarks>
        public IEnumerable<Bosques> ObtenerBosques()
        {
            IEnumerable<Bosques> result = null;
            try
            {
                result = repositorio.GetAll();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }


        /// <summary>
        /// Obtener coleccion de los departamentos de la base de datos.
        /// </summary>
        /// <returns>Retorna coleccion de numeros enteros</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 18/12/2017</br>
        /// <b>Descripcion:</b> Obtiene una coleccion de los departamentos, segun los bosques registrados.
        /// </remarks>
        public IEnumerable<int> ObtenerBosquesDepartamentos()
        {
            IEnumerable<int> result = null;
            try
            {
                var query = repositorio.GetAll();
                var departamentos = query.Select(d => d.Id_Departamento).Distinct();
                result = ServicioDepartamento.ObtenerCodigoDaneDepartamentos(departamentos.ToList());
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }

        /// <summary>
        /// Obtener coleccion de los municipios de la base de datos.
        /// </summary>
        /// <returns>Retorna coleccion de numeros enteros</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 18/12/2017</br>
        /// <b>Descripcion:</b> Obtiene una coleccion de los municipios, segun los bosques registrados.
        /// </remarks>
        public IEnumerable<int> ObtenerBosquesMunicipios()
        {
            IEnumerable<int> result = null;
            try
            {
                var query = repositorio.GetAll();
                var municipios = query.Select(d => d.Id_Municipio).Distinct();
                result = ServicioMunicipio.ObtenerCodigoDaneMunicipios(municipios.ToList());
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }

        /// <summary>
        /// Obtener el nombre de los Bosques de la base de datos.
        /// </summary>
        /// <param name="idBosque">Identificador del bosque</param>
        /// <returns>Retorna cadena de caracteres</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 18/12/2017</br>
        /// <b>Descripcion:</b> Obtiene el nombre de un bosque segun el numero de identificador que ingrese.
        /// </remarks>
        public string ObtenerNombreBosqueActividadPorId(int idBosque)
        {
            string result = string.Empty;
            try
            {
                Expression<Func<Bosques, bool>> predicate = x => x.Id_Bosque == idBosque;
                result = repositorio.GetBy(predicate).FirstOrDefault().NombreBosque;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }

        /// <summary>
        /// Método que retorna una coleccion de datos, segun sus parametros.
        /// </summary>
        /// <param name="pageIndex">Número de paginas.</param>
        /// <param name="pageCount">Número de paginaciones.</param>
        /// <param name="columnaOrden">Cadena de caracteres de una columna.</param>
        /// <param name="filtros">Cadena de caracteres</param>
        /// <param name="ascending">Tipo de dato verdadero o falso</param>
        /// <returns>Retorna coleccion de Bosques</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 18/12/2017</br>
        /// <b>Descripcion:</b> Método que permite obtener los datos de Bosques, y organizarlos con el proposito de retornarlos a una grilla.
        /// </remarks>
        public IEnumerable<Bosques> ObtenerBosques(int pageIndex, int pageCount, string columnaOrden, IDictionary<string, string> filtros, bool ascending)
        {
            Expression<Func<Bosques, bool>> predicate = x => x.Id_Bosque > 0;
            Expression<Func<Bosques, int>> order = x => x.Id_Bosque;
            Expression<Func<Bosques, DateTime>> orderDate = null;
            bool first = true;
            bool isDate = false;

            foreach (var filtro in filtros)
            {
                switch (filtro.Key)
                {
                    default:
                        predicate = x => false;
                        break;
                    case nameof(Bosques.NombreBosque):
                        string NombreBosque = filtro.Value;
                        predicate = (first ? x => x.NombreBosque == NombreBosque : predicate.And(x => x.NombreBosque == NombreBosque));
                        break;
                    case nameof(Bosques.Departamento):
                        int Id_Departamento = ServicioDepartamento.ObtenerIdDepartamentoPorNombre(filtro.Value);
                        predicate = (first ? x => x.Id_Departamento == Id_Departamento : predicate.And(x => x.Id_Departamento == Id_Departamento));
                        break;
                    case nameof(Bosques.Municipio):
                        int Id_Municipio = ServicioMunicipio.ObtenerIdMunicipioPorNombre(filtro.Value);
                        predicate = (first ? x => x.Id_Municipio == Id_Municipio : predicate.And(x => x.Id_Municipio == Id_Municipio));
                        break;

                }
                first = false;
            }

            CantidadRegistros = repositorio.Count(predicate);

            switch (columnaOrden)
            {
                case nameof(Bosques.Id_Bosque):
                    order = x => x.Id_Bosque;
                    isDate = false;
                    break;

            }


            IEnumerable<Bosques> result;
            if (isDate)
                result = repositorio.GetPagedElements<DateTime>(pageIndex, pageCount, orderDate, predicate, ascending);
            else
                result = repositorio.GetPagedElements<int>(pageIndex, pageCount, order, predicate, ascending);

            return result.ToList();
        }

    }
}