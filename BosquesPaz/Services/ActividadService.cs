﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BosquesPaz.Models;
using BosquesPaz.Repositories;
using System.Linq.Expressions;
using BosquesPaz.Repositories.Especificaciones;

namespace BosquesPaz.Services
{
    /// <summary>
    /// Clase que verifica que las peticiones de los datos sean correctas. 
    /// </summary>
    /// <remarks>
    /// <h3>Calidad</h3>
    /// </hr>
    /// <b>Requerimiento: Help Desk</b></br>
    /// <b>Version: 1</b></br>
    /// <b>Autor: Grupo Cubo Ltda.</b></br>
    /// <b>Fecha: 13/12/2017<b></br>
    /// <b>Descripcion:</b> Funciona para solicitar los datos al repositorio de actividades y retornar los datos obtenidos.
    /// </remarks>
    public class ActividadService
    {
        /// <summary>
        /// Propiedad que instancia la clase ActividadRepository
        /// </summary>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 13/12/2017</br>
        /// <b>Descripcion:</b> Propiedad que instancia el repositorio ActividadRepository, para enviar peticiones y recibir el resultado.
        /// </remarks>
        private ActividadRepository repositorio { get { return new ActividadRepository(); } }

        /// <summary>
        /// Propiedad que instancia la clase EstadosActividadesService
        /// </summary>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 13/12/2017</br>
        /// <b>Descripcion:</b> Propiedad que instancia el servicio EstadosActividadesService, para enviar peticiones y recibir el resultado.
        /// </remarks>
        private EstadosActividadesService ServicioEstado { get { return new EstadosActividadesService(); } }

        /// <summary>
        /// Propiedad que instancia la clase TiposActividadesService
        /// </summary>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 13/12/2017</br>
        /// <b>Descripcion:</b> Propiedad que instancia el servicio TiposActividadesService, para enviar peticiones y recibir el resultado.
        /// </remarks>
        private TiposActividadesService ServicioTipo { get { return new TiposActividadesService(); } }

        /// <summary>
        /// Propiedad de número entero
        /// </summary>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 13/12/2017</br>
        /// <b>Descripcion:</b> Propiedad que define un número entero, para verificar el numero de registros que existen en una entidad.
        /// </remarks>
        public int CantidadRegistros { get; private set; }


        /// <summary>
        /// Guardar una instancia de ActividadesBosques en la base de datos.
        /// </summary>
        /// <param name="actividad">Instancia de ActividadesBosques</param>
        /// <returns>Retorna instancia de ActividadesBosques</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 18/12/2017</br>
        /// <b>Descripcion:</b> Recibe una instancia de ActividadesBosques y la envia a la clase de repositorio, y retorna la instancia.
        /// </remarks>
        public ActividadesBosques GuardarActividad(ActividadesBosques actividad)
        {
            try
            {
                actividad = repositorio.Create(actividad);
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return actividad;
        }

        /// <summary>
        /// Actualizar datos de ActividadesBosques en la base de datos.
        /// </summary>
        /// <param name="actividad">Instancia de ActividadesBosques</param>
        /// <returns>Retorna valor booleano</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 18/12/2017</br>
        /// <b>Descripcion:</b> Recibe una instancia de ActividadesBosques y la envia a la clase de repositorio, y retorna un valor booleano.
        /// </remarks>
        public bool ActualizarActividad(ActividadesBosques actividad)
        {
            bool result = false;
            try
            {
                if (repositorio.Update(actividad))
                {
                    result = true;
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return result;
        }

        /// <summary>
        /// Obtener datos de ActividadesBosques de la base de datos.
        /// </summary>
        /// <param name="idActividadBosque">Identificador de la actividad</param>
        /// <returns>Retorna instancia de ActividadesBosques</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 18/12/2017</br>
        /// <b>Descripcion:</b> Obtiene una instancia de la actividad segun el numero de identificador que ingrese.
        /// </remarks>
        public ActividadesBosques ObtenerActividadBosque(int idActividadBosque)
        {
            ActividadesBosques result = null;
            try
            {
                result = repositorio.GetByKey(idActividadBosque);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }


        public List<ActividadesBosques> ObtenerActividadesBosque(int id) {


            IEnumerable<ActividadesBosques> allActividades = repositorio.GetAll();
         List<ActividadesBosques> result = new List<ActividadesBosques>();

            foreach(var actividad in allActividades)
            {
                if (actividad.Id_Bosque == id) {
                    result.Add(actividad);
                }
            }

            return result;
        }


        /// <summary>
        /// Método que retorna una coleccion de datos, segun sus parametros.
        /// </summary>
        /// <param name="pageIndex">Número de paginas.</param>
        /// <param name="pageCount">Número de paginaciones.</param>
        /// <param name="columnaOrden">Cadena de caracteres de una columna.</param>
        /// <param name="filtros">Cadena de caracteres</param>
        /// <param name="ascending">Tipo de dato verdadero o falso</param>
        /// <param name="id">Identificador del bosque</param>
        /// <returns>Retorna coleccion de ActividadesBosques</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 18/12/2017</br>
        /// <b>Descripcion:</b> Método que permite obtener los datos de ActividadesBosques y organizarlos con el proposito de retornarlos a una grilla.
        /// </remarks>
        public IEnumerable<ActividadesBosques> ObtenerActividadBosques(int pageIndex, int pageCount, string columnaOrden, IDictionary<string, string> filtros, bool ascending, int id)
        {
            Expression<Func<ActividadesBosques, bool>> predicate = x => x.Id_Bosque == id;
            Expression<Func<ActividadesBosques, int>> order = x => x.Id_ActividadBosque;
            Expression<Func<ActividadesBosques, DateTime>> orderDate = null;
            bool first = true;
            bool isDate = false;

            foreach (var filtro in filtros)
            {
                switch (filtro.Key)
                {
                    default:
                        predicate = x => false;
                        break;
                    case nameof(ActividadesBosques.TipoActividad):
                        int TipoActividad = ServicioTipo.ObtenerIdTiposActividadesPorNombre(filtro.Value);
                        predicate = (first ? x => x.Id_TipoActividad == TipoActividad : predicate.And(x => x.Id_TipoActividad == TipoActividad));
                        break;
                    case nameof(ActividadesBosques.EstadoActividad):
                        int EstadoActividad = ServicioEstado.ObtenerIdEstadosActividadesPorNombre(filtro.Value);
                        predicate = (first ? x => x.Id_EstadosActividades == EstadoActividad : predicate.And(x => x.Id_EstadosActividades == EstadoActividad));
                        break;
                    case nameof(ActividadesBosques.NombreActividad):
                        string NombreActividad = filtro.Value;
                        predicate = (first ? x => x.NombreActividad == NombreActividad : predicate.And(x => x.NombreActividad == NombreActividad));
                        break;

                    case nameof(ActividadesBosques.FechaInicioStr):
                        DateTime FechaInicio = Convert.ToDateTime(filtro.Value);
                        predicate = (first ? x => x.FechaInicio == FechaInicio : predicate.And(x => x.FechaInicio == FechaInicio));
                        break;

                    case nameof(ActividadesBosques.FechaFinStr):
                        DateTime FechaFin = Convert.ToDateTime(filtro.Value);
                        predicate = (first ? x => x.FechaFin == FechaFin : predicate.And(x => x.FechaFin == FechaFin));
                        break;

                    case nameof(ActividadesBosques.PorcentajeAvance):
                        int PorcentajeAvance = Convert.ToInt32(filtro.Value);
                        predicate = (first ? x => x.PorcentajeAvance == PorcentajeAvance : predicate.And(x => x.PorcentajeAvance == PorcentajeAvance));
                        break;

                    case nameof(ActividadesBosques.Avance):
                        string Avance = filtro.Value;
                        predicate = (first ? x => x.Avance == Avance : predicate.And(x => x.Avance == Avance));
                        break;
                }
                first = false;
            }

            CantidadRegistros = repositorio.Count(predicate);

            switch (columnaOrden)
            {
                case nameof(ActividadesBosques.Id_ActividadBosque):
                    order = x => x.Id_ActividadBosque;
                    isDate = false;
                    break;
            }


            IEnumerable<ActividadesBosques> result;
            if (isDate)
                result = repositorio.GetPagedElements<DateTime>(pageIndex, pageCount, orderDate, predicate, ascending);
            else
                result = repositorio.GetPagedElements<int>(pageIndex, pageCount, order, predicate, ascending);

            return result.ToList();
        }
    }
}