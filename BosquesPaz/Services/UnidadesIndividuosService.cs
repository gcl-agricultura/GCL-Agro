﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Linq.Expressions;
using BosquesPaz.Models;
using BosquesPaz.Repositories;

namespace BosquesPaz.Services
{
    /// <summary>
    /// Clase que verifica que las peticiones de los datos sean correctas. 
    /// </summary>
    /// <remarks>
    /// <h3>Calidad</h3>
    /// </hr>
    /// <b>Requerimiento:</b> Help Desk</br>
    /// <b>Version:</b> 1</br>
    /// <b>Autor:</b> Grupo Cubo Ltda.</br>
    /// <b>Fecha:</b> 18/12/2017</br>
    /// <b>Descripcion:</b> Funciona para solicitar los datos al repositorio de unidades individuo/especies y retornar los datos obtenidos.
    /// </remarks>
    public class UnidadesIndividuosService
    {
        /// <summary>
        /// Propiedad que instancia la clase UnidadesIndividuosRepository
        /// </summary>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 18/12/2017</br>
        /// <b>Descripcion:</b> Propiedad que instancia el repositorio UnidadesIndividuosRepository, para enviar peticiones y recibir el resultado.
        /// </remarks>
        private UnidadesIndividuosRepository repositorio
        {
            get
            {
                return new UnidadesIndividuosRepository();
            }
        }

        /// <summary>
        /// Obtener coleccion de UnidadesIndividuosEspecies de la base de datos.
        /// </summary>
        /// <returns>Retorna coleccion de UnidadesIndividuosEspecies</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 18/12/2017</br>
        /// <b>Descripcion:</b> Obtiene una coleccion de los tipos de unidades de individuo/especies.
        /// </remarks>
        public IEnumerable<UnidadesIndividuosEspecies> ObtenerUnidadesIndividuosEspecies()
        {
            IEnumerable<UnidadesIndividuosEspecies> unidadesIndividuosEspecies = new List<UnidadesIndividuosEspecies>();
            try
            {
                unidadesIndividuosEspecies = repositorio.GetAll();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return unidadesIndividuosEspecies;        
        }

        /// <summary>
        /// Obtener identificador de UnidadesIndividuosEspecies de la base de datos.
        /// </summary>
        /// <param name="nombre">Nombre del tipo de unidades de individuos/especies</param>
        /// <returns>Retorna numero entero</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 18/12/2017</br>
        /// <b>Descripcion:</b> Obtiene el identificador de unidades individuos/especies segun el nombre que se ingrese.
        /// </remarks>
        public int ObtenerIdUnidadIndividuoEspeciePorNombre(string nombre)
        {
            int result;
            try
            {
                Expression<Func<UnidadesIndividuosEspecies, bool>> predicate = x => x.Nombre == nombre;
                result = repositorio.GetBy(predicate).FirstOrDefault().Id_UnidadIndividuoEspecie;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return result;
        }

        /// <summary>
        /// Obtener nombre de UnidadesIndividuosEspecies de la base de datos.
        /// </summary>
        /// <param name="idUnidadIndividuoEspecie">Identificador de unidades individuos/especies</param>
        /// <returns>Retorna cadena de caracteres</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 18/12/2017</br>
        /// <b>Descripcion:</b> Obtiene el nombre de unidades individuos/especies segun el numero de identificador que se ingrese.
        /// </remarks>
        public string ObtenerNombreIndividuoEspeciePorId(int idUnidadIndividuoEspecie)
        {
            string result;
            try
            {
                Expression<Func<UnidadesIndividuosEspecies, bool>> predicate = x => x.Id_UnidadIndividuoEspecie == idUnidadIndividuoEspecie;
                result = repositorio.GetBy(predicate).FirstOrDefault().Nombre;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return result;
        }
    }
}