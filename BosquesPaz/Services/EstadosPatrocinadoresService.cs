﻿using BosquesPaz.Models;
using BosquesPaz.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;

namespace BosquesPaz.Services
{
    /// <summary>
    /// Clase que verifica que las peticiones de los datos sean correctas. 
    /// </summary>
    /// <remarks>
    /// <h3>Calidad</h3>
    /// </hr>
    /// <b>Requerimiento:</b> Help Desk</br>
    /// <b>Version:</b> 1</br>
    /// <b>Autor:</b> Grupo Cubo Ltda.</br>
    /// <b>Fecha:</b> 18/12/2017</br>
    /// <b>Descripcion:</b> Funciona para solicitar los datos al repositorio de estados patrocinadores y retornar los datos obtenidos.
    /// </remarks>
    public class EstadosPatrocinadoresService
    {
        /// <summary>
        /// Propiedad que instancia la clase EstadosPatrocinadoresRepository
        /// </summary>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 18/12/2017</br>
        /// <b>Descripcion:</b> Propiedad que instancia el repositorio EstadosPatrocinadoresRepository, para enviar peticiones y recibir el resultado.
        /// </remarks>
        private EstadosPatrocinadoresRepository repositorio
        {
            get
            {
                return new EstadosPatrocinadoresRepository();
            }
        }

        /// <summary>
        /// Obtener coleccion de EstadosPatrocinadores de la base de datos.
        /// </summary>
        /// <returns>Retorna coleccion de EstadosPatrocinadores</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 18/12/2017</br>
        /// <b>Descripcion:</b> Obtiene una coleccion de los estados de patrocinadores.
        /// </remarks>
        public IEnumerable<EstadosPatrocinadores> ObtenerEstadosPatrocinadores()
        {
            IEnumerable<EstadosPatrocinadores> estadosPatrocinadores = new List<EstadosPatrocinadores>();
            try
            {
                estadosPatrocinadores = repositorio.GetAll();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return estadosPatrocinadores;
        }

        /// <summary>
        /// Obtener identificador de EstadosPatrocinadores de la base de datos.
        /// </summary>
        /// <param name="nombre">nombre del estado del patrocinador</param>
        /// <returns>Retorna numero entero</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 18/12/2017</br>
        /// <b>Descripcion:</b> Obtiene el identificador del estado del patrocinador segun el nombre que ingrese.
        /// </remarks>
        public int ObtenerIdEstadosPatrocinadorPorNombre(string nombre)
        {
            int result;
            try
            {
                Expression<Func<EstadosPatrocinadores, bool>> predicate = x => x.Nombre == nombre;
                result = repositorio.GetBy(predicate).FirstOrDefault().Id_EstadoPatrocinador;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return result;
        }

        /// <summary>
        /// Obtener el nombre de EstadosActividades de la base de datos.
        /// </summary>
        /// <param name="idEstadosPatrocinador">Identificador del estado del patrocinador</param>
        /// <returns>Retorna cadena de caracteres</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 18/12/2017</br>
        /// <b>Descripcion:</b> Obtiene el nombre del estado del estado del patrocinador segun el numero de identificador que ingrese.
        /// </remarks>
        public string ObtenerNombreEstadosPatrocinadorPorId(int idEstadosPatrocinador)
        {
            string result = string.Empty;
            try
            {
                Expression<Func<EstadosPatrocinadores, bool>> predicate = x => x.Id_EstadoPatrocinador == idEstadosPatrocinador;
                result = repositorio.GetBy(predicate).FirstOrDefault().Nombre;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return result;
        }
    }
}