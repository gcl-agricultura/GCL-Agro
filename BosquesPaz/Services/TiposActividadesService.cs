﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BosquesPaz.Models;
using System.Linq.Expressions;
using BosquesPaz.Repositories;

namespace BosquesPaz.Services
{
    /// <summary>
    /// Clase que verifica que las peticiones de los datos sean correctas. 
    /// </summary>
    /// <remarks>
    /// <h3>Calidad</h3>
    /// </hr>
    /// <b>Requerimiento:</b> Help Desk</br>
    /// <b>Version:</b> 1</br>
    /// <b>Autor:</b> Grupo Cubo Ltda.</br>
    /// <b>Fecha:</b> 18/12/2017</br>
    /// <b>Descripcion:</b> Funciona para solicitar los datos al repositorio de tipos de actividades y retornar los datos obtenidos.
    /// </remarks>
    public class TiposActividadesService
    {
        /// <summary>
        /// Propiedad que instancia la clase TiposActividadesRepository
        /// </summary>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 18/12/2017</br>
        /// <b>Descripcion:</b> Propiedad que instancia el repositorio TiposActividadesRepository, para enviar peticiones y recibir el resultado.
        /// </remarks>
        private TiposActividadesRepository repositorio
        {
            get
            {
                return new TiposActividadesRepository();
            }
        }

        /// <summary>
        /// Obtener coleccion de TiposActividades de la base de datos.
        /// </summary>
        /// <returns>Retorna coleccion de TiposActividades</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 18/12/2017</br>
        /// <b>Descripcion:</b> Obtiene una coleccion de los tipos de actividades.
        /// </remarks>
        public IEnumerable<TiposActividades> ObtenerTiposRecursos()
        {
            IEnumerable<TiposActividades> tiposActividades = new List<TiposActividades>();
            try
            {
                tiposActividades = repositorio.GetAll();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return tiposActividades;
        }

        /// <summary>
        /// Obtener identificador de TiposActividades de la base de datos.
        /// </summary>
        /// <param name="nombre">Nombre del tipo de actividad</param>
        /// <returns>Retorna numero entero</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 18/12/2017</br>
        /// <b>Descripcion:</b> Obtiene el identificador del tipo de actividad segun el nombre que se ingrese.
        /// </remarks>
        public int ObtenerIdTiposActividadesPorNombre(string nombre)
        {
            int result;
            try
            {
                Expression<Func<TiposActividades, bool>> predicate = x => x.Nombre == nombre;
                result = repositorio.GetBy(predicate).FirstOrDefault().Id_TipoActividad;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }

        /// <summary>
        /// Obtener nombre de TiposActividades de la base de datos.
        /// </summary>
        /// <param name="idTipoActividad">Identificador de tipo de actividad</param>
        /// <returns>Retorna cadena de caracteres</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 18/12/2017</br>
        /// <b>Descripcion:</b> Obtiene el nombre del tipo de actividad segun el numero de identificador que se ingrese.
        /// </remarks>
        public string ObtenerNombreTipoActividadPorId(int idTipoActividad)
        {
            string result = string.Empty;
            try
            {
                Expression<Func<TiposActividades, bool>> predicate = x => x.Id_TipoActividad == idTipoActividad;
                result = repositorio.GetBy(predicate).FirstOrDefault().Nombre;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }
    }
}