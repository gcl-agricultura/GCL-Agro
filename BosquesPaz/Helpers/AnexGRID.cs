﻿using System.Collections.Generic;

namespace BosquesPaz.Helpers
{
    public class AnexGRID
    {
        private AnexGRIDResponde aresponde = new AnexGRIDResponde();
        public string columna { get; set; }
        public string columna_orden { get; set; }
        public List<AnexGRIDFiltro> filtros { get; set; }
        public int limite { get; set; }
        public int pagina { get; set; }
        public List<AnexGRIDParametro> parametros { get; set; }

        public void Inicializar()
        {
            /* Cantidad de registros por página */
            pagina = pagina - 1;

            /* Desde que número de fila va a paginar */
            if (pagina > 0) pagina = pagina * limite;

            /* Filtros */
            if (filtros == null)
                filtros = new List<AnexGRIDFiltro>();

            /* Parametros adicionales */
            if (parametros == null)
                parametros = new List<AnexGRIDParametro>();
        }

        public AnexGRIDResponde responde()
        {
            return aresponde;
        }

        public void SetData(dynamic data, int total)
        {
            aresponde = new AnexGRIDResponde
            {
                data = data,
                total = total
            };
        }
    }

    public class AnexGRIDFiltro
    {
        public string columna { get; set; }
        public string valor { get; set; }
    }

    public class AnexGRIDParametro
    {
        public string clave { get; set; }
        public string valor { get; set; }
    }

    public class AnexGRIDResponde
    {
        public dynamic data { get; set; }
        public int total { get; set; }
    }
}