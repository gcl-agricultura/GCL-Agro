﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BosquesPaz.ServiceReferenceFuncionarios;
using System.ServiceModel;

namespace BosquesPaz.Helpers
{
    public class Infraestructura 
    {
        public EntidadInfoUsuario AutorizaUsuario(string usuario, string contrasena)
        {
            EntidadInfoUsuario entity = null;
            FuncionariosClient client = null;

            try
            {
                client = new FuncionariosClient();
                entity = client.Validate(usuario, contrasena, Aplicaciones.BosquesPaz);
            }
            catch (FaultException<InteropClientFault> ex) { throw ex; }
            catch (FaultException<InteropServiceFault> ex) { throw ex; }
            catch (ApplicationException ex) { throw ex; }
            finally
            {
                if (client != null && client.State == CommunicationState.Opened)
                    client.Close();
            }
            return entity;
        }

        public bool PermisoUsuario(EntidadInfoUsuario info, string accion, string modulo)
        {
            EntidadInfoUsuario entity = null;
            entity = info;
            bool result = false;

            if (entity != null)
            {
                result = entity.AccionesModulos.Any(x => x.Accion == accion && x.Modulo == modulo);
                //result = true;
            }

            return result;
        }

        public bool SesionUsuario(EntidadInfoUsuario info)
        {
            EntidadInfoUsuario entity = null;
            entity = info;
            bool result = false;
            if (entity != null)
            {
                result = true;
            }
            return result;
        }

        public string NombreUsuario(EntidadInfoUsuario info)
        {
            EntidadInfoUsuario entity = null;
            entity = info;
            string result = null;
            if(entity != null)
            {
                result = entity.Usuario;
            }
            return result;
        }
    }
}