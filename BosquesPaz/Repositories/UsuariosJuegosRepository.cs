﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BosquesPaz.Models;
using  BosquesPaz.Repositorios;

namespace BosquesPaz.Repositories
{
    /// <summary>
    /// Clase para realizar consultas en la base de datos. 
    /// </summary>
    /// <remarks>
    /// <h3>Calidad</h3>
    /// </hr>
    /// <b>Requerimiento:</b> Help Desk</br>
    /// <b>Version:</b> 1</br>
    /// <b>Autor:</b> Grupo Cubo Ltda.</br>
    /// <b>Fecha:</b> 12/12/2017</br>
    /// <b>Descripcion:</b> Hereda varios metodos que permiten realizar consultas a la entidadad UsuariosJuegos.
    /// </remarks>
    public class UsuariosJuegosRepository : GenericRepository<UsuariosJuegos, int>
    {
        /// <summary>
        /// Metodo constructor que hereda el contexto 
        /// </summary>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 12/12/2017</br>
        /// <b>Descripcion:</b> Metodo constructor que hereda los metodos que posee el contexto.
        /// </remarks>
        public UsuariosJuegosRepository() : base(new BosquesPazContext())
        {
                
        }

        /// <summary>
        /// Obtener coleccion de UsuariosJuegos.
        /// </summary>
        /// <returns>Retorna coleccion de UsuariosJuegos</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 12/12/2017</br>
        /// <b>Descripcion:</b> Realiza la consulta de todos los usuarios de juegos y los retorna en una coleccion de UsuariosJuegos.
        /// </remarks>
        public IEnumerable<UsuariosJuegos> ObtenerUsuarioJuego()
        {
            IEnumerable<UsuariosJuegos> user = null;
            try
            {
                using (var db = new BosquesPazContext())
                {
                    user = db.UsuariosJuegos.ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return user;
        }

        /// <summary>
        /// Obtener coleccion de UsuariosJuegos.
        /// </summary>
        /// <param name="usuario">Nombre de un usuario</param>
        /// <param name="modulo">Modulo del juego</param>
        /// <param name="juego">Nombre del juego</param>
        /// <returns>Retorna coleccion de UsuariosJuegos</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 12/12/2017</br>
        /// <b>Descripcion:</b> Realiza la consulta del usuario de juegos segun el usario, modulo y nombre del juego y los retorna en una coleccion de UsuariosJuegos.
        /// </remarks>
        public IEnumerable<UsuariosJuegos> ObtenerUsuarioJuegoPorNickName(string usuario, string modulo, string juego)
        {
            IEnumerable<UsuariosJuegos> user = null;
            try
            {
                using (var db = new BosquesPazContext())
                {
                    user = db.UsuariosJuegos.Where(u=> u.Usuario == usuario && u.Modulo == modulo && u.Juego == juego).ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return user;
        }

        /// <summary>
        /// Obtener coleccion de UsuariosJuegos.
        /// </summary>
        /// <param name="usuario">Nombre de un usuario</param>
        /// <param name="modulo">Modulo del juego</param>
        /// <param name="juego">Nombre del juego</param>
        /// <returns>Retorna coleccion de UsuariosJuegos</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 12/12/2017</br>
        /// <b>Descripcion:</b> Realiza la consulta de los puntajes de un usuario de juegos segun el usario, modulo y nombre del juego y los retorna en una coleccion de UsuariosJuegos.
        /// </remarks>
        public IEnumerable<UsuariosJuegos> ObtenerPuntajeSopa(string usuario, string modulo, string juego)
        {
            IEnumerable<UsuariosJuegos> result = null;
            try
            {
                using (var db = new BosquesPazContext())
                {
                    result = db.UsuariosJuegos.Include("NivelesPuntajes")
                                               .Where(r => r.Usuario == usuario && r.Modulo == modulo)
                                               .ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }

        /// <summary>
        /// Obtener coleccion de UsuariosJuegos.
        /// </summary>
        /// <param name="usuario">Nombre de un usuario</param>
        /// <param name="modulo">Modulo del juego</param>
        /// <param name="juego">Nombre del juego</param>
        /// <returns>Retorna coleccion de UsuariosJuegos</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 12/12/2017</br>
        /// <b>Descripcion:</b> Realiza la consulta de los puntajes de un usuario de juegos segun el usario, modulo y nombre del juego y los retorna en una coleccion de UsuariosJuegos.
        /// </remarks>
        public IEnumerable<UsuariosJuegos> ObtenerPuntajeRompe(string usuario, string modulo, string juego)
        {
            IEnumerable<UsuariosJuegos> result = null;
            try
            {
                using (var db = new BosquesPazContext())
                {
                    result = db.UsuariosJuegos.Include("NivelesPuntajes")
                                               .Where(r => r.Usuario == usuario && r.Modulo == modulo && r.Juego == juego)
                                               .ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }
    }
}