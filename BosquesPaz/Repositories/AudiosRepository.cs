﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BosquesPaz.Models;

namespace BosquesPaz.Repositories
{
    /// <summary>
    /// Clase para realizar consultas en la base de datos. 
    /// </summary>
    /// <remarks>
    /// <h3>Calidad</h3>
    /// </hr>
    /// <b>Requerimiento: Help Desk</b></br>
    /// <b>Version: </b>1</br>
    /// <b>Autor: </b>Grupo Cubo Ltda.</br>
    /// <b>Fecha: </b>12/12/2017</br>
    /// <b>Descripcion:</b> Realiza las consultas o inserciones en la base de datos para la entidad AudiosHistorias.
    /// </remarks>
    public class AudiosRepository
    {
        /// <summary>
        /// Guardar audio en la base de datos. 
        /// </summary>
        /// <param name="audios">Coleccion de AudiosHistorias</param>
        /// <returns>Retorna Coleccion de AudiosHistorias</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 12/12/2017</br>
        /// <b>Descripcion:</b> Realiza la insercion de una coleccion de AudiosHistorias en la base de datos.
        /// </remarks>
        public IEnumerable<AudiosHistorias> GuardarAudiosHistorias(IEnumerable<AudiosHistorias> audios)
        {
            try
            {
                using (var db = new BosquesPazContext())
                {
                    db.AudiosHistorias.AddRange(audios);
                    db.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return audios;
        }

        /// <summary>
        /// Metodo para consultar en la base de datos. 
        /// </summary>
        /// <param name="idHistoriaBosque">Identificador de HistoriaBosque</param>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento: </b>Help Desk</br>
        /// <b>Version: </b>1</br>
        /// <b>Autor: </b>Grupo Cubo Ltda.</br>
        /// <b>Fecha: </b>12/12/2017</br>
        /// <b>Descripcion:</b> Realiza la consulta de un audio historia segun el identificador y la retorna en una coleccion de AudiosHiastorias.
        /// </remarks>
        public IEnumerable<AudiosHistorias> ObtenerAudioBosque(int idHistoriaBosque)
        {
            IEnumerable<AudiosHistorias> result = null;
            try
            {
                using (var db = new BosquesPazContext())
                {
                    result = db.AudiosHistorias.Where(f => f.Id_HistoriaBosque == idHistoriaBosque).ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }
    }
}