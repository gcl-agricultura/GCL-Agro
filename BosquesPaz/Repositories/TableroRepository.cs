﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BosquesPaz.Models;
using  BosquesPaz.Repositorios;

namespace BosquesPaz.Repositories
{
    public class TableroRepository : GenericRepository<HistorialIndicadores, int>
    {
        public TableroRepository() : base(new BosquesPazContext())
        {

        }

        public IEnumerable<ActividadesGRFBosques> ActividadesBosques()
        {
            IEnumerable<ActividadesGRFBosques> result = new List<ActividadesGRFBosques>();
            try
            {
                using (var db = new BosquesPazContext())
                {
                    var query = db.ActividadesGRFBosques().ToList();
                    if (query.Count > 0)
                        result = query;
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return result;
        }

        public IEnumerable<BeneficiandoBosques> BeneficiandoBosques()
        {
            IEnumerable<BeneficiandoBosques> result = new List<BeneficiandoBosques>();
            try
            {
                using (var db = new BosquesPazContext())
                {
                    var query = db.BeneficiandoBosques().ToList();
                    if (query.Count > 0)
                        result = query;
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return result;
        }

        public CantidadBosques CantidadBosques()
        {
            CantidadBosques result = new CantidadBosques();
            try
            {
                using (var db = new BosquesPazContext())
                {
                    var query = db.CantidadBosques().ToList();
                    if (query.Count > 0)
                        result = query.FirstOrDefault();
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return result;
        }

        public IEnumerable<DistribucionBosques> DistribucionBosques()
        {
            IEnumerable<DistribucionBosques> result = new List<DistribucionBosques>();
            try
            {
                using (var db = new BosquesPazContext())
                {
                    var query = db.DistribucionBosques().ToList();
                    if (query.Count > 0)
                        result = query;
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return result;
        }

        public IEnumerable<InversionesBosques> InversionesBosques()
        {
            IEnumerable<InversionesBosques> result = new List<InversionesBosques>();
            try
            {
                using (var db = new BosquesPazContext())
                {
                    var query = db.InversionesBosques().ToList();
                    if (query.Count > 0)
                        result = query;
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return result;
        }

        public IEnumerable<InversionistasBosques> InversionistasBosques()
        {
            IEnumerable<InversionistasBosques> result = new List<InversionistasBosques>();
            try
            {
                using (var db = new BosquesPazContext())
                {
                    var query = db.InversionistasBosques().ToList();
                    if (query.Count > 0)
                        result = query;
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return result;
        }

        public IEnumerable<LogrosBosques> LogrosBosques()
        {
            IEnumerable<LogrosBosques> result = new List<LogrosBosques>();
            try
            {
                using (var db = new BosquesPazContext())
                {
                    var query = db.LogrosBosques().ToList();
                    if (query.Count > 0)
                        result = query;
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return result;
        }

        public IEnumerable<ProductosServiciosBosques> ProductosServiciosBosques()
        {
            IEnumerable<ProductosServiciosBosques> result = new List<ProductosServiciosBosques>();
            try
            {
                using (var db = new BosquesPazContext())
                {
                    var query = db.ProductosServiciosBosques().ToList();
                    if (query.Count > 0)
                        result = query;
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return result;
        }

    }
}