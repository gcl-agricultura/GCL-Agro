﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BosquesPaz.Models;
using  BosquesPaz.Repositorios;

namespace BosquesPaz.Repositories
{
    /// <summary>
    /// Clase para realizar consultas en la base de datos. 
    /// </summary>
    /// <remarks>
    /// <h3>Calidad</h3>
    /// </hr>
    /// <b>Requerimiento:</b> Help Desk</br>
    /// <b>Version:</b> 1</br>
    /// <b>Autor:</b> Grupo Cubo Ltda.</br>
    /// <b>Fecha:</b> 12/12/2017</br>
    /// <b>Descripcion:</b> Hereda varios metodos que permiten realizar consultas a la entidadad NivelesPuntajes.
    /// </remarks>
    public class NivelesPuntajesRepository : GenericRepository<NivelesPuntajes, int>
    {
        /// <summary>
        /// Metodo constructor que hereda el contexto.
        /// </summary>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 12/12/2017</br>
        /// <b>Descripcion:</b> Metodo constructor que hereda los metodos que posee el contexto.
        /// </remarks>
        public NivelesPuntajesRepository() : base(new BosquesPazContext())
        {

        }

        /// <summary>
        /// Obtener una coleccion de nivel puntaje segun el identificador.
        /// </summary>
        /// <param name="idPuntaje">Identificador de NivelesPuntajes</param>
        /// <returns>Retorna coleccion de NivelesPuntajes</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 12/12/2017</br>
        /// <b>Descripcion:</b> Obtiene una coleccion de NivelesPuntajes segun el numero de identificador y retorna una coleccion de NivelesPuntajes.
        /// </remarks>
        public IEnumerable<NivelesPuntajes> ObtenerNivelPuntaje(int idPuntaje)
        {
            IEnumerable<NivelesPuntajes> nivel = null;
            try
            {
                using (var db = new BosquesPazContext())
                {
                    nivel = db.NivelesPuntajes.Where(n=> n.Id_NivelPuntaje == idPuntaje).ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return nivel;
        }
    }
}