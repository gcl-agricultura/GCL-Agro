﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BosquesPaz.Models;
using  BosquesPaz.Repositorios;

namespace BosquesPaz.Repositories
{
    /// <summary>
    /// Clase para realizar consultas en la base de datos. 
    /// </summary>
    /// <remarks>
    /// <h3>Calidad</h3>
    /// </hr>
    /// <b>Requerimiento:</b> Help Desk</br>
    /// <b>Version:</b> 1</br>
    /// <b>Autor:</b> Grupo Cubo Ltda.</br>
    /// <b>Fecha:</b> 12/12/2017</br>
    /// <b>Descripcion:</b> Hereda varios metodos que permiten realizar consultas a la entidadad FichasBosques.
    /// </remarks>
    public class FichasRepository : GenericRepository<FichasBosques, int>
    {
        /// <summary>
        /// Metodo constructor que hereda el contexto.
        /// </summary>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 12/12/2017</br>
        /// <b>Descripcion:</b> Metodo constructor que hereda los metodos que posee el contexto.
        /// </remarks>
        public FichasRepository() : base(new BosquesPazContext())
        {

        }

        /// <summary>
        /// Obtener una instancia de la ficha del bosque segun el identificador.
        /// </summary>
        /// <param name="idFichaBosque">Identificador de la ficha</param>
        /// <returns>Retorna instancia de FichasBosques</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento: Help Desk</b></br>
        /// <b>Version: 1</b></br>
        /// <b>Autor: Grupo Cubo Ltda.</b></br>
        /// <b>Fecha: 12/12/2017<b></br>
        /// <b>Descripcion: Obtiene una instancia de la ficha del bosque segun el numero identificador que ingrese.
        /// </remarks>
        public override FichasBosques GetByKey(int idFichaBosque)
        {
            FichasBosques result = null;
            try
            {
                using (var db = new BosquesPazContext())
                {
                    result = db.FichasBosques.Include("EstadosFichas")
                                               .Where(r => r.Id_FichaBosque == idFichaBosque)
                                               .FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }
    }
}