﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BosquesPaz.Models;
using  BosquesPaz.Repositorios;

namespace BosquesPaz.Repositories
{
    /// <summary>
    /// Clase para realizar consultas en la base de datos. 
    /// </summary>
    /// <remarks>
    /// <h3>Calidad</h3>
    /// </hr>
    /// <b>Requerimiento:</b> Help Desk</br>
    /// <b>Version:</b> 1</br>
    /// <b>Autor:</b> Grupo Cubo Ltda.</br>
    /// <b>Fecha:</b> 12/12/2017</br>
    /// <b>Descripcion: Hereda varios metodos que permiten realizar consultas a la entidadad IndividuosEspecies.
    /// </remarks>
    public class IndividuosRepository : GenericRepository<IndividuosEspecies, int>
    {
        /// <summary>
        /// Metodo constructor que hereda el contexto.
        /// </summary>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 12/12/2017</br>
        /// <b>Descripcion:</b> Metodo constructor que hereda los metodos que posee el contexto.
        /// </remarks>
        public IndividuosRepository() : base(new BosquesPazContext())
        {

        }

        /// <summary>
        /// Obtener una instancia del indiviudo/especie segun el identificador.
        /// </summary>
        /// <param name="idActividadBosque">Indentificador de IndividuosEspecies</param>
        /// <returns>Retorna instancia de IndividuosEspecies</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 12/12/2017</br>
        /// <b>Descripcion:</b> Obtiene una instancia del individuo/especie segun el numero de identificador que ingrese.
        /// </remarks>
        public override IndividuosEspecies GetByKey(int IdIndividuoEspecie)
        {
            IndividuosEspecies result = null;
            try
            {
                using (var db = new BosquesPazContext())
                {
                    result = db.IndividuosEspecies.Include("TiposIndividuosEspecies")
                                                  .Include("UnidadesIndividuosEspecies")
                                                  .Where(i => i.Id_IndividuoEspecie == IdIndividuoEspecie)
                                                  .FirstOrDefault();
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return result;
        }
    }
}