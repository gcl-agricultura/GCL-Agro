﻿
using BosquesPaz.Repositories.Especificaciones;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using System.Transactions;

namespace BosquesPaz.Repositorios
{
    /// <summary>
    /// 
    /// </summary>
    /// <typeparam name="TEntity">The type of the entity.</typeparam>
    /// <typeparam name="TKey">The type of the key.</typeparam>
    public abstract class GenericRepository<TEntity, TKey> where TEntity : class, new() where TKey : struct
    {
        /// <summary>
        /// The context
        /// </summary>
        protected DbContext _context;

        /// <summary>
        /// Initializes a new instance of the <see cref="GenericRepository{TEntity, TKey}"/> class.
        /// </summary>
        /// <param name="context">The context.</param>
        public GenericRepository(DbContext context)
        {
            _context = context;
            _context.Configuration.LazyLoadingEnabled = false;
            _context.Configuration.ProxyCreationEnabled = false;
        }

        /// <summary>
        /// Counts the specified predicate.
        /// </summary>
        /// <param name="_predicate">The predicate.</param>
        /// <returns></returns>
        public virtual int Count(Expression<Func<TEntity, bool>> _predicate)
        {
            return _context.Set<TEntity>().Where(_predicate).Count();
        }

        /// <summary>
        /// Counts the asynchronous.
        /// </summary>
        /// <param name="_predicate">The predicate.</param>
        /// <returns></returns>
        public virtual async Task<int> CountAsync(Expression<Func<TEntity, bool>> _predicate)
        {
            return await _context.Set<TEntity>().Where(_predicate).CountAsync();
        }

        /// <summary>
        /// Creates the specified entity.
        /// </summary>
        /// <param name="_entity">The entity.</param>
        /// <returns></returns>
        public virtual TEntity Create(TEntity _entity)
        {
            _context.Set<TEntity>().Add(_entity);
            _context.SaveChanges();

            return _entity;
        }

        /// <summary>
        /// Creates the asynchronous.
        /// </summary>
        /// <param name="_entity">The entity.</param>
        /// <returns></returns>
        public virtual async Task<TEntity> CreateAsync(TEntity _entity)
        {
            try
            {
                _context.Set<TEntity>().Add(_entity);
                await _context.SaveChangesAsync();
                return _entity;
            }
            catch (Exception exc)
            {
                throw exc;
            }
        }

        /// <summary>
        /// Deletes the specified entity.
        /// </summary>
        /// <param name="_entity">The entity.</param>
        /// <returns></returns>
        public virtual bool Delete(TEntity _entity)
        {
            using (TransactionScope transaction = new TransactionScope())
            {
                _context.Set<TEntity>().Remove(_entity);
                _context.SaveChanges();
                transaction.Complete();
                return true;
            }
        }

        /// <summary>
        /// Deletes the asynchronous.
        /// </summary>
        /// <param name="_entity">The entity.</param>
        /// <returns></returns>
        public virtual async Task<bool> DeleteAsync(TEntity _entity)
        {
            using (TransactionScope transaction = new TransactionScope())
            {
                _context.Set<TEntity>().Remove(_entity);
                await _context.SaveChangesAsync();
                transaction.Complete();
                return true;
            }
        }

        /// <summary>
        /// Gets all.
        /// </summary>
        /// <returns></returns>
        public virtual IEnumerable<TEntity> GetAll()
        {
            IQueryable<TEntity> query = _context.Set<TEntity>().AsQueryable<TEntity>();
            return query;
        }

        /// <summary>
        /// Gets all asynchronous.
        /// </summary>
        /// <returns></returns>
        public virtual async Task<IEnumerable<TEntity>> GetAllAsync()
        {
            var query = await _context.Set<TEntity>().ToListAsync<TEntity>();
            return query.AsEnumerable();
        }

        /// <summary>
        /// Gets the by.
        /// </summary>
        /// <param name="_predicate">The predicate.</param>
        /// <returns></returns>
        public virtual IQueryable<TEntity> GetBy(Expression<Func<TEntity, bool>> _predicate)
        {
            IQueryable<TEntity> query = _context.Set<TEntity>().Where(_predicate).AsQueryable();
            return query;
        }

        /// <summary>
        /// Gets the by aspect.
        /// </summary>
        /// <param name="specification">The specification.</param>
        /// <returns></returns>
        public virtual IQueryable<TEntity> GetByAspect(ISpecification<TEntity> specification)
        {
            return _context.Set<TEntity>().Where(specification.SatisfiedBy()).AsQueryable();
        }

        /// <summary>
        /// Gets the by aspect asynchronous.
        /// </summary>
        /// <param name="specification">The specification.</param>
        /// <returns></returns>
        public virtual async Task<IQueryable<TEntity>> GetByAspectAsync(ISpecification<TEntity> specification)
        {
            var query = await _context.Set<TEntity>().Where(specification.SatisfiedBy()).ToListAsync();
            return query.AsQueryable();
        }

        /// <summary>
        /// Gets the by asynchronous.
        /// </summary>
        /// <param name="_predicate">The predicate.</param>
        /// <returns></returns>
        public virtual async Task<IQueryable<TEntity>> GetByAsync(Expression<Func<TEntity, bool>> _predicate)
        {
            var query = await _context.Set<TEntity>().Where(_predicate).ToListAsync<TEntity>();
            return query.AsQueryable();
        }

        /// <summary>
        /// Gets the by key.
        /// </summary>
        /// <param name="_key">The key.</param>
        /// <returns></returns>
        public virtual TEntity GetByKey(TKey _key)
        {
            return _context.Set<TEntity>().Find(_key);
        }

        /// <summary>
        /// Gets the by key asynchronous.
        /// </summary>
        /// <param name="_key">The key.</param>
        /// <returns></returns>
        public virtual async Task<TEntity> GetByKeyAsync(TKey _key)
        {
            return await _context.Set<TEntity>().FindAsync(_key);
        }

        /// <summary>
        /// Gets the paged elements.
        /// </summary>
        /// <typeparam name="S"></typeparam>
        /// <param name="pageIndex">Index of the page.</param>
        /// <param name="pageCount">The page count.</param>
        /// <param name="orderByExpression">The order by expression.</param>
        /// <param name="specification">The specification.</param>
        /// <param name="ascending">if set to <c>true</c> [ascending].</param>
        /// <returns></returns>
        public virtual IQueryable<TEntity> GetPagedElements<S>(int pageIndex, int pageCount, Expression<Func<TEntity, S>> orderByExpression, Expression<Func<TEntity, bool>> predicate, bool ascending)
        {
            try
            {
                if (ascending)
                {
                    var query = _context.Set<TEntity>().Where(predicate).OrderBy(orderByExpression).Skip(pageCount * (pageIndex - 1)).Take(pageCount).ToList<TEntity>();
                    return query.AsQueryable();
                }
                else
                {
                    var query = _context.Set<TEntity>().Where(predicate).OrderByDescending(orderByExpression).Skip(pageCount * (pageIndex - 1)).Take(pageCount).ToList<TEntity>();
                    return query.AsQueryable();
                }
            }
            catch (Exception exc)
            {
                throw exc;
            }
        }

        /// <summary>
        /// Gets the paged elements asynchronous.
        /// </summary>
        /// <typeparam name="S"></typeparam>
        /// <param name="pageIndex">Index of the page.</param>
        /// <param name="pageCount">The page count.</param>
        /// <param name="orderByExpression">The order by expression.</param>
        /// <param name="predicate">The predicate.</param>
        /// <param name="ascending">if set to <c>true</c> [ascending].</param>
        /// <returns></returns>
        public virtual async Task<IQueryable<TEntity>> GetPagedElementsAsync<S>(int pageIndex, int pageCount, Expression<Func<TEntity, S>> orderByExpression, Expression<Func<TEntity, bool>> predicate, bool ascending)
        {
            try
            {
                if (ascending)
                {
                    var queryAsc = await _context.Set<TEntity>().Where(predicate).OrderBy(orderByExpression).Skip(pageCount * (pageIndex - 1)).Take(pageCount).ToListAsync<TEntity>();
                    return queryAsc.AsQueryable();
                }
                else
                {
                    var queryAsc = await _context.Set<TEntity>().Where(predicate).OrderByDescending(orderByExpression).Skip(pageCount * (pageIndex - 1)).Take(pageCount).ToListAsync<TEntity>();
                    return queryAsc.AsQueryable();
                }
            }
            catch (Exception exc)
            {
                throw exc;
            }
        }

        /// <summary>
        /// Updates the specified entity.
        /// </summary>
        /// <param name="_entity">The entity.</param>
        /// <returns></returns>
        public virtual bool Update(TEntity _entity)
        {
            using (TransactionScope transaction = new TransactionScope())
            {
                _context.Entry(_entity).State = EntityState.Modified;
                _context.SaveChanges();
                transaction.Complete();
                return true;
            }
        }

        /// <summary>
        /// Updates the asynchronous.
        /// </summary>
        /// <param name="_entity">The entity.</param>
        /// <returns></returns>
        public virtual async Task<bool> UpdateAsync(TEntity _entity)
        {
            try
            {
                _context.Entry(_entity).State = EntityState.Modified;
                await _context.SaveChangesAsync();
                return true;
            }
            catch (Exception exc)
            {
                throw exc;
            }
        }
    }
}