﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BosquesPaz.Models;
using  BosquesPaz.Repositorios;
using System.Linq.Expressions;

namespace BosquesPaz.Repositories
{
    /// <summary>
    /// Clase para realizar consultas en la base de datos. 
    /// </summary>
    /// <remarks>
    /// <h3>Calidad</h3>
    /// </hr>
    /// <b>Requerimiento:</b> Help Desk</br>
    /// <b>Version:</b> 1</br>
    /// <b>Autor:</b> Grupo Cubo Ltda.</br>
    /// <b>Fecha:</b> 12/12/2017</br>
    /// <b>Descripcion:</b> Realiza las consultas en la base de datos para varios procedimientos almacenados.
    /// </remarks>
    public class EstadisticasRepository
    {
        /// <summary>
        /// Obtener coleccion de SP_DepartamentoCantidadBosque.
        /// </summary>
        /// <param name="pageIndex">Numero de Paginas</param>
        /// <param name="pageCount">Numero de filas</param>
        /// <returns>Retorna coleccion de SP_DepartamentoCantidadBosque</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 12/12/2017</br>
        /// <b>Descripcion:</b> Realiza la consulta de la cobertura de bosques y los retorna en una coleccion de SP_DepartamentoCantidadBosque.
        /// </remarks>
        public IEnumerable<SP_DepartamentoCantidadBosque> DepartamentoCantidadBosque(int pageIndex, int pageCount)
        {
            IEnumerable<SP_DepartamentoCantidadBosque> result = null;
            try
            {
                using (var db = new BosquesPazContext())
                {

                    result = db.SP_DepartamentoCantidadBosque().AsQueryable().Skip(pageCount * (pageIndex - 1)).Take(pageCount).ToList<SP_DepartamentoCantidadBosque>();
                   
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }

        /// <summary>
        /// Obtener coleccion de SP_DepartamentoTipoEstadoCantidadBosque.
        /// </summary>
        /// <param name="pageIndex">Numero de Paginas</param>
        /// <param name="pageCount">Numero de filas</param>
        /// <returns>Retorna coleccion de SP_DepartamentoTipoEstadoCantidadBosque</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 12/12/2017</br>
        /// <b>Descripcion:</b> Realiza la consulta de la cobertura de recursos y los retorna en una coleccion de SP_DepartamentoTipoEstadoCantidadBosque.
        /// </remarks>
        public IEnumerable<SP_DepartamentoTipoEstadoCantidadBosque> DepartamentoTipoEstadoCantidadBosque(int pageIndex, int pageCount)
        {
            IEnumerable<SP_DepartamentoTipoEstadoCantidadBosque> result = null;
            try
            {
                using (var db = new BosquesPazContext())
                {
                    result = db.SP_DepartamentoTipoEstadoCantidadBosque().AsQueryable().Skip(pageCount * (pageIndex - 1)).Take(pageCount).ToList<SP_DepartamentoTipoEstadoCantidadBosque>();
                    
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }

        /// <summary>
        /// Obtener coleccion de SP_DepartamentoTipoEstadoPresupuestoCantidadBosque.
        /// </summary>
        /// <param name="pageIndex">Numero de Paginas</param>
        /// <param name="pageCount">Numero de filas</param>
        /// <returns>Retorna coleccion de SP_DepartamentoTipoEstadoPresupuestoCantidadBosque</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 12/12/2017</br>
        /// <b>Descripcion:</b> Realiza la consulta de la cobertura de actividades y los retorna en una coleccion de SP_DepartamentoTipoEstadoCantidadBosque.
        /// </remarks>
        public IEnumerable<SP_DepartamentoTipoEstadoPresupuestoCantidadBosque> DepartamentoTipoEstadoPresupuestoCantidadBosque(int pageIndex, int pageCount)
        {
            IEnumerable<SP_DepartamentoTipoEstadoPresupuestoCantidadBosque> result = null;
            try
            {
                using (var db = new BosquesPazContext())
                {
                   result = db.SP_DepartamentoTipoEstadoPresupuestoCantidadBosque().AsQueryable().Skip(pageCount * (pageIndex - 1)).Take(pageCount).ToList<SP_DepartamentoTipoEstadoPresupuestoCantidadBosque>();
                   
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }

        /// <summary>
        /// Obtener coleccion de SP_DepartamentoTipoNombreCantidadBosque.
        /// </summary>
        /// <param name="pageIndex">Numero de Paginas</param>
        /// <param name="pageCount">Numero de filas</param>
        /// <returns>Retorna coleccion de SP_DepartamentoTipoNombreCantidadBosque</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 12/12/2017</br>
        /// <b>Descripcion:</b> Realiza la consulta de la cobertura de individuos/especies y los retorna en una coleccion de SP_DepartamentoTipoNombreCantidadBosque.
        /// </remarks>
        public IEnumerable<SP_DepartamentoTipoNombreCantidadBosque> DepartamentoTipoNombreCantidadBosque(int pageIndex, int pageCount)
        {
            IEnumerable<SP_DepartamentoTipoNombreCantidadBosque> result = null;
            try
            {
                using (var db = new BosquesPazContext())
                {
                    result = db.SP_DepartamentoTipoNombreCantidadBosque().AsQueryable().Skip(pageCount * (pageIndex - 1)).Take(pageCount).ToList<SP_DepartamentoTipoNombreCantidadBosque>();
                   
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }

        /// <summary>
        /// Obtener coleccion de SP_DepartamentoTipoNombreEstadoBosque.
        /// </summary>
        /// <param name="pageIndex">Numero de Paginas</param>
        /// <param name="pageCount">Numero de filas</param>
        /// <returns>Retorna coleccion de SP_DepartamentoTipoNombreEstadoBosque</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 12/12/2017</br>
        /// <b>Descripcion:</b> Realiza la consulta de la cobertura de indicadores y los retorna en una coleccion de SP_DepartamentoTipoNombreEstadoBosque.
        /// </remarks>
        public IEnumerable<SP_DepartamentoTipoNombreEstadoBosque> DepartamentoTipoNombreEstadoBosque(int pageIndex, int pageCount)
        {
            IEnumerable<SP_DepartamentoTipoNombreEstadoBosque> result = null;
            try
            {
                using (var db = new BosquesPazContext())
                {
                     result = db.SP_DepartamentoTipoNombreEstadoBosque().AsQueryable().Skip(pageCount * (pageIndex - 1)).Take(pageCount).ToList<SP_DepartamentoTipoNombreEstadoBosque>();
                   
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }

        /// <summary>
        /// Obtener coleccion de SP_DepartamentoTipoUnidadCantidadBosque.
        /// </summary>
        /// <param name="pageIndex">Numero de Paginas</param>
        /// <param name="pageCount">Numero de filas</param>
        /// <returns>Retorna coleccion de SP_DepartamentoTipoUnidadCantidadBosque</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 12/12/2017</br>
        /// <b>Descripcion:</b> Realiza la consulta de la cobertura de productos y servicios y los retorna en una coleccion de SP_DepartamentoTipoUnidadCantidadBosque.
        /// </remarks>
        public IEnumerable<SP_DepartamentoTipoUnidadCantidadBosque> DepartamentoTipoUnidadCantidadBosque(int pageIndex, int pageCount)
        {
            IEnumerable<SP_DepartamentoTipoUnidadCantidadBosque> result = null;
            try
            {
                using (var db = new BosquesPazContext())
                {
                     result = db.SP_DepartamentoTipoUnidadCantidadBosque().AsQueryable().Skip(pageCount * (pageIndex - 1)).Take(pageCount).ToList<SP_DepartamentoTipoUnidadCantidadBosque>();
                    
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }
    }
}