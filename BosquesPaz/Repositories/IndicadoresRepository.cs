﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BosquesPaz.Models;
using  BosquesPaz.Repositorios;

namespace BosquesPaz.Repositories
{
    /// <summary>
    /// Clase para realizar consultas en la base de datos. 
    /// </summary>
    /// <remarks>
    /// <h3>Calidad</h3>
    /// </hr>
    /// <b>Requerimiento:</b> Help Desk</br>
    /// <b>Version:</b> 1</br>
    /// <b>Autor:</b> Grupo Cubo Ltda.</br>
    /// <b>Fecha:</b> 12/12/2017</br>
    /// <b>Descripcion:</b> Hereda varios metodos que permiten realizar consultas a la entidadad IndicadoresBosques.
    /// </remarks>
    public class IndicadoresRepository : GenericRepository<IndicadoresBosques, int>
    {
        /// <summary>
        /// Metodo constructor que hereda el contexto.
        /// </summary>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 12/12/2017</br>
        /// <b>Descripcion:</b> Metodo constructor que hereda los metodos que posee el contexto.
        /// </remarks>
        public IndicadoresRepository() : base(new BosquesPazContext())
        {

        }

        /// <summary>
        /// Obtener una instancia del indicador segun el identificador.
        /// </summary>
        /// <param name="idIndicadorBosque">Identificador del indicador</param>
        /// <returns>Retorna instancia de IndicadoresBosques</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 12/12/2017</br>
        /// <b>Descripcion:</b> Obtiene una instancia del indicador segun el numero de identificador que ingrese.
        /// </remarks>
        public override IndicadoresBosques GetByKey(int idIndicadorBosque)
        {
            IndicadoresBosques result = null;
            try
            {
                using (var db = new BosquesPazContext())
                {
                    result = db.IndicadoresBosques.Include("TiposIndicadores")
                                                  .Include("EstadosIndicadores")
                                                  .Where(i => i.Id_IndicadorBosque == idIndicadorBosque)
                                                  .FirstOrDefault();
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return result;
        }
    }
}