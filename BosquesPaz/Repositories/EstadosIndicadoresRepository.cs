﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BosquesPaz.Models;
using  BosquesPaz.Repositorios;

namespace BosquesPaz.Repositories
{
    /// <summary>
    /// Clase para realizar consultas en la base de datos. 
    /// </summary>
    /// <remarks>
    /// <h3>Calidad</h3>
    /// </hr>
    /// <b>Requerimiento:</b> Help Desk</br>
    /// <b>Version:</b> 1</br>
    /// <b>Autor:</b> Grupo Cubo Ltda.</br>
    /// <b>Fecha:</b> 12/12/2017</br>
    /// <b>Descripcion:</b> Hereda varios metodos que permiten realizar consultas a la entidadad EstadosIndicadores.
    /// </remarks>
    public class EstadosIndicadoresRepository : GenericRepository<EstadosIndicadores, int>
    {
        /// <summary>
        /// Metodo constructor que hereda el contexto.
        /// </summary>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 12/12/2017</br>
        /// <b>Descripcion:</b> Metodo constructor que hereda los metodos que posee el contexto.
        /// </remarks>
        public EstadosIndicadoresRepository() : base(new BosquesPazContext())
        {

        }
    }
}