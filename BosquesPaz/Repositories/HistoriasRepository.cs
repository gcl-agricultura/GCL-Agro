﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BosquesPaz.Models;
using  BosquesPaz.Repositorios;

namespace BosquesPaz.Repositories
{
    /// <summary>
    /// Clase para realizar consultas en la base de datos. 
    /// </summary>
    /// <remarks>
    /// <h3>Calidad</h3>
    /// </hr>
    /// <b>Requerimiento:</b> Help Desk</br>
    /// <b>Version:</b> 1</br>
    /// <b>Autor:</b> Grupo Cubo Ltda.</br>
    /// <b>Fecha:</b> 12/12/2017</br>>
    /// <b>Descripcion:</b> Hereda varios metodos que permiten realizar consultas a la entidadad HistoriasBosques.
    /// </remarks>
    public class HistoriasRepository : GenericRepository<HistoriasBosques, int>
    {
        /// <summary>
        /// Metodo constructor que hereda el contexto.
        /// </summary>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 12/12/2017</br>
        /// <b>Descripcion:</b> Metodo constructor que hereda los metodos que posee el contexto.
        /// </remarks>
        public HistoriasRepository() : base(new BosquesPazContext())
        {

        }

        /// <summary>
        /// Obtener una instancia de la historia segun el identificador.
        /// </summary>
        /// <param name="idHistoriaBosque">Identificador de la historia</param>
        /// <returns>Retorna instancia de HistoriasBosques</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 12/12/2017</br>
        /// <b>Descripcion:</b> Obtiene una instancia de la historia segun el numero de identificador que ingrese.
        /// </remarks>
        public override HistoriasBosques GetByKey(int idHistoriaBosque)
        {
            HistoriasBosques result = null;
            try
            {
                using (var db = new BosquesPazContext())
                {
                    result = db.HistoriasBosques.Include("Tematicas")
                                                .Include("EstadosHistorias")
                                               .Where(r => r.Id_HistoriaBosque == idHistoriaBosque)
                                               .FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }

        /// <summary>
        /// Obtener una instancia de la historia segun el identificador.
        /// </summary>
        /// <param name="idHistoriaBosque">Identificador de HistoriasBosques</param>
        /// <returns>Retorna instancia de HistoriasBosques</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 12/12/2017</br>
        /// <b>Descripcion:</b> Obtiene una instancia de la historia segun el numero de identificador que ingrese.
        /// </remarks>
        public HistoriasBosques ObtenerFecha(int idHistoriaBosque)
        {
            HistoriasBosques result = null;
            try
            {
                using (var db = new BosquesPazContext())
                {
                    result = db.HistoriasBosques.Include("AudiHistoriasBosques")
                                               .Where(r => r.Id_HistoriaBosque == idHistoriaBosque)
                                               .LastOrDefault();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }
    }
}