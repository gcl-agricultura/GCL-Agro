﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BosquesPaz.Models;
using  BosquesPaz.Repositorios;

namespace BosquesPaz.Repositories
{
    /// <summary>
    /// Clase para realizar consultas en la base de datos. 
    /// </summary>
    /// <remarks>
    /// <h3>Calidad</h3>
    /// </hr>
    /// <b>Requerimiento:</b> Help Desk</br>
    /// <b>Version:</b> 1</br>
    /// <b>Autor:</b> Grupo Cubo Ltda.</br>
    /// <b>Fecha:</b> 12/12/2017</br>
    /// <b>Descripcion: Hereda varios metodos que permiten realizar consultas a la entidadad RecursosBosques.
    /// </remarks>
    public class RecursosRepository : GenericRepository<RecursosBosques, int>
    {
        /// <summary>
        /// Metodo constructor que hereda el contexto.
        /// </summary>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 12/12/2017</br>
        /// <b>Descripcion:</b> Metodo constructor que hereda los metodos que posee el contexto.
        /// </remarks>
        public RecursosRepository() : base(new BosquesPazContext())
        {

        }

        /// <summary>
        /// Obtener una instancia del recurso segun el identificador.
        /// </summary>
        /// <param name="idRecursoBosque">Identificador de RecursosBosques</param>
        /// <returns>Retorna instancia de RecursosBosques</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 12/12/2017</br>
        /// <b>Descripcion:</b> Obtiene una instancia del recurso segun el numero de identificador que ingrese.
        /// </remarks>
        public override RecursosBosques GetByKey(int idRecursoBosque)
        {
            RecursosBosques result = null;
            try
            {
                using (var db = new BosquesPazContext())
                {
                    result = db.RecursosBosques.Include("TiposRecursos")
                                               .Include("Estados")
                                               .Where(r => r.Id_RecursoBosque == idRecursoBosque)
                                               .FirstOrDefault(); 
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }

    }
}