﻿using  BosquesPaz.Repositorios;
using BosquesPaz.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;

namespace BosquesPaz.Repositories
{
    /// <summary>
    /// Clase para realizar consultas en la base de datos. 
    /// </summary>
    /// <remarks>
    /// <h3>Calidad</h3>
    /// </hr>
    /// <b>Requerimiento:</b> Help Desk</br>
    /// <b>Version:</b> 1</br>
    /// <b>Autor:</b> Grupo Cubo Ltda.</br>
    /// <b>Fecha:</b> 12/12/2017</br>
    /// <b>Descripcion:</b> Hereda varios metodos que permiten realizar consultas a la entidadad Bosques.
    /// </remarks>
    public class BosquesRepository : GenericRepository<Bosques, int>
    {
        /// <summary>
        /// Metodo constructor que hereda el contexto.
        /// </summary>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 12/12/2017</br>
        /// <b>Descripcion:</b> Metodo constructor que hereda los metodos que posee el contexto.
        /// </remarks>
        public BosquesRepository() : base(new BosquesPazContext())
        {
        }

        /// <summary>
        /// Obtener una instancia del bosque segun el identificador.
        /// </summary>
        /// <param name="idBosque">Identificador del bosque</param>
        /// <returns>Retorna instancia de Bosques</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 12/12/2017</br>
        /// <b>Descripcion:</b> Obtiene una instancia del bosque segun el numero de identificador que ingrese.
        /// </remarks>
        public override Bosques GetByKey(int idBosque)
        {
            Bosques result = null;
            try
            {
                using (var db = new BosquesPazContext())
                {
                    result = db.Bosques.Include("Municipios")
                                       .Include("Departamentos")
                                       .Where(f => f.Id_Bosque == idBosque)
                                       .FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }
    }
}