﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BosquesPaz.Models;
using  BosquesPaz.Repositorios;

namespace BosquesPaz.Repositories
{
    /// <summary>
    /// Clase para realizar consultas en la base de datos. 
    /// </summary>
    /// <remarks>
    /// <h3>Calidad</h3>
    /// </hr>
    /// <b>Requerimiento:</b> Help Desk</br>
    /// <b>Version:</b> 1</br>
    /// <b>Autor:</b> Grupo Cubo Ltda.</br>
    /// <b>Fecha:</b> 12/12/2017</br>
    /// <b>Descripcion:</b> Hereda varios metodos que permiten realizar consultas a la entidadad DatosUsuarios.
    /// </remarks>
    public class DatosUsuariosRepository : GenericRepository<DatosUsuarios, int>
    {
        /// <summary>
        /// Metodo constructor que hereda el contexto.
        /// </summary>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 12/12/2017</br>
        /// <b>Descripcion:</b> Metodo constructor que hereda los metodos que posee el contexto.
        /// </remarks>
        public DatosUsuariosRepository() : base(new BosquesPazContext()) 
        {
                
        }

        /// <summary>
        /// Obtener una instancia de la actividad segun el identificador.
        /// </summary>
        /// <param name="idDatoUsuario">Identificador de los datos de usuario</param>
        /// <returns>Retorna instancia de DatosUsuarios</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 12/12/2017</br>
        /// <b>Descripcion:</b> Obtiene una instancia de los datos de un usuario segun el numero de identificador que ingrese.
        /// </remarks>
        public override DatosUsuarios GetByKey(int idDatoUsuario)
        {
            DatosUsuarios result = null;
            try
            {
                using (var db = new BosquesPazContext())
                {
                    result = db.DatosUsuarios.Include("Municipios")
                                       .Include("Departamentos")
                                       .Where(f => f.Id_DatoUsuario == idDatoUsuario)
                                       .FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }
    }
}