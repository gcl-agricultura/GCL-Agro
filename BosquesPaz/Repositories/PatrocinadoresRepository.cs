﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BosquesPaz.Models;
using  BosquesPaz.Repositorios;

namespace BosquesPaz.Repositories
{
    /// <summary>
    /// Clase para realizar consultas en la base de datos. 
    /// </summary>
    /// <remarks>
    /// <h3>Calidad</h3>
    /// </hr>
    /// <b>Requerimiento:</b> Help Desk</br>
    /// <b>Version:</b> 1</br>
    /// <b>Autor:</b> Grupo Cubo Ltda.</br>
    /// <b>Fecha:</b> 12/12/2017</br>
    /// <b>Descripcion:</b> Hereda varios metodos que permiten realizar consultas a la entidadad PatrocinadoresBosques.
    /// </remarks>
    public class PatrocinadoresRepository : GenericRepository<PatrocinadoresBosques, int>
    {
        /// <summary>
        /// Metodo constructor que hereda el contexto.
        /// </summary>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 12/12/2017</br>
        /// <b>Descripcion:</b> Metodo constructor que hereda los metodos que posee el contexto.
        /// </remarks>
        public PatrocinadoresRepository() : base(new BosquesPazContext())
        {

        }

        /// <summary>
        /// Obtener una instancia del patrocinador segun el identificador.
        /// </summary>
        /// <param name="idPatrocinadorBosque">Identificador del patrocinador</param>
        /// <returns>Retorna instancia de PatrocinadoresBosques</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 12/12/2017</br>
        /// <b>Descripcion:</b> Obtiene una instancia del patrocinador segun el numero de identificador que ingrese.
        /// </remarks>
        public override PatrocinadoresBosques GetByKey(int idPatrocinadorBosque)
        {
            PatrocinadoresBosques result = null;
            try
            {
                using (var db = new BosquesPazContext())
                {
                    result = db.PatrocinadoresBosques.Include("TiposPatrocinadores")
                                               .Include("EstadosPatrocinadores")
                                               .Include("Municipios")
                                               .Include("Departamentos")
                                               .Where(r => r.Id_PatrocinadorBosque == idPatrocinadorBosque)
                                               .FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }
    }
}