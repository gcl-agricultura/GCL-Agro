﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BosquesPaz.Models;
using  BosquesPaz.Repositorios;

namespace BosquesPaz.Repositories
{
    /// <summary>
    /// Clase para realizar consultas en la base de datos. 
    /// </summary>
    /// <remarks>
    /// <h3>Calidad</h3>
    /// </hr>
    /// <b>Requerimiento:</b> Help Desk</br>
    /// <b>Version:</b> 1</br>
    /// <b>Autor:</b> Grupo Cubo Ltda.</br>
    /// <b>Fecha:</b> 12/12/2017</br>
    /// <b>Descripcion:</b> Hereda varios metodos que permiten realizar consultas a la entidadad ProductosServicios.
    /// </remarks>
    public class ProductosServiciosRepository : GenericRepository<ProductosServicios, int>
    {
        /// <summary>
        /// Metodo constructor que hereda el contexto.
        /// </summary>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 12/12/2017</br>
        /// <b>Descripcion:</b> Metodo constructor que hereda los metodos que posee el contexto.
        /// </remarks>
        public ProductosServiciosRepository() : base(new BosquesPazContext())
        {

        }

        /// <summary>
        /// Obtener una instancia del producto/servicio segun el identificador.
        /// </summary>
        /// <param name="IdProductoServicio">Identificador de producto/servicio</param>
        /// <returns>Retorna instancia de ProductosServicios</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 12/12/2017</br>
        /// <b>Descripcion:</b> Obtiene una instancia de producto/servicio segun el numero de identificador que ingrese.
        /// </remarks>
        public override ProductosServicios GetByKey(int IdProductoServicio)
        {
            ProductosServicios result = null;
            try
            {
                using (var db = new BosquesPazContext())
                {
                    result = db.ProductosServicios.Include("TiposProductosServicios")
                                                  .Include("EstadosProductosServicios")
                                                  .Where(i => i.Id_ProductoServicio == IdProductoServicio)
                                                  .FirstOrDefault();
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return result;
        }
    }
}