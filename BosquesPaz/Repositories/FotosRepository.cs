﻿using BosquesPaz.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BosquesPaz.Repositories
{
    /// <summary>
    /// Clase para realizar consultas en la base de datos. 
    /// </summary>
    /// <remarks>
    /// <h3>Calidad</h3>
    /// </hr>
    /// <b>Requerimiento:</b> Help Desk</br>
    /// <b>Version:</b> 1</br>
    /// <b>Autor:</b> Grupo Cubo Ltda.</br>
    /// <b>Fecha:</b> 12/12/2017</br>
    /// <b>Descripcion:</b> Realiza las consultas en la base de datos para varias entidades.
    /// </remarks>
    public class FotosRepository
    {
        /// <summary>
        /// Guardar foto un de Bosque en la base de datos.
        /// </summary>
        /// <param name="fotos">coleccion de FotosBosques</param>
        /// <returns>Retorna coleccion de FotosBosques</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 12/12/2017</br>
        /// <b>Descripcion:</b> Realiza la insercion de una foto de un Bosque y la retorna en una coleccion de FotosBosques.
        /// </remarks>
        public IEnumerable<FotosBosques> GuardarFotosBosque(IEnumerable<FotosBosques> fotos)
        {
            try
            {
                using (var db = new BosquesPazContext())
                {
                    db.FotosBosques.AddRange(fotos);
                    db.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return fotos;
        }

        /// <summary>
        /// Obtener coleccion de FotosBosques.
        /// </summary>
        /// <param name="id">Identificador de foto</param>
        /// <returns>Retorna coleccion de FotosBosques</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 12/12/2017</br>
        /// <b>Descripcion:</b> Realiza la consulta de una foto de un Bosque segun el identificador y la retorna en una coleccion de FotosBosques.
        /// </remarks>
        public IEnumerable<FotosBosques> ObtenerFotosBosquesPorId(int id)
        {
            IEnumerable<FotosBosques> foto = null;
            try
            {
                using (var db = new BosquesPazContext())
                {
                    foto = db.FotosBosques.Where(f => f.Id_FotoBosque == id).ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return foto;
        }

        /// <summary>
        /// Obtener coleccion de FotosBosques.
        /// </summary>
        /// <returns>Retorna coleccion de FotosBosques</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 12/12/2017</br>
        /// <b>Descripcion:</b> Realiza la consulta de todas las fotos de Bosques y las retorna en una coleccion de FotosBosques.
        /// </remarks>
        public IEnumerable<FotosBosques> ObtenerFotosBosques()
        {
            IEnumerable<FotosBosques> foto = null;
            try
            {
                using (var db = new BosquesPazContext())
                {
                    foto = db.FotosBosques.ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return foto;
        }

        /// <summary>
        /// Obtener coleccion de FotosBosques.
        /// </summary>
        /// <param name="idBosque">Identificador de Bosque</param>
        /// <returns>Retorna coleccion de FotosBosques</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 12/12/2017</br>
        /// <b>Descripcion:</b> Realiza la consulta de todas las fotos de un Bosque segun el identificador y las retorna en una coleccion de FotosBosques.
        /// </remarks>
        public IEnumerable<FotosBosques> ObtenerFotosBosque(int idBosque)
        {
            IEnumerable<FotosBosques> result = null;
            try
            {
                using (var db = new BosquesPazContext())
                {
                    result = db.FotosBosques.Where(f => f.Id_Bosque == idBosque).ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }

        /// <summary>
        /// Guardar foto de un Individuo/Especie en la base de datos.
        /// </summary>
        /// <param name="fotos">coleccion de FotosIndividuosEspecies</param>
        /// <returns>Retorna coleccion de FotosIndividuosEspecies</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 12/12/2017</br>
        /// <b>Descripcion:</b> Realiza la insercion de una foto de un Individuo/Especie y la retorna en una coleccion de FotosBosques.
        /// </remarks>
        public IEnumerable<FotosIndividuosEspecies> GuardarFotosIndividuosEspecies(IEnumerable<FotosIndividuosEspecies> fotos)
        {
            try
            {
                using (var db = new BosquesPazContext())
                {
                    db.FotosIndividuosEspecies.AddRange(fotos);
                    db.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return fotos;
        }

        /// <summary>
        /// Obtener coleccion de FotosIndividuosEspecies.
        /// </summary>
        /// <param name="id">Identificador de FotosIndividuosEspecies</param>
        /// <returns>Retorna coleccion de FotosIndividuosEspecies</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 12/12/2017</br>
        /// <b>Descripcion:</b> Realiza la consulta de una foto de un Individuo/Especie segun el identificador y las retorna en una coleccion de FotosIndividuosEspecies.
        /// </remarks>
        public IEnumerable<FotosIndividuosEspecies> ObtenerFotoIndividuoEspeciePorId(int id)
        {
            IEnumerable<FotosIndividuosEspecies> foto = null;
            try
            {
                using (var db = new BosquesPazContext())
                {
                    foto = db.FotosIndividuosEspecies.Where(f => f.Id_FotoIndividuoEspecie == id).ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return foto;
        }

        /// <summary>
        /// Obtener coleccion de FotosIndividuosEspecies.
        /// </summary>
        /// <returns>Retorna coleccion de FotosIndividuosEspecies</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 12/12/2017</br>
        /// <b>Descripcion:</b> Realiza la consulta de todas las fotos de Individuo/Especie y las retorna en una coleccion de FotosIndividuosEspecies.
        /// </remarks>
        public IEnumerable<FotosIndividuosEspecies> ObtenerFotosIndividuosEspecies()
        {
            IEnumerable<FotosIndividuosEspecies> foto = null;
            try
            {
                using (var db = new BosquesPazContext())
                {
                    foto = db.FotosIndividuosEspecies.ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return foto;
        }

        /// <summary>
        /// Obtener coleccion de FotosIndividuosEspecies.
        /// </summary>
        /// <param name="idIndividuoEspecie">Identificador de Individuo/Especie</param>
        /// <returns>Retorna coleccion de FotosIndividuosEspecies</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 12/12/2017</br>
        /// <b>Descripcion:</b> Realiza la consulta de todas las fotos de Individuo/Especie segun el Identificador y las retorna en una coleccion de FotosIndividuosEspecies.
        /// </remarks>
        public IEnumerable<FotosIndividuosEspecies> ObtenerFotosIndividuosEspecies(int idIndividuoEspecie)
        {
            IEnumerable<FotosIndividuosEspecies> result = null;
            try
            {
                using (var db = new BosquesPazContext())
                {
                    result = db.FotosIndividuosEspecies.Where(f => f.Id_IndividuoEspecie == idIndividuoEspecie).ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }

        /// <summary>
        /// Guardar foto de un Productos/Servicios en la base de datos.
        /// </summary>
        /// <param name="fotos">coleccion de FotosProductosServicios</param>
        /// <returns>Retorna coleccion de FotosProductosServicios</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 12/12/2017</br>
        /// <b>Descripcion:</b> Realiza la insercion de una foto de un Productos/Servicios y la retorna en una coleccion de FotosProductosServicios.
        /// </remarks>
        public IEnumerable<FotosProductosServicios> GuardarFotosProductosServicios(IEnumerable<FotosProductosServicios> fotos)
        {
            try
            {
                using (var db = new BosquesPazContext())
                {
                    db.FotosProductosServicios.AddRange(fotos);
                    db.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return fotos;
        }

        /// <summary>
        /// Obtener coleccion de FotosProductosServicios.
        /// </summary>
        /// <returns>Retorna coleccion de FotosProductosServicios</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 12/12/2017</br>
        /// <b>Descripcion:</b> Realiza la consulta de todas las fotos de Productos/Servicios y las retorna en una coleccion de FotosProductosServicios.
        /// </remarks>
        public IEnumerable<FotosProductosServicios> ObtenerFotosProductosServicios()
        {
            IEnumerable<FotosProductosServicios> foto = null;
            try
            {
                using (var db = new BosquesPazContext())
                {
                    foto = db.FotosProductosServicios.ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return foto;
        }

        /// <summary>
        /// Obtener coleccion de FotosProductosServicios.
        /// </summary>
        /// <param name="id">Identificador de FotosProductosServicios</param>
        /// <returns>Retorna coleccion de FotosProductosServicios</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 12/12/2017</br>
        /// <b>Descripcion:</b> Realiza la consulta de todas las fotos de Productos/Servicios segun el identificador y las retorna en una coleccion de FotosProductosServicios.
        /// </remarks>
        public IEnumerable<FotosProductosServicios> ObtenerFotoProductosServiciosPorId(int id)
        {
            IEnumerable<FotosProductosServicios> foto = null;
            try
            {
                using (var db = new BosquesPazContext())
                {
                    foto = db.FotosProductosServicios.Where(f => f.Id_FotoProductoServicio == id).ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return foto;
        }

        /// <summary>
        /// Obtener coleccion de FotosProductosServicios.
        /// </summary>
        /// <param name="idProductosServicios">Identificador de ProductosServicios</param>
        /// <returns>Retorna coleccion de FotosProductosServicios</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 12/12/2017</br>
        /// <b>Descripcion:</b> Realiza la consulta de todas las fotos de Productos/Servicios segun el identificador y las retorna en una coleccion de FotosProductosServicios.
        /// </remarks>
        public IEnumerable<FotosProductosServicios> ObtenerFotosProductosServicios(int idProductosServicios)
        {
            IEnumerable<FotosProductosServicios> result = null;
            try
            {
                using (var db = new BosquesPazContext())
                {
                    result = db.FotosProductosServicios.Where(f => f.Id_ProductoServicio == idProductosServicios).ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }

        /// <summary>
        /// Obtener coleccion de FotosUsuarios.
        /// </summary>
        /// <returns>Retorna coleccion de FotosUsuarios</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 12/12/2017</br>
        /// <b>Descripcion:</b> Realiza la consulta de todas las fotos de Fotos usuarios y las retorna en una coleccion de FotosUsuarios.
        /// </remarks>
        public IEnumerable<FotosUsuarios> ObtenerFotosUsuarios()
        {
            IEnumerable<FotosUsuarios> result = null;
            try
            {
                using (var db = new BosquesPazContext())
                {
                    result = db.FotosUsuarios.ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }

        /// <summary>
        /// Obtener coleccion de FotosProductosServicios.
        /// </summary>
        /// <param name="idFotoUsuario">Identificador de FotosUsuarios</param>
        /// <returns>Retorna coleccion de FotosUsuarios</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 12/12/2017</br>
        /// <b>Descripcion:</b> Realiza la consulta de todas las fotos de Fotos usuarios segun el identificador y las retorna en una coleccion de FotosUsuarios.
        /// </remarks>
        public IEnumerable<FotosUsuarios> ObtenerFotosUsuariosPorId(int idFotoUsuario)
        {
            IEnumerable<FotosUsuarios> result = null;
            try
            {
                using (var db = new BosquesPazContext())
                {
                    result = db.FotosUsuarios.Where(f => f.Id_FotoUsuario == idFotoUsuario).ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }
    }
}