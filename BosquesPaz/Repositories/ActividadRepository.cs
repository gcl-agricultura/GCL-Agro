﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BosquesPaz.Models;
using  BosquesPaz.Repositorios;

namespace BosquesPaz.Repositories
{
    /// <summary>
    /// Clase para realizar consultas en la base de datos. 
    /// </summary>
    /// <remarks>
    /// <h3>Calidad</h3>
    /// </hr>
    /// <b>Requerimiento:</b> Help Desk</br>
    /// <b>Version:</b> 1</br>
    /// <b>Autor: </b> Grupo Cubo Ltda.</br>
    /// <b>Fecha:</b> 12/12/2017</br>
    /// <b>Descripcion:</b>Hereda varios metodos que permiten realizar consultas a la entidadad ActividadesBosques.
    /// </remarks>
    public class ActividadRepository : GenericRepository<ActividadesBosques, int>
    {
        /// <summary>
        /// Metodo constructor que hereda el contexto.
        /// </summary>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version: </b>1</br>
        /// <b>Autor: </b>Grupo Cubo Ltda.</br>
        /// <b>Fecha: </b>12/12/2017</br>
        /// <b>Descripcion:</b> Metodo constructor que hereda los metodos que posee el contexto.
        /// </remarks>
        public ActividadRepository() : base(new BosquesPazContext())
        {

        }

        /// <summary>
        /// Obtener una instancia de la actividad segun el identificador.
        /// </summary>
        /// <param name="idActividadBosque">Indentificador de la actividad</param>
        /// <returns>Retorna instancia de ActividadesBosques</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento: </b>Help Desk</br>
        /// <b>Version: </b>1</br>
        /// <b>Autor: </b>Grupo Cubo Ltda.</br>
        /// <b>Fecha: </b>12/12/2017</br>
        /// <b>Descripcion: </b>Obtiene una instancia de la actividad segun el numero de identificador que ingrese.
        /// </remarks>
        public override ActividadesBosques GetByKey(int idActividadBosque)
        {
            ActividadesBosques result = null;
            try
            {
                using (var db = new BosquesPazContext())
                {
                    result = db.ActividadesBosques.Include("TiposActividades")
                                                  .Include("EstadosActividades")
                                                  .Where(a => a.Id_ActividadBosque == idActividadBosque)
                                                  .FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                
                throw ex;
            }
            return result;
        }
    }
}