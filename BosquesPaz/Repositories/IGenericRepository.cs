﻿using BosquesPaz.Repositories.Especificaciones;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace BosquesPaz.Repositorios
{
    public interface IGenericRepository<TEntity, Tkey> where TEntity : class, new()
    {
        int Count(Expression<Func<TEntity, bool>> _predicate);

        Task<int> CountAsync(Expression<Func<TEntity, bool>> _predicate);

        TEntity Create(TEntity _entity);

        Task<TEntity> CreateAsync(TEntity _entity);

        bool Delete(TEntity _entity);

        Task<bool> DeleteAsync(TEntity _entity);

        IEnumerable<TEntity> GetAll();

        Task<IEnumerable<TEntity>> GetAllAsync();

        IQueryable<TEntity> GetBy(System.Linq.Expressions.Expression<Func<TEntity, bool>> _predicate);

        IQueryable<TEntity> GetByAspect(ISpecification<TEntity> specification);

        Task<IQueryable<TEntity>> GetByAspectAsync(ISpecification<TEntity> specification);

        Task<IQueryable<TEntity>> GetByAsync(Expression<Func<TEntity, bool>> _predicate);

        TEntity GetByKey(Tkey _key);

        Task<TEntity> GetByKeyAsync(Tkey _key);

        IQueryable<TEntity> GetPagedElements<S>(int pageIndex, int pageCount, Expression<Func<TEntity, S>> orderByExpression, ISpecification<TEntity> specification, bool ascending);

        Task<IQueryable<TEntity>> GetPagedElementsAsync<S>(int pageIndex, int pageCount, Expression<Func<TEntity, S>> orderByExpression, Expression<Func<TEntity, bool>> predicate, bool ascending);

        bool Update(TEntity _entity);

        Task<bool> UpdateAsync(TEntity _entity);
    }
}