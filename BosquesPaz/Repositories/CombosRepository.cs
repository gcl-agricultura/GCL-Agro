﻿using BosquesPaz.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BosquesPaz.Repositories
{
    /// <summary>
    /// Clase para realizar consultas en la base de datos. 
    /// </summary>
    /// <remarks>
    /// <h3>Calidad</h3>
    /// </hr>
    /// <b>Requerimiento: </b>Help Desk</br>
    /// <b>Version:</b> 1</br>
    /// <b>Autor: </b>Grupo Cubo Ltda.</br>
    /// <b>Fecha: </b>12/12/2017</br>
    /// <b>Descripcion:</b> Realiza las consultas en la base de datos para varias entidades.
    /// </remarks>
    public class CombosRepository
    {
        /// <summary>
        /// Obtener coleccion de Departamentos.
        /// </summary>
        /// <returns>Retorna coleccion de Departamentos</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version: </b>1</br>
        /// <b>Autor: </b>Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 12/12/2017</br>
        /// <b>Descripcion:</b> Realiza la consulta de todos los departamentos y los retorna en una coleccion de Departamentos.
        /// </remarks>
        public IEnumerable<Departamentos> ObtenerDepartamentos()
        {
            IEnumerable<Departamentos> departamentos = new List<Departamentos>();
            try
            {
                using (var db = new BosquesPazContext())
                {
                    var query = db.Departamentos.ToList();
                    if (query.Count > 0)
                        departamentos = query;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return departamentos;
        }

        /// <summary>
        /// Obtener coleccion de Municipio. 
        /// </summary>
        /// <param name="idDepartamento">Identificador de Municipio</param>
        /// <returns>Retorna coleccion de Municipios</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 12/12/2017</br>
        /// <b>Descripcion:</b> Realiza la consulta de un Municipio segun el identificador y la retorna en una coleccion de Municipio.
        /// </remarks>
        public IEnumerable<Municipios> ObtenerMunicipios(int idDepartamento)
        {
            IEnumerable<Municipios> Municipios = new List<Municipios>();
            try
            {
                using (var db = new BosquesPazContext())
                {
                    var query = db.Municipios.Where(M => M.Id_Departamento == idDepartamento).ToList();
                    if (query.Count > 0)
                        Municipios = query;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return Municipios;
        }

        /// <summary>
        /// Obtener coleccion de TiposRecursos. 
        /// </summary>
        /// <returns>Retorna coleccion de TiposRecursos</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 12/12/2017</br>
        /// <b>Descripcion:</b> Realiza la consulta de todos los tipos recursos y los retorna en una coleccion de TiposRecursos.
        /// </remarks>
        public IEnumerable<TiposRecursos> ObtenerTiposRecursos()
        {
            IEnumerable<TiposRecursos> TiposRecursos = new List<TiposRecursos>();
            try
            {
                using (var db = new BosquesPazContext())
                {
                    var query = db.TiposRecursos.ToList();
                    if (query.Count > 0)
                    {
                        TiposRecursos = query;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return TiposRecursos;
        }

        /// <summary>
        /// Obtener coleccion de Estados
        /// </summary>
        /// <returns>Retorna coleccion de Estados</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 12/12/2017</br>
        /// <b>Descripcion:</b> Realiza la consulta de todos los estados de recursos y los retorna en una coleccion de Estados.
        /// </remarks>
        public IEnumerable<Estados> ObtenerEstados()
        {
            IEnumerable<Estados> Estados = new List<Estados>();
            try
            {
                using (var db = new BosquesPazContext())
                {
                    var query = db.Estados.ToList();
                    if (query.Count > 0)
                    {
                        Estados = query;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return Estados;
        }

        /// <summary>
        /// Obtener coleccion de TiposActividades. 
        /// </summary>
        /// <returns>Retorna coleccion de TiposActividades</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 12/12/2017</br>
        /// <b>Descripcion:</b> Realiza la consulta de todos los tipos de actividades y los retorna en una coleccion de TiposActividades.
        /// </remarks>
        public IEnumerable<TiposActividades> ObtenerTiposActividades()
        {
            IEnumerable<TiposActividades> TiposActividades = new List<TiposActividades>();
            try
            {
                using (var db = new BosquesPazContext())
                {
                    var query = db.TiposActividades.ToList();
                    if (query.Count > 0)
                    {
                        TiposActividades = query;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return TiposActividades;
        }

        /// <summary>
        /// Obtener coleccion de EstadosActividades 
        /// </summary>
        /// <returns>Retorna coleccion de EstadosActividades</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 12/12/2017</br>
        /// <b>Descripcion:</b> Realiza la consulta de todos los estados actividades y los retorna en una coleccion de EstadosActividades.
        /// </remarks>
        public IEnumerable<EstadosActividades> ObtenerEstadosActividades()
        {
            IEnumerable<EstadosActividades> EstadosActividades = new List<EstadosActividades>();
            try
            {
                using (var db = new BosquesPazContext())
                {
                    var query = db.EstadosActividades.ToList();
                    if (query.Count > 0)
                    {
                        EstadosActividades = query;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return EstadosActividades;
        }

        /// <summary>
        /// Obtener coleccion de TiposIndividuosEspecies. 
        /// </summary>
        /// <returns>Retorna coleccion de TiposIndividuosEspecies</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor: </b>Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 12/12/2017</br>
        /// <b>Descripcion:</b> Realiza la consulta de todos los tipos individuos y los retorna en una coleccion de TiposIndividuosEspecies.
        /// </remarks>
        public IEnumerable<TiposIndividuosEspecies> ObtenerTiposIndividuosEspecies()
        {
            IEnumerable<TiposIndividuosEspecies> TiposIndividuosEspecies = new List<TiposIndividuosEspecies>();
            try
            {
                using (var db = new BosquesPazContext())
                {
                    var query = db.TiposIndividuosEspecies.ToList();
                    if (query.Count > 0)
                    {
                        TiposIndividuosEspecies = query;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return TiposIndividuosEspecies;
        }

        /// <summary>
        /// Obtener coleccion de UnidadesIndividuosEspecies. 
        /// </summary>
        /// <returns>Retorna coleccion de UnidadesIndividuosEspecies</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor: </b>Grupo Cubo Ltda.</br>
        /// <b>Fecha: </b>12/12/2017</br>
        /// <b>Descripcion:</b> Realiza la consulta de todas las unidades de individuos especies y los retorna en una coleccion de UnidadesIndividuosEspecies.
        /// </remarks>
        public IEnumerable<UnidadesIndividuosEspecies> ObtenerUnidadesIndividuosEspecies()
        {
            IEnumerable<UnidadesIndividuosEspecies> UnidadesIndividuosEspecies = new List<UnidadesIndividuosEspecies>();
            try
            {
                using (var db = new BosquesPazContext())
                {
                    var query = db.UnidadesIndividuosEspecies.ToList();
                    if (query.Count > 0)
                    {
                        UnidadesIndividuosEspecies = query;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return UnidadesIndividuosEspecies;
        }

        /// <summary>
        /// Obtener coleccion de TiposIndicadores. 
        /// </summary>
        /// <returns>Retorna coleccion de TiposIndicadores</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor: </b>Grupo Cubo Ltda.</br>
        /// <b>Fecha: </b>12/12/2017</br>
        /// <b>Descripcion:</b> Realiza la consulta de todos los tipos indicadores y los retorna en una coleccion de TiposIndicadores.
        /// </remarks>
        public IEnumerable<TiposIndicadores> ObtenerTiposIndicadores()
        {
            IEnumerable<TiposIndicadores> TiposIndicadores = new List<TiposIndicadores>();
            try
            {
                using (var db = new BosquesPazContext())
                {
                    var query = db.TiposIndicadores.ToList();
                    if (query.Count > 0)
                    {
                        TiposIndicadores = query;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return TiposIndicadores;
        }

        /// <summary>
        /// Obtener coleccion de EstadosIndicadores. 
        /// </summary>
        /// <returns>Retorna coleccion de EstadosIndicadores</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor: </b>Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 12/12/2017</br>
        /// <b>Descripcion:</b> Realiza la consulta de todos los estados indicadores y los retorna en una coleccion de EstadosIndicadores.
        /// </remarks>
        public IEnumerable<EstadosIndicadores> ObtenerEstadosIndicadores()
        {
            IEnumerable<EstadosIndicadores> EstadosIndicadores = new List<EstadosIndicadores>();
            try
            {
                using (var db = new BosquesPazContext())
                {
                    var query = db.EstadosIndicadores.ToList();
                    if (query.Count > 0)
                    {
                        EstadosIndicadores = query;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return EstadosIndicadores;
        }

        /// <summary>
        /// Obtener coleccion de TiposProductosServicios. 
        /// </summary>
        /// <returns>Retorna coleccion de TiposProductosServicios</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor: </b>Grupo Cubo Ltda.</br>
        /// <b>Fecha: </b>12/12/2017</br>
        /// <b>Descripcion:</b> Realiza la consulta de todos los tipos de productos y servicios y los retorna en una coleccion de TiposProductosServicios.
        /// </remarks>
        public IEnumerable<TiposProductosServicios> ObtenerTiposProductosServicios()
        {
            IEnumerable<TiposProductosServicios> TiposProductosServicios = new List<TiposProductosServicios>();
            try
            {
                using (var db = new BosquesPazContext())
                {
                    var query = db.TiposProductosServicios.ToList();
                    if (query.Count > 0)
                    {
                        TiposProductosServicios = query;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return TiposProductosServicios;
        }

        /// <summary>
        /// Obtener coleccion de EstadosProductosServicios. 
        /// </summary>
        /// <returns>Retorna coleccion de EstadosProductosServicios</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 12/12/2017</br>
        /// <b>Descripcion:</b> Realiza la consulta de todos los estados de productos y servicios y los retorna en una coleccion de EstadosProductosServicios.
        /// </remarks>
        public IEnumerable<EstadosProductosServicios> ObtenerEstadosProductosServicios()
        {
            IEnumerable<EstadosProductosServicios> EstadosProductosServicios = new List<EstadosProductosServicios>();
            try
            {
                using (var db = new BosquesPazContext())
                {
                    var query = db.EstadosProductosServicios.ToList();
                    if (query.Count > 0)
                    {
                        EstadosProductosServicios = query;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return EstadosProductosServicios;
        }

        /// <summary>
        /// Obtener coleccion de EstadosFichas. 
        /// </summary>
        /// <returns>Retorna coleccion de EstadosFichas</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha: </b>12/12/2017</br>
        /// <b>Descripcion:</b> Realiza la consulta de todos los estados de la ficha y los retorna en una coleccion de EstadosFichas.
        /// </remarks>
        public IEnumerable<EstadosFichas> ObtenerEstadosFichas()
        {
            IEnumerable<EstadosFichas> EstadosFichas = new List<EstadosFichas>();
            try
            {
                using (var db = new BosquesPazContext())
                {
                    var query = db.EstadosFichas.ToList();
                    if (query.Count > 0)
                    {
                        EstadosFichas = query;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return EstadosFichas;
        }

        /// <summary>
        /// Obtener coleccion de Tematicas. 
        /// </summary>
        /// <returns>Retorna coleccion de Tematicas</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento: </b>Help Desk</br>
        /// <b>Version: </b>1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 12/12/2017</br>
        /// <b>Descripcion:</b> Realiza la consulta de todas las tematicas de historias y los retorna en una coleccion de Tematicas.
        /// </remarks>
        public IEnumerable<Tematicas> ObtenerTematicas()
        {
            IEnumerable<Tematicas> Tematicas = new List<Tematicas>();
            try
            {
                using (var db = new BosquesPazContext())
                {
                    var query = db.Tematicas.ToList();
                    if (query.Count > 0)
                    {
                        Tematicas = query;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return Tematicas;
        }

        /// <summary>
        /// Obtener coleccion de Estados Historias. 
        /// </summary>
        /// <returns>Retorna coleccion de EstadosHistorias</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 12/12/2017</br>
        /// <b>Descripcion:</b> Realiza la consulta de todas los estados historias de historias y los retorna en una coleccion de EstadosHistorias.
        /// </remarks>
        public IEnumerable<EstadosHistorias> ObtenerEstadosHistorias()
        {
            IEnumerable<EstadosHistorias> EstadosHistorias = new List<EstadosHistorias>();
            try
            {
                using (var db = new BosquesPazContext())
                {
                    var query = db.EstadosHistorias.ToList();
                    if (query.Count > 0)
                    {
                        EstadosHistorias = query;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return EstadosHistorias;
        }

        /// <summary>
        /// Obtener coleccion de TiposPatrocinadores. 
        /// </summary>
        /// <returns>Retorna coleccion de TiposPatrocinadores</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 12/12/2017</br>
        /// <b>Descripcion:</b> Realiza la consulta de todos los Tipos de patrocinadores y los retorna en una coleccion de TiposPatrocinadores.
        /// </remarks>
        public IEnumerable<TiposPatrocinadores> ObtenerTiposPatrocinadores()
        {
            IEnumerable<TiposPatrocinadores> TiposPatrocinadores = new List<TiposPatrocinadores>();
            try
            {
                using (var db = new BosquesPazContext())
                {
                    var query = db.TiposPatrocinadores.ToList();
                    if (query.Count > 0)
                    {
                        TiposPatrocinadores = query;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return TiposPatrocinadores;
        }

        /// <summary>
        /// Obtener coleccion de EstadosPatrocinadores. 
        /// </summary>
        /// <returns>Retorna coleccion de EstadosPatrocinadores</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 12/12/2017</br>
        /// <b>Descripcion:</b> Realiza la consulta de todos los estados de patrocinadores y los retorna en una coleccion de EstadosPatrocinadores.
        /// </remarks>
        public IEnumerable<EstadosPatrocinadores> ObtenerEstadosPatrocinadores()
        {
            IEnumerable<EstadosPatrocinadores> EstadosPatrocinadores = new List<EstadosPatrocinadores>();
            try
            {
                using (var db = new BosquesPazContext())
                {
                    var query = db.EstadosPatrocinadores.ToList();
                    if (query.Count > 0)
                    {
                        EstadosPatrocinadores = query;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return EstadosPatrocinadores;
        }

        /// <summary>
        /// Obtener coleccion de TiposPatrocinadores para compras alianzas. 
        /// </summary>
        /// <returns>Retorna coleccion de TiposPatrocinadores</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 29/12/2017</br>
        /// <b>Descripcion:</b> Realiza la consulta de los tipos de patrocinadores para compras alianzas y los retorna en una coleccion de TiposPatrocinadores.
        /// </remarks>
        public IEnumerable<TiposPatrocinadores> ObtnerPatrocinadoresComprasAlianzas()
        {
            IEnumerable<TiposPatrocinadores> patrocinadoresComprasAlianzas = new List<TiposPatrocinadores>();
            try
            {
                using (var db = new BosquesPazContext())
                {
                    patrocinadoresComprasAlianzas = db.TiposPatrocinadores.Where(p=> p.Id_TipoPatrocinador == 3 || p.Id_TipoPatrocinador == 4).ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return patrocinadoresComprasAlianzas;
        }

        /// <summary>
        /// Obtener coleccion de Bosques. 
        /// </summary>
        /// <returns>Retorna coleccion de Bosques</returns>
        /// <remarks>
        /// <h3>Calidad</h3>
        /// </hr>
        /// <b>Requerimiento:</b> Help Desk</br>
        /// <b>Version:</b> 1</br>
        /// <b>Autor:</b> Grupo Cubo Ltda.</br>
        /// <b>Fecha:</b> 12/12/2017</br>
        /// <b>Descripcion:</b> Realiza la consulta de todos los bosques y los retorna en una coleccion de Bosques.
        /// </remarks>
        public IEnumerable<Bosques> ObtenerBosques()
        {
            IEnumerable<Bosques> Bosques = new List<Bosques>();
            try
            {
                using (var db = new BosquesPazContext())
                {
                    var query = db.Bosques.ToList();
                    if (query.Count > 0)
                    {
                        Bosques = query;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return Bosques;
        }
    }
}