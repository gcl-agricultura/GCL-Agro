﻿using System.Web;
using System.Web.Optimization;

namespace BosquesPaz
{
    public class BundleConfig
    {
        // Para obtener más información sobre Bundles, visite http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryui").Include(
            "~/Scripts/jquery-ui-{version}.js"));
            // Utilice la versión de desarrollo de Modernizr para desarrollar y obtener información. De este modo, estará
            // preparado para la producción y podrá utilizar la herramienta de compilación disponible en http://modernizr.com para seleccionar solo las pruebas que necesite.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/site.css",
                      "~/Content/w3.css"
                      ));
            bundles.Add(new StyleBundle("~/Content/Chosen").Include(
                      "~/Content/Chosen/chosen.css"
                      ));
            bundles.Add(new ScriptBundle("~/bundles/Chosen").Include(
                      "~/Scripts/Chosen/chosen.jquery.js",
                      "~/Scripts/Chosen/chosen.proto.js"));
            bundles.Add(new ScriptBundle("~/bundles/Leaflet").Include(
                      "~/Scripts/Leaflet/leaflet-providers.js"));
            bundles.Add(new ScriptBundle("~/bundles/AnexGrid").Include(
                      "~/Scripts/AnexGrid/jquery.anexgrid.min.js"));
            bundles.Add(new ScriptBundle("~/bundles/jqueryLoad").Include(
                "~/Scripts/jquery.validate.min.js",
                        "~/Scripts/jquery.validate.unobtrusive.min.js",
                        "~/Scripts/jquery.unobtrusive-ajax.min.js"));
            bundles.Add(new ScriptBundle("~/bundles/bootstrapDatepicker").Include(
                      "~/Scripts/bootstrap-datepicker.min.js"));
            bundles.Add(new ScriptBundle("~/bundles/recursos").Include(
                      "~/Scripts/Bosques/recursos.js"));
                      

        }
    }
}
