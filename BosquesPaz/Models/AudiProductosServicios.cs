//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace BosquesPaz.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class AudiProductosServicios
    {
        public int Id_ProductoServicioAuditoria { get; set; }
        public string NombreAuditoria { get; set; }
        public string DescripcionAuditoria { get; set; }
        public int PrecioAuditoria { get; set; }
        public string SentenciaAuditoria { get; set; }
        public System.DateTime FechaAuditoria { get; set; }
        public int Id_ProductoServicio { get; set; }
        public int Id_TipoProductoServicio { get; set; }
        public int Id_EstadoProductoServicio { get; set; }
        public string Aud_User { get; set; }
        public string Aud_Origen { get; set; }
    
        public virtual ProductosServicios ProductosServicios { get; set; }
    }
}
