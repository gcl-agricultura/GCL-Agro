//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace BosquesPaz.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class AudiRecursosBosques
    {
        public int Id_RecursoBosqueAuditoria { get; set; }
        public string NombreRecursoAuditoria { get; set; }
        public decimal PresupuestoAsignadoAuditoria { get; set; }
        public decimal PresupuestoEjecutadoAuditoria { get; set; }
        public System.DateTime FechaActualizacionAuditoria { get; set; }
        public string SentenciaAuditoria { get; set; }
        public int Id_RecursoBosque { get; set; }
        public int Id_TipoRecurso { get; set; }
        public int Id_Estado { get; set; }
        public System.DateTime FechaAuditoria { get; set; }
        public string Aud_User { get; set; }
        public string Aud_Origen { get; set; }
    
        public virtual RecursosBosques RecursosBosques { get; set; }
    }
}
