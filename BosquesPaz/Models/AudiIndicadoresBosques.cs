//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace BosquesPaz.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class AudiIndicadoresBosques
    {
        public int Id_IndicadorBosqueAuditoria { get; set; }
        public string NombreIndicadorAuditoria { get; set; }
        public int ValorMetaIndicadorAuditoria { get; set; }
        public string UnidadAuditoria { get; set; }
        public int ValorSeguimientoAuditoria { get; set; }
        public string ObservacionAuditoria { get; set; }
        public System.DateTime FechaSeguimiento { get; set; }
        public string SentenciaAuditoria { get; set; }
        public System.DateTime FechaAuditoria { get; set; }
        public int Id_IndicadorBosque { get; set; }
        public int Id_TipoIndicador { get; set; }
        public int Id_EstadoIndicador { get; set; }
        public string Aud_User { get; set; }
        public string Aud_Origen { get; set; }
    
        public virtual IndicadoresBosques IndicadoresBosques { get; set; }
    }
}
