﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BosquesPaz.Services;
using System.ComponentModel;

namespace BosquesPaz.Models
{
    public partial class AudiIndicadoresBosques
    {
        private TiposIndicadoresService ServicioTipoIndicador { get { return new TiposIndicadoresService(); } }
        private EstadosIndicadoresService ServicioEstadoIndicador { get { return new EstadosIndicadoresService(); } }

        [DisplayName("TipoIndicador")]
        public string TipoIndicador { get { return ServicioTipoIndicador.ObtenerNombreTipoIndicadorPorId(Id_TipoIndicador); } }

        [DisplayName("EstadoIndicador")]
        public string EstadoIndicador { get { return ServicioEstadoIndicador.ObtenerNombreEstadoIndicadorPorId(Id_EstadoIndicador); } }

        public string FechaSeguimientoStr { get { return FechaSeguimiento.ToShortDateString(); } }
        public string FechaAuditoriaStr { get { return FechaAuditoria.ToShortDateString() + "-" + FechaAuditoria.ToLongTimeString(); } }
        
    }
}