﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BosquesPaz.Services;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace BosquesPaz.Models
{
    [MetadataType(typeof(IndicadoresBosques.Metadata))]

    public partial class IndicadoresBosques
    {
        private TiposIndicadoresService ServicioTipoIndicador { get { return new TiposIndicadoresService(); } }
        private EstadosIndicadoresService ServicioEstadoIndicador { get { return new EstadosIndicadoresService(); } }

        [DisplayName("TipoIndicador")]
        public string TipoIndicador { get { return ServicioTipoIndicador.ObtenerNombreTipoIndicadorPorId(Id_TipoIndicador); } }

        [DisplayName("EstadoIndicador")]
        public string EstadoIndicador { get { return ServicioEstadoIndicador.ObtenerNombreEstadoIndicadorPorId(Id_EstadoIndicador); } }

        [DisplayName("Estado")]
        public string FechaSeguimientoStr { get { return FechaSeguimiento.ToShortDateString(); } }

        sealed class Metadata
        {
            [RegularExpression("^[A-Z a-zñÑáéíóúÁÉÍÓÚ]*$", ErrorMessage = "Por favor sólo ingrese letras.")]
            [Required(ErrorMessage = "El campo nombre es obligatorio.")]
            public string NombreIndicador { get; set; }

            [Required(ErrorMessage = "El campo valor meta es obligatorio.")]
            public int ValorMeta { get; set; }

            [RegularExpression("^[A-Z a-zñÑáéíóúÁÉÍÓÚ]*$", ErrorMessage = "Por favor sólo ingrese letras.")]
            [Required(ErrorMessage = "El campo unidad es obligatorio.")]
            public string Unidad { get; set; }

            [Required(ErrorMessage = "El campo valor seguimiento es obligatorio.")]
            public int ValorSeguimiento { get; set; }

            [RegularExpression("^[A-Z a-zñÑáéíóúÁÉÍÓÚ]*$", ErrorMessage = "Por favor sólo ingrese letras.")]
            [Required(ErrorMessage = "El campo observaciones es obligatorio.")]
            public string Observacion { get; set; }

            [Required(ErrorMessage = "El campo fecha seguimiento es obligatorio.")]
            public System.DateTime FechaSeguimiento { get; set; }

            [Required(ErrorMessage = "El campo tipo es obligatorio.")]
            public int Id_TipoIndicador { get; set; }

            [Required(ErrorMessage = "El campo estado es obligatorio.")]
            public int Id_EstadoIndicador { get; set; }
        }
    }
}