﻿using BosquesPaz.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace BosquesPaz.Models
{
    [MetadataType(typeof(Bosques.Metadata))]

    public partial class Bosques
    {
        private DepartamentosService ServicioDepartamento { get { return new DepartamentosService(); } }
        private MunicipiosService ServicioMunicipio { get { return new MunicipiosService(); } }
        [DisplayName("Departamento")]
        public string Departamento { get { return ServicioDepartamento.ObtenerNombreDepartamentoPorId(Id_Departamento); } }

        private DepartamentosService service { get { return new DepartamentosService(); } }
        [DisplayName("Municipio")]
        public string Municipio { get { return ServicioMunicipio.ObtenerNombreMunicipioPorId(Id_Municipio); } }

        sealed class Metadata
        {
            [Required(ErrorMessage = "El campo departamento es obligatorio.")]
            public int Id_Departamento { get; set; }

            [Required(ErrorMessage = "El campo municipio es obligatorio.")]
            public int Id_Municipio { get; set; }

            [RegularExpression("^[A-Z a-zñÑáéíóúÁÉÍÓÚ]*$", ErrorMessage = "Por favor sólo ingrese letras.")]
            [Required(ErrorMessage = "El campo nombre es obligatorio.")]
            public string NombreBosque { get; set; }

            [Required(ErrorMessage = "El campo latitud es obligatorio.")]
            public string Latitud { get; set; }

            [Required(ErrorMessage = "El campo longitud es obligatorio.")]
            public string Longitud { get; set; }

            [RegularExpression("^[A-Z a-zñÑáéíóúÁÉÍÓÚ]*$", ErrorMessage = "Por favor sólo ingrese letras.")]
            [Required(ErrorMessage = "El campo comunidades es obligatorio.")]
            public string Comunidades { get; set; }
        }
    }
}