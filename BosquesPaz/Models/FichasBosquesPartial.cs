﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BosquesPaz.Services;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace BosquesPaz.Models
{
    [MetadataType(typeof(FichasBosques.Metadata))]

    public partial class FichasBosques
    {
        [DisplayName("FechaSolicitudStr")]
        public string FechaSolicitudStr { get{ return FechaSolicitud.ToShortDateString(); } }

        [DisplayName("FechaAutorizacionStr")]
        public string FechaAutorizacionStr { get{ return FechaAutorizacion.ToShortDateString(); } }

        sealed class Metadata
        {
            [Required(ErrorMessage = "El campo Area(hectáreas) es obligatorio.")]
            public int NumeroArea { get; set; }

            [Required(ErrorMessage = "El campo número de habitantes es obligatorio.")]
            public int NumeroHabitante { get; set; }

            [Required(ErrorMessage = "El campo fecha solicitud es obligatorio.")]
            public System.DateTime FechaSolicitud { get; set; }

            [Required(ErrorMessage = "El campo fecha autorización es obligatorio.")]
            public System.DateTime FechaAutorizacion { get; set; }

            [Required(ErrorMessage = "El campo caracteristicas del territorio es obligatorio.")]
            public string CaracteristicaTerritorio { get; set; }

            [Required(ErrorMessage = "El campo caracteristicas sociales es obligatorio.")]
            public string CaracteristicaSocial { get; set; }

            [Required(ErrorMessage = "El campo caracteristicas demográficas es obligatorio.")]
            public string CaracteristicaDemografica { get; set; }

            [Required(ErrorMessage = "El campo caracteristicas medioambientales es obligatorio.")]
            public string CaracteristicaMedioAmbiental { get; set; }

            [Required(ErrorMessage = "El campo descripción de como llegar es obligatorio.")]
            public string DescripcionUbicacion { get; set; }

            [Required(ErrorMessage = "El campo estado es obligatorio.")]
            public int Id_EstadoFicha { get; set; }
        }
    }
}