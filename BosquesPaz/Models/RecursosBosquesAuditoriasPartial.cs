﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BosquesPaz.Services;
using System.ComponentModel;

namespace BosquesPaz.Models
{
    public partial class AudiRecursosBosques
    {
        private TiposRecursosService ServicioTipoRecurso { get { return new TiposRecursosService(); } }
        private EstadosService ServicioEstado { get { return new EstadosService(); } }

        [DisplayName("TipoRecurso")]
        public string TipoRecurso { get { return ServicioTipoRecurso.ObtenerNombreTipoRecursoPorId(Id_TipoRecurso); } }

        [DisplayName("Estado")]
        public string Estado { get { return ServicioEstado.ObtenerNombreEstadoPorId(Id_Estado); } }

        
        public string FechaActualizacionStrAuditoria { get { return FechaActualizacionAuditoria.ToShortDateString(); } }

        public string FechaAuditoriaStr { get { return FechaAuditoria.ToShortDateString() + "-" + FechaAuditoria.ToLongTimeString(); } }
    }
}