﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BosquesPaz.Services;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace BosquesPaz.Models
{
    [MetadataType(typeof(Oportunidades.Metadata))]

    public partial class Oportunidades
    {
        private PatrocinadoresService servicioPatrocinador { get{ return new PatrocinadoresService(); } }
        private TiposPatrocinadoresService servicioTipoPatrocinador { get{ return new TiposPatrocinadoresService(); } }
        private EstadosPatrocinadoresService servicioEstadoPatrocinador { get{ return new EstadosPatrocinadoresService(); } }

        [DisplayName("Patrocinador")]
        private PatrocinadoresBosques Patrocinador { get { return servicioPatrocinador.ObtenerPatrocinadorBosque(Id_PatrocinadorBosque); } }

        [DisplayName("Tipo")]
        public string Tipo { get { return servicioTipoPatrocinador.ObtenerNombreTiposPatrocinadorPorId(Patrocinador.Id_TipoPatrocinador); } }

        [DisplayName("Estado")]
        public string Estado { get { return servicioEstadoPatrocinador.ObtenerNombreEstadosPatrocinadorPorId(Patrocinador.Id_EstadoPatrocinador); } }

        sealed class Metadata
        {
            [RegularExpression("^[A-Z a-zñÑáéíóúÁÉÍÓÚ]*$", ErrorMessage = "Por favor sólo ingrese letras.")]
            [Required(ErrorMessage = "El campo oportunidad es obligatorio.")]
            public string NombreOportunidad { get; set; }

            [Required(ErrorMessage = "El campo cantidad es obligatorio.")]
            public int Cantidad { get; set; }
        }
    }
}