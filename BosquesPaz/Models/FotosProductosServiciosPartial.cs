﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel;

namespace BosquesPaz.Models
{
    public partial class FotosProductosServicios
    {
        [DisplayName("Foto")]
        public IEnumerable<HttpPostedFileBase> Fotos { get; set; }
    }
}