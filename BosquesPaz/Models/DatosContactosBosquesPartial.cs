﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace BosquesPaz.Models
{
    [MetadataType(typeof(DatosContactosBosques.Metadata))]

    public partial class DatosContactosBosques
    {
        sealed class Metadata
        {
            [RegularExpression("^[A-Z a-zñÑáéíóúÁÉÍÓÚ]*$", ErrorMessage = "Por favor sólo ingrese letras.")]
            [Required(ErrorMessage = "El campo responsable es obligatorio.")]
            public string Responsable { get; set; }

            [Required(ErrorMessage = "El campo teléfono es obligatorio.")]
            public string Telefono { get; set; }

            [RegularExpression("^[a-zA-Z0-9_\\.-]+@([a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6}$", ErrorMessage = "El correo no es valido.")]
            [Required(ErrorMessage = "El campo correo electronico es obligatorio.")]
            public string CorreoElectronico { get; set; }

            [Required(ErrorMessage = "El campo dirección es obligatorio.")]
            public string Direccion { get; set; }

            [Required(ErrorMessage = "El campo indicaciones de como llegar es obligatorio.")]
            public string Indicaciones { get; set; }

            [Required(ErrorMessage = "El campo jurisdicción a cargo es obligatorio.")]
            public string JurisdiccionCargo { get; set; }
        }
    }
}