﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;


namespace BosquesPaz.Models
{
    [MetadataType(typeof(DatosUsuarios.Metadata))]
    public partial class DatosUsuarios
    {
        sealed class Metadata
        {
            [RegularExpression("^[A-Z a-zñÑáéíóúÁÉÍÓÚ]*$", ErrorMessage = "Por favor sólo ingrese letras.")]
            [Required(ErrorMessage = "El campo nombre es obligatorio.")]
            public string Nombre { get; set; }

            [RegularExpression("^[a-zA-Z0-9_\\.-]+@([a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6}$", ErrorMessage = "El correo no es valido.")]
            [Required(ErrorMessage = "El campo correo es obligatorio.")]
            public string Correo { get; set; }

            [RegularExpression("^[A-Za-z]*$", ErrorMessage = "Por favor sólo ingrese letras.")]
            [Required(ErrorMessage = "El campo nickname es obligatorio.")]
            public string NickName { get; set; }

            [Required(ErrorMessage = "El campo departamento es obligatorio.")]
            public int Id_Departamento { get; set; }

            [Required(ErrorMessage = "El campo municipio es obligatorio.")]
            public int Id_Municipio { get; set; }

            [Required(ErrorMessage = "El campo imagen es obligatorio.")]
            public int Id_FotoUsuario { get; set; }
        }
    }
}