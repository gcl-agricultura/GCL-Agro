﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BosquesPaz.Services;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace BosquesPaz.Models
{
    [MetadataType(typeof(PatrocinadoresBosques.Metadata))]

    public partial class PatrocinadoresBosques
    {
        private DepartamentosService ServicioDepartamento { get { return new DepartamentosService(); } }
        private MunicipiosService ServicioMunicipio { get { return new MunicipiosService(); } }
        private TiposPatrocinadoresService ServicioTipoPatrocinador { get { return new TiposPatrocinadoresService(); } }
        private EstadosPatrocinadoresService ServicioEstadoPatrocinador { get { return new EstadosPatrocinadoresService(); } }

        [DisplayName("Departamento")]
        public string Departamento { get { return ServicioDepartamento.ObtenerNombreDepartamentoPorId(Id_Departamento); } }

        private DepartamentosService service { get { return new DepartamentosService(); } }
        [DisplayName("Municipio")]
        public string Municipio { get { return ServicioMunicipio.ObtenerNombreMunicipioPorId(Id_Municipio); } }

        [DisplayName("TipoPatrocinador")]
        public string TipoPatrocinador { get { return ServicioTipoPatrocinador.ObtenerNombreTiposPatrocinadorPorId(Id_TipoPatrocinador); } }

        [DisplayName("EstadoPatrocinador")]
        public string EstadoPatrocinador { get { return ServicioEstadoPatrocinador.ObtenerNombreEstadosPatrocinadorPorId(Id_EstadoPatrocinador); } }

        [DisplayName("FechaDeCreacion")]
        public string FechaDeCreacion { get { return FechaCreacion.ToShortDateString(); } }

        sealed class Metadata
        {
            [RegularExpression("^[A-Z a-zñÑáéíóúÁÉÍÓÚ]*$", ErrorMessage = "Por favor sólo ingrese letras.")]
            [Required(ErrorMessage = "El campo nombre es obligatorio.")]
            public string NombreRazon { get; set; }

            [RegularExpression("^[A-Z a-zñÑáéíóúÁÉÍÓÚ]*$", ErrorMessage = "Por favor sólo ingrese letras.")]
            [Required(ErrorMessage = "El campo responsable es obligatorio.")]
            public string Responsable { get; set; }

            [Required(ErrorMessage = "El campo teléfono es obligatorio.")]
            public string Telefono { get; set; }

            [RegularExpression("^[a-zA-Z0-9_\\.-]+@([a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6}$", ErrorMessage = "El correo no es valido.")]
            [Required(ErrorMessage = "El campo correo es obligatorio.")]
            public string Correo { get; set; }

            [Required(ErrorMessage = "El campo sitio web es obligatorio.")]
            public string SitioWeb { get; set; }

            [Required(ErrorMessage = "El campo departamento es obligatorio.")]
            public int Id_Departamento { get; set; }

            [Required(ErrorMessage = "El campo municipio es obligatorio.")]
            public int Id_Municipio { get; set; }

            [Required(ErrorMessage = "El campo tipo es obligatorio.")]
            public int Id_TipoPatrocinador { get; set; }
        }
    }
}