﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BosquesPaz.Services;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace BosquesPaz.Models
{
    [MetadataType(typeof(ActividadesBosques.Metadata))]

    public partial class ActividadesBosques
    {
        private TiposActividadesService ServicioTipoActividad { get { return new TiposActividadesService(); } }
        private EstadosActividadesService ServicioEstadoActividad { get { return new EstadosActividadesService(); } }
        [DisplayName("TipoActividad")]
        public string TipoActividad { get { return ServicioTipoActividad.ObtenerNombreTipoActividadPorId(Id_TipoActividad); } }

        [DisplayName("EstadoActividad")]
        public string EstadoActividad { get { return ServicioEstadoActividad.ObtenerNombreEstadoActividadPorId(Id_EstadosActividades); } }


        public string FechaInicioStr { get { return FechaInicio.ToShortDateString(); } }
        public string FechaFinStr { get { return FechaInicio.ToShortDateString(); } }

        sealed class Metadata
        {
            [RegularExpression("^[A-Z a-zñÑáéíóúÁÉÍÓÚ]*$", ErrorMessage = "Por favor sólo ingrese letras.")]
            [Required(ErrorMessage = "El campo nombre es obligatorio.")]
            public string NombreActividad { get; set; }

            [Required(ErrorMessage = "El campo fecha inicio es obligatorio.")]
            public System.DateTime FechaInicio { get; set; }

            [Required(ErrorMessage = "El campo fecha fin es obligatorio.")]
            public System.DateTime FechaFin { get; set; }

            [Required(ErrorMessage = "El campo porcentaje de avance es obligatorio.")]
            public int PorcentajeAvance { get; set; }
            
            [Required(ErrorMessage = "El campo avance es obligatorio.")]
            public string Avance { get; set; }

            [Required(ErrorMessage = "El campo estado es obligatorio.")]
            public int Id_EstadosActividades { get; set; }

            [Required(ErrorMessage = "El campo tipo es obligatorio.")]
            public int Id_TipoActividad { get; set; }
        }
    }
}