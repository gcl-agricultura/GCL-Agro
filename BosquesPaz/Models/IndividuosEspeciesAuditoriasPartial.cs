﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BosquesPaz.Services;
using System.ComponentModel;

namespace BosquesPaz.Models
{
    public partial class AudiIndividuosEspecies
    {
        private TiposIndicadoresService ServicioTipoIndividuo { get { return new TiposIndicadoresService(); } }
        private UnidadesIndividuosService ServicioUnidades { get { return new UnidadesIndividuosService(); } }

        [DisplayName("TipoIndividuo")]
        public string TipoIndividuo { get { return ServicioTipoIndividuo.ObtenerNombreTipoIndicadorPorId(Id_TipoIndividuoEspecie); } }

        [DisplayName("UnidadIndividuo")]
        public string UnidadIndividuo { get { return ServicioUnidades.ObtenerNombreIndividuoEspeciePorId(Id_UnidadIndividuoEspecie); } }

        [DisplayName("FechaAuditoriaStr")]
        public string FechaAuditoriaStr { get { return FechaAuditoria.ToShortDateString() + "-" + FechaAuditoria.ToLongTimeString(); } }
    }
}