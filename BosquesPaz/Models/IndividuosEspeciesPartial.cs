﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BosquesPaz.Services;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace BosquesPaz.Models
{
    [MetadataType(typeof(IndividuosEspecies.Metadata))]

    public partial class IndividuosEspecies
    {
        private TiposIndividuosService ServiciosTipoIndividuo { get{ return new TiposIndividuosService(); } }
        private UnidadesIndividuosService ServicioUnidadEspecie { get{ return new UnidadesIndividuosService(); } }

        [DisplayName("TipoInidividuoEspecie")]
        public string TipoInidividuoEspecie { get{ return ServiciosTipoIndividuo.ObtenerNombreTipoIndividuoPorId(Id_TipoIndividuoEspecie); } }

        [DisplayName("UnidadIndividuoEspecie")]
        public string UnidadIndividuoEspecie { get { return ServicioUnidadEspecie.ObtenerNombreIndividuoEspeciePorId(Id_UnidadIndividuoEspecie); } }

        sealed class Metadata
        {
            [RegularExpression("^[A-Z a-zñÑáéíóúÁÉÍÓÚ]*$", ErrorMessage = "Por favor sólo ingrese letras.")]
            [Required(ErrorMessage = "El campo nombre es obligatorio.")]
            public string Nombre { get; set; }
            
            [Required(ErrorMessage = "El campo descripción es obligatorio.")]
            public string Descripcion { get; set; }

            [Required(ErrorMessage = "El campo cantidad es obligatorio.")]
            public long Cantidad { get; set; }

            [Required(ErrorMessage = "El campo tipo es obligatorio.")]
            public int Id_TipoIndividuoEspecie { get; set; }

            [Required(ErrorMessage = "El campo unidad es obligatorio.")]
            public int Id_UnidadIndividuoEspecie { get; set; }
        }
    }
}