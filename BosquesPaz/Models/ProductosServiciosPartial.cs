﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BosquesPaz.Services;
using System.ComponentModel;
using BosquesPaz.Models;
using System.ComponentModel.DataAnnotations;

namespace BosquesPaz.Models
{
    [MetadataType(typeof(ProductosServicios.Metadata))]

    public partial class ProductosServicios
    {
        private TiposProductosServiciosService ServicioTipoProductoServicio { get { return new TiposProductosServiciosService(); } }
        private EstadosProductosServiciosService ServicioEstadoProductoServicio { get { return new EstadosProductosServiciosService(); } }
        private ProductosServiciosService ServicioProducto { get{ return new ProductosServiciosService(); } }
        private BosquesService ServicioBosque { get{ return new BosquesService(); } }
        private DepartamentosService ServicioDepartamento { get{ return new DepartamentosService(); } }
        private MunicipiosService ServicioMunicipio { get { return new MunicipiosService(); } }

        [DisplayName("TipoProductoServicio")]
        public string TipoProductoServicio { get { return ServicioTipoProductoServicio.ObtenerNombreTipoProductoServicioPorId(Id_TipoProductoServicio); } }

        [DisplayName("EstadoProductoServicio")]
        public string EstadoProductoServicio { get { return ServicioEstadoProductoServicio.ObtenerNombreProductoServicioPorId(Id_EstadoProductoServicio); } }

        [DisplayName("FechaActualizacion")]
        public string FechaActualizacion { get { return ServicioProducto.ObtenerFechaAuditoriaPorId(Id_ProductoServicio); } }

        [DisplayName("NombreBosque")]
        public string NombreBosque { get{ return ServicioBosque.ObtenerNombreBosqueActividadPorId(Id_Bosque); } }

        private Bosques bosque { get{ return ServicioBosque.ObtenerBosque(Id_Bosque); } }

        [DisplayName("Departamento")]
        public string Departamento { get{ return ServicioDepartamento.ObtenerNombreDepartamentoPorId(bosque.Id_Departamento); } }

        [DisplayName("Municipio")]
        public string Municipio { get{ return ServicioMunicipio.ObtenerNombreMunicipioPorId(bosque.Id_Municipio); } }

        sealed class Metadata
        {
            [RegularExpression("^[A-Z a-zñÑáéíóúÁÉÍÓÚ]*$", ErrorMessage = "Por favor sólo ingrese letras.")]
            [Required(ErrorMessage = "El campo nombre es obligatorio.")]
            public string Nombre { get; set; }

            [RegularExpression("^[A-Z a-zñÑáéíóúÁÉÍÓÚ]*$", ErrorMessage = "Por favor sólo ingrese letras.")]
            [Required(ErrorMessage = "El campo descripción es obligatorio.")]
            public string Descripcion { get; set; }
            
            [Required(ErrorMessage = "El campo precio es obligatorio.")]
            public int Precio { get; set; }

            [Required(ErrorMessage = "El campo tipo es obligatorio.")]
            public int Id_TipoProductoServicio { get; set; }

            [Required(ErrorMessage = "El campo estado es obligatorio.")]
            public int Id_EstadoProductoServicio { get; set; }
        }
    }
}