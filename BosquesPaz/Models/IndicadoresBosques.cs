//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace BosquesPaz.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class IndicadoresBosques
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public IndicadoresBosques()
        {
            this.AudiIndicadoresBosques = new HashSet<AudiIndicadoresBosques>();
        }
    
        public int Id_IndicadorBosque { get; set; }
        public string NombreIndicador { get; set; }
        public int ValorMeta { get; set; }
        public string Unidad { get; set; }
        public int ValorSeguimiento { get; set; }
        public string Observacion { get; set; }
        public System.DateTime FechaSeguimiento { get; set; }
        public int Id_Bosque { get; set; }
        public int Id_TipoIndicador { get; set; }
        public int Id_EstadoIndicador { get; set; }
        public string Aud_User { get; set; }
        public string Aud_Origen { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AudiIndicadoresBosques> AudiIndicadoresBosques { get; set; }
        public virtual Bosques Bosques { get; set; }
        public virtual EstadosIndicadores EstadosIndicadores { get; set; }
        public virtual TiposIndicadores TiposIndicadores { get; set; }
    }
}
