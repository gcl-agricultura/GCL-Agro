﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace BosquesPaz.Models
{
    public partial class FotosBosques
    {
        [DisplayName("Foto")]
        public IEnumerable<HttpPostedFileBase> Fotos { get; set; }
    }
}