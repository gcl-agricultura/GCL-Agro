﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BosquesPaz.Services;
using System.ComponentModel;

namespace BosquesPaz.Models
{
    public partial class AudiActividadesBosques
    {
        private TiposActividadesService ServicioTipoActividad { get { return new TiposActividadesService(); } }
        private EstadosActividadesService ServicioEstadoActividad { get { return new EstadosActividadesService(); } }

        [DisplayName("TipoActividad")]
        public string TipoActividad { get { return ServicioTipoActividad.ObtenerNombreTipoActividadPorId(Id_TipoActividad); } }

        [DisplayName("EstadoActividad")]
        public string EstadoActividad { get { return ServicioEstadoActividad.ObtenerNombreEstadoActividadPorId(Id_EstadosActividades); } }


        public string FechaInicioStr { get { return FechaInicioAuditoria.ToShortDateString(); } }
        public string FechaFinStr { get { return FechaFinAuditoria.ToShortDateString(); } }
        public string FechaAuditoriaStr { get { return FechaAuditoria.ToShortDateString() + "-" + FechaAuditoria.ToLongTimeString(); } }
    }
}