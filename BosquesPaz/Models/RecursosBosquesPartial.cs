﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BosquesPaz.Services;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace BosquesPaz.Models
{
    [MetadataType(typeof(RecursosBosques.Metadata))]

    public partial class RecursosBosques
    {
        private TiposRecursosService ServicioTipoRecurso { get { return new TiposRecursosService(); } }
        private EstadosService ServicioEstado { get { return new EstadosService(); } }
        [DisplayName("TipoRecurso")]
        public string TipoRecurso { get { return ServicioTipoRecurso.ObtenerNombreTipoRecursoPorId(Id_TipoRecurso); } }

        [DisplayName("Estado")]
        public string Estado { get { return ServicioEstado.ObtenerNombreEstadoPorId(Id_Estado); } }
        
        [DisplayName("Estado")]
        public string FechaActualizacionStr { get { return FechaActualizacion.ToShortDateString(); } }

        sealed class Metadata
        {

            public int Id_RecursoBosque { get; set; }

            [RegularExpression("^[A-Z a-zñÑáéíóúÁÉÍÓÚ]*$", ErrorMessage = "Por favor sólo ingrese letras.")]
            [Required(ErrorMessage = "El campo nombre es obligatorio.")]
            public string NombreRecurso { get; set; }

            [Required(ErrorMessage = "El campo presupuesto asignado es obligatorio.")]
            public int PresupuestoAsignado { get; set; }

            [Required(ErrorMessage = "El campo presupuesto ejecutado es obligatorio.")]
            public int PresupuestoEjecutado { get; set; }

            [Required(ErrorMessage = "El campo fecha actualización es obligatorio.")]
            public System.DateTime FechaActualizacion { get; set; }

            [Required(ErrorMessage = "El campo tipo es obligatorio.")]
            public int Id_TipoRecurso { get; set; }

            [Required(ErrorMessage = "El campo estado es obligatorio.")]
            public int Id_Estado { get; set; }
        }
    }
}