﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace BosquesPaz.Models
{
    public partial class AudiosHistorias
    {
        [DisplayName("Audio")]
        public IEnumerable<HttpPostedFileBase> Audios { get; set; }
    }
}