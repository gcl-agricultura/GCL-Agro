﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BosquesPaz.Services;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace BosquesPaz.Models
{
    [MetadataType(typeof(HistoriasBosques.Metadata))]

    public partial class HistoriasBosques
    {
        private TematicasService ServicioTematica { get { return new TematicasService(); } }
        private EstadosHistoriasService ServicioEstado { get { return new EstadosHistoriasService(); } }
        private HistoriasService ServicioHistoria { get { return new HistoriasService(); } }
        

        [DisplayName("Tematica")]
        public string Tematica { get { return ServicioTematica.ObtenerNombreTematicasPorId(Id_Tematica); } }

        [DisplayName("Estado")]
        public string Estado { get { return ServicioEstado.ObtenerNombreEstadosHistoriasPorId(Id_EstadoHistoria); } }

        [DisplayName("FechaActualizacion")]
        public string FechaActualizacion { get { return ServicioHistoria.ObtenerFechaAuditoriaPorId(Id_HistoriaBosque); } }

        sealed class Metadata
        {
            [RegularExpression("^[A-Z a-zñÑáéíóúÁÉÍÓÚ]*$", ErrorMessage = "Por favor sólo ingrese letras.")]
            [Required(ErrorMessage = "El campo nombre es obligatorio.")]
            public string Nombre { get; set; }

            [RegularExpression("^[A-Z a-zñÑáéíóúÁÉÍÓÚ]*$", ErrorMessage = "Por favor sólo ingrese letras.")]
            [Required(ErrorMessage = "El campo descripción es obligatorio.")]
            public string Descripcion { get; set; }

            [Required(ErrorMessage = "El campo enlace de vídeo es obligatorio.")]
            public string EnlaceVideo { get; set; }

            [RegularExpression("^[A-Z a-zñÑáéíóúÁÉÍÓÚ.,;¿?!¡]*$", ErrorMessage = "Por favor sólo ingrese letras.")]
            [Required(ErrorMessage = "El campo historia es obligatorio.")]
            public string Historia { get; set; }

            [Required(ErrorMessage = "El campo tematica es obligatorio.")]
            public int Id_Tematica { get; set; }

            [Required(ErrorMessage = "El campo estado es obligatorio.")]
            public int Id_EstadoHistoria { get; set; }
        }
    }
}